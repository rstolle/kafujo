package net.kafujo.wicket.panel;

import net.kafujo.container.TripleString;
import net.kafujo.reflect.KafuReflect;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

import java.util.List;

/**
 * Created by stolle on 2017-07-01-Saturday.
 */
public class ObjectInfo extends Panel {

    /**
     * Shows a table with the values bla ...
     *
     * @param id
     * @param obj
     * @param onlyGetters
     * @param maxCharacters
     */
    public ObjectInfo(final String id, final Object obj, final boolean onlyGetters, final int maxCharacters) {
        super(id);

        final List<TripleString> tmp = KafuReflect.objectStateList(obj, onlyGetters, maxCharacters);
        // Sto.lgr.trace(ToolsReflection.objectStateString(obj));

        add(new ListView<TripleString>("methods", tmp) {
            int count = 1;

            @Override
            protected void populateItem(final ListItem<TripleString> item) {
                item.add(new Label("count", count++));
                item.add(new Label("type", item.getModelObject().getLeft()));
                item.add(new Label("method", item.getModelObject().getMiddle()));
                item.add(new Label("result", item.getModelObject().getRight()));
            }
        });

    }

}
