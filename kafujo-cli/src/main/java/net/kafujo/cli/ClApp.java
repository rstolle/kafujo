package net.kafujo.cli;

import net.kafujo.base.RequirementException;
import net.kafujo.config.SystemProperty;
import net.kafujo.io.KafuFile;
import net.kafujo.io.KafuInput;
import net.kafujo.units.KafuDateTime;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Adds some convenient methods to {@link ClAppCore}
 */
public abstract class ClApp extends ClAppCore {

    protected void overline(String text) {
        if (SystemProperty.KAFUJO_CL_HEADER.isAvailable()) {
            header();
        }
        out.println(" ----------------------------------------------------------------------------");
        out.println(" |  " + text);
        out.println(" ----------------------------------------------------------------------------");
        out.println();
    }

    public void header() {
        String version = getClass().getPackage().getImplementationVersion();
        if (version == null) {
            version = "SNAPSHOT";
        }

        version = " |  kafujo " + version;

        out.println();
        out.println(" ----------------------------------------------------------------------------");
        out.println(version + StringUtils.leftPad(KafuDateTime.full(), 77 - version.length()));
        out.println(" |  " + SystemProperty.combineVmInfo());
        out.println(" |  Working directory: " + KafuFile.getWorkingDir());

    }

    protected void success(String text) {
        out.println();
        out.println(" " + text);
        out.println();
    }

    protected void fail(String text) {
        out.println();
        out.println(" FAILED: " + text);
        out.println();
    }

    /**
     * Prints message and ask for confirmation. User has to enter y or n.
     *
     * @param message question to be confirmed or not.
     * @return true, if user confirms
     */
    protected boolean confirmNextStep(final String message) {
        out.print("\n    " + message + "       [Y]/[n]: ");
        final String line = KafuInput.nextLine();

        if (line.isBlank() || line.equals("y") || line.equals("Y")) {
            out.println("\n       (: .... PROCESSING .... :)\n");
            return true;
        } else {
            out.println("\n       !!!!  ABORTED  !!!!\n");
            return false;
        }
    }

    /**
     * Checks if file exists and asks, if it can be overwritten.
     *
     * @param file file to be checked
     * @return false if file exists and user doen't confirm overwrite. Else true.
     * @throws RequirementException if file exists but is directory
     */
    protected boolean confirmOverwrite(Path file) {
        if (Files.exists(file)) {
            return confirmNextStep(file + " (" + KafuFile.sizeOfFile(file) + ") will be overwitten");
        }
        return true;
    }

}
