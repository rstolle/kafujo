package net.kafujo.cli;

import net.kafujo.config.SystemProperty;
import net.kafujo.io.KafuFile;
import net.kafujo.units.KafuDateTime;
import org.apache.commons.lang3.StringUtils;

@Deprecated // use ClApp instead
public abstract class CliAppKafujo extends CliApp {

    protected void overline(String text) {

        String version = getClass().getPackage().getImplementationVersion();

        if (version == null) {
            version = "SNAPSHOT";
        }

        version = " |  kafujo " + version;

        out.println();
        out.println(" ----------------------------------------------------------------------------");
        out.println(version + StringUtils.leftPad(KafuDateTime.full(), 77 - version.length()));
        out.println(" |  " + SystemProperty.combineVmInfo());
        out.println(" |  Working directory: " + KafuFile.getWorkingDir());
        out.println(" ----------------------------------------------------------------------------");
        out.println(" |  " + text);
        out.println(" ----------------------------------------------------------------------------");
        out.println();
    }

    protected void success(String text) {
        out.println();
        out.println(" " + text);
        out.println();
    }

    protected void fail(String text) {
        out.println();
        out.println(" FAILED: " + text);
        out.println();
    }
}
