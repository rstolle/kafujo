package net.kafujo.cli;

import org.apache.commons.cli.Option;

import java.lang.annotation.*;

@Repeatable(ClOptions.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ClOption {
    /**
     * Maps to {@link Option#getOpt()}}
     *
     * @return the short name of this option, eg 'i' for -i
     */
    char opt();

    /**
     * Maps to {@link Option#setDescription(String)}
     *
     * @return the description of this option.
     */
    String description();

    /**
     * Maps to {@link Option#setLongOpt(String)}
     *
     * @return the long option, eg. "deep-dive" for  --deep-dive
     */
    String longOpt() default "";

    /**
     * Combines {@link Option#setArgName(String)} and {@link Option.Builder#hasArg(boolean)}.
     * {@link Option#setArgs(int)} will be called automatically with 1, if {@link #argCount()}
     * is not specified. This option enables argument(s) to be read with
     * {@link org.apache.commons.cli.CommandLine#getOptionValue(char)}.
     *
     * @return the name of the argument(s) for this option
     */
    String arg() default "";

    /**
     * Maps to {@link Option#setArgs(int)}. To be used only if {@link #arg()} is specified and there
     * are more than one arguments.
     *
     * @return the number of arguments allowed for this option.
     */
    int argCount() default -1;     // same default value as in commons cli

    /**
     * Maps to {@link Option#setOptionalArg(boolean)}.
     *
     * @return true, if the arguments for this option are optional. Default is false.
     */
    boolean argOptional() default false;

    /**
     * Maps to {@link Option#setRequired(boolean)}.
     *
     * @return true, if this option is mandatory.
     */
    boolean required() default false;
}
