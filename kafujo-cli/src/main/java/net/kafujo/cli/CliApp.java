package net.kafujo.cli;

import net.kafujo.base.KafuException;
import net.kafujo.base.RequirementException;
import net.kafujo.config.SystemProperty;
import net.kafujo.config.TypedReader;
import net.kafujo.container.RunnableResult;
import net.kafujo.io.KafuFile;
import net.kafujo.io.KafuInput;
import net.kafujo.units.KafuDuration;
import org.apache.commons.cli.*;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Makes it easier to create Apache Commons CLI apps. Offers three useful standard options.
 */
@Deprecated // use ClAppCore instead
public class CliApp implements TypedReader {

    private Duration standardWaitDuration = Duration.ofMillis(Long.MAX_VALUE);
    private String appName = getClass().getSimpleName();

    protected final PrintStream out = System.out;
    protected final PrintStream err = System.err;

    private final String standardHelpOpt;
    private final String standardStatsOpt;
    private final String standardWaitOpt;
    private final boolean showHelpOnNoArgs;

    // the code for different options. return true, to continue after executions, false to stop
    private final Map<String, RunnableResult<Boolean>> runnables = new HashMap<>();
    private final Options options = new Options();
    protected CommandLine commandLine = null; // will be set on run. After this, no more adds ensured by addRunnable()

    /**
     * Default ctor with default values:
     * <ul>
     * <li> '?' help (showing help when there are no args)</li>
     * <li> 'S' stats </li>
     * <li> 'W' wait  (waiting forever)</li>
     * </ul>
     */
    public CliApp() {
        standardHelpOpt = "?";
        standardStatsOpt = "S";
        standardWaitOpt = "W";
        showHelpOnNoArgs = true;
    }

    public CliApp(final String standardHelpOpt, final boolean showHelpOnNoArgs, final String standardStatsOpt,
                  final String standardWaitOpt, final Duration standardWaitDuration) {
        this.standardHelpOpt = standardHelpOpt;
        this.showHelpOnNoArgs = showHelpOnNoArgs;
        this.standardStatsOpt = standardStatsOpt;
        this.standardWaitOpt = standardWaitOpt;
        this.standardWaitDuration = standardWaitDuration;
    }

    /**
     * Adds all default options.
     */
    protected final void addStandardOptions() {
        addStandardHelpOption();
        addStandardStatsOption();
        addStandardWaitOption();
    }

    private RunnableResult<Boolean> throwOnExecute = () -> {
        throw new IllegalStateException("This is not supposed to be executed");
    };

    /**
     * Adds an option wi
     */
    protected void addStandardHelpOption() {
        addNoArgOption(throwOnExecute, standardHelpOpt, "*standard* help");
    }

    protected void addStandardStatsOption() {
        addNoArgOption(throwOnExecute, standardStatsOpt, "*standard* running statistics when done");
    }

    protected void addStandardWaitOption() {
        addOptionalArgOption(throwOnExecute, standardWaitOpt, "*standard* wait X millis to finish; default: " + standardWaitDuration);
    }

    void doHeader() {

    }

    /**
     * Will be executed on the standard help option. There will be no {@link #doHeader() header} and execution will finished
     * right after this method. All other options will be ignored.
     * <p>
     * This implementation shows options and their description, but can be overwritten.
     */
    void doStandardHelp() {
        var formatter = new HelpFormatter();
        formatter.printUsage(new PrintWriter(out, true), 100, getClass().getName(), options);
        formatter.printHelp("options:", options);
    }

    /**
     * The default help. You can overwrite this in the actual class to show a specialized help screen.
     *
     * @return always false to signal the end of the program
     */
    protected boolean help() {
        // todo: clean up, add help string
        //    try (var stdout = new PrintWriter(out, true)) {
        //  }
        return false; // finish program
    }

    /**
     * Makes sure, the option is not used yet and add stores the runnable for it
     *
     * @param opt  (short) option string
     * @param code runnable to be executed on this option
     */
    private void addRunnable(final String opt, final RunnableResult<Boolean> code) {
        if (commandLine != null) {
            doStandardHelp();
            throw new IllegalStateException("Commandline already parsed, cannot add more options. ");
        }
        if (runnables.containsKey(opt)) {
            throw new IllegalStateException("option '" + opt + "' is already in use!");
        }
        runnables.put(opt, code);
    }

    protected void add(final Option option) {
        options.addOption(option);
    }

    protected void add(final RunnableResult<Boolean> code, final Option option) {
        options.addOption(option);
        addRunnable(option.getOpt(), code);
    }

    protected void addNoArgOption(final RunnableResult<Boolean> code, final String opt, final String description) {
        options.addOption(new Option(opt, false, description));
        addRunnable(opt, code);
    }


    protected void addArgOption(final RunnableResult<Boolean> code, String opt, final String description) {
        options.addOption(new Option(opt, true, description));
        addRunnable(opt, code);
    }

    /**
     * Adds an {@link Option } with ONE optional argument.
     *
     * @param code        {@link RunnableResult runnable} for this option.
     * @param opt         short name
     * @param description describes what it does
     */
    protected void addOptionalArgOption(final RunnableResult<Boolean> code, final String opt, final String description) {
        final Option option = new Option(opt, description);
        option.setOptionalArg(true);
        option.setArgs(1); // did not work without and the docu is crap.
        // https://stackoverflow.com/questions/33338787/java-apache-cli-optional-command-line-arguments-not-working
        add(code, option);
    }


    @Override
    public boolean isAvailable(CharSequence key) {
        if (!commandLine.hasOption(key.toString())) {
            return false;
        }
        String val = commandLine.getOptionValue(key.toString());
        return val != null && !val.isBlank();
    }

    @Override
    public String readString(final CharSequence key) {
        if (!isAvailable(key)) {
            throw new IllegalStateException("No option '" + key + "'");
        }
        return commandLine.getOptionValue(key.toString());
    }


    protected void run(String[] args) {

        if ((args == null || args.length == 0) && showHelpOnNoArgs) {
            doStandardHelp();
            return;
        }

        try {
            commandLine = new DefaultParser().parse(options, args);
        } catch (ParseException e) {
            out.println();
            out.println(KafuException.extractMessage(e));
            out.println();
            doStandardHelp();
            return;
        }


        if (commandLine.hasOption(standardHelpOpt)) {
            doStandardHelp();
            return;
        }

        // make sure the duration can be parsed before going on
        if (commandLine.hasOption(standardWaitOpt)) {
            standardWaitDuration = readDurationOfMillisFbk(standardWaitOpt, standardWaitDuration);
        }

        Instant start = Instant.now();
        for (final String opt : runnables.keySet()) {
            if (opt.equals(standardStatsOpt) || opt.equals(standardWaitOpt)) {
                continue; // help is covered above already todo: this way, S and W will never work
            }

            if (commandLine.hasOption(opt)) {
                if (!runnables.get(opt).execute()) {
                    break;
                }
            }
        }

        if (commandLine.hasOption(standardStatsOpt)) {
            out.println("\n *** Running Time: " + KafuDuration.adaptUnits(Duration.between(start, Instant.now())));
        }

        if (commandLine.hasOption(standardWaitOpt)) {
            out.print("\n\n *** Wait " + KafuDuration.adaptUnits(standardWaitDuration) + " to finish; "); // todo: until!
            out.print("\n\n *** Wait till " + LocalDateTime.now().plus(standardWaitDuration) + " to finish; "); // todo: until!
            out.println(SystemProperty.JMXREMOTE_PORT);
            KafuDuration.sleep(standardWaitDuration);
            out.println(" *** TOTAL Running Time: " + KafuDuration.adaptUnits(Duration.between(start, Instant.now())));
        }
    }

    /**
     * Prints message and ask for confirmation. User has to enter y or n.
     *
     * @param message question to be confirmed or not.
     * @return true, if user confirms
     */
    protected boolean confirmNextStep(final String message) {
        out.print("\n    " + message + "       [Y]/[n]: ");
        final String line = KafuInput.nextLine();

        if (line.isBlank() || line.equals("y") || line.equals("Y")) {
            out.println("\n       (: .... PROCESSING .... :)\n");
            return true;
        } else {
            out.println("\n       !!!!  ABORTED  !!!!\n");
            return false;
        }
    }

    /**
     * Checks if file exists and asks, if it can be overwritten.
     *
     * @param file file to be checked
     * @return false if file exists and user doen't confirm overwrite. Else true.
     * @throws RequirementException if file exists but is directory
     */
    protected boolean confirmOverwrite(Path file) {
        if (Files.exists(file)) {
            return confirmNextStep(file + " (" + KafuFile.sizeOfFile(file) + ") will be overwitten");
        }
        return true;
    }
}
