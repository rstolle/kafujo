package net.kafujo.cli;

import net.kafujo.base.UncheckedException;
import net.kafujo.config.SystemProperty;
import net.kafujo.config.TypedReader;
import net.kafujo.units.KafuNumber;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * CommandlineApp
 */
public class ClAppCore implements TypedReader {

    /**
     * To be used as an example for individual apps. Help footer as suggested in
     * https://commons.apache.org/proper/commons-cli/apidocs/org/apache/commons/cli/HelpFormatter.html
     */
    static String DEFAULT_HELP_FOOTER = "Please report issues to rstolle@posteo.de";

    Integer exitCode = null;

    @Override
    public boolean isAvailable(CharSequence opt) {
        return commandLine.hasOption(opt.toString());
    }

    @Override
    public String readString(CharSequence opt) {
        Objects.requireNonNull(opt, "REQUIRE option read read value for");
        if (!isAvailable(opt)) {
            throw new IllegalStateException("Missing required value for option '" + opt + "'");
        }
        return commandLine.getOptionValue(opt.toString());
    }

    protected static Logger LGR = LoggerFactory.getLogger(ClAppCore.class);

    private final Map<Character, Method> optionsAnnotated;
    private final Options options = new Options();

    protected PrintStream out = System.out;
    protected PrintStream err = System.err;
    protected CommandLine commandLine;

    public ClAppCore() {
        optionsAnnotated = Collections.unmodifiableMap(collectOptions());
        if (optionsAnnotated.isEmpty()) {
            LGR.info("No Cli annotations in " + this.getClass());
        } else {
            LGR.debug(optionsAnnotated.size() + " cli annotations in  " + this.getClass()
                    + ": " + optionsAnnotated.keySet());
        }
    }

    Method fetchActionMethod() throws ParseException {
        Method action = null;
        for (Character opt : optionsAnnotated.keySet()) {

            if (!commandLine.hasOption(opt)) {
                continue;
            }

            if (optionsAnnotated.get(opt) != null) {
                if (action != null) {
                    throw new ParseException("More than one action option in cmdline");
                }
                action = optionsAnnotated.get(opt);
            }
        }
        return action;
    }

    /**
     * Parses the cmd line and executes the action. If no action was found, {@link #defaultAction()} will be executed.
     * todo: consider: returning int as the return value for the app?
     *
     * @param args cmd line arguments to be parsed against the current options
     * @throws UncheckedException carrying InvocationTargetException, IllegalAccessException
     *                            or Exceptions caused by execution
     */
    public void run(String[] args) throws Throwable {
        try {
            // set command line and action anew (can be done more than once)
            commandLine = new DefaultParser().parse(options, args);
            final Method action = fetchActionMethod();

            if (action == null) {
                defaultAction();
            } else {
                action.invoke(this);
            }
        } catch (ParseException parseProblem) {
            if (SystemProperty.KAFUJO_CL_EXCEPTION.isAvailable()) {
                throw parseProblem;
            } else {
                err.println("\n  Wrong cmd line: " + parseProblem.getMessage());
                err.println();
                printHelp();
            }

        } catch (InvocationTargetException | IllegalAccessException e) {
            if (SystemProperty.KAFUJO_CL_EXCEPTION.isAvailable()) {
                throw e;
            } else {
                err.println(e.getCause());
            }
        } catch (Exception e) {
            if (SystemProperty.KAFUJO_CL_EXCEPTION.isAvailable()) {
                throw e;
            } else {
                err.println("defaultAction() threw " + e);
            }
        }
    }

    private Map<Character, Method> collectOptions() {
        final var options = new HashMap<Character, Method>();
        collectMethodOptions(options);
        collectClassOptions(options);
        return options;
    }

    private void ensureUnused(ClOption anno) {

        Option alreadyInUse = options.getOption(Character.toString(anno.opt()));

        if (alreadyInUse != null) {
            throw new IllegalArgumentException("Already used: " + alreadyInUse);
        }
    }

    /**
     * Creates an Option out of the annotated data. Its public static so it could be used anywhere.
     *
     * @param anno
     * @return
     */
    public static Option creatOption(ClOption anno) {

        Option option = new Option(Character.toString(anno.opt()), anno.description());
        // some checks not done by commons cli
        if (anno.arg().isBlank()) {
            if (anno.argOptional()) {
                throw new IllegalArgumentException("no args but optionalArg is set to true");
            }
            if (anno.argCount() != -1) {
                throw new IllegalArgumentException("No point setting argCount when there is no arg");
            }
        } else {
            option.setArgName(anno.arg());
            if (anno.argCount() == -1) { // still the default value
                option.setArgs(1); // the default when there is an arg
            } else {
                KafuNumber.requireGreaterThanNull(anno.argCount());
                option.setArgs(anno.argCount());
            }
            option.setOptionalArg(anno.argOptional());
        }

        option.setRequired(anno.required());

        if (!anno.longOpt().isBlank()) {
            option.setLongOpt(anno.longOpt());
        }
        return option;
    }

    private void collectMethodOptions(final Map<Character, Method> methods) {
        for (Method method : this.getClass().getDeclaredMethods()) {
            if (!method.isAnnotationPresent(ClOption.class)) {
                continue;
            }
            var anno = method.getDeclaredAnnotation(ClOption.class);
            ensureUnused(anno);
            Option option = creatOption(anno);
            options.addOption(option);
            methods.put(anno.opt(), method);
        }
    }

    private void collectClassOptions(final Map<Character, Method> methods) {
        for (var anno : this.getClass().getAnnotationsByType(ClOption.class)) {
            ensureUnused(anno);
            Option option = creatOption(anno);
            options.addOption(option);
            methods.put(anno.opt(), null);
        }
    }

    public Map<Character, Method> getOptionsAnnotated() {
        return optionsAnnotated;
    }

    public int countOptions() {
        return optionsAnnotated.size();
    }

    /**
     * If there is no action option, the default action is called. If not overwritten, {@link #printHelp()} will be
     * called.
     *
     * @throws Exception overwrites might throw any Exception to be caught ba {@link #run(String[])}
     */
    public void defaultAction() throws Exception {
        printHelp();
    }

    /**
     * Default implementation for {@link #printHelp(String, String, String) the help info} without
     * syntax, without header and the {@link #DEFAULT_HELP_FOOTER}. However the options are presented
     * nicely already, which makes it sufficient in most cases. If not, it should be overwritten to call
     * {@link #printHelp(String, String, String)} with individual infos. Such an overwrite should always
     * look like:
     * <pre>{@code
     *  public void printHelp() {
     *   super.printHelp("my syntax or null", "my header or null", "my footer or null");
     *  }
     * }</pre>
     */
    public void printHelp() {
        printHelp(this.getClass().getSimpleName(), null, DEFAULT_HELP_FOOTER);
    }

    /**
     * Tames {@link HelpFormatter} in a convenient way. For your application you can overwrite
     * {@link #printHelp()} like this:
     * <pre>{@code
     *   public void printHelp() {
     *     super.printHelp("awesomeComparer file1 file2",
     *            "compares file1 with file2 counting the different bytes",
     *          "For help, better Call Saul");
     *   }
     * }</pre>
     * <p>
     * Without any {@link ClOptions options}, this would produce:
     * <pre>
     *  usage: awesomeComparer file1 file2
     *  compares file1 with file2 counting the different bytes
     *
     *  For help, better Call Saul
     * </pre>
     * <p>
     * Or with a simple {@link ClOption} it could look:
     * <pre>
     *  usage: awesomeComparer file1 file2 [-v]
     *  compares file1 with file1 counting the different bytes
     *
     *  -v,--verbose   verbose output
     *
     *  For help, better Call Saul
     * </pre>
     *
     * @param cmdLineSyntax some text which will be followed by the options
     * @param header        text between syntax line and the detailed list of options
     * @param footer        footer text
     */
    public final void printHelp(final String cmdLineSyntax, String header, String footer) {
        Objects.requireNonNull(cmdLineSyntax, "REQUIRE command line syntax String");
        var formatter = new HelpFormatter();

        if (header != null) {
            header += "\n\n";
        }

        if (footer != null) {
            footer = "\n" + footer;
        }
        var pw = new PrintWriter(out, true); // Close would close out as well!
        formatter.printHelp(pw, 100, cmdLineSyntax, header, options,
                HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD,
                footer, true);

    }

    public void setExitCode (int exitCode) {
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        if (exitCode == null) { // todo: throw ???
            return 0;
        }
        return exitCode;
    }

    public boolean hasExitCode () {
        return exitCode != null;
    }

}
