package junit;

import net.kafujo.config.SystemProperty;
import support.data.Args;
import support.examples.valid.DefaultActionOnly;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class DefaultActionOnlyTests {

    @Test
    void run() throws Throwable {
        var dao = new DefaultActionOnly();

        assertEquals(0, dao.countOptions());
        assertThat(dao.getOptionsAnnotated()).hasSize(0);

        dao.run(Args.EMPTY); // no option defined, so its fine to call without parameters
        // applying any cmdline parameters will cause rte:
        assertThrows(UnrecognizedOptionException.class, () -> dao.run(Args.a));
    }

}
