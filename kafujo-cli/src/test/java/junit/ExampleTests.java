package junit;

import org.junit.jupiter.api.Test;
import support.examples.valid.Example;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ExampleTests {
    @Test
    void name() {
        Example ex = new Example();
        ex.info();

        assertEquals(3, ex.getOptionsAnnotated().size());
        assertThat(ex.getOptionsAnnotated()).hasSize(3).hasFieldOrProperty("i");
    }
}
