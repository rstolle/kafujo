package junit;

import org.apache.commons.cli.MissingOptionException;
import support.examples.valid.Multiplier;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MultiplierTests {

    @Test
    void works() throws Throwable {
        Multiplier mult = new Multiplier(3,3);
        assertEquals (9, mult.product);

        mult.run(new String[]{"-c", "-a=2", "-b=5"});
        assertEquals(10, mult.product);

        mult.run(new String[]{"-c", "-a=88", "-b=88"});
        assertEquals(7744, mult.product);
    }

    @Test
    void fails()  {
        Multiplier mult = new Multiplier(3,3);
        assertEquals (9, mult.product);

        assertThrows(MissingOptionException.class, () -> mult.run(new String[]{"-c", "-b=5"}));
        assertEquals(9, mult.product);

    }
}
