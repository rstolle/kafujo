package junit;

import org.junit.jupiter.api.Test;
import support.data.Args;
import support.examples.valid.OptionalArg;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OptionalArgTests {
    @Test
    void oa() throws Throwable {
        OptionalArg oa = new OptionalArg();
        assertEquals("untouched", oa.result);

        oa.run(Args.a);
        assertEquals("no arg", oa.result);

        oa.run(Args.a_home);
        assertEquals("/home", oa.result);

        oa.run(Args.a_home_opt); // todo: should this fail?
        assertEquals("/home", oa.result);
    }
}
