package junit;

import org.junit.jupiter.api.Test;
import support.examples.invalid.NoArgButOptionalArg;
import support.examples.invalid.SameOptionTwice;
import support.examples.invalid.SameOptionTwice2;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidTests {
    @Test
    void fails() {
        assertThrows(IllegalArgumentException.class, SameOptionTwice::new);
        assertThrows(IllegalArgumentException.class, SameOptionTwice2::new);
        assertThrows(IllegalArgumentException.class, NoArgButOptionalArg::new);
    }
}
