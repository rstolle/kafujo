package junit;

import support.data.Args;
import support.examples.valid.DefaultActionVerbose;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class DefaultActionVerboseTests {

    @Test
    void run() throws Throwable {
        var dav = new DefaultActionVerbose();

        assertEquals(1, dav.countOptions());
        assertThat(dav.getOptionsAnnotated()).hasSize(1).containsKey('v');

        assertNull(dav.getOptionsAnnotated().get('v'));

        assertNull(dav.message);
        assertNull(dav.verbose);

        dav.run(Args.EMPTY);

        assertNotNull(dav.message);
        assertFalse(dav.verbose);

        dav.run(Args.v);
        assertTrue(dav.verbose);
    }


}
