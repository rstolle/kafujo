package junit;

import org.apache.commons.cli.ParseException;
import org.junit.jupiter.api.*;
import support.data.Args;
import support.examples.valid.TwoActionMethods;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TwoActionMethodsTests {

    final static TwoActionMethods tam = new TwoActionMethods();

    @Test
    @Order(1)
    void init() {

        assertEquals(3, tam.countOptions());
        assertThat(tam.getOptionsAnnotated()).hasSize(3)
                .containsKey('a').containsKey('b').containsKey('v');

        assertAccesses(0, 0, 0, 0);
    }

    @Test
    @Order(2)
    void noargs() throws Throwable {
        tam.run(Args.EMPTY);
        assertAccesses(0, 0, 1, 0);
        tam.run(Args.EMPTY);
        assertAccesses(0, 0, 2, 0);
    }

    @Test
    @Order(3)
    void verboseOnly() throws Throwable {
        tam.run(Args.v);
        assertAccesses(0, 0, 3, 1);
        tam.run(Args.EMPTY);
        assertAccesses(0, 0, 4, 1);
    }

    @Test
    @Order(4)
    void a() throws Throwable {
        tam.run(Args.a);
        assertAccesses(1, 0, 4, 1);
        tam.run(Args.av);
        assertAccesses(2, 0, 4, 2);
        tam.run(Args.a_v);
        assertAccesses(3, 0, 4, 3);
    }

    @Test
    @Order(5)
    void b() throws Throwable {
        tam.run(Args.b);
        assertAccesses(3, 1, 4, 3);
        tam.run(Args.bv);
        assertAccesses(3, 2, 4, 4);
    }

    @Test
    @Order(6)
    void abFail() {
        assertThrows(ParseException.class, () -> tam.run(Args.ab));
        assertAccesses(3, 2, 4, 4);
        assertThrows(ParseException.class, () -> tam.run(Args.a_b));
        assertAccesses(3, 2, 4, 4);
    }

    private void assertAccesses(int a, int b, int def, int withVerbose) {
        assertEquals(a, tam.countA, "a count wrong!");
        assertEquals(b, tam.countB, "b count wrong!");
        assertEquals(def, tam.countDefault, "default count wrong!");
        assertEquals(withVerbose, tam.countWithVerbose, "verbose count wrong!");
    }


}
