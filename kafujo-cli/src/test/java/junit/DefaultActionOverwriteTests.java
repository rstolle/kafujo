package junit;

import support.data.Args;
import support.examples.valid.DefaultActionOverwrite;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class DefaultActionOverwriteTests {

    @Test
    void run() throws Throwable{
        var dao = new DefaultActionOverwrite();
        
        assertEquals(0, dao.countOptions());
        assertThat (dao.getOptionsAnnotated()).hasSize(0);

        assertNull (dao.message);
        dao.run(Args.EMPTY);
        assertNotNull (dao.message);

    }


}
