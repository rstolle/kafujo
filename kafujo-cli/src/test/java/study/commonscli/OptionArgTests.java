package study.commonscli;

import org.apache.commons.cli.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import support.data.Args;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Option arg == option value.
 */
class OptionArgTests {

    private CommandLineParser parser;
    private Options options;
    private Option opt_a;

    @BeforeEach
    void beforEach() {
        opt_a = new Option("a", "show something");
        options = new Options();
        parser = new DefaultParser(); // I am not sure, if I could just reuse one parser.
    }

    @Test
    void plain() {
        assertFalse(opt_a.hasArg());
        assertFalse(opt_a.hasLongOpt());
        assertFalse(opt_a.isRequired());
        assertNull(opt_a.getArgName());
        assertEquals(-1, opt_a.getArgs());
        assertEquals("show something", opt_a.getDescription());
        assertEquals((char) 0, opt_a.getValueSeparator()); // whatever that means (found in OptionBuilder)
    }

    @Test
    void addUselessArgName() {
        opt_a.setArgName("arg");

        assertEquals("arg", opt_a.getArgName()); // the only effect!!

        // the rest stays same
        assertFalse(opt_a.hasArg());
        assertEquals(-1, opt_a.getArgs());
    }

    @Test
    void addOneArg() {
        opt_a.setArgs(1);

        assertTrue(opt_a.hasArg());
        assertNull(opt_a.getArgName());
        assertEquals(1, opt_a.getArgs());
    }

    @Test
    void addTwoArgs() {
        opt_a.setArgs(2);

        assertTrue(opt_a.hasArg());
        assertNull(opt_a.getArgName());
        assertEquals(2, opt_a.getArgs());
    }

    @Test
    void parse1() throws ParseException {
        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home);
        assertNull(line.getOptionValue('a')); // arg is ignored!!
        assertNull(line.getOptionValues('a'));
    }

    @Test
    void parse1_fixed() throws ParseException {
        opt_a.setArgs(1); // the fix

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home);
        assertEquals("/home", line.getOptionValue('a'));
        assertThat(line.getOptionValues('a')).hasSize(1).contains("/home");
    }

    @Test
    void parse2() throws ParseException {
        opt_a.setArgs(2); // the fix

        options.addOption(opt_a);
        assertThrows(MissingArgumentException.class, () -> parser.parse(options, Args.a_home));
    }

    @Test
    void parse2_fix1() throws ParseException {
        opt_a.setArgs(2);

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home_opt);  // the fix
        assertEquals("/home", line.getOptionValue('a'));
        assertThat(line.getOptionValues('a')).hasSize(2).contains("/home", "/opt");
    }

    @Test
    void parse2_fix2() throws ParseException {
        opt_a.setArgs(2);
        opt_a.setOptionalArg(true); // the fix

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a);
        assertNull(line.getOptionValue('a'));
        assertNull(line.getOptionValues('a'));
    }

    @Test
    void parse2_fix3() throws ParseException {
        opt_a.setArgs(2);
        opt_a.setOptionalArg(true);

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home);
        assertEquals("/home", line.getOptionValue('a'));
        assertThat(line.getOptionValues('a')).hasSize(1).contains("/home");
    }

    @Test
    void parse2_fix4() throws ParseException {
        opt_a.setArgs(2);
        opt_a.setOptionalArg(true);

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home_opt);
        assertEquals("/home", line.getOptionValue('a'));
        assertThat(line.getOptionValues('a')).hasSize(2).contains("/home", "/opt");
    }

    @Test
    void parse2_fix5() throws ParseException {
        opt_a.setArgs(1);
        opt_a.setOptionalArg(true);

        options.addOption(opt_a);
        CommandLine line = parser.parse(options, Args.a_home_opt); // should not work, but does
        assertEquals("/home", line.getOptionValue('a'));
        assertThat(line.getOptionValues('a')).hasSize(1).contains("/home");
    }

}
