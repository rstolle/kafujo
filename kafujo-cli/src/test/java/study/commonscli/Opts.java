package study.commonscli;

import org.apache.commons.cli.Option;

public class Opts {
    public static final Option A_MINIMAL = new Option("a", "see all");
    public static final Option A_ARG = new Option("a", true, "see all");
    public static final Option A_ARG_LONG = new Option("a", "all", true, "see all");
}
