package study.commonscli;

import org.apache.commons.cli.Options;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * These tests come from https://github.com/apache/commons-cli, adopted to juni5 and slightly extended.
 */
public class OriginalTests {

    /**
     * This might be a nasty feature, if you have lots of options.
     */
    @Test
    public void testDuplicateSimple()
    {
        final Options opts = new Options();
        opts.addOption("a", false, "toggle -a");
        opts.addOption("a", true, "toggle -a*");

        assertEquals( "toggle -a*", opts.getOption("a").getDescription(), "last one in wins");
        assertEquals(1, opts.getOptions().size());
    }

}
