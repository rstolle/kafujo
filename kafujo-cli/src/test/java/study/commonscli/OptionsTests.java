package study.commonscli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 */
class OptionsTests {

    @Test
    void empty() {
        final Options options = new Options();
        assertThat(options.getOptions()).isEmpty();
        assertNull(options.getOption("a"));
        assertThat(options.getRequiredOptions()).isEmpty();
    }

    @Test
    void optsAB() {
        final Options options = new Options();
        options.addOption("a", "show all");
        options.addOption("b", "beat it");

        assertThat(options.getOptions()).hasSize(2);
    }

    @Test
    void optsAB2() {
        final Options options = new Options();
        options.addOption("a", "show all");

        Option b = new Option("b", "beat it");
        options.addOption(b);

        assertThat(options.getOptions()).hasSize(2);
        assertEquals(b, options.getOption("b"));
        assertNotEquals(b, options.getOption("a"));
    }

}
