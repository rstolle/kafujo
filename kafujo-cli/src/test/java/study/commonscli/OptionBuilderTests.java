package study.commonscli;

import org.apache.commons.cli.Option;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OptionBuilderTests {
    @Test
    public void optOnly() {
        final Option option = Option.builder("a").desc("see all").build();
        assertEquals("a", option.getOpt());
        assertFalse(option.hasArg());
        assertFalse(option.hasArgName());
        assertFalse(option.hasOptionalArg());
        assertFalse(option.hasLongOpt());

        assertEquals(option, Opts.A_ARG);
        assertNotSame(option, Opts.A_ARG);
    }

    @Test
    public void optionArg() {
        final Option option = Option.builder("b").hasArg().argName("SIZE").build();
        assertEquals("b", option.getOpt());

        assertTrue(option.hasArg());
        assertFalse(option.hasArgs());
        assertFalse(option.hasOptionalArg());
        assertTrue(option.hasArgName());

        assertEquals("SIZE", option.getArgName());
        assertFalse(option.hasLongOpt());
    }

    @Test
    public void optionArgs() {
        final Option option = Option.builder("b").hasArgs().build();
        assertEquals("b", option.getOpt());

        assertTrue(option.hasArg());
        assertTrue(option.hasArgs());
        assertFalse(option.hasArgName());
        assertFalse(option.hasOptionalArg());

        assertFalse(option.hasLongOpt());
    }

    @Test
    public void optionalArg() {
        final Option option = Option.builder("b").optionalArg(true).hasArg().build();
        assertEquals("b", option.getOpt());

        assertTrue(option.hasArg());          // QUIRK: now we have non optional and ...
        assertTrue(option.hasOptionalArg());  // ... optional at the same time

        assertFalse(option.hasArgs());
        assertFalse(option.hasArgName());

        assertFalse(option.hasLongOpt());
    }

}
