package study.commonscli;

import support.data.Args;
import org.apache.commons.cli.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CmdLineTests {

    private CommandLineParser parser;

    @BeforeEach
    void beforEach() {
        // I am not sure, if I could just reuse one parser.
        // So I make sure its done like in the most real apps: one parser for one cmdline
        parser = new DefaultParser();
    }

    @Test
    void onlyOptA() throws ParseException {
        final Options options = new Options();
        options.addOption("a", "show all");

        CommandLine line = parser.parse(options, Args.a);

        // option a is defined and given in the command line
        assertTrue(line.hasOption("a"));
        assertTrue(line.hasOption('a'));

        assertThat(line.getArgs()).hasSize(0);
        assertThat(line.getArgList()).hasSize(0);

        // you can query a parsed commandline for options which are not defined at all ...
        assertFalse(line.hasOption('b'));
        // but you cannot parse a new commandline with undefined options
        assertThrows(UnrecognizedOptionException.class, () -> parser.parse(options, Args.a_b));
    }

    @Test
    void optsAB() throws ParseException {
        final Options options = new Options();
        options.addOption("a", "show all");
        options.addOption("b", "beat it");

        final CommandLine line = parser.parse(options, Args.a);

        assertThat(line.getOptions()).hasSize(1);

        assertTrue(line.hasOption("a"));
        assertFalse(line.hasOption('b'));
        assertFalse(line.hasOption('c'));

        assertThat(line.getArgs()).hasSize(0);
        assertThat(line.getArgList()).hasSize(0);
    }


    @Test
    void oneArgOnly() throws ParseException {

        final Options options = new Options();

        CommandLine line = parser.parse(options, Args.home);
        assertThrows(UnrecognizedOptionException.class, () -> parser.parse(options, Args.home_a));

        assertThat(line.getOptions()).hasSize(0);
        assertThat(line.getArgs()).hasSize(1).contains("/home");
        assertThat(line.getArgList()).hasSize(1).contains("/home");
    }

    @Test
    void oneArgOneOpt() throws ParseException {

        final Options options = new Options();
        options.addOption("a", "show all");

        CommandLine line = parser.parse(options, Args.home_a);

        assertTrue(line.hasOption("a"));
        assertTrue(line.hasOption('a'));

        assertThat(line.getArgs()).hasSize(1).contains("/home");
        assertThat(line.getArgList()).hasSize(1).contains("/home");
    }

    /**
     * This is a really strange argumnent and I did not really get it.
     *
     * @throws ParseException
     */
    @Test
    void stopAtNonOption() throws ParseException {
        assertThrows(UnrecognizedOptionException.class, () -> parser.parse(new Options(), Args.a, false));
        CommandLine cmdline = parser.parse(new Options(), Args.a, true);

        assertFalse(cmdline.hasOption("a"));
        assertThat(cmdline.getArgList()).hasSize(1).contains("-a");
    }

    /**
     * prg -a /home and prg /home -a is the same.
     *
     * @throws ParseException not here.
     */
    @Test
    void order() throws ParseException {
        Options options = new Options().addOption(Opts.A_MINIMAL);

        CommandLine ahome = parser.parse(options, Args.a_home);
        CommandLine homea = parser.parse(options, Args.home_a);

        assertTrue(ahome.hasOption("a"));
        assertTrue(homea.hasOption("a"));

        assertThat(ahome.getArgList()).hasSize(1).contains("/home");
        assertThat(homea.getArgList()).hasSize(1).contains("/home");
    }
}
