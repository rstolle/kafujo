package study.commonscli;

import support.data.Args;
import org.apache.commons.cli.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CmdlineTypedTests {

    final CommandLineParser parser = new DefaultParser();

    @Test
    public void failsWithInteger() throws ParseException {
        final Option option = Option.builder("s").hasArg().type(Integer.TYPE).build();

        Options options = new Options();
        options.addOption(option);

        CommandLine line = parser.parse(options, Args.s100);

        assertEquals("100", line.getOptionValue("s"));  // it basically works but ...
        assertNull(line.getParsedOptionValue("s"));              // ... not with the given type (Integer) ...
        assertEquals(Integer.TYPE, option.getType());            // ... even though this works
    }

    @Test
    public void worksWithNumber() throws ParseException {
        final Option option = Option.builder("s").hasArg().type(Number.class).build();

        Options options = new Options();
        options.addOption(option);

        CommandLine line = parser.parse(options, Args.s100);

        assertEquals("100", line.getOptionValue("s"));

        Object typed = line.getParsedOptionValue("s");
        assertTrue (typed instanceof Number);
        assertEquals(100L, typed);
        assertEquals(Number.class, option.getType());
    }

    @Test
    public void optionArgTyped() throws ParseException {
        final Option option = Option.builder("s").hasArg().argName("SIZE").type(Number.class).build();

        assertEquals("s", option.getOpt());
        assertTrue(option.hasArg());
        assertTrue(option.hasArgName());
        assertEquals("SIZE", option.getArgName());
        assertEquals(Number.class, option.getType());
        assertFalse(option.hasOptionalArg());
        assertFalse(option.hasLongOpt());

        CommandLineParser parser = new DefaultParser();

        Options options = new Options();
        options.addOption(option);

        System.out.println(options.getRequiredOptions());

        CommandLine line = parser.parse(options, Args.s100);

        var r = line.getParsedOptionValue("s");


        System.out.println(line.getOptionValue("s"));
        System.out.println(r);





    }

}
