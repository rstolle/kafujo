package study.commonscli;

import org.apache.commons.cli.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import support.data.Args;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * As these tests show, it can become quite confusing having options with more than one
 * letter and using the unix style -ab thing, to mark option a and option b. What if there
 * is an option "ab" ...
 */
class OneLetterTests {
    private CommandLineParser parser;
    private Options options;
    private Option opt_a;
    private Option opt_b;

    @BeforeEach
    void beforEach() {
        opt_a = new Option("a", "show something");
        opt_b = new Option("b", "show something else");
        options = new Options();
        parser = new DefaultParser(); // I am not sure, if I could just reuse one parser.
    }

    @Test
    void ab() throws ParseException {
        options.addOption(opt_a);
        options.addOption(opt_b);
        var line = parser.parse(options, Args.ab);

        assertTrue(line.hasOption('a'));
        assertTrue(line.hasOption('b'));
    }

    @Test
    void a_b() throws ParseException {
        options.addOption(opt_a);
        options.addOption(opt_b);
        var line = parser.parse(options, Args.a_b);

        assertTrue(line.hasOption('a'));
        assertTrue(line.hasOption('b'));
    }

    @Test
    void abInOne() throws ParseException {
        options.addOption(new Option("ab", "wild stuff"));
        var line = parser.parse(options, Args.ab);

        assertTrue(line.hasOption("ab"));
        assertFalse(line.hasOption('a'));
        assertFalse(line.hasOption('b'));
    }

    @Test
    void abInOneCollision1() throws ParseException {
        options.addOption(new Option("ab", "wild stuff"));
        options.addOption(opt_a);
        var line = parser.parse(options, Args.ab);

        assertTrue(line.hasOption("ab"));
        assertFalse(line.hasOption('a'));
        assertFalse(line.hasOption('b'));
    }

    @Test
    void abInOneCollision2() throws ParseException {
        options.addOption(new Option("ab", "wild stuff"));
        options.addOption(opt_a);
        options.addOption(opt_b);
        var line = parser.parse(options, new String[]{"-a", "-b", "-ab"});

        assertTrue(line.hasOption("ab"));
        assertTrue(line.hasOption('a'));
        assertTrue(line.hasOption('b'));
    }

}
