package study.commonscli;

import org.apache.commons.cli.Option;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OptionCtorTests {
    @Test
    public void simple() {
        final Option option = new Option("a", "different description");

        assertFalse(option.hasArg());
        assertFalse(option.hasOptionalArg());

        assertEquals(option, Opts.A_MINIMAL);     // even different descriptions
        assertEquals(option, Opts.A_ARG);         // different desc and args!
        assertNotEquals(option, Opts.A_ARG_LONG); // QUIRK: suddenly they actual differ
    }

    @Test
    public void longer() {
        final Option option = new Option("a", "all", true, "see all");

        assertTrue(option.hasArg());
        assertFalse(option.hasOptionalArg());

        assertEquals(option, Opts.A_ARG_LONG);  // equal even different arg setting
    }

    @Test
    public void noDescription() {
        final Option option = new Option("a", null);
        assertNull(option.getDescription());
    }
}
