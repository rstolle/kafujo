package support.data;

public class Args {

    public static final String[] EMPTY = new String[]{};
    public static final String[] a = new String[]{"-a"};
    public static final String[] b = new String[]{"-b"};
    public static final String[] v = new String[]{"-v"};

    public static final String[] ab = new String[]{"-ab"};
    public static final String[] abv = new String[]{"-abv"};
    public static final String[] av = new String[]{"-av"};
    public static final String[] bv = new String[]{"-bv"};
    public static final String[] a_v = new String[]{"-a", "-v"};
    public static final String[] a_b = new String[]{"-a", "-b"};
    public static final String[] a_b_c = new String[]{"-a", "-b", "-c"};

    public static final String[] s100 = new String[]{"-s=100"};
    public static final String[] size100 = new String[]{"--size=101"};
    public static final String[] home = new String[]{"/home"};
    public static final String[] home_a = new String[]{"/home", "-a"};
    public static final String[] a_home = new String[]{"-a", "/home"};
    public static final String[] a_home_opt = new String[]{"-a", "/home", "/opt"};
    public static final String[] home_ab = new String[]{"/home", "-a", "-s=100"};
}
