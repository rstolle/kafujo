package support.examples.valid;


import net.kafujo.cli.ClAppCore;

public class DefaultActionOverwrite extends ClAppCore {

    public String message = null;

    @Override
    public void defaultAction() {
        message = "This is a pretty useless cmdline app. But valid!";
        out.println(message);
    }

    public static void main(String[] args) throws Throwable {
        new DefaultActionOverwrite().run(args);
    }
}
