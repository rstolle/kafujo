package support.examples.valid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

@ClOption(opt = 'v', description = "version")
public class TwoActionMethods extends ClAppCore {

    public int countDefault = 0;
    public int countA = 0;
    public int countB = 0;

    public int countWithVerbose = 0;

    @Override
    public void defaultAction() {
        countDefault++;
        verboseCheck();
    }

    @ClOption(opt = 'a', description = "all")
    public void a() {
        countA++;
        verboseCheck();
    }

    @ClOption(opt = 'b', description = "description")
    public void b() {
        countB++;
        verboseCheck();
    }

    private void verboseCheck() {
        if (commandLine.hasOption("v")) {
            countWithVerbose++;
        }
    }

    public static void main(String[] args) throws Throwable {
        new TwoActionMethods().run(args);
    }
}
