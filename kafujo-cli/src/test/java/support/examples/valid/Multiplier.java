package support.examples.valid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

/**
 * Creation must fail, cause there is two times option 'i'.
 */
@ClOption(opt = 'a', arg = "value", required = true,
        longOpt = "set-a", description = "a value")
@ClOption(opt = 'b', arg = "value", required = true,
        longOpt = "set-b", description = "b value")

public class Multiplier extends ClAppCore {

    public int a;
    public int b;
    public int product;

    public Multiplier(int a, int b) {
        this.a = a;
        this.b = b;
        product = a * b;
    }

    @ClOption(opt = 'c', description = "description")
    public void calculate () {
        a = readInt("a");
        b = readInt("b");
        product = a * b;
    }
}
