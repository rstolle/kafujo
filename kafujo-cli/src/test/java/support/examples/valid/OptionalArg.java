package support.examples.valid;

import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

public class OptionalArg extends ClAppCore {

    public String result = "untouched";

    @ClOption(opt = 'a', description = "description", arg = "filter", argOptional = true)
    public void add() {
        var ov = commandLine.getOptionValue('a');
        if (ov == null) {
            result = "no arg";
        } else {
            result = ov;
        }
    }

    public static void main(String[] args) throws Throwable {
        var cl = new OptionalArg();
        cl.run(args);
        System.out.println("-----------------------------");
        System.out.println("result -> " + cl.result);
    }
}
