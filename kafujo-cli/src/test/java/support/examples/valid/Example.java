package support.examples.valid;

import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

@ClOption(opt = 'v', description = "verbose output")
@ClOption(opt = 'S', description = "sleep after done")
public class Example extends ClAppCore {

    @ClOption(opt = 'i', longOpt = "info", description = "info about the project")
    public void info () {
        System.out.println("info");
    }

    public static void main(String[] args) throws Throwable {
        new Example().run(args);
    }
}
