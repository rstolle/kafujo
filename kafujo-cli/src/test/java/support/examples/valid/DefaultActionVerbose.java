package support.examples.valid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

@ClOption(opt = 'v', description = "description")
public class DefaultActionVerbose extends ClAppCore {

    public String message = null;

    public Boolean verbose = null;

    @Override
    public void defaultAction() {
        message = "This is a pretty useless cmdline app. But valid!";

        if (commandLine.hasOption("v")) {
            verbose = true;
            out.println(message);
        } else {
            verbose = false;
        }
    }

    public static void main(String[] args) throws Throwable {
        new DefaultActionVerbose().run(args);
    }
}
