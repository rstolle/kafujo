package support.examples.valid;


import net.kafujo.cli.ClAppCore;

public class NoCliAnnotations extends ClAppCore {

    public static void main(String[] args) throws Throwable {
        new NoCliAnnotations().run(args);
    }
}
