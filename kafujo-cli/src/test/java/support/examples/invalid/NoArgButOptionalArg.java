package support.examples.invalid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

/**
 * Must fail on creation, cause there is an arg name on no arg argument
 */
public class NoArgButOptionalArg extends ClAppCore {

    @ClOption(opt = 'd', description = "show definitions", argOptional = true)
    public void definitions() {

    }

}
