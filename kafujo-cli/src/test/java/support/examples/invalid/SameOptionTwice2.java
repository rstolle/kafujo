package support.examples.invalid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

/**
 * Creation must fail, cause there is two times option 'i'.
 */
@ClOption(opt = 'i', description = "info")
public class SameOptionTwice2 extends ClAppCore {

    @ClOption(opt = 'i', description = "info")
    public void info() {

    }

    @ClOption(opt = 'i', description = "print internals")
    public void internal() {

    }
}
