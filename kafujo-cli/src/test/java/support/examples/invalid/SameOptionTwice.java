package support.examples.invalid;


import net.kafujo.cli.ClAppCore;
import net.kafujo.cli.ClOption;

/**
 * Must fail on creation already, cause there is two times option 'i'.
 */
public class SameOptionTwice extends ClAppCore {

    @ClOption(opt = 'i', description = "info")
    public void info() {

    }

    @ClOption(opt = 'i', description = "print internals")
    public void internal() {

    }
}
