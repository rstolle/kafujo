package net.useful;

import net.useless.Util;

import java.util.Objects;
import java.util.Properties;

/**
 * A Properties class which cannot become "compromised". Read the javadoc of
 * {@link Properties} if you dont understand this.
 */
public class PropertiesSafe extends Properties {

    public static int count = 0;

    static {
        Util.load(PropertiesSafe.class);
    }

    public PropertiesSafe() {
        Util.ctor(PropertiesSafe.class, ++count);
    }

    @Override
    public synchronized Object put(Object key, Object value) {
        return super.put(Objects.toString(key), Objects.toString(value));
    }
}
