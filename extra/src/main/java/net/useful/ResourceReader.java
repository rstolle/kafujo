package net.useful;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Properties;

/**
 * Uses resources from within test.jar
 */
public class ResourceReader {

    public String read (String key) {
        try (var in = this.getClass().getClassLoader().getResourceAsStream("net/useless/const.properties")) {
            Properties props = new Properties();
            props.load(in);
            return props.getProperty(key);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
