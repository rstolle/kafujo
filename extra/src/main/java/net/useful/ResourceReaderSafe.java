package net.useful;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Properties;

/**
 * Uses resources and another class from within test.jar.
 */
public class ResourceReaderSafe {

    public String read (String key) {
        try (var in = this.getClass().getClassLoader().getResourceAsStream("net/useless/const.properties")) {
            PropertiesSafe props = new PropertiesSafe();
            props.load(in);
            return props.getProperty(key);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
