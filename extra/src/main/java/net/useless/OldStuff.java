package net.useless;


@Deprecated
public class OldStuff {
    private static int count = 0;

    static {
        Util.load(OldStuff.class);
    }

    public OldStuff() {
        Util.ctor(this.getClass(), ++count);
    }
}
