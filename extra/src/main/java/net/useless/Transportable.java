package net.useless;

import java.io.Serializable;

public class Transportable implements Serializable {

    public static int count = 0;

    static {
        Util.load(Transportable.class);
    }

    public Transportable() {
        Util.ctor(Transportable.class, ++count);
    }
}
