package net.useless;

public class Base {

    private static int count = 0;

    static {
        Util.load(Base.class);
    }

    public Base() {
        Util.ctor(Base.class, this.getClass(), ++count);
    }
}

