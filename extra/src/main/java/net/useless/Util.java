package net.useless;

public class Util {

    public static void load (final Class<?> clazz) {
        System.out.println("[test.jar      LOAD] " + clazz.getSimpleName());
    }

    public static void ctor (final Class<?> clazz, final int count) {
        System.out.println("[test.jar CONSTRUCT] " + clazz + " object: " + count);
    }

    public static void ctor (final Class<?> clazz, final Class<? extends Base> subClazz, final int count) {
        System.out.println("[test.jar CONSTRUCT] " + clazz + "." + subClazz + " object: " + count);
    }
}
