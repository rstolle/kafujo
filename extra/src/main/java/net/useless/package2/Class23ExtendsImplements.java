package net.useless.package2;

import net.useless.Base;
import net.useless.Interface;
import net.useless.Util;

public class Class23ExtendsImplements extends Base implements Interface {

    private static int count = 0;

    static {
        Util.load(Class23ExtendsImplements.class);
    }

    public Class23ExtendsImplements() {
        Util.ctor(Class23ExtendsImplements.class, ++count);
    }
}
