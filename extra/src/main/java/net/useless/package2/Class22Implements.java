package net.useless.package2;

import net.useless.Interface;
import net.useless.Util;

import java.io.Serializable;

public class Class22Implements implements Interface, Serializable {

    private static int count = 0;

    static {
        Util.load(Class22Implements.class);
    }

    private static class PrivateWithinClass22 {
        static {
            Util.load(PrivateWithinClass22.class);
        }
    }

    protected static class ProtectedWithinClass22 {
        static {
            Util.load(ProtectedWithinClass22.class);
        }
    }


    public Class22Implements() {
        Util.ctor(Class22Implements.class, ++count);
    }
}
