package net.useless.package2;

import net.useless.Util;

public class Class21 {

    private static int count = 0;

    static {
        Util.load(Class21.class);
    }

    public Class21() {
        Util.ctor(Class21.class, ++count);
    }
}
