package net.useless.package2;

import net.useless.Util;

import java.rmi.Remote;

public class Class24ImplementsRemote implements Remote {

    private static int count = 0;

    static {
        Util.load(Class24ImplementsRemote.class);
    }

    public Class24ImplementsRemote() {
        Util.ctor(Class24ImplementsRemote.class, ++count);
    }
}
