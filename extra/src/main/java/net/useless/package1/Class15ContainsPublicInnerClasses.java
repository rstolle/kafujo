package net.useless.package1;

import net.useless.Base;
import net.useless.Interface;
import net.useless.Util;

import java.io.Serializable;

/**
 * Hello world!
 */
public class Class15ContainsPublicInnerClasses {

    // 1.class:
    public static Comparable<String> USELESS_CLASS = new Comparable<>() {
        @Override
        public int compareTo(String o) {
            return 0;
        }
    };

    public static Comparable<Base> USELESS_LAMBDA = object -> 2;      // lambda wont show up

    public class PublicWithinClass15NonStatic {
        private int count = 0;
        public PublicWithinClass15NonStatic() {
            Util.ctor(this.getClass(), ++count);
        }
    }

    public static class PublicWithinClass15 {
        static {
            Util.load(PublicWithinClass15.class);
        }
    }

    public static class PublicWithinClass15Base extends Base {
        static {
            Util.load(PublicWithinClass15Base.class);
        }
    }

    public static class PublicWithinClass15Interface implements Interface {
        static {
            Util.load(PublicWithinClass15Interface.class);
        }
    }

    public static class PublicWithinClass15Serializable implements Serializable {
        static {
            Util.load(PublicWithinClass15Serializable.class);
        }
    }

    public static int count = 0;

    static {
        Util.load(Class15ContainsPublicInnerClasses.class);
    }

    public Class15ContainsPublicInnerClasses() {
        Util.ctor(this.getClass(), ++count);
    }
}
