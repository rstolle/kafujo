package net.useless.package1;

import net.useless.Interface;
import net.useless.Util;

/**
 * Hello world!
 */
public class Class13Implements implements Interface {

    public static int count = 0;

    static {
        Util.load(Class13Implements.class);
    }

    public Class13Implements() {
        Util.ctor(this.getClass(), ++count);
    }
}
