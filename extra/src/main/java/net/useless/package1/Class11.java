package net.useless.package1;

import net.useless.Util;

/**
 * Hello world!
 */
public class Class11 {

    public static int count = 0;

    static {
        Util.load(Class11.class);
    }

    public Class11() {
        Util.ctor(this.getClass(), ++count);
    }
}
