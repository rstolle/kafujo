package net.useless.package1;

import net.useless.Base;
import net.useless.Util;

/**
 * Hello world!
 */
public class Class12Extends extends Base {

    public static int count = 0;

    static {
        Util.load(Class12Extends.class);
    }

    public Class12Extends() {
        Util.ctor(this.getClass(), ++count);
    }
}
