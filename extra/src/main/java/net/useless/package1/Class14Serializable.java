package net.useless.package1;

import net.useless.Util;

import java.io.Serializable;

/**
 * Hello world!
 */
public class Class14Serializable implements Serializable {

    public static int count = 0;

    static {
        Util.load(Class14Serializable.class);
    }

    public Class14Serializable() {
        Util.ctor(this.getClass(), ++count);
    }
}
