package net.useless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Hello world!
 */
public final class App {

    /**
     * A logger for the whole project.
     */
    public static final Logger LGR = LoggerFactory.getLogger(App.class);

    private App() {
        // pure utility class
    }

    /**
     * A extremely useful main method.
     *
     * @param args the args from the cmdline
     */
    public static void main(final String[] args) {
        assertThat(LGR).isNotNull();

        LGR.debug("Hello World!");
    }
}
