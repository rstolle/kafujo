/**
 * This package is hopefully outside of any package to be tested. I wann test
 * only the public interface. If there is the need to test any private methods,
 * it should be done in separate package.
 */
package outside;
