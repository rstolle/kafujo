package outside;


import net.useful.PropertiesSafe;
import net.useful.ResourceReader;
import net.useful.ResourceReaderSafe;
import net.useless.Interface;
import net.useless.package1.Class12Extends;
import net.useless.package2.Class21;
import net.useless.package2.Class22Implements;
import net.useless.package2.Class23ExtendsImplements;
import org.junit.jupiter.api.Test;

import java.io.Serializable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test for simple App.
 */
public class TheTest {
    /**
     * Rigorous Test :-).
     */

    @Test
    public void loadAll() {
        Class12Extends c111 = new Class12Extends();
        assertEquals(1, Class12Extends.count);

        System.out.println("---------------------------------------------");
        var c22 = new Class22Implements();
        assertTrue(c22 instanceof Serializable);
        assertTrue(c22 instanceof Interface);


        var propSave = new PropertiesSafe();
        assertTrue(propSave instanceof Serializable);
    }

    @Test
    void resources() {
        ResourceReader rr = new ResourceReader();
        assertEquals("3.1415", rr.read("pi"));
        assertEquals("42", rr.read("dont.panic"));
        assertEquals("hello", rr.read("greeting"));
    }

    @Test
    void resourcesSafe() {
        ResourceReaderSafe rr = new ResourceReaderSafe();
        assertEquals("3.1415", rr.read("pi"));
        assertEquals("42", rr.read("dont.panic"));
        assertEquals("hello", rr.read("greeting"));
    }
}
