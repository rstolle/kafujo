
== test.jar

This projects implements different kinds of classes to be jared. Its main purpose is to give a testing base for the kafujo reflection package.

There is one link:src/main/java/net/useless/Base.java[base class], one link:src/main/java/net/useless/Interface.java[interface], and some classes which extend and/or implement them. Every time one of these classes is loaded, there is a sout message. The same, when objects of these classes are created. Some classes implement Serializable and one class extends Properties. Here a list of all classes in alphabetical order:

. `net.useful.PropertiesSafe extends java.util.Properties implements Serializable`
. `net.useful.PropertiesSafe extends java.util.ResourceReader` (reads net/useless/const.properties)
. `net.useful.PropertiesSafe extends java.util.ResourceReaderSafe` (reads resource and uses class)
. `net.useless.App`
. `net.useless.Base`
. `net.useless.Interface`
. `net.useless.Transportable implements Serializable`
. `net.useless.Util`
. `net.useless.package1.Class11`
. `net.useless.package1.Class12Extends`
. `net.useless.package1.Class13Implements implements Interface`
. `net.useless.package1.Class14Serializable implements Serializable`
. `net.useless.package1.Class15ContainsPublicInnerClasses`
. `net.useless.package1.Class15ContainsPublicInnerClasses$1 implements Comparable`
. `net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15`
. `net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15Base extends Base`
. `net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15Interface implements Interface`
. `net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15NonStatic`
. `net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15Serializable implements Serializable`
. `net.useless.package2.Class21`
. `net.useless.package2.Class22Implements implements Interface, Serializable`
. `net.useless.package2.Class22Implements$PrivateWithinClass22`
. `net.useless.package2.Class22Implements$ProtectedWithinClass22`
. `net.useless.package2.Class23ExtendsImplements extends Base implements Interface`
. `net.useless.package2.Class24ImplementsRemote implements Remote`

This is the content of the jar file created by `mvn clean compile jar:jar`:
----
$ tree  -v | nl -v 0
     0	.
     1	├── META-INF
     2	│   ├── MANIFEST.MF
     3	│   └── maven
     4	│       └── net.samples42
     5	│           └── java-basics
     6	│               ├── pom.properties
     7	│               └── pom.xml
     8	└── net
     9	    ├── useful
    10	    │   ├── PropertiesSafe.class
    11	    │   ├── ResourceReader.class
    12	    │   └── ResourceReaderSafe.class
    13	    └── useless
    14	        ├── App.class
    15	        ├── Base.class
    16	        ├── Interface.class
    17	        ├── OldStuff.class
    18	        ├── Transportable.class
    19	        ├── Util.class
    20	        ├── const.properties
    21	        ├── package1
    22	        │   ├── Class11.class
    23	        │   ├── Class12Extends.class
    24	        │   ├── Class13Implements.class
    25	        │   ├── Class14Serializable.class
    26	        │   ├── Class15ContainsPublicInnerClasses$1.class
    27	        │   ├── Class15ContainsPublicInnerClasses$PublicWithinClass15.class
    28	        │   ├── Class15ContainsPublicInnerClasses$PublicWithinClass15Base.class
    29	        │   ├── Class15ContainsPublicInnerClasses$PublicWithinClass15Interface.class
    30	        │   ├── Class15ContainsPublicInnerClasses$PublicWithinClass15NonStatic.class
    31	        │   ├── Class15ContainsPublicInnerClasses$PublicWithinClass15Serializable.class
    32	        │   └── Class15ContainsPublicInnerClasses.class
    33	        └── package2
    34	            ├── Class21.class
    35	            ├── Class22Implements$PrivateWithinClass22.class
    36	            ├── Class22Implements$ProtectedWithinClass22.class
    37	            ├── Class22Implements.class
    38	            ├── Class23ExtendsImplements.class
    39	            └── Class24ImplementsRemote.class

9 directories, 29 files
----
