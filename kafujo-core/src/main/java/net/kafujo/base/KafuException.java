package net.kafujo.base;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.util.Objects;

public final class KafuException {

    private KafuException() {
        // pure utility class
    }

    /**
     * Pretty much like apache commons ExceptionsUlils.getMessage() but wont accept null and uses newer java features.
     *
     * @param throwable Exception to be analyzed
     * @return the message of {@code throwable} or its toString, if there is no message.
     */
    public static String extractMessage(Throwable throwable) {
        Objects.requireNonNull(throwable, "REQUIRE throwable");

        String message = throwable.getMessage();

        if (message == null || message.isBlank()) {
            return throwable.toString();
        }
        return message;
    }

    /**
     * Pretty much like apache commons ExceptionsUlils.getMessage() but wont accept null and uses newer java features.
     *
     * @param throwable Exception to be analyzed
     * @return the message of {@code throwable} or its toString, if there is no message.
     */
    public static String extractClassAndMessage(Throwable throwable) {
        Objects.requireNonNull(throwable, "REQUIRE throwable");

        String message = throwable.getMessage();

        if (message == null || message.isBlank()) {
            return throwable.toString();
        }

        return throwable.getClass().getSimpleName() + ": " + message;
    }

    /**
     * todo: comment or better stick with apache commons?
     *
     * @param throwable Exception to be analyzed
     * @return the content {@link Throwable#printStackTrace()} as a String
     */
    public static String extractStackTrace(Throwable throwable) {
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw, true)) {

            throwable.printStackTrace(pw);
            return sw.getBuffer().toString();
        } catch (IOException io) {
            throw new UncheckedIOException("Problem converting stacktrace to String", io);
        }
    }
}
