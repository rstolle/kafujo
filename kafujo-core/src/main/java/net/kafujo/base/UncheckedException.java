package net.kafujo.base;

/**
 * An Exception to wrap annoying checked Exception.
 *
 * @author rstolle
 * @since 0.2.0 (then as CheckedExceptionWrapper)
 */
public class UncheckedException extends KafujoException {

    public UncheckedException(Throwable cause) {
        super("Checked exception wrapped", cause);
    }

    public UncheckedException(String message, Throwable cause) {
        super(message, cause);
    }
}
