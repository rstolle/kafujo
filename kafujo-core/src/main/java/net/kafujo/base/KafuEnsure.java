package net.kafujo.base;

import java.util.Objects;

/**
 * Ensure methods which don't fit somewhere else.
 *
 * @author rstolle
 * @since 0.2.0
 */
public final class KafuEnsure {

    private KafuEnsure() {
        // pure utility class
    }

    /**
     * Ensures the the enum name wont exceed a given length.
     *
     * @param enumToBeChecked the enum (name) to be checked
     * @param maxLenOfName    max len
     * @throws IllegalStateException if the actual
     */
    public static void enumNameLength(final Enum<?> enumToBeChecked, final int maxLenOfName) {
        Objects.requireNonNull(enumToBeChecked, "REQUIRE enum to be checked");
        if (enumToBeChecked.name().length() > maxLenOfName) {
            throw new IllegalStateException(enumToBeChecked.getClass() +
                    ": Length of enum name bigger than " + maxLenOfName + ": " + enumToBeChecked);
        }
    }

}
