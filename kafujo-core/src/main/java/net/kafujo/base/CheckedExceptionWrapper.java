package net.kafujo.base;

/**
 * An Exception to wrap those annoying checked Exception.
 *
 * @author rstolle
 * @since 0.2.0
 */
@Deprecated // to be replaced with UncheckedException
public class CheckedExceptionWrapper extends KafujoException {

    public CheckedExceptionWrapper(Throwable cause) {
        super("Checked exception wrapped", cause);
    }

    public CheckedExceptionWrapper(String message, Throwable cause) {
        super(message, cause);
    }
}
