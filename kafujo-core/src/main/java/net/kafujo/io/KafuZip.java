/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.io;

import net.kafujo.base.RequirementException;
import net.kafujo.units.DataSize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Basic methods to handle zip files. All code here is based on jdk and covers only simple zipping and unzipping of
 * files and directories.  If you need to add, extract, update, remove files from a zip file, I recommend
 * net.lingala.zip4j which covers encryption as well.
 * <p>
 * Inspired by the first example in https://www.javadevjournal.com/java/zipping-and-unzipping-in-java/
 *
 * @author rstolle
 * @since 0.0.5
 */
public final class KafuZip {

    private static final Logger lgr = LoggerFactory.getLogger(KafuZip.class);

    // Its an pure utility class - not need for instances
    private KafuZip() {
    }

    /**
     * Zips in to out.
     *
     * @param in  data to be compressed
     * @param out file to be created with compressed data
     * @throws NullPointerException if in or out is null
     * @throws RequirementException if in exists or out is a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static void zipFile(final Path in, final Path out) {

        lgr.debug("About to zip file {} to {}", in.toAbsolutePath(), out.toAbsolutePath());

        KafuFile.requireNotDirectory(in);
        KafuFile.createFileOrUseEmpty(out);

        try (var outputStream = Files.newOutputStream(out);
             var zipOutputStream = new ZipOutputStream(outputStream)) {

            zipOutputStream.putNextEntry(new ZipEntry(in.getFileName().toString()));
            // zipOutputStream.write(Files.readAllBytes(in));
            Files.copy(in, zipOutputStream);
            zipOutputStream.closeEntry();

            lgr.info("zipped {} from {} to {}", in, KafuFile.sizeOfFile(in), KafuFile.sizeOfFile(out));

        } catch (IOException e) {
            throw new UncheckedIOException("IO problem while zipping file: " + in.toAbsolutePath(), e);
        }
    }

    /**
     * Zips a whole directory. Robustness and traceability are the main criteria, not speed. So it will be
     * logged to info, how many files were compressed in what ratio. In trace level, you can see every single
     * file added to the zip.
     *
     * @param in  the directory to be zipped
     * @param out the resulting zip file. If it not exists, it will be created. If it exists, it must be an
     *            {@link KafuFile#requireWritableEmptyFile(Path) empty writable file}.
     * @return the number of files zipped
     * @throws NullPointerException if in or out is null
     * @throws RequirementException if in exists or out is not a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static int zipDirectory(final Path in, final Path out) {

        lgr.debug("About to zip directory {} to {}", in.toAbsolutePath(), out.toAbsolutePath());

        KafuFile.requireDirectory(in);
        KafuFile.createFileOrUseEmpty(out); // from now on, out exists

        int fileCount = 0;
        long unzippedSize = 0;
        final long zippedSize;

        try (var outputStream = Files.newOutputStream(out);
             var zipOutputStream = new ZipOutputStream(outputStream);
             var pwalk = Files.walk(in)) {

            final var it = pwalk.filter(path -> !Files.isDirectory(path)).iterator();

            while (it.hasNext()) {
                Path current = it.next();
                unzippedSize += Files.size(current);
                fileCount++;

                if (lgr.isTraceEnabled()) {
                    lgr.trace("Adding {} to {}", current, out);
                }

                ZipEntry zipEntry = new ZipEntry(in.relativize(current).toString());
                zipOutputStream.putNextEntry(zipEntry);
                Files.copy(current, zipOutputStream);
                zipOutputStream.closeEntry();
            }
            zippedSize = Files.size(out);
        } catch (IOException ioException) {
            throw new UncheckedIOException("IO problem while zipping directory " + in.toAbsolutePath(), ioException);
        }

        lgr.info("zipped {} files from {} to {}", fileCount, DataSize.of(unzippedSize), DataSize.of(zippedSize));
        return fileCount;
    }

    public static int zip(final Path in, final Path out) {

        KafuFile.requireExists(in); // out will be checked anyway

        if (Files.isDirectory(in)) {
            return zipDirectory(in, out);
        } else {
            zipFile(in, out);
            return 1;
        }
    }

    /**
     * considers in and out as path and zips the content of in to out.
     *
     * @param in  path to file or directory to be zipped
     * @param out zip file
     * @return number of files zipped
     */
    public static int zip(final String in, final String out) {
        Objects.requireNonNull(in, "REQUIRE String in");
        Objects.requireNonNull(out, "REQUIRE String out");

        return zip(Path.of(in), Path.of(out));
    }

    public static Path zipToTempFile(final String in) {
        Objects.requireNonNull(in, "REQUIRE String in");
        return zipToTempFile(Path.of(in));
    }

    public static Path zipToTempFile(final Path in) {
        Objects.requireNonNull(in, "REQUIRE in (path to be zipped)");

        final Path out = KafuFile.createTempFile();
        zip(in, out);
        return out;
    }

}
