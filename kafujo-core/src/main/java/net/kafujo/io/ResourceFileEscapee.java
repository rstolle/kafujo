package net.kafujo.io;

import java.io.Closeable;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Deals with the ever returning challenge, to handle files bundled as resources and normal files in the file system.
 * Here you can almost seamlessly mix both. Resource Files are marked with a prefix and extracted to a temp files
 * the first time they're needed. This temp file can be used like an regular file.
 *
 * Regular files are recognized by not having the {@link #getResourcePrefix() prefix} and are accessed directly.
 * todo: Remove this: A access counter tracks how often the files were accessed.
 *
 * @author rstolle
 * @since 0.3.0
 */
public class ResourceFileEscapee implements Closeable {

    private final Map<String, Integer> accessCounter = new HashMap<>();
    private final Map<String, Path> mapper = new HashMap<>();
    private final String resourcePrefix;
    private int cleanUps = 0;

    public ResourceFileEscapee(String prefix) {
        Objects.requireNonNull(prefix, "REQUIRE prefix to mark resources");
        this.resourcePrefix = prefix;
    }

    /**
     * Deletes the escaped temporary files. This Escapee can still be used after close.
     * In most cases, it wont be possible to use try().
     */
    @Override
    public void close() {
        for (String handle : mapper.keySet()) {
            KafuFile.deleteQuietly(mapper.get(handle));
            mapper.remove(handle);
        }
        ++cleanUps;
    }

    public String getResourcePrefix() {
        return resourcePrefix;
    }

    public Map<String, Path> getMapper() {
        return Collections.unmodifiableMap(mapper);
    }

    public Map<String, Integer> getAccessCounter() {
        return Collections.unmodifiableMap(accessCounter);
    }

    public int getAccessCount() {
        AtomicInteger count = new AtomicInteger(0);
        accessCounter.keySet().forEach(handle -> count.addAndGet(accessCounter.get(handle)));
        return count.intValue();
    }

    public int getAccessCount(final Path handle) {
        Objects.requireNonNull(handle, "REQUIRE handle to fetch access count");
        return getAccessCount(handle.toString());
    }

    public int getAccessCount(final String handle) {
        Objects.requireNonNull(handle, "REQUIRE handle to fetch access count");
        Integer ret = accessCounter.get(handle);
        if (ret == null) {
            return 0;
        }
        return ret;
    }

    public long getResourceCount() {
        return accessCounter.keySet().stream().filter(handle -> handle.startsWith(resourcePrefix)).count();
    }

    public long getFileCount() {
        return accessCounter.keySet().stream().filter(handle -> !handle.startsWith(resourcePrefix)).count();
    }

    public Path toPath(final Path handle) {
        Objects.requireNonNull(handle, "REQUIRE handle to access file or resource");
        return toPath(handle.toString());
    }

    public Path toPath(final String handle) {
        Objects.requireNonNull(handle, "REQUIRE handle to access file or resource.");

        accessCounter.merge(handle, 1, Integer::sum); // this way, it works even after clean up and reloading

        if (!handle.startsWith(resourcePrefix)) {
            return Path.of(handle);
        }

        Path destination = mapper.get(handle);

        if (destination == null) {
            destination = Resources.ofCurrentThread().asTempFile(handle.substring(resourcePrefix.length()));
            mapper.put(handle, destination);
        }
        return destination;
    }

    @Override
    public String toString() {
        return String.format("%s '%s'; %d resources, %d files, %d cleanups, total access count: %d",
                getClass().getName(), resourcePrefix, getResourceCount(), getFileCount(), cleanUps, getAccessCount());
    }

    public String createStats() {
        var create = new StringBuilder(toString());

        if (getResourceCount() == 0) {
            create.append("\n").append("NO internal resources (marked with ").append(resourcePrefix).append(") used!");
        } else {
            create.append("\n").append("Resources (marked with ").append(resourcePrefix).append("):");

            accessCounter.forEach((handle, file) -> {
                if (handle.startsWith(resourcePrefix)) {
                    Path mappedTo = mapper.get(handle);
                    create.append(String.format("\n %s mapped to %s; %s",
                            handle.substring(resourcePrefix.length()), mappedTo, KafuFile.sizeOfFile(mappedTo)));
                }
            });
        }

        if (getFileCount() == 0) {
            create.append("\n").append("NO external files used!");
        } else {
            create.append("\n").append("External files:");

            accessCounter.forEach((handle, file) -> {
                if (!handle.startsWith(resourcePrefix)) {
                    create.append(String.format("\n %s %s", handle, KafuFile.sizeOfFile(handle)));
                }
            });
        }


        return create.toString();
    }
}
