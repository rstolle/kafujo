package net.kafujo.io;

import net.kafujo.config.SystemProperty;
import net.kafujo.units.KafuDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Quickly dumping data into files with decent names.
 */
public class FileDump {

    public static final Logger LGR = LoggerFactory.getLogger(FileDump.class);

    public static final FileDump DEFAULT = new FileDump(Naming.RANDOM, "", ".tmp");
    public static final FileDump KAFUJO = new FileDump(Naming.RANDOM, "kafujo_", ".tmp");
    public static final FileDump SECONDS = new FileDump(Naming.TIMESTAMP_SECONDS, "", ".tmp");
    public static final FileDump MILLIS = new FileDump(Naming.TIMESTAMP_MILLIS, "", ".tmp");

    // charset for text - maybe i make in an attribute one day
    private static final Charset CS = StandardCharsets.UTF_8;

    public enum Naming {
        RANDOM {
            @Override
            public Path createEmptyFile(Path base, String prefix, String suffix) {
                try {
                    return Files.createTempFile(base, prefix, suffix);
                } catch (IOException io) {
                    throw new UncheckedIOException("Problem creating temp file", io);
                }
            }
        },

        TIMESTAMP_SECONDS {
            @Override
            public Path createEmptyFile(Path base, String prefix, String suffix) {
                Path finalDest = base.resolve(stampSeconds(prefix, suffix));
                return KafuFile.createFile(finalDest);
            }
        },

        TIMESTAMP_MILLIS {
            @Override
            public Path createEmptyFile(Path base, String prefix, String suffix) {
                Path finalDest = base.resolve(stampMillis(prefix, suffix));
                return KafuFile.createFile(finalDest);
            }
        };

        // todo: INCREASING_NUMBER;

        public abstract Path createEmptyFile(Path base, String prefix, String suffix);
    }

    final Naming naming;
    final Path base;
    final String prefix;
    final String suffix;

    /**
     *
     * @param base the base dir, e.g. {@link SystemProperty#JAVA_IO_TMPDIR}
     * @param naming naming strategy.
     * @param prefix filename suffix as in {@link Files#createTempFile(String, String, FileAttribute[])}
     * @param suffix filename prefix as in {@link Files#createTempFile(String, String, FileAttribute[])}
     */
    public FileDump(Path base, Naming naming, String prefix, String suffix) {
        this.base = base;
        this.naming = naming;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    /**
     * Shortcut using {@link SystemProperty#JAVA_IO_TMPDIR} as base.
     * @param naming see {@link #FileDump}
     * @param prefix see {@link #FileDump}
     * @param suffix see {@link #FileDump}
     */
    public FileDump(Naming naming, String prefix, String suffix) {
        this (SystemProperty.JAVA_IO_TMPDIR.asPath(), naming, prefix, suffix);
    }


    /**
     * Creates an empty file.
     *
     * @return path to new file
     */
    public Path create() {
        Path dest = naming.createEmptyFile(base, prefix, suffix);
        if (LGR.isTraceEnabled()) {
            LGR.trace("created: " + KafuFile.sizeAndPath(dest));
        }
        return dest;
    }

    public Path create(byte[] data) {
        try {
            final Path dest = create();
            Files.write(dest, data);
            if (LGR.isDebugEnabled()) {
                LGR.debug("wrote bytes: " + KafuFile.sizeAndPath(dest));
            }
            return dest;
        } catch (IOException e) {
            throw new UncheckedIOException("Problem writing bytes", e);
        }
    }

    public Path create(CharSequence text) {
        Objects.requireNonNull(text, "REQUIRE text to write to file");
        try {
            final Path dest = create();
            Files.writeString(dest, text, CS);
            if (LGR.isDebugEnabled()) {
                LGR.debug("wrote text: " + KafuFile.sizeAndPath(dest));
            }
            return dest;
        } catch (IOException e) {
            throw new UncheckedIOException("Problem writing bytes", e);
        }
    }

    public Path create(Iterable<? extends CharSequence> lines) {
        Objects.requireNonNull(lines, "REQUIRE text lines to write to file");
        final Path dest = create();
        int count = 0;
        // pretty much the same as Files.write(lines), but counting lines
        try (var out = Files.newOutputStream(dest);
             var stream = new OutputStreamWriter(out, CS);
             var writer = new BufferedWriter(stream)) {
            for (CharSequence line : lines) {
                writer.append(line);
                writer.newLine();
                count++;
            }
        } catch (IOException e) {
            throw new UncheckedIOException("Problem writing lines at line " + count, e);
        }
        if (LGR.isInfoEnabled()) {
            LGR.info("wrote {} lines to {}", count, KafuFile.sizeAndPath(dest));
        }
        return dest;
    }

    public Path create(InputStream in) {
        Objects.requireNonNull(in, "REQUIRE stream to write to file");
        try (in) {
            final Path dest = create();
            Files.copy(in, dest, StandardCopyOption.REPLACE_EXISTING); // REPLACE, cause dest was created already
            if (LGR.isInfoEnabled()) {
                LGR.info("wrote stream to {}", KafuFile.sizeAndPath(dest));
            }
            return dest;
        } catch (IOException e) {
            throw new UncheckedIOException("Problem writing input stream", e);
        }
    }

    /**
     * Stamps the time on a filename, e.g. prefix_2020-04-23-129022.suffix
     *
     * @param format time format
     * @param prefix filename suffix as in {@link Files#createTempFile(String, String, FileAttribute[])}
     * @param suffix filename prefix as in {@link Files#createTempFile(String, String, FileAttribute[])}
     * @return ready to use filename
     */
    public static Path stamp(DateTimeFormatter format, String prefix, String suffix) {
        Objects.requireNonNull(format, "REQUIRE a time format to stamp filename with");

        if (suffix == null || suffix.isBlank()) {
            suffix = "";
        }

        String dest = LocalDateTime.now().format(format) + suffix;
        if (prefix != null && !prefix.isBlank()) {
            dest = prefix + '_' + dest;
        }
        return KafuFile.sanitizeFilename(dest);
    }

    public static Path stampSeconds(String prefix, String suffix) {
        return stamp(KafuDateTime.FILENAME_SECONDS, prefix, suffix);
    }

    public static Path stampMillis(String prefix, String suffix) {
        return stamp(KafuDateTime.FILENAME_MILLIS, prefix, suffix);
    }
}
