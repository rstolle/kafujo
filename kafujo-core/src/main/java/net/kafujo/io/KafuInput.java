/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Converting InputStreams to something more useful.
 * <b>All methods will close the InputStream when done</b>
 *
 * @author rstolle
 * @since 0.0.2
 */
public final class KafuInput {

    // Its an pure utility class - not need for instances
    private KafuInput() {
    }

    /**
     * Converts the entire data of inputStream to one String. Its basically the same as apache commons io
     * IOUtils.toString(inputStream, StandardCharsets.UTF_8), but closes the stream when done. Note, the
     * stream is considered being encoded in UTF-8.
     * <p>
     * https://stackoverflow.com/questions/309424/how-do-i-read-convert-an-inputstream-into-a-string-in-java
     *
     * @param inputStream data to convert, will be closed when done
     * @return String containing the data of inputStream
     * @throws UncheckedIOException on any IO error
     */
    public static String asString(final InputStream inputStream) {
        try (var s = new Scanner(inputStream, StandardCharsets.UTF_8).useDelimiter("\\A")) {
            return s.hasNext() ? s.next() : "";
        } // inputStream will be closed by Scanner
    }

    /**
     * Converts the data of inputStream line by line to String
     *
     * @param inputStream data to convert, will be closed when done
     * @return List containing all the lines of inputStream
     */
    public static List<String> asLines(final InputStream inputStream) {
        try (inputStream;
             var reader = new InputStreamReader(inputStream);
             var buffered = new BufferedReader(reader)) {

            final var list = new LinkedList<String>();
            String line;
            while ((line = buffered.readLine()) != null) {
                list.add(line);
            }
            return list;
        } catch (IOException e) {
            throw new UncheckedIOException("Problem converting stream to lines", e);
        }
    }

    /**
     * The whole data of {@code inputStream} goes into a byte array.
     *
     * @param inputStream data to convert, will be closed when done.
     * @return byte array containing the entire inputStream.
     */
    public static byte[] asBytes(final InputStream inputStream) {
        try (inputStream) {
             return inputStream.readAllBytes();
        } catch (IOException e) {
            throw new UncheckedIOException("Problem converting stream to bytes", e);
        }
    }

    /**
     * Shortcut to a {@link Scanner} of from {@link System#in}
     *
     * @return the next line
     */
    public static String nextLine() {
        return new Scanner(System.in).nextLine(); // closing scanner would close System.in!
    }

    /**
     * Shortcut to a {@link Scanner} of from {@link System#in}
     *
     * @return the next long
     * @throws java.util.InputMismatchException if input is not an long
     */
    public static long nextLong() {
        return new Scanner(System.in).nextLong(); // closing scanner would close System.in!
    }

    /**
     * Shortcut to a {@link Scanner} of from {@link System#in}
     *
     * @return the next int
     * @throws java.util.InputMismatchException if input is not an int
     */
    public static int nextInt() {
        return new Scanner(System.in).nextInt(); // closing scanner would close System.in!
    }

}
