/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.io;

import net.kafujo.base.RequirementException;
import net.kafujo.config.KafuLog;
import net.kafujo.config.SystemProperty;
import net.kafujo.sec.Randomize;
import net.kafujo.text.KafuText;
import net.kafujo.units.DataSize;
import net.kafujo.units.KafuDateTime;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import static net.kafujo.config.SystemProperty.*;

/**
 * Utility class concerning (temporary) files and directories based on {@link java.nio}. A main focus is to
 * ensure requirements and to wrap checked to unchecked exceptions.
 *
 * @author rstolle
 * @since 0.0.2
 */
public final class KafuFile {

    private static final Logger lgr = LoggerFactory.getLogger(KafuFile.class);

    /**
     * The Unix separator character.
     */
    public static final char UNIX_SEPARATOR = '/';

    /**
     * The Windows separator character.
     */
    public static final char WINDOWS_SEPARATOR = '\\';

    /**
     * Pattern with characters which shouldn't cause trouble in file names. Its inspired from
     * https://stackoverflow.com/a/17745189, I just added - and $. Before I used "\\W+" as
     * discussed in https://stackoverflow.com/a/1184185 suggests , but I found this too rigid.
     */

    public static final Pattern VALID_FILENAME_CHARS = Pattern.compile("[^a-zA-Z0-9._\\-$]+");

    static final String KAFUJO_TEMP_PREFIX = "kafujo_"; // for all kafujo temporary files
    private static final String KAFUJO_TEMP_SUFFIX_DEFAULT = ".tmp";

    // no need to instantiate objects (nor to create javadoc)
    private KafuFile() {
    }

    /**
     * A null safe check for the existence of a file or directory.
     *
     * @param path file or directory to be checked
     * @return true if {@code path} exists, false if not which includes {@code path==null}
     */
    public static boolean exists(final String path) {
        if (path == null) {
            return false;
        }
        return Files.exists(Path.of(path));
    }

    /**
     * Makes sure the given path exists, which could be a file or directory.
     *
     * @param path file or directory to be checked
     * @return {@code path}
     * @throws RequirementException if this path does not exist
     * @throws NullPointerException if {@code path} is null
     */
    public static Path requireExists(final Path path) {
        Objects.requireNonNull(path, "REQUIRE path");
        if (!Files.exists(path)) {
            throw new RequirementException("PATH DOES NOT EXIST: " + path);
        }
        return path;
    }

    /**
     * Converts {@code path} to {@link Path} and calls {@link #requireExists(Path)}
     *
     * @param path file or directory to be checked
     * @return {@code path} as {@link Path}
     * @throws NullPointerException if {@code path} is null
     * @throws InvalidPathException if the path string cannot be converted to a {@code Path}
     * @throws RequirementException if this path does not exist
     */
    public static Path requireExists(final String path) {
        Objects.requireNonNull(path, "REQUIRE path string");
        return requireExists(Path.of(path));
    }

    /**
     * Makes sure a path does not exist.
     *
     * @param path file or directory to be checked
     * @return {@code path}
     * @throws NullPointerException if {@code path} is null
     * @throws RequirementException if this destination exists in any form. I would have loved to use
     *                              {@link java.nio.file.FileAlreadyExistsException}, but its a checked exception.
     */
    public static Path requireNotExists(final Path path) {
        Objects.requireNonNull(path, "REQUIRE path");
        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                throw new RequirementException("EXISTS AS DIRECTORY: " + path);
            } else {
                throw new RequirementException("EXISTS AS FILE: " + path);
            }
        }
        return path;
    }

    /**
     * Converts {@code path} to {@link Path} and calls {@link #requireNotExists(Path)}
     *
     * @param path file or directory to be checked
     * @return {@code path} as {@link Path}
     * @throws NullPointerException if {@code path} is null
     * @throws InvalidPathException if the path string cannot be converted to {@code Path}
     * @throws RequirementException if this path exists in any form. I would have loved to use
     *                              {@link java.nio.file.FileAlreadyExistsException}, but its a checked exception.
     */
    public static Path requireNotExists(final String path) {
        Objects.requireNonNull(path, "REQUIRE path string");
        return requireNotExists(Path.of(path));
    }

    /**
     * Makes sure a file or directory {@link #requireExists exists} and is readable.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws RequirementException if this destination is not readable or does not exist
     * @throws NullPointerException if path is null
     */
    public static Path requireReadable(final Path path) {
        requireExists(path);
        if (!Files.isReadable(path)) {
            throw new RequirementException("PATH NOT READABLE: " + path);
        }
        return path;
    }

    /**
     * Makes sure a path {@link #requireExists exists} and points to a directory.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws NullPointerException if path is null
     * @throws RequirementException if this destination is not a directory or does not exist
     */
    public static Path requireDirectory(final Path path) {
        requireExists(path);
        if (!Files.isDirectory(path)) {
            throw new RequirementException("IS NOT A DIRECTORY: " + path);
        }
        return path;
    }

    /**
     * Makes sure a path {@link #requireExists exists} and points NOT to a directory.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws RequirementException if this destination is a directory or does not exist
     * @throws NullPointerException if path is null
     */
    public static Path requireNotDirectory(final Path path) {
        requireExists(path);
        if (Files.isDirectory(path)) {
            throw new RequirementException("IS A DIRECTORY: " + path);
        }
        return path;
    }

    /**
     * Makes sure a path {@link #requireExists exists} and points NOT to a directory.
     *
     * @param pathStr path to be checked as String
     * @return checked {@link Path}
     * @throws RequirementException if this destination is a directory or does not exist
     * @throws NullPointerException if path is null
     */
    public static Path requireNotDirectory(final String pathStr) {
        final Path path = Path.of(pathStr);
        requireExists(path);
        if (Files.isDirectory(path)) {
            throw new RequirementException("IS A DIRECTORY: " + path);
        }
        return path;
    }

    /**
     * Makes sure a path {@link #requireExists exists} and points to an executable file or directory.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws RequirementException if this destination is not executable or does not exist
     * @throws NullPointerException if path is null
     */
    public static Path requireExecutable(final Path path) {
        requireExists(path);
        if (!Files.isExecutable(path)) {
            throw new RequirementException("IS NOT EXECUTABLE: " + path);
        }
        return path;
    }

    /**
     * Makes sure a path {@link #requireExists exists} and points to an writeable file or directory.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws RequirementException if this destination is not writeable or does not exist
     * @throws NullPointerException if path is null
     */
    public static Path requireWritable(final Path path) {
        requireExists(path);
        if (!Files.isWritable(path)) {
            throw new RequirementException("IS NOT WRITABLE: " + path);
        }
        return path;
    }

    /**
     * Makes sure path is a writable, empty file. Usecase: you pass path created with
     * {@link Files#createTempFile(String, String, FileAttribute[])} to a method which is supposed to write data
     * in this file.
     *
     * @param path path to be checked
     * @return {@code path}
     * @throws NullPointerException if path is null
     * @throws RequirementException if path is a directory or not writable or not empty
     * @since 0.0.5
     */
    public static Path requireWritableEmptyFile(final Path path) {
        requireNotDirectory(path);
        requireWritable(path);

        final DataSize size = sizeOfFile(path);
        if (!size.isZero()) {
            throw new RequirementException("IS NOT EMPTY: " + path + "; SIZE=" + size);
        }
        return path;
    }

    /**
     * Makes sure, the given path is an empty directory.
     *
     * @param directory path to be checked
     * @return the same path
     * @throws RequirementException if {@code directory} is not a writable directory.
     * @throws UncheckedIOException on any IO problem
     */
    public static Path requireWritableEmptyDirectory(final Path directory) {
        requireDirectory(directory);
        requireWritable(directory);

        // in case its empty, this will be fast
        final long count = countRootEntries(directory);

        // in case its not empty, this information might help
        if (count != 0) {
            throw new RequirementException("DIRECTORY NOT EMPTY: " + directory + "; CONTAINS " + count + " ENTRIES");
        }
        return directory;
    }

    /**
     * Checks {@link #requireWritableEmptyFile(Path) empty writable file} for an existing file or creates a new one.
     * This is useful when you need to write to files which might be named
     *
     * @param path file to be used or newly created
     * @return the same path
     */
    public static Path createFileOrUseEmpty(final Path path) {
        if (Files.exists(path)) {
            requireWritableEmptyFile(path);
            lgr.debug("going to use EXISTING, empty file: {}", path);
        } else {
            createFile(path);
            lgr.debug("created NEW file: {}", path);
        }

        return path;
    }

    /**
     * Creates an empty file using {@link Files#createFile}.
     *
     * @param file file to be created
     * @return path to the newly created file. Its an empty file which now exists and therefore needs to be overwritten.
     * @throws NullPointerException if path is null
     * @throws RequirementException if {@code file} exists (as file or directory or whatever). I would have loved to use
     *                              {@link java.nio.file.FileAlreadyExistsException}, but its a checked exception.
     * @throws UncheckedIOException on any IO problem
     */
    public static Path createFile(final Path file) {
        requireNotExists(file);
        try {
            return Files.createFile(file);
        } catch (IOException io) {
            throw new UncheckedIOException("Problem creating file", io);
        }
    }

    /**
     * Writes all bytes of {@code content} to {@code file}, which must not exist yet.
     *
     * @param content the data to be written
     * @param file    the destination file
     * @return amount of data written to {@code destination}
     * @throws UncheckedIOException on any IO problem
     * @throws RequirementException if {@code file} exists already.
     */
    public static DataSize createFile(final Path file, final byte[] content) {
        Objects.requireNonNull(content, "REQUIRE content");
        KafuFile.requireNotExists(file);

        try {
            Files.write(file, content);
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
        return DataSize.of(content.length);
    }


    /**
     * Creates an empty temp file using {@link Files#createTempFile}.
     *
     * @return path to the newly created file. Its an empty file which now exists and therefore needs to be overwritten.
     * @throws UncheckedIOException on any IO problem
     */
    public static Path createTempFile() {
        try {
            return Files.createTempFile(KAFUJO_TEMP_PREFIX, KAFUJO_TEMP_SUFFIX_DEFAULT);
        } catch (IOException io) {
            throw new UncheckedIOException("Problem creating temp file", io);
        }
    }

    /**
     * Copies the entire content from inputStream to a newly created temporary file.
     *
     * @param inputStream stream to write to file, will be closed
     * @return path to the new file
     */
    public static Path createTempFile(final InputStream inputStream) {
        return FileDump.KAFUJO.create(inputStream);
    }

    /**
     * Creates a temp file containing data.
     *
     * @param text text data to be written
     * @return path to the newly created tmp file containing content
     */
    public static Path createTempFile(final CharSequence text) {
        return FileDump.KAFUJO.create(text);
    }

    /**
     * Creates a temp file containing text data.
     *
     * @param content text lines to be written
     * @return path to the newly created tmp file containing content
     */
    public static Path createTempFile(final Iterable<? extends CharSequence> content) {
        return FileDump.KAFUJO.create(content);
    }

    /**
     * Creates a temp file containing binary data.
     *
     * @param bytes data to write to a tmp file
     * @return the path to the newly created (temp) file
     */
    public static Path createTempFile(final byte[] bytes) {
        return FileDump.KAFUJO.create(bytes);
    }

    /**
     * Simple wrapper for {@link Files#createTempDirectory(String, FileAttribute[])}
     *
     * @return path to a new temp directory
     * @throws UncheckedIOException on fail
     */
    public static Path createTempDirectory() {
        try {
            return Files.createTempDirectory(KAFUJO_TEMP_PREFIX);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Tries to delete a file, but wont complain if this fails. Success or fail will be info-level logged to the
     * {@link org.slf4j.Logger Kafujo logger} and returned as a boolean. In most cases, the user might not care.
     *
     * @param path the file to be deleted. Can be anything but a directory!
     * @return true, if path was successfully deleted, false otherwise
     * @throws RequirementException if path points to a directory. If you don't care what to delete, call {@link #deleteQuietly(Path)}
     */
    public static boolean deleteFileQuietly(Path path) {

        path = path.toAbsolutePath();

        if (!Files.exists(path)) {
            lgr.info("quietly did nothing, path didn't exist {}", path);
            return false;
        }

        requireNotDirectory(path);

        try {
            Files.delete(path);
            lgr.info("quietly deleted {}", path);
            return true;
        } catch (final RuntimeException | IOException ioAndRt) {
            if (lgr.isDebugEnabled()) {
                lgr.debug("QUIETLY FAILED TO DELETE FILE " + path, ioAndRt);
            } else {
                lgr.info("QUIETLY FAILED TO DELETE FILE {} cause {}", path, ioAndRt.toString());
            }
            return false;
        }
    }

    public static boolean deleteQuietly(final Path... paths) {
        int failCount = 0;
        for (Path path : paths) {
            if (!deleteQuietly(path)) {
                failCount++;
            }
        }
        return failCount == 0;
    }

    public static boolean deleteFileQuietly(final String path) {
        Objects.requireNonNull(path, "REQUIRE path");
        return deleteFileQuietly(Path.of(path));
    }


    /**
     * Recursively deletes a directory.
     * <p>
     * This is from https://stackoverflow.com/questions/779519/delete-directories-recursively-in-java
     *
     * @param dir directory to be deleted
     * @return the number of files deleted including the root dir (empty dir == 1)
     * @throws NullPointerException  if dir is null
     * @throws IllegalStateException if dir is not a path
     * @throws UncheckedIOException  on any IO error.
     */
    public static int deleteDirectory(final Path dir) {
        requireDirectory(dir);

        final var visitor = new SimpleFileVisitor<Path>() {

            int count = 0;

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                count++;
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                count++;
                return FileVisitResult.CONTINUE;
            }
        };


        try {
            Files.walkFileTree(dir, visitor);
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
        return visitor.count;
    }

    public static boolean deleteDirectoryQuietly(Path path) {

        path = path.toAbsolutePath();

        if (!Files.exists(path)) {
            lgr.info("quietly did nothing, path didn't exist {}", path);
            return false;
        }

        requireDirectory(path);

        try {
            int count = deleteDirectory(path);
            lgr.info("quietly deleted {} files from {}", count, path);
            return true;
        } catch (final RuntimeException anyRt) {
            KafuLog.logThrowableOnDebug(lgr, "QUIETLY FAILED TO DELETE DIRECTORY " + path, anyRt);
        }
        return false;
    }

    public static boolean deleteDirectoryQuietly(final String path) {
        Objects.requireNonNull(path, "REQUIRE path");
        return deleteDirectoryQuietly(Path.of(path));
    }


    /**
     * Quietly deletes a file or directory. Beside the return value, there will be no feedback on fail.
     * However fail and success of this operation will be logged in info level and more detailed in debug level.
     *
     * @param path points to a file or directory
     * @return true, if {@code path} could be deleted, false otherwise.
     * @throws NullPointerException if {@code path} is null.
     */
    public static boolean deleteQuietly(final Path path) {
        Objects.requireNonNull(path, "REQUIRES: path to be deleted");
        if (!Files.exists(path)) {
            lgr.info("quietly did nothing, path didn't exist {}", path.toAbsolutePath());
            return false;
        }

        if (Files.isDirectory(path)) {
            return deleteDirectoryQuietly(path);
        } else {
            return deleteFileQuietly(path);
        }
    }

    public static int deleteQuietly(final List<Path> paths) {
        Objects.requireNonNull(paths, "REQUIRES: paths to be deleted");

        int count = 0;
        for (Path path : paths) {
            if (deleteQuietly(path)) {
                count++;
            }
        }
        return count;
    }


    /**
     * Shortcut to call {@link #deleteQuietly(Path)}
     *
     * @param path points to a file or directory
     * @return true, if {@code path} could be deleted, false otherwise.
     * @throws NullPointerException if {@code path} is null.
     * @see #deleteQuietly(Path)
     */
    public static boolean deleteQuietly(final String path) {
        Objects.requireNonNull(path, "REQUIRE path");
        return deleteFileQuietly(Path.of(path));
    }

    /**
     * Gets the path to the {@link SystemProperty#JAVA_IO_TMPDIR system temporary directory} and checks:
     * <ol>
     * <li>if it really exists</li>
     * <li>if it is a directory</li>
     * <li>if it is executable</li>
     * <li>if it is writable</li>
     * </ol>
     *
     * @return absolute path to the system temporary directory.
     * @throws NullPointerException if {@link SystemProperty#JAVA_IO_TMPDIR} is null
     * @throws RequirementException if {@link SystemProperty#JAVA_IO_TMPDIR} does not exist as a system property or does not point
     *                              to a existing writeable directory
     * @since 0.0.3
     */
    public static Path getTempDir() {
        // todo: do this all only once which would be a nice system check at startup time.
        final Path tmp = JAVA_IO_TMPDIR.asPath().toAbsolutePath();
        KafuFile.requireDirectory(tmp);
        KafuFile.requireExecutable(tmp);
        KafuFile.requireWritable(tmp);
        return tmp;
    }

    /**
     * Returns the path to the user home directory according to {@link SystemProperty#USER_HOME}.
     *
     * @return absolute path to the user home directory.
     * @throws RequirementException if {@link SystemProperty#USER_HOME} does not exist or is not a directory.
     * @throws NullPointerException if {@link SystemProperty#USER_HOME} is null
     * @since 0.0.3
     */
    public static Path getUserHome() {
        final Path home = USER_HOME.asPath().toAbsolutePath();
        KafuFile.requireDirectory(home);
        return home;
    }

    /**
     * Returns the path to the working directory according to {@link SystemProperty#USER_DIR}. There is a lot of
     * discussion about this (e.g. https://stackoverflow.com/q/4871051),  so I am not sure to use it at all. Plus,
     * System Properties can be changed, so what when some evil lib changes {@link SystemProperty#USER_DIR} like
     * System.setProperty("user.dir", trouble) (see test!).
     * For this reason I created {@link #getWorkingDir()}, which doesn't have the same problem.
     *
     * @return absolute path to the working directory.
     * @throws RequirementException if {@link SystemProperty#USER_DIR} does not exist or is not a directory.
     * @throws NullPointerException if {@link SystemProperty#USER_DIR} is null
     * @since 0.3.0
     */
    public static Path getUserDir() {
        final Path home = USER_DIR.asPath().toAbsolutePath();
        KafuFile.requireDirectory(home);
        return home;
    }

    /**
     * Returns the path to the working derived from Path.of("."). This is apparently not depending on
     * {@link SystemProperty#USER_DIR} (See tests)
     *
     * @return absolute, normalized path to the working directory.
     * @throws RequirementException if "." does not exist or is not a directory.
     * @since 0.3.0
     */
    public static Path getWorkingDir() {
        Path working = Path.of(".").toAbsolutePath().normalize();
        KafuFile.requireDirectory(working);
        return working;
    }

    /**
     * Gets the {@link FileStore} of the given file.
     * <p>
     * I could not find out, which would be the most reliable way for this. There are three options
     * <ul>
     * <li>using the path as it is</li>
     * <li>using {@link Path#toAbsolutePath()}</li>
     * <li>using {@link Path#toRealPath(LinkOption...)} </li>
     * </ul>
     * This method is using the last, kind of inspired by
     * https://stackoverflow.com/questions/19465889/how-to-use-file-getfreespace-with-any-file-to-get-the-free-space-in-java
     * todo: find a definitive answer
     *
     * @param path path to be queried
     * @return the hopefully correct FileStore
     * @throws NullPointerException if path is null
     * @throws RequirementException if the path does not exist
     * @throws UncheckedIOException on any IO problem
     */
    public static FileStore getFileStore(final Path path) {
        requireExists(path);
        try {
            return Files.getFileStore(path.toRealPath());
        } catch (IOException io) {
            throw new UncheckedIOException("KafuFile.getFileSore failed: " + io.getMessage(), io);
        }
    }

    /**
     * Determines the {@link FileStore} of the given path and calls {@link FileStore#getUsableSpace()}. It doesn't
     * matter what kind of file path is.
     *
     * @param path path into the volume of interest
     * @return {@link FileStore#getUsableSpace() usable space} of the {@link Path#toRealPath(LinkOption...) real path}
     * @throws NullPointerException if {@code path} is null
     * @throws RequirementException if {@code path} doesn't exist
     * @throws UncheckedIOException an any IO problems
     */
    public static DataSize getUsableSpace(final Path path) {
        try {
            final long bytes = KafuFile.getFileStore(path).getUsableSpace();
            return DataSize.of(bytes);
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
    }

    /**
     * String version of {@link #getUsableSpace(Path)}.
     *
     * @param path to be considered as a path for {@link #getUsableSpace(Path)}
     * @return {@link #getUsableSpace(Path)}
     */
    public static DataSize getUsableSpace(final String path) {
        return getUsableSpace(Path.of(path));
    }

    /**
     * Calls {@link #getUsableSpace(Path)} on an existing path returns a default value, e.g.:
     * {@code
     * print ("/opt/ :" +  getUsableSpace("/opt", "does not exist")
     * }
     *
     * @param path path to be checked
     * @param def  default value, will be returned if path does not exist
     * @return a {@link net.kafujo.units.DataSize} formatted String or defaultValue if path does not exist
     * @throws NullPointerException if path is null
     */
    public static String getUsableSpace(final Path path, final String def) {
        Objects.requireNonNull(path, "REQUIRE path");
        if (!Files.exists(path)) {
            return def;
        } else {
            return getUsableSpace(path).toString();
        }
    }

    /**
     * inspired by https://stackoverflow.com/questions/5930087/how-to-check-if-a-directory-is-empty-in-java
     *
     * @param directory path to be analyzed
     * @return true if {@code directory} is an empty directory, false if its an non-empty directory.
     * @throws RequirementException if {@code directory} is not a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static boolean isEmptyDirectory(final Path directory) {
        requireDirectory(directory);
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            return !dirStream.iterator().hasNext();
        } catch (IOException io) {
            throw new UncheckedIOException("Problem checking directory for emptiness", io);
        }
        //  Files.list(dir).findAny().isPresent() would not be more efficient, cause it works pretty much the same
    }

    /**
     * Convenient wrapper for {@link #isEmptyDirectory(Path)}
     *
     * @param directory path to be analyzed
     * @return true if {@code directory} is an empty directory, false if its an non-empty directory.
     * @throws RequirementException if {@code directory} is not a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static boolean isEmptyDirectory(final String directory) {
        Objects.requireNonNull(directory, "REQUIRE directory as String");
        return isEmptyDirectory(Path.of(directory));
    }

    public static boolean isEmptyFile(final Path file) {
        return sizeOfFile(file).equals(DataSize.ZERO);
    }

    public static boolean isEmptyFile(final String file) {
        Objects.requireNonNull(file, "REQUIRE file as String");
        return isEmptyFile(Path.of(file));
    }


    public static boolean isEmpty(final Path fileOrDirectory) {
        Objects.requireNonNull(fileOrDirectory, "REQUIRE file or directory");
        if (Files.isDirectory(fileOrDirectory)) {
            return isEmptyDirectory(fileOrDirectory);
        } else {
            return isEmptyFile(fileOrDirectory);
        }
    }

    public static boolean isEmpty(final String fileOrDirectory) {
        Objects.requireNonNull(fileOrDirectory, "REQUIRE file or directory as String");
        return isEmpty(Path.of(fileOrDirectory));
    }


    /**
     * Counts the root entries of a directory. If you need all entries, use {@link #countAllEntries(Path) this}.
     *
     * @param directory the directory to be analyzed
     * @return the number of entries (files, links, directories) within {@code directory}
     * @throws RequirementException if {@code directory} is not a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static long countRootEntries(final Path directory) {
        requireDirectory(directory);
        try {
            return Files.list(directory).count();
        } catch (IOException io) {
            throw new UncheckedIOException("Problem counting elements in directory", io);
        }
    }

    /**
     * Counts recursively all entries of a directory.
     *
     * @param directory the directory to be analyzed
     * @return the number of files within {@code directory}
     * @throws RequirementException if {@code directory} is not a directory
     * @throws UncheckedIOException on any IO problem
     */
    public static long countAllEntries(final Path directory) {

        requireDirectory(directory);

        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            long count = 0;
            for (Path path : dirStream) {
                if (Files.isDirectory(path)) {
                    count += countAllEntries(path);
                } else {
                    count++;
                }
            }
            return count;
        } catch (IOException io) {
            throw new UncheckedIOException("Problem counting elements in directory", io);
        }
    }

    /**
     * @param directory path to be analyzed.
     * @return the sum of all file sizes in {@code directory}.
     * @throws RequirementException if {@code directory} is not a directory.
     * @throws UncheckedIOException on any IO problem
     */
    public static long sizeOfDirectoryBytes(final Path directory) {
        Objects.requireNonNull(directory, "REQUIRE directory");
        requireDirectory(directory);

        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            long size = 0;
            for (Path fileOrDirectory : dirStream) {
                if (Files.isDirectory(fileOrDirectory)) {
                    size += sizeOfDirectoryBytes(fileOrDirectory);
                } else {
                    size += Files.size(fileOrDirectory);
                }
            }
            return size;
        } catch (IOException io) {
            throw new UncheckedIOException("Problem traversing elements in directory", io);
        }
    }

    /**
     * Packs the result of {@link #sizeOfDirectoryBytes(Path)} in a {@link DataSize}
     *
     * @param directory path to be analyzed.
     * @return the combined size of all files {@code direcotry}.
     * @throws RequirementException if {@code direcotry} is not a directory.
     * @throws UncheckedIOException on any IO problem
     */
    public static DataSize sizeOfDirectory(final Path directory) {
        return DataSize.of(sizeOfDirectoryBytes(directory));
    }

    /**
     * Determines the size of the given path which can be a single file or a directory. In the second case,
     * {@link #sizeOfDirectory(Path)} will be called.
     *
     * @param path file or a directory
     * @return sum of the size of all files
     */
    public static DataSize size(final Path path) {
        if (Files.isDirectory(path)) {
            return sizeOfDirectory(path);
        } else {
            return sizeOfFile(path);
        }
    }

    /**
     * Retrieves the size of a given file.
     *
     * @param file {@link Path} to the file to be
     * @return size of file
     * @throws RequirementException if path does not exist or is a directory
     * @throws UncheckedIOException if io fails
     */
    public static DataSize sizeOfFile(final Path file) {
        requireNotDirectory(file);
        try {
            return DataSize.of(Files.size(file));
        } catch (IOException io) {
            throw new UncheckedIOException("IO problem fetching size of file " + file.toAbsolutePath(), io);
        }
    }

    /**
     * Wrapper for {@link #sizeOfFile(Path)}.
     *
     * @param file path to the file in question to be interpreted as {@link Path}
     * @return size of file
     * @throws RequirementException if {@code file} does not exist or is a directory
     * @throws UncheckedIOException if io fails
     */
    public static DataSize sizeOfFile(final String file) {
        Objects.requireNonNull(file, "REQUIRE file to be used as path");
        return sizeOfFile(Path.of(file));
    }

    /**
     * Same as {@link #sizeOfDirectory(Path)} but quitely ignores io problems. These will be logged
     * {@link KafuLog#logThrowableOnDebug(Logger, String, Throwable) depending on log level}
     * <p>
     * The code is inspiered by https://stackoverflow.com/a/19877372.
     * und achtung. für sia3-code.zip sagt Eigenschaften: 991kB (genau wie diese funktion)
     * aber Größe auf Datentrager 2.8 MB ! du -s liefert im selben fall 2.3 MB !?!
     * Mhm, da gibt es sicher noch Disskussionen
     *
     * @param fileOrDirectory {@link Path} to the directory to be checked
     * @return summarized size of all files within {@code path}
     * @throws RequirementException if {@code fileOrDirectory} does not exist at all
     */
    public static DataSize sizeQuietly(final Path fileOrDirectory) {
        KafuFile.requireExists(fileOrDirectory);

        final AtomicLong size = new AtomicLong(0);

        try {
            Files.walkFileTree(fileOrDirectory, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
                    size.addAndGet(attrs.size());
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(final Path file, final IOException exc) {
                    KafuLog.logThrowableOnDebug(lgr, "QUIETLY SKIPPED PATH", exc);
                    return FileVisitResult.CONTINUE; // Skip folders that can't be traversed
                }

                @Override
                public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) {
                    if (exc != null) {
                        KafuLog.logThrowableOnDebug(lgr, "QUIETLY HAVING TROUBLE TRAVERSING", exc);
                    }
                    return FileVisitResult.CONTINUE;  // Ignore errors traversing a folder
                }
            });
        } catch (final IOException e) {
            throw new AssertionError("walkFileTree will not throw IOException if the FileVisitor does not");
        }

        return DataSize.of(size.get());
    }

    public static DataSize sizeQuietly(final String fileOrDirectory) {
        Objects.requireNonNull(fileOrDirectory, "REQUIRE fileOrDirectory to be used as path");
        return sizeQuietly(Path.of(fileOrDirectory));
    }


    public static Map<FileStore, DataSize> getUseableSpace(boolean silent) {

        Map<FileStore, DataSize> map = new HashMap<>();
        try {
            for (FileStore store : FileSystems.getDefault().getFileStores()) {
                map.put(store, DataSize.of(store.getUsableSpace()));
            }
        } catch (IOException io) {
            if (!silent) {
                throw new UncheckedIOException(io);
            }
        }
        return map;
    }


    public static String getFileStoreStats(boolean silent) {

        StringBuilder sb = new StringBuilder(String.format("%-25s %-10s %10s %12s %10s%n",
                "filestore", "type", "total", "useable", "used"
        ));
        try {
            for (FileStore store : FileSystems.getDefault().getFileStores()) {
                long total = store.getTotalSpace();
                long usable = store.getUsableSpace();

                double used = 100;

                if (total != 0) {
                    used = ((total - usable) * 100.0) / total;
                }

                sb.append(String.format("%-25s %-10s %10s %12s   %8.2f%% %s%n",
                        store, store.type(),
                        DataSize.toString(total),
                        DataSize.toString(usable),
                        used, store.isReadOnly() ? "READONLY" : ""
                        )
                );
            }
        } catch (IOException io) {
            if (silent) {
                sb.append("problem fetching file store information").append(io);
            } else {
                throw new UncheckedIOException(io);
            }
        }
        return sb.toString();
    }

    public static Path createDirectories(final String dir) {
        return createDirectories(Path.of(dir));
    }

    /**
     * An {@link UncheckedIOException} version of {@link Files#createDirectories(Path, FileAttribute[])}.
     * <p>
     * If {@code dir} exists, it will be checked to be a directory.
     *
     * @param dir the directory to be created
     * @return {@code dir}
     * @throws RequirementException if {@code dir} exists as a non directory. In this case, {@link Files#createDirectories(Path, FileAttribute[])}
     *                              would throw a {@link FileAlreadyExistsException}
     */
    public static Path createDirectories(final Path dir) {
        if (!Files.exists(dir)) {
            try {
                return Files.createDirectories(dir);
            } catch (final IOException io) {
                throw new UncheckedIOException(io);
            }
        }
        return requireDirectory(dir);
    }

    /**
     * Cleans a String from problematic letters when used as a filename. Its inspired from
     * https://stackoverflow.com/a/17745189
     * <p>
     * todo examples
     *
     * @param rawName String to be used as filename
     * @return a Path created of non problematic letters
     */
    public static Path sanitizeFilename(final CharSequence rawName) {
        Objects.requireNonNull(rawName, "REQUIRE filename");
        String out = rawName.toString().strip();

        if (out.isEmpty()) {
            throw new IllegalArgumentException("cannot use blank string to form a sane filename");
        }

        if (out.startsWith("-")) { // make sure, filename will not be used as argument
            out = out.replaceFirst("-", "_");
        }

        out = KafuText.replaceUmlauts(out);
        out = VALID_FILENAME_CHARS.matcher(out).replaceAll("_");
        return Path.of(out);
    }

    /**
     * Creates a safe file name out of string fragments. Each fragment will be stripped to create compact namws. Blank
     * fragments will be ignored.
     *
     * @param connector         string between the fragments. Use "" if you dont want.
     * @param filenameFragments fragments to build the filename with
     * @return a sane filename containing the given fragments
     * @throws IllegalArgumentException if all fragments are blank;
     */
    public static Path sanitizeFilename(final CharSequence connector, final CharSequence... filenameFragments) {
        Objects.requireNonNull(filenameFragments, "REQUIRE connector. Use emypty String, if you don't need one");
        Objects.requireNonNull(filenameFragments, "REQUIRE filename");

        if (StringUtils.isAllBlank(filenameFragments)) {
            throw new IllegalArgumentException("All filename fragments are blank - would result in empty file name");
        }

        if (VALID_FILENAME_CHARS.matcher(connector).find()) {
            throw new IllegalArgumentException("Connector contains invalid chars '" + connector + "'; regex rule: " + VALID_FILENAME_CHARS);
        }

        final var combinedFragments = new StringBuilder();
        int countNonBlanks = 0;

        for (int idx = 0; idx < filenameFragments.length; idx++) {
            String fragment = filenameFragments[idx].toString().strip();

            if (fragment.isBlank()) {
                continue;
            }
            countNonBlanks++;

            if (idx == filenameFragments.length - 1 && fragment.startsWith(".")) {
                combinedFragments.append(fragment);
                continue;
            }

            if (countNonBlanks > 1) {
                combinedFragments.append(connector);
            }
            combinedFragments.append(fragment);
        }

        return sanitizeFilename(combinedFragments);
    }

    /**
     * Adds a {@link KafuDateTime#FILENAME_SECONDS} timestamp ahead of a filename and makes sure, the
     * resulting String is {@link #sanitizeFilename(CharSequence) safe to use as a filename}.
     * Deprecated: use {@link FileDump#stampSeconds(String, String)}
     * @param rawName filename to be complemented
     * @param zone    zone to create the timestamp
     * @return yyyy-MM-dd-HHmmss-filename
     */
    @Deprecated
    public static Path timestampFilename(final String rawName, final ZoneId zone) {
        Objects.requireNonNull(rawName, "REQUIRE filename");
        String output = rawName.strip();

        if (output.length() == 0) {
            throw new IllegalArgumentException("cannot use an empty filename");
        }

        output = LocalDateTime.now(zone).format(KafuDateTime.FILENAME_SECONDS) + "-" + rawName;
        return sanitizeFilename(output);
    }

    /**
     * Calls {@link #timestampFilename(String, ZoneId)} with {@link ZoneId#systemDefault()}.
     *
     * @param rawName filename to be complemented
     * @return yyyy-MM-dd-HHmmss-filename
     */
    @Deprecated
    public static Path timestampFilename(final String rawName) {
        return timestampFilename(rawName, ZoneId.systemDefault());
    }


    /**
     * Compares two files for binary equality.
     * Inspired by https://stackoverflow.com/a/27379566
     *
     * @param file1 first file
     * @param file2 file to be compared
     * @return true, if both files are the same byte by byte
     */
    public static boolean equalContent(final Path file1, final Path file2) {
        requireNotDirectory(file1);
        requireNotDirectory(file2);

        if (file1.equals(file2)) {  // speeds up massively when file1==file2==BIG
            return true;  // todo: better throw exception ?
        }

        try {
            final long size1 = Files.size(file1);

            if (size1 != Files.size(file2)) {
                return false;
            }

            if (size1 < 2048) {
                return Arrays.equals(Files.readAllBytes(file1), Files.readAllBytes(file2));
            }

        } catch (IOException ioTrouble) {
            throw new UncheckedIOException("trouble comparing small files", ioTrouble);
        }

        try (InputStream is1 = Files.newInputStream(file1);
             InputStream is2 = Files.newInputStream(file2)) {
            // Compare byte-by-byte.
            // Note that this can be sped up drastically by reading large chunks
            // (e.g. 16 KBs) but care must be taken as InputStream.read(byte[])
            // does not neccessarily read a whole array!
            int data;
            while ((data = is1.read()) != -1) {
                if (data != is2.read()) {
                    return false;
                }
            }
        } catch (IOException ioTrouble) {
            throw new UncheckedIOException("trouble comparing input streams", ioTrouble);
        }

        return true;
    }


    /**
     * Creates a file with random data.
     *
     * @param dataSize size of the file to be created
     * @return path to the newly created file
     */
    public static Path createRandomBinaryTempFile(final DataSize dataSize) {
        Objects.requireNonNull(dataSize, "REQUIRE size of random binary file");

        // todo: after refactoring DataSize:
//        if (dataSize.isBiggerThan(DataSize.of(Long.MAX_VALUE))) {
//            throw new IllegalArgumentException("Cannot create random binary file bigger than ")
//        }

        final long bytes = dataSize.longValue();

        if (bytes < 1) {
            throw new IllegalArgumentException("data size must be at least one byte");
        }

        final int chunkSize = Randomize.JUL.nextInt(1984, 13742);
        final byte[] chunk = new byte[chunkSize];
        long bytesWritten = 0;

        final Path destination = createTempFile();

        try (var os = Files.newOutputStream(destination)) {
            do {
                final long missing = bytes - bytesWritten;

                if (missing >= chunkSize) {
                    os.write(Randomize.JUL.nextBytes(chunk));
                    bytesWritten += chunkSize;
                } else {
                    os.write(Randomize.JUL.bytes((int) missing)); // missing is max chunkSize
                    bytesWritten += missing;
                }
            } while (bytesWritten != bytes);

        } catch (IOException ioTrouble) {
            throw new UncheckedIOException("Problem writing random temp file", ioTrouble);
        }

        return destination;
    }

    /**
     * Creates a String with filename and {@link #size(Path) size}. Uses only the filename, not the whole path.
     * Use {@link #sizeAndPath(Path)} (Path)} if you want it more detailed.
     *
     * @param path path to be informed about
     * @return something like "hello.png. (70 Byte)"
     */
    public static String sizeAndName(Path path) {
        Objects.requireNonNull(path, "REQUIRE path");
        return path.getFileName() + " (" + size(path) + ')';
    }

    /**
     * Creates a String with {@link Path#toAbsolutePath()} absolute} and {@link Path#normalize()} normalized}
     * filename and {@link #size(Path) size}. Use {@link #sizeAndName(Path)} if you want it more comppact.
     *
     * @param path path to be informed about
     * @return something like "hello.png. (70 Byte)"
     */
    public static String sizeAndPath(Path path) {
        Objects.requireNonNull(path, "REQUIRE path");
        return path.toAbsolutePath().normalize() + " (" + size(path) + ')';
    }

    /**
     * Check whether {@code path} is an abslolute dos path starting with  c: or D:\
     *
     * @param path path to checked
     * @return true, if absolute dos path
     */
    public static boolean isAbsoluteDosPath(String path) {
        Objects.requireNonNull(path, "REQUIRE path to be checked");
        if (path.length() < 2) {
            return false; // most likely not a valid path anyway
        }

        return path.substring(0, 2).matches("[a-zA-Z]:");
    }

    /**
     * Warpper for {@link #separatorsToUnix(String)}.
     *
     * @param path path to be transformed
     * @return transformed path
     */
    public static String separatorsToUnix(Path path) {
        Objects.requireNonNull(path, "REQUIRE path to transform to unix style");
        return separatorsToUnix(path.toString());
    }

    /**
     * Pretty much like apache commons FilenameUtils.separatorsToUnix(), but wont accept
     * {@link #isAbsoluteDosPath(String) abslute} dos paths.
     *
     * @param path a path, dos or unix style (e.g. "windows\help\me")
     * @return path in unix style (e.g. "windows/help/me")
     * @throws NullPointerException     if {@code path} is null
     * @throws IllegalArgumentException if {@code path} is an {@link #isAbsoluteDosPath(String) abslute} dos path.
     */
    public static String separatorsToUnix(final String path) {
        Objects.requireNonNull(path, "REQUIRE path to transform to unix style");

        if (path.indexOf(WINDOWS_SEPARATOR) == -1) {
            return path;
        }

        if (isAbsoluteDosPath(path)) {
            throw new IllegalArgumentException("cannot convert absolute dos path to unix style path");
        }

        return path.replace(WINDOWS_SEPARATOR, UNIX_SEPARATOR);
    }


}
