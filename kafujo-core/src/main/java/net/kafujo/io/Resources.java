/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.io;

import net.kafujo.base.RequirementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.util.List;
import java.util.Objects;

import static net.kafujo.io.KafuFile.KAFUJO_TEMP_PREFIX;
import static net.kafujo.io.KafuFile.sizeAndPath;

/**
 * SIMPLIFLY {@link ResourceReader}
 *
 * @author rstolle
 * @since 0.8.0
 */
public class Resources {

    private static final Logger lgr = LoggerFactory.getLogger(Resources.class);

    /**
     * Creates/reuses a Resources object using the {@link ClassLoader} of the current thread. This is the
     * most common approach.
     * <p>
     *
     * @return Resources based on the current thread.
     */
    public static Resources ofCurrentThread() {
        return new Resources(Thread.currentThread().getContextClassLoader());
    }

    public static Resources of(final Thread thread) {
        return new Resources(thread.getContextClassLoader());
    }

    /**
     * Creates a ResourceReader using the {@link ClassLoader} of {@code clazz}.
     *
     * @param clazz class to take the ClassLoader from
     * @throws NullPointerException if {@code clazz} is null
     */
    public static Resources of(final Class<?> clazz) {
        Objects.requireNonNull(clazz, "REQUIRE clazz");
        return new Resources(clazz.getClassLoader());
    }

    public static Resources of(final ClassLoader loader) {
        return new Resources(Objects.requireNonNull(loader, "REQUIRE loader"));
    }

    private final ClassLoader loader;

    private Resources(final ClassLoader loader) {
        this.loader = loader;  // null check is done by the static ofXy()
    }


    /**
     * The {@link ClassLoader} used by this ResourceReader.
     *
     * @return {@link ClassLoader} used by this ResourceReader
     */
    public ClassLoader getLoader() {
        return loader;
    }

    /**
     * Checks the availability of a resource given by its name.
     *
     * @param resourceName the name of the resource in question
     * @return true, if there is a resource named {@code resourceName}, false otherwise
     * @throws NullPointerException     if {@code resourceName} is null
     * @throws IllegalArgumentException if {@code resourceName} {@link String#isBlank() is blank}
     */
    public boolean isAvailable(final CharSequence resourceName) {
        Objects.requireNonNull(resourceName, "REQUIRE resource name");
        if (resourceName.toString().isBlank()) {
            throw new IllegalArgumentException(" resource name cannot be blank");
        }
        return loader.getResource(resourceName.toString()) != null;  // Note: loader.getResource("") != null on win7,jdk11
    }

    /**
     * Makes sure, {@code resourceName} is available as a resource. This check will be done by each asXy() method.
     *
     * @param resourceName resource to be checked
     * @return {@code resourceName}
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if this resource does not exist
     */
    public String requireAvailability(final String resourceName) {
        if (!isAvailable(resourceName)) {
            throw new RequirementException("Resource '" + resourceName + "' not available");
        }
        return resourceName;
    }

    /**
     * Makes sure, {@code resourceName} is available as a resource.
     *
     * @param resourceName resource to be checked
     * @param message      will be added to the exception text - Resource 'Xy' not available: {@code message}
     * @return {@code resourceName}
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if this resource does not exist
     */
    public String requireAvailability(final String resourceName, final String message) {
        if (!isAvailable(resourceName)) {
            throw new RequirementException("Resource '" + resourceName + "' not available: " + message);
        }
        return resourceName;
    }

    /**
     * The absolute path to a resource <b>within an unpacked archive</b>.
     *
     * <em>
     * Note: wont work with a packed project like jar or war!
     * </em>
     *
     * @param resourceName the name of the resource
     * @return the absolute Path to the resource in the source tree.
     * @throws RequirementException if the resource cannot be found by any reason
     */
    @SuppressWarnings("ConstantConditions") // NPE safe through requireAvailability()
    public Path asPath(final String resourceName) {
        requireAvailability(resourceName);

        try {
            final URL url = loader.getResource(resourceName);
            Path file = Path.of(url.toURI());
            return file.toAbsolutePath();
        } catch (URISyntaxException e) {
            throw new RequirementException("Having trouble fetching resource: " + resourceName, e);
        }
    }

    /**
     * A new {@link InputStream} reading the given resource. This one should be closed after usage.
     *
     * @param resourceName the name of the resource
     * @return a ready to read InputStream
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if there is no resource named {@code resourceName}
     *                              {@link #requireAvailability(String) available}.
     */
    public InputStream asStream(final String resourceName) {
        requireAvailability(resourceName);
        return loader.getResourceAsStream(resourceName);
    }

    /**
     * Reads the given resource directly into a String.
     *
     * @param resourceName the name of the resource
     * @return resource content as String
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if there is no resource named {@code resourceName}
     *                              {@link #requireAvailability(String) available}.
     */
    public String asString(final String resourceName) {
        return KafuInput.asString(asStream(resourceName));
    }

    /**
     * Reads the given resource line by line into a List of Strings.
     *
     * @param resourceName the name of the resource
     * @return resource content as list of Strings
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if there is no resource named {@code resourceName}
     *                              {@link #requireAvailability(String) available}.
     */
    public List<String> asLines(final String resourceName) {
        return KafuInput.asLines(asStream(resourceName));
    }

   /**
     * Reads the given resource into a byte array.
     *
     * @param resourceName the name of the resource
     * @return resource content byte by byte
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if there is no resource named {@code resourceName}
     *                              {@link #requireAvailability(String) available}.
     */
    public byte[] asBytes(final String resourceName) {
        return KafuInput.asBytes(asStream(resourceName));
    }

    /**
     * Writes the content of the given resource to a {@link Files#createTempFile(String, String, FileAttribute[]) newly
     * created temporary file}. Filename and extension will be preserved. Name and size of the newly created file will
     * be logged in DEBUG level.
     *
     * @param resourceName the name of the resource
     * @return the path to the newly created file. This will end with {@code resourceName}, so potentially meaningful
     * file extensions will be preserved.
     * @throws NullPointerException if {@code resourceName} is null
     * @throws RequirementException if there is no resource named {@code resourceName}
     *                              {@link #requireAvailability(String) available}.
     */
    public Path asTempFile(final String resourceName) {
        requireAvailability(resourceName);
        final String filenamePart = Path.of(resourceName).getFileName().toString();

        Path path = null;
        try (var resourceInputStream = asStream(resourceName)) {
            path = Files.createTempFile(KAFUJO_TEMP_PREFIX, '_' + filenamePart);
            Files.copy(resourceInputStream, path, StandardCopyOption.REPLACE_EXISTING);
            lgr.debug("copied resource {} to {}", resourceName, sizeAndPath(path));
        } catch (IOException io) {
            String msg = "IO PROBLEM WRITING RESOURCE '" + resourceName + "' TO TEMP FILE " + path;
            lgr.info(msg, io);
            throw new UncheckedIOException(msg, io);
        }
        return path;
    }

    /**
     * Writes the given resource to a file.
     *
     * @param resourceName resource to be written to file
     * @param destination  file to be (over)written.
     * @param options      {@link StandardCopyOption write options}
     * @return {@code destination}
     * @throws UncheckedIOException on IO problems
     */
    public Path asFile(final String resourceName, final Path destination, CopyOption... options) {
        requireAvailability(resourceName);
        if (Files.exists(destination)) {
            KafuFile.requireNotDirectory(destination);
        }

        try (var resourceInputStream = asStream(resourceName)) {
            Files.copy(resourceInputStream, destination, options);
            lgr.debug("copied resource {} to {}", resourceName, sizeAndPath(destination));
        } catch (IOException io) {
            String msg = "IO PROBLEM WRITING RESOURCE '" + resourceName + "' TO " + destination.toAbsolutePath();
            lgr.info(msg, io);
            throw new UncheckedIOException(msg, io);
        }
        return destination;
    }

    /**
     * Calls {@link #asFile(String, Path, CopyOption...) asFile} with {@link StandardCopyOption#REPLACE_EXISTING}.
     *
     * @param resourceName resource to be written to file
     * @param destination  file to be (over)written.
     * @return {@code destination}
     * @throws UncheckedIOException on IO problems
     */
    public Path asFile(final String resourceName, final Path destination) {
        return asFile(resourceName, destination, StandardCopyOption.REPLACE_EXISTING);
    }
}
