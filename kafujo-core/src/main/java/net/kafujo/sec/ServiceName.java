package net.kafujo.sec;

import java.security.Provider;
import java.util.Objects;

/**
 * A cage for the names of {@link java.security.Provider.Service} names. E.g. {@code Cipher.GCM}
 * or one of its aliases {@code Alg.Alias.Cipher.OID.2.16.840.1.101.3.4.1.26}
 */
public class ServiceName implements Comparable<ServiceName> {

    public static final String ALIAS_PREFIX = "Alg.Alias.";

    private final boolean alias;
    private final String type;
    private final String algorithm;

    /**
     * Creates a ServiceName out of a {@link Provider.Service} full name.
     *
     * @param fullName the full name of a service as it might come from {@link Provider#keySet()}
     */
    public ServiceName(String fullName) {
        if (fullName.startsWith(ALIAS_PREFIX)) {
            fullName = fullName.substring(ALIAS_PREFIX.length());
            alias = true;
        } else {
            alias = false;
        }

        int firstDot = fullName.indexOf('.');
        if (firstDot < 1) {
            throw new IllegalArgumentException("no decent dot in " + fullName);
        }
        type = fullName.substring(0, firstDot);
        algorithm = fullName.substring(firstDot + 1);
    }

    /**
     * Creates a ServiceName out of the single parts of a service name.
     *
     * @param type  Type of Service.
     * @param algorithm   Algorithm
     * @param alias true, if it is just an alias name.
     */
    public ServiceName(String type, String algorithm, boolean alias) {
        this.algorithm = Objects.requireNonNull(algorithm, "REQUIRE service algorithm");
        this.type = Objects.requireNonNull(type, "REQUIRE service type");
        this.alias = alias;
    }

    /**
     * Creates a ServiceName out of a {@link Provider.Service}.
     *
     * @param service Service to extract name from. This cannot be an alias, cause the
     *                service namen end up being the "official" name, even when the service
     *                was created using an alias name.
     */
    public ServiceName(final Provider.Service service) {
        this(service.getType(), service.getAlgorithm(), false);
    }

    /**
     * Shortcut to {@link #ServiceName(String)}
     *
     * @param fullName the full name of a service as it might come from {@link Provider#keySet()}
     */
    public ServiceName(final Object fullName) {
        this(fullName.toString());
    }

    public String getType() {
        return type;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public boolean isAlias() {
        return alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceName that = (ServiceName) o;
        return alias == that.alias &&
                type.equals(that.type) &&
                algorithm.equals(that.algorithm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, algorithm, alias);
    }

    @Override
    public int compareTo(ServiceName o) {

        if (type.equals(o.type)) {
            if (algorithm.equals(o.algorithm)) {
                return Boolean.compare(alias, o.alias);
            } else {
                return algorithm.compareTo(o.algorithm);
            }
        }
        return type.compareTo(o.type);
    }

    public String getName() {
        return type + '.' + algorithm;
    }

    /**
     * Creates the same scheme as in {@link Provider#keySet()}
     *
     * @return the full name, including {@link #ALIAS_PREFIX}
     */
    @Override
    public String toString() {
        if (alias) {
            return ALIAS_PREFIX + getName();
        }
        return getName();
    }
}
