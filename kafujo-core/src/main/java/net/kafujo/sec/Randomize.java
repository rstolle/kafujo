package net.kafujo.sec;

import net.kafujo.base.UncheckedException;
import net.kafujo.units.DataSize;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * A unified and more convenient Random class. It gets initialized with a {@link Random} object and then offers some
 * additional methods to generate random data. There are two ready to use randomizer available: {@link #JUL}
 * (based on {@link java.util.Random}) and {@link #SECURE} which uses {@link java.security.SecureRandom}.
 *
 * <pre>
 * {@code
 *   int julRandom = Randomize.JUL.nextInt();
 *   int secRandom = Randomize.SECURE.nextInt();
 *
 *   Randomize strong = Randomize.createSecureRandomStrong();
 *   int strongRandom = strong.nextInt();
 *
 *   Randomize othere = Randomize.createSecureRandom("SHA1PRNG");
 *   int prngRandom = strong.nextInt();
 *
 * }
 * </pre>
 *
 * @author rstolle
 * @since 0.8.0
 */
public class Randomize {

    /**
     * A ready to use Randomize object, initialized with {@link Random}.
     */
    public static final Randomize JUL = new Randomize(new Random()); // gets a decent seed, no need to seed here

    /**
     * A ready to use Randomize object, initialized with default {@link SecureRandom}. According to
     * https://stackoverflow.com/a/27638413/7441000 and many others, this seems the way to go if you don't
     * know better. If you have specific requirements, use the constructor, {@link #createSecureRandom(String)},
     * {#link {@link #createSHA1PRNG()}} or
     */
    public static final Randomize SECURE = new Randomize(new SecureRandom());

    public static Randomize createSecureRandom(final String algorithm) {
        try {
            return new Randomize(SecureRandom.getInstance(algorithm));
        } catch (NoSuchAlgorithmException failed) {
            throw new UncheckedException("No such algorithm for SecureRandom to create Randomize", failed);
        }
    }

    public static Randomize createSHA1PRNG() {
        return createSecureRandom("SHA1PRNG");
    }

    /**
     * Creates a strong SecureRandom. I dont initialize this by default, cause on some systems this can take some time
     * or even cause some freezing (https://stackoverflow.com/q/40862857/7441000).
     *
     * @return strong SecureRandom
     */
    public static Randomize createSecureRandomStrong() {
        try {
            return new Randomize(SecureRandom.getInstanceStrong());
        } catch (NoSuchAlgorithmException noAlg) {
            throw new UncheckedException("Cannot create strong SecureRandom", noAlg);
        }
    }

    /**
     * An initialized {@link Random} object to be used within this class. I was tempted to make it public, but then
     * one might manipulate randomness in a predictable way with {@link Random#setSeed(long)}.
     */
    private final Random rand;

    public Randomize(Random rand) {
        this.rand = rand;
    }

    /**
     * Wrapper for {@link Random#nextBoolean()}
     *
     * @return a random boolean
     */
    public boolean nextBoolean() {
        return rand.nextBoolean();
    }

    /**
     * Wrapper for {@link Random#nextInt()}
     *
     * @return a random int
     */
    public int nextInt() {
        return rand.nextInt();
    }

    /**
     * Wrapper for {@link Random#nextLong()}
     *
     * @return a random long
     */
    public long nextLong() {
        return rand.nextLong();
    }

    /**
     * Wrapper for {@link Random#nextInt(int)}
     *
     * @param bound as a for {@link Random#nextInt(int)}
     * @return a random int between 0 (inclusive) and bound (exclusive)
     */
    public int nextInt(int bound) {
        return rand.nextInt(bound);
    }

    /**
     * Calculates a random number within a specific range. The range can be the entire int range.
     *
     * @param startInclusive start of range
     * @param endExclusive   end of range
     * @return a random in between {@code startInclusive} and {@code endExclusive}
     * @throws IllegalArgumentException if {@code startInclusive >= endExclusive}
     */
    public int nextInt(int startInclusive, int endExclusive) {
        if (startInclusive >= endExclusive) {
            throw new IllegalArgumentException("start must be smaller than end");
        }

        long bound = (long) endExclusive - startInclusive; // switch to long cause we have to use randomLong

        if (bound == 1) {
            return startInclusive;
        }

        long next = nextLong(bound) + startInclusive;

        if (next >= Integer.MAX_VALUE || next < Integer.MIN_VALUE) {
            throw new AssertionError("out of integer range: " + next); // cannot happen, remove soon
        }

        return (int) next;
    }

    /**
     * Random does not have a bounded nextLong(). Here it is.
     *
     * @param bound the upper bound (exclusive).  Must be positive.
     * @return a random number between 0 and {@code bound-1}
     * @throws IllegalArgumentException if bound is not positive
     */
    public long nextLong(final long bound) {

        if (bound < 1) {
            throw new IllegalArgumentException("bound must be >= 1 but is " + bound);
        }

        if (bound < Integer.MAX_VALUE) {
            return rand.nextInt((int) bound);
        }

        if (rand.nextInt(4) == 0) {  // put another random element in it
            return rand.nextInt(Integer.MAX_VALUE);
        }

        long next = rand.nextLong();

        if (next < 0) {
            next *= -1;
        }

        while (next >= bound) {
            next /= 2;
        }

        return next;
    }

    /**
     * Wrapper for {@link Random#nextBytes(byte[])}
     *
     * @param bytes the byte array to be filled
     * @return {@code bytes} - the same array given in
     */
    public byte[] nextBytes(final byte[] bytes) {
        rand.nextBytes(bytes);
        return bytes;
    }

    /**
     * Creates an array of size bytes and fills it using {@link Random#nextBytes(byte[])}
     *
     * @param size size of the random byte array
     * @return the created byte array
     */
    public byte[] bytes(final int size) {
        return nextBytes(new byte[size]);
    }

    /**
     * Creates an array of size bytes and fills it using {@link Random#nextBytes(byte[])}
     *
     * @param size size of the random byte array
     * @return the created byte array
     */
    public byte[] bytes(DataSize size) {
        byte[] bytes = size.newByteArray();
        rand.nextBytes(bytes);
        return bytes;
    }
}
