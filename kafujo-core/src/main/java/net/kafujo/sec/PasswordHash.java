package net.kafujo.sec;

import java.util.Base64;
import java.util.Objects;

/**
 * Immutable storage a password hash and salt.
 */
public class PasswordHash {

    private final String hashB64; // using String instead of byte[] gives us immutability without ...
    private final String saltB64; // ... cloning an array on each get(). And base64 is often needed anyway.

    /**
     *
     * @param hash the already computed password hash
     * @param salt the salt
     */
    public PasswordHash(final byte[] hash, final byte[] salt) {
        this.hashB64 = Base64.getEncoder().encodeToString(Objects.requireNonNull(hash, "REQUIRE hash"));
        this.saltB64 = Base64.getEncoder().encodeToString(Objects.requireNonNull(salt, "REQUIRE salt"));
    }

    public String getHashB64() {
        return hashB64;
    }

    public String getSaltB64() {
        return saltB64;
    }

    public byte[] getHash() {
        return Base64.getDecoder().decode(hashB64);
    }

    public byte[] getSalt() {
        return Base64.getDecoder().decode(saltB64);
    }
}
