package net.kafujo.sec;

import net.kafujo.base.UncheckedException;
import net.kafujo.units.DataSize;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Objects;

/**
 * Implements password hashing using {@link PBEKeySpec}. The idea is to create a immutable Hasher for a project and
 * do all the password hashing and verifying with it:
 * <pre>
 * {@code
 *     var hasher = PasswordHasher.DEFAULT; // you can also define it with different attributes using new
 *     var hash = hasher.hash("s3cret");
 *
 *     if (!hasher.verify("secret", hash)) {
 *             // WRONG PASSWORD!
 *     }
 * }
 * </pre>
 * <p>
 * This implementation started with https://www.quickprogrammingtips.com/java/how-to-securely-store-passwords-in-java.html
 * and keeps most of its assumptions.
 *
 * @author rstolle
 * @since 0.8.0
 */
public class PasswordHasher {

    public static final String PBKDF2_WITH_HMAC_SHA1 = "PBKDF2WithHmacSHA1";

    private static final Randomize RANDOMIZE = Randomize.createSHA1PRNG();

    /**
     * Parameters as described in https://www.quickprogrammingtips.com/java/how-to-securely-store-passwords-in-java.html
     * which was the starting point of this class.
     */
    public static final PasswordHasher DEFAULT =
            new PasswordHasher(PBKDF2_WITH_HMAC_SHA1, // use PBKDF2 hash algorithm
                    160,               // SHA1
                    2000,                     // NIST specifies 10000
                    DataSize.of(8));                        // NIST recommends minimum 4 bytes

    public static final PasswordHasher NIST =
            new PasswordHasher(PBKDF2_WITH_HMAC_SHA1, // use PBKDF2 hash algorithm
                    160,               // SHA1
                    1000,
                    DataSize.of(4));                   // minimum recommendation

    private final String algorithm;
    private final int derivedKeyLength;
    private final int iterations;
    private final DataSize saltSize;

    /**
     * Creates a new PasswordHasher. In most cases you can just stick wih {@link #DEFAULT} or {@link #NIST}.
     *
     * @param algorithm        algorithm to be used
     * @param derivedKeyLength key (hash) length
     * @param iterations       iterations
     * @param saltSize         size of the randomly generated salt
     */
    public PasswordHasher(final String algorithm, final int derivedKeyLength, final int iterations, final DataSize saltSize) {
        this.algorithm = algorithm;
        this.derivedKeyLength = derivedKeyLength;
        this.iterations = iterations;
        this.saltSize = saltSize;
    }

    /**
     * Hashing the given password using a random salt.
     *
     * @param password phrase to be hashed
     * @return hash and salt
     * @throws NullPointerException if {@code password} is null.
     * @throws UncheckedException   holding a {@link GeneralSecurityException} if encryption fails.
     */
    public PasswordHash hash(final CharSequence password) {
        Objects.requireNonNull(password, "REQUIRE password to be hashed");
        var salt = RANDOMIZE.bytes(saltSize);
        var hash = hash(password, salt);
        return new PasswordHash(hash, salt);
    }

    /**
     * Verifies {@code password} against {@link PasswordHash hash}.
     *
     * @param password plain password to be verified
     * @param hash     a given hash with salt
     * @return true, if {@code password} is correct, false otherwise.
     * @throws NullPointerException if any parameter is null
     * @throws UncheckedException   holding a {@link GeneralSecurityException} if encryption fails.
     */
    public boolean verify(final CharSequence password, final PasswordHash hash) {
        return verify(password, hash.getHash(), hash.getSalt());
    }

    /**
     * Verifies {@code password} against hash and salt.
     *
     * @param password plain password to be verified
     * @param hash     a given hash
     * @param salt     a given salt
     * @return true, if {@code password} is correct, false otherwise.
     * @throws NullPointerException if any parameter is null
     * @throws UncheckedException   holding a {@link GeneralSecurityException} if encryption fails.
     */
    public boolean verify(final CharSequence password, final byte[] hash, final byte[] salt) {
        Objects.requireNonNull(password, "REQUIRE password to be verified");
        Objects.requireNonNull(hash, "REQUIRE hash to verify password");
        Objects.requireNonNull(salt, "REQUIRE salt to verify password");

        if (salt.length != saltSize.intValue()) {
            return false; // no exception to make it harder to spy!?
        }
        // todo: check hash size for derivedKeyLength as well?

        // hash the password again and compare the new hash with the given hash
        return Arrays.equals(hash(password, salt), hash);
    }

    /**
     * Does the actual encryption.
     *
     * @param password plain password to be hashed
     * @param salt     salt to be used
     * @return the hash
     * @throws UncheckedException holding a {@link GeneralSecurityException} if encryption fails.
     */
    private byte[] hash(final CharSequence password, final byte[] salt) {
        // null checks for all the parameters were done before so lets start right away
        final KeySpec spec = new PBEKeySpec(password.toString().toCharArray(), salt, iterations, derivedKeyLength);

        try {
            SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);
            return f.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new UncheckedException("Problem hashing password", e);
        }
    }
}
