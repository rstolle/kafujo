package net.kafujo.sec;

import net.kafujo.base.UncheckedException;
import net.kafujo.sec.Randomize;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;


/**
 * Following https://www.linkedin.com/learning/learn-java-cryptography/performing-symmetric-encryption-in-java?u=57700073
 */
public class AES {

    public static final String CIPHER_NAME = "AES";
    public static final String CIPHER_FULL = "AES/CBC/PKCS5Padding";

    private static final SecureRandom RANDOM = new SecureRandom();

    public static SecretKey createKey(int size) {
        try {
            KeyGenerator gen = KeyGenerator.getInstance(CIPHER_NAME);
            gen.init(size, RANDOM);
            return gen.generateKey();
        } catch (NoSuchAlgorithmException e) {
            throw new UncheckedException("Problem generating AES key", e);
        }
    }

    private final SecretKey key;
    private final IvParameterSpec iv;


    /**
     *
     * @param keySize
     * @throws InvalidParameterException if {@code keySize} is not 128, 192 or 256
     */
    public AES(int keySize) {
        this.key = createKey(keySize);
        this.iv = new IvParameterSpec(Randomize.SECURE.bytes(16));
    }

    public byte[] encrypt (byte[] data) {
        try {
            var cipher = Cipher.getInstance(CIPHER_FULL);
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            return cipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new UncheckedException("Problem encrypting", e);
        }
    }

    public byte[] decrypt (byte[] data) {
        try {
            var cipher = Cipher.getInstance(CIPHER_FULL);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            return cipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new UncheckedException("Problem decrypting", e);
        }
    }

}
