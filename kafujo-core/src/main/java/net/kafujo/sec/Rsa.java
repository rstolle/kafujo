package net.kafujo.sec;

import net.kafujo.base.UncheckedException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.util.Arrays;
import java.util.Objects;

/**
 * Convenient usage of java RSA encryption using the default {@link java.security.Provider}.
 */
public class Rsa {

    public static final String CIPHER_NAME = "RSA";

    private static final SecureRandom RANDOM = new SecureRandom();

    /**
     * @param size key size
     * @return a RSA key pair with the given key size.
     * @throws java.security.InvalidParameterException when key is smaller than 512
     */
    public static KeyPair createKeyPair(int size) {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance(CIPHER_NAME);
            generator.initialize(size, RANDOM);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new UncheckedException("Problem creating KeyPair", e); // SHALL NEVER HAPPEN
        }
    }

    /**
     * Creates a RSA Cipher.
     *
     * @return RSA Cipher ready to be used.
     */
    public static Cipher createCipher() {
        try {
            return Cipher.getInstance(CIPHER_NAME);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException never) {
            throw new UncheckedException("Problem creating RSA cipher: " + never, never);
        }
    }

    /**
     * Checks, if the {@code pair} contains corresponding keys.
     *
     * @param pair           keys to be checked.
     * @param checkAlgorithm check the name of the algorithm to be {@link #CIPHER_NAME}
     * @return true, if encrypted data (public key) could be successfully decrypted.
     */
    public static boolean isCorresponding(final KeyPair pair, final boolean checkAlgorithm) {
        Objects.requireNonNull(pair, "REQUIRE KeyPair for corresponding check");
        Objects.requireNonNull(pair.getPrivate(), "REQUIRE private key for corresponding check");
        Objects.requireNonNull(pair.getPublic(), "REQUIRE public key for corresponding check");

        Rsa rsa = new Rsa(pair, checkAlgorithm);

        var plain = Randomize.JUL.bytes(42);
        var enc = rsa.encrypt(plain);

        try {
            return Arrays.equals(plain, rsa.decrypt(enc));
        } catch (UncheckedException failed) {
            return false;
        }
    }

    /**
     * Checks, if the {@code pair} contains corresponding keys. Calls {@link #isCorresponding(KeyPair, boolean)} with
     * algorithm check.
     *
     * @param pair key pair to be checked.
     * @return true if public and private key belong together.
     */
    public static boolean isCorresponding(final KeyPair pair) {
        return isCorresponding(pair, true);
    }

    private final Cipher cipher = createCipher();
    private final KeyPair pair;

    /**
     * Creates an Rsa object using the given KeyPair which must contain RSA keys.
     *
     * @param pair keys to be used
     */
    public Rsa(final KeyPair pair) {
        this(pair, true);
    }

    public Rsa(final KeyPair pair, boolean checkAlgorithms) {
        this.pair = Objects.requireNonNull(pair, "REQUIRE pair");

        if (pair.getPublic() == null && pair.getPrivate() == null) {
            throw new IllegalArgumentException("REQUIRE at least one key private or public");
        }

        if (checkAlgorithms && pair.getPrivate() != null) {
            final String algPrivate = pair.getPrivate().getAlgorithm();
            if (!CIPHER_NAME.equalsIgnoreCase(algPrivate)) {
                throw new IllegalArgumentException("private key is not RSA but: " + algPrivate);
            }
        }

        if (checkAlgorithms && pair.getPublic() != null) {
            final String algPublic = pair.getPublic().getAlgorithm();
            if (!CIPHER_NAME.equalsIgnoreCase(algPublic)) {
                throw new IllegalArgumentException("public key is not RSA but: " + algPublic);
            }
        }
    }

    public Rsa(PublicKey pubk) {
        this(new KeyPair(pubk, null)); // null check done there
    }

    public Rsa(PrivateKey prvk) {
        this(new KeyPair(null, prvk)); // null check done there
    }

    /**
     * Creates a RSA using a newly generated Keypair.
     *
     * @param keySize size of the keys to be generated.
     * @throws UncheckedException as in {@link #createKeyPair(int)}
     */
    public Rsa(int keySize) {
        this.pair = createKeyPair(keySize);
    }

    public byte[] encrypt(byte[] data) {
        Objects.requireNonNull(pair.getPublic(), "REQUIRE public key to encrypt");
        try {
            cipher.init(Cipher.ENCRYPT_MODE, pair.getPublic());
            return cipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new UncheckedException("Problem encrypting", e);
        }
    }

    public byte[] decrypt(byte[] data) {
        Objects.requireNonNull(pair.getPrivate(), "REQUIRE private key to decrypt");
        try {
            cipher.init(Cipher.DECRYPT_MODE, pair.getPrivate());
            return cipher.doFinal(data);
        } catch (GeneralSecurityException e) {
            throw new UncheckedException("Problem decrypting", e);
        }
    }
}
