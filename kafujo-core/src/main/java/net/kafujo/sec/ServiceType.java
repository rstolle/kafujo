package net.kafujo.sec;

import java.security.Provider;
import java.security.Security;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a list of the most common ServiceType taken from all the jdk
 * {@link java.security.Provider providers} and BouncyCastle 2.65. The latter adds only
 * X509Store and X509StreamParser
 */
public enum ServiceType {
    AlgorithmParameterGenerator,
    AlgorithmParameters,
    CertPathBuilder,
    CertPathValidator,
    CertStore,
    CertificateFactory,
    Cipher,
    Configuration,
    GssApiMechanism,
    KeyAgreement,
    KeyFactory,
    KeyGenerator,
    KeyInfoFactory,
    KeyManagerFactory,
    KeyPairGenerator,
    KeyStore,
    Mac,
    MessageDigest,
    Policy,
    SSLContext,
    SaslClientFactory,
    SaslServerFactory,
    SecretKeyFactory,
    SecureRandom,
    Signature,
    TerminalFactory,
    TransformService,
    TrustManagerFactory,
    X509Store,
    X509StreamParser,
    XMLSignatureFactory;


    public final static List<ServiceType> LIST = List.of(values());
    public final static List<String> LIST_STR = LIST.stream().map(Enum::name).collect(Collectors.toUnmodifiableList());

    /**
     * Checks which of all available ServiceTypes are not covered in this enum.
     *
     * @return a list with ServiceTypes not covered yet.
     */
    public static List<String> fetchNotListed() {
        var allCurrentlyAvailable = KafuSec.fetchServiceTypeList();
        var result = new LinkedList<String>();

        for (String name : allCurrentlyAvailable) {
            if (!LIST_STR.contains(name)) {
                result.add(name);
            }
        }
        return result;
    }

    /**
     * Checks which ServiceTypes covered in this enum are currently not available.
     *
     * @return a list with ServiceTypes currently not available.
     */
    public static List<String> fetchCurrentlyNotAvailable() {

        var currentlyAvailable = KafuSec.fetchServiceTypeList();

        var result = new LinkedList<>(LIST_STR);

        for (String name : LIST_STR) {
            if (currentlyAvailable.contains(name)) {
                result.remove(name);
            }
        }
        return result;
    }

    public List<String> fetchAlgorithmList (Provider provider) {
        var services = provider.getServices();
        var result = new LinkedList<String>();
        for (var service : services) {
            if (service.getType().equals(this.name())) {
                result.add(service.getAlgorithm());
            }
        }
        Collections.sort(result);
        return result;
    }

    public List<String> fetchAliasList (Provider provider) {
        var services = KafuSec.fetchAliasNameList(provider);
        var result = new LinkedList<String>();
        for (var service : services) {
            if (service.getType().equals(this.name())) {
                result.add(service.getAlgorithm());
            }
        }
        Collections.sort(result);
        return result;
    }

    public List<String> fetchAliasList () {
        var services = KafuSec.fetchAliasNameList();
        var result = new LinkedList<String>();
        for (var service : services) {
            if (service.getType().equals(this.name())) {
                result.add(service.getAlgorithm());
            }
        }
        Collections.sort(result);
        return result;
    }

    /**
     *
     * @param provider
     * @return sorted list of algorithm names and aliases
     */
    public List<String> fetchFullList (Provider provider) {
        var result = new LinkedList<>(fetchAlgorithmList(provider));
        result.addAll(fetchAliasList(provider));
        Collections.sort(result);
        return result;
    }

    public List<String> fetchFullList () {
        var result = new LinkedList<>(fetchAlgorithmList());
        result.addAll(fetchAliasList());
        Collections.sort(result);
        return result;
    }

    /**
     *
     * @return sorted set of algorithms
     */
    public SortedSet<String> fetchAlgorithmSet () {
        return new TreeSet<>(Security.getAlgorithms(this.name()));
    }

    /**
     *
     * @return sorted list of alg.
     */
    public List<String> fetchAlgorithmList () {
        var list = new ArrayList<>(Security.getAlgorithms(this.name()));
        Collections.sort(list);
        return list;
    }
}
