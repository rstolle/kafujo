package net.kafujo.sec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Provider;
import java.security.Security;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Utils for {@link Security Java Security}.
 */
public final class KafuSec {

    private static final Logger LGR = LoggerFactory.getLogger(KafuSec.class);

    private KafuSec() {
        // pure utility class
    }

    /**
     * @return a sorted list of the names of all currently loaded providers.
     */
    public static List<String> fetchProviderNameList() {
        return Arrays.stream(Security.getProviders()).map(Provider::getName).sorted().collect(Collectors.toList());
    }


    /**
     * Fetches the {@link Provider#getServices() service types} {@code provider} offers.
     *
     * @param provider provider to be looked at.
     * @return a sorted list of service types.
     */
    public static SortedSet<String> fetchServiceTypesSet(final Provider provider) {
        final var serviceTypes = new TreeSet<String>();
        for (var service : provider.getServices()) {
            serviceTypes.add(service.getType());
        }
        return serviceTypes;
    }

    /**
     * Fetches the {@link Provider#getServices() service types} of all {@link Security#addProvider(Provider) loaded}
     * {@link Provider}.
     *
     * @return a sorted list of all service types currently available.
     */
    public static SortedSet<String> fetchServiceTypesSet() {
        final var serviceTypes = new TreeSet<String>();
        for (Provider provider : Security.getProviders()) {
            serviceTypes.addAll(fetchServiceTypesSet(provider));
        }
        return serviceTypes;
    }

    /**
     * Shortcut to {@link #fetchServiceTypesSet(Provider)} when {@link List} ist needed
     *
     * @param provider provider to be looked at.
     * @return a sorted list of service types.
     */
    public static List<String> fetchServiceTypeList(final Provider provider) {
        return new ArrayList<>(fetchServiceTypesSet(provider));
    }

    /**
     * Shortcut to {@link #fetchServiceTypesSet()} when {@link List} ist needed.
     *
     * @return a sorted list of all service types currently available.
     */
    public static List<String> fetchServiceTypeList() {
        return new ArrayList<>(fetchServiceTypesSet());
    }

    public static Set<ServiceName> fetchServiceNameSet(Provider provider) {
        Set<ServiceName> serviceSet = new TreeSet<>();
        for (Provider.Service service : provider.getServices()) {
            serviceSet.add(new ServiceName(service));
        }
        return serviceSet;
    }

    public static Set<ServiceName> fetchServiceNameSet() {
        Set<ServiceName> serviceSet = new TreeSet<>();

        for (Provider provider : Security.getProviders()) {
            serviceSet.addAll(fetchServiceNameSet(provider));
        }
        return serviceSet;
    }

    public static List<ServiceName> fetchServiceNameList() {
        return new ArrayList<>(fetchServiceNameSet());
    }

    public static List<ServiceName> fetchAliasNameList(Provider provider) {
        List<ServiceName> result = new LinkedList<>();
        var keys = provider.keys();

        while (keys.hasMoreElements()) {
            ServiceName name = new ServiceName(keys.nextElement().toString());

            if (name.isAlias()) {
                result.add(name);
            }
        }
        return result;
    }

    public static List<ServiceName> fetchAliasNameList() {
        Set<ServiceName> result = new TreeSet<>();

        for (Provider provider : Security.getProviders()) {
            result.addAll(fetchAliasNameList(provider));
        }

        return new ArrayList<>(result);
    }

    public static Map<ServiceName, List<Provider>> createServiceNameProviderMap() {
        Map<ServiceName, List<Provider>> result = new HashMap<>();
        for (Provider provider : Security.getProviders()) {

            for (Provider.Service service : provider.getServices()) {
                ServiceName name = new ServiceName(service);
                if (result.containsKey(name)) {
                    result.get(name).add(provider);
                } else {
                    var list = new LinkedList<Provider>();
                    list.add(provider);
                    result.put(name, list);
                }
            }
        }
        return result;
    }

    public static Map<ServiceName, List<Provider>> findMultipleImplementations() {
        var result = createServiceNameProviderMap();
        var services = List.copyOf(result.keySet());

        for (var service : services) {
            if (result.get(service).size() == 1) {
                result.remove(service);
            }
        }
        return result;
    }


    /**
     * @param provider
     * @return
     */
    public static Map<ServiceName, List<ServiceName>> createServiceNameAliasMap(Provider provider) {
        final Map<ServiceName, List<ServiceName>> result = new HashMap<>();
        final List<ServiceName> noServiceForAlias = new LinkedList<>();

        // 1. create an empty list for each "official" service name
        for (Provider.Service service : provider.getServices()) {
            var officialName = new ServiceName(service);
            if (result.containsKey(officialName)) {
                throw new IllegalStateException("ServiceName already used: " + officialName);
            }
            result.put(officialName, new LinkedList<>());
        }


        for (ServiceName aliasName : fetchAliasNameList(provider)) {
            // create a service object with the alias name
            final var service = provider.getService(aliasName.getType(), aliasName.getAlgorithm());
            if (service == null) { // rarely, but sometimes, there is no service
                LGR.debug("NO service for alias name '" + aliasName + "'");
                noServiceForAlias.add(aliasName);
                continue;
            }
            var officialName = new ServiceName(service);
            result.get(officialName).add(aliasName); // NPE if not there, but that should not happen
        }

        if (!noServiceForAlias.isEmpty()) {
            result.put(null, noServiceForAlias);
        }
        return result;
    }


    public static Map<ServiceName, List<ServiceName>> createServiceNameAliasMapStripped(Provider provider) {
        var orig = createServiceNameAliasMap(provider);
        var keys = new ArrayList<>(orig.keySet()); // List.copyOf fails when there is null key
        for (ServiceName officialName : keys) {
            if (orig.get(officialName).isEmpty()) {
                orig.remove(officialName);
            }
        }
        return orig;
    }
}
