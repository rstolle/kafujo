/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.container;

import java.util.*;

/**
 * KafuCollection class
 *
 * @author rstolle
 * @since 0.0.1
 */
public final class KafuCollection {

    private KafuCollection() {
        // pure utility class
    }

    /**
     * <p>properties2StringMap.</p>
     *
     * @param props a {@link java.util.Properties} object.
     * @return a {@link java.util.Map} object.
     */
    public static Map<String, String> properties2StringMap(final Properties props) {
        Map<String, String> map = new HashMap<>(props.size());
        for (String key : props.stringPropertyNames()) {
            map.put(key, props.getProperty(key));
        }
        return map;
    }

    /**
     * <p>stringMap2properties.</p>
     *
     * @param map a {@link java.util.Map} object.
     * @return a {@link java.util.Properties} object.
     */
    public static Properties stringMap2properties(final Map<String, String> map) {
        Properties props = new Properties();
        for (String key : map.keySet()) {
            props.setProperty(key, map.get(key));
        }
        return props;
    }



    public static List<Integer> range(int endExclusive) {
        return range(0, endExclusive, 1);
    }

    public static List<Integer> range(int startInclusive, int endExclusive) {
        return range(startInclusive, endExclusive, 1);
    }

    public static List<Integer> range(int startInclusive, int endExclusive, int step) {
        if (startInclusive >= endExclusive) {
            throw new IllegalArgumentException("start must be smaller than end");
        }

        var list = new ArrayList<Integer>((endExclusive-startInclusive) / step);

        for (int idx = startInclusive; idx < endExclusive; idx += step) {
            list.add(idx);
        }

        return list;
    }


}
