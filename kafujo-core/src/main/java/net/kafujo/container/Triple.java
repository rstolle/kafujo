package net.kafujo.container;

import java.util.Comparator;

/**
 * Created by stolle on 2017-07-01-Saturday.
 * Es ist schon verückt, in wie vielen Varianten man eine so simple Datenstruktur implementieren kann:
 * - Mit einer (Comparable) Generics
 * - Mit verschiedenen (Comparable) Generics für alle 'Spalten'
 * - Mit Festen Datentypen
 * - Mit einem bestimmten Datentyp für alle Spalten.
 * Also, wie baut man nun eine wirklich geile API? Und warum hat das JDK kein Tuple/Triple?
 * Egal, die Anforderungen: In den meisten Fällen wird man nicht nur einen Tuple haben, sondern
 * einige und die packt man sicher in eine Collection. Also
 * TODO:
 * Testen ohne equals? - wenn zwei wertgleiche tubles in einer Liste/Set
 */
public class Triple<L extends Comparable, M extends Comparable, R extends Comparable> {

    private L left;
    private M middle;
    private R right;

    public Triple() {
    }

    public Triple(final L left, final M middle, final R right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public void setLeft(final L left) {
        this.left = left;
    }

    public M getMiddle() {
        return middle;
    }

    public void setMiddle(final M middle) {
        this.middle = middle;
    }

    public R getRight() {
        return right;
    }

    public void setRight(final R right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "(" + left + ")(" + middle + ")(" + right + ')';
    }

    public static final Comparator<Triple> MIDDLE = new Comparator<Triple>() {
        @Override
        public int compare(final Triple o1, final Triple o2) {
            if (o1 == null || o2 == null) {
                return 0; // todo: how to make it right ..
            }

            return o1.middle.compareTo(o2.middle);

        }
    };

    // Es ist schon crazy, in wie vielen Varianten man so einen Triple implemtieren kann!
    // Folgende Methoden sind 1:1 von: https://stackoverflow.com/questions/6010843/java-how-to-store-data-triple-in-a-list

    /**
     * <p>Compares this triple to another based on the three elements.</p>
     *
     * @param obj the object to compare to, null returns false
     * @return true if the elements of the triple are equal
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof Triple<?, ?, ?>) {
            final Triple<?, ?, ?> other = (Triple<?, ?, ?>) obj;

            return equals(getLeft(), other.getLeft()) &&
                    equals(getMiddle(), other.getMiddle()) &&
                    equals(getRight(), other.getRight());
        }
        return false;
    }

    private boolean equals(
            final Object object1,
            final Object object2) {
        return !(object1 == null || object2 == null) &&
                (object1 == object2 || object1.equals(object2));
    }

    /**
     * <p>Returns a suitable hash code.</p>
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        return (getLeft() == null ? 0 : getLeft().hashCode()) ^
                (getMiddle() == null ? 0 : getMiddle().hashCode()) ^
                (getRight() == null ? 0 : getRight().hashCode());
    }

}
