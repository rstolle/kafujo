package net.kafujo.container;

import java.io.Serializable;

/**
 * Created by stolle on 2017-07-01-Saturday.
 */
public class TripleString extends Triple<String, String, String> implements Serializable {

    public TripleString(final String left, final String middle, final String right) {
        super(left, middle, right);
    }

   /* public static final Comparator<TripleString> MIDDLE = new Comparator<TripleString>() {
        @Override
        public int compare(TripleString o1, TripleString o2)
        {
            if (o1 == null || o2 == null)
                return 0; // todo: how to make it right ..

            return o1.middle.compareTo(o2.middle);

        }
    } ;
*/
}
