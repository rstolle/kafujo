package net.kafujo.container;

/**
 * Similar to {@link Runnable} but {@link #execute()} can return something
 *
 * @since 0.2.0
 */
@FunctionalInterface
public interface RunnableResult<T> {
    T execute();
}
