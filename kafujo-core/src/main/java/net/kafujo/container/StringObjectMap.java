/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.container;

import net.kafujo.base.RequirementException;
import net.kafujo.io.KafuFile;
import net.kafujo.units.DataSize;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created to optimize springs jdbc methods and not very ueseful otherwise.
 * To not always create a new map for each insert and update, I implemented a queue of reusable
 * maps. You can poll a map and put it back to the pool.
 * <p>
 * todo: Denkbar wäre auch pollFilledMap(DomainObj) mit einträgen für alle getter!? Aber wie stellen wir dann sicher,
 * dass nicht database get/is nicht benutzt wird?
 *
 * @author sto
 * @version $Id: $Id
 */
public class StringObjectMap extends HashMap<String, Object> implements AutoCloseable {

    private static final Queue<StringObjectMap> pool = new ConcurrentLinkedQueue<>();
    private static int created = 0;
    private static int hits = 0;
    private static int fails = 0;
    private static int biggest = 0;

    /**
     * This class does not make sure, that a map in the pool is not still used elsewhere.
     * The private ctor at least ensures, that here is the only place to build maps.
     */
    private StringObjectMap() {
        super(23);
        ++created;
    }

    /**
     * Polls an empty map from the queue.  If no map available, a new one is generated.
     *
     * @return a {@link net.kafujo.container.StringObjectMap} object.
     */
    public static StringObjectMap poll() {
        StringObjectMap map = pool.poll();
        if (map == null) {
            fails++;
            map = new StringObjectMap();
        } else {
            hits++;
        }
        return map;
    }

    /**
     * Clears the map and make it available again.
     */
    public void close() {
        if (this.size() > biggest)
            biggest = this.size();
        this.clear();
        pool.add(this);
    }

    /**
     * <p>Getter for the field <code>hits</code>.</p>
     *
     * @return a int.
     */
    public static int getHits() {
        return hits;
    }

    /**
     * <p>Getter for the field <code>fails</code>.</p>
     *
     * @return a int.
     */
    public static int getFails() {
        return fails;
    }

    /**
     * <p>stats.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String stats() {
        return "StringObjectMap size: " + pool.size() + ", created: " + created + ", fails:" + fails + ", hits: " + hits;
    }

    /**
     * Puts a non null enum in the som
     *
     * @param key     a {@link java.lang.String} object.
     * @param enumObj a {@link java.lang.Enum} object.
     */
    public void putEnum(final String key, final Enum enumObj) {
        putEnum(key, enumObj, false);
    }

    /**
     * Puts an enum in the database using its name
     *
     * @param key         a {@link java.lang.String} object.
     * @param enumObj     a {@link java.lang.Enum} object.
     * @param nullAllowed a boolean.
     */
    public void putEnum(final String key, final Enum enumObj, final boolean nullAllowed) {
        if (enumObj == null) {
            if (!nullAllowed) {
                throw new IllegalStateException("Null enum not allowed for Enum " + key);
            }

            put(key, null);
        } else {
            put(key, enumObj.name());
        }
    }

    /**
     * <p>putDataSize.</p>
     *
     * @param key   a {@link java.lang.String} object.
     * @param bytes a {@link net.kafujo.units.DataSize} object.
     */
    public void putDataSize(final String key, final DataSize bytes) {
        putDataSize(key, bytes, false);
    }

    /**
     * <p>putDataSize.</p>
     *
     * @param key         a {@link java.lang.String} object.
     * @param dataSize    a {@link net.kafujo.units.DataSize} object.
     * @param nullAllowed a boolean.
     */
    public void putDataSize(final String key, final DataSize dataSize, final boolean nullAllowed) {
        if (dataSize == null) {
            if (!nullAllowed) {
                throw new RequirementException("Null value not allowed for DataSize " + key);
            }
            put(key, null);
        } else {
            put(key, dataSize.longValue());
        }
    }

    /**
     * <p>putPath.</p>
     *
     * @param key  a {@link java.lang.String} object.
     * @param path a {@link java.nio.file.Path} object.
     */
    public void putPath(final String key, final Path path) {
        putPath(key, path, false);
    }

    /**
     * <p>putPath.</p>
     *
     * @param key         a {@link java.lang.String} object.
     * @param path        a {@link java.nio.file.Path} object.
     * @param nullAllowed a boolean.
     */
    public void putPath(final String key, final Path path, final boolean nullAllowed) {
        if (path == null) {
            if (!nullAllowed) {
                throw new RequirementException("Null value not allowed for Path " + key);
            }
            put(key, null);
        } else {
            put(key, KafuFile.separatorsToUnix(path));
        }
    }
}
