/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;

enum ConfigPropertySource {
    SYS("jvm system parameter (-D...)"),  // (prefixed) -D parameter - HIGHEST PRIORITY
    ENV("os envronment variable"),        // (prefixed) environment variable
    PROP("(config file) property"),       // property comming most likely from a property file
    SRC("src code default value");        // LOWEST PRIORITY, will not be registered as mbean if already defined otherwise

    public final String description;

    ConfigPropertySource(String description) {
        this.description = description;
    }
}
