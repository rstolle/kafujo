/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;

import net.kafujo.base.RequirementException;
import net.kafujo.container.KafuCollection;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * <p>ConfigPropertyManager class.</p>
 *
 * @author sto
 * @version $Id: $Id
 */
public class ConfigPropertyManager {

    private final Map<String, ConfigProperty> propertyMap = new TreeMap<>();

    private final String keyPrefix;

    /**
     * <p>fromProperties.</p>
     *
     * @param props     a {@link java.util.Properties} object.
     * @param keyPrefix a {@link java.lang.String} object.
     * @return a {@link net.kafujo.config.ConfigPropertyManager} object.
     */
    public static ConfigPropertyManager fromProperties(Properties props, String keyPrefix) {
        return new ConfigPropertyManager(props, keyPrefix);
    }

    private ConfigPropertyManager(Properties props, String keyPrefix) {
        this.keyPrefix = keyPrefix;
        createAndRegisterFileProps(props);
        Properties probsEnv = KafuCollection.stringMap2properties(System.getenv());
        createAndRegisterProps(probsEnv, ConfigPropertySource.ENV);
        createAndRegisterProps(System.getProperties(), ConfigPropertySource.SYS);
    }


    private void createAndRegisterFileProps(Properties props) {
        for (String key : props.stringPropertyNames()) {
            register(new ConfigProperty(key, props.getProperty(key), ConfigPropertySource.PROP));
        }
    }

    private void createAndRegisterProps(Properties properties, ConfigPropertySource source) {
        for (String key : properties.stringPropertyNames()) {
            String keyWithoutPrefix = key;

            if (keyPrefix != null) {
                if (!key.startsWith(keyPrefix)) {
                    continue;
                } else {
                    keyWithoutPrefix = key.substring(keyPrefix.length());
                }
            }

            ConfigProperty cp = propertyMap.get(keyWithoutPrefix);

            if (cp != null) { // update the value of an existing ConfigProperty
                cp.setValue(properties.getProperty(key), source);
            } else {  // create a new ConfigProperty
                register(new ConfigProperty(keyWithoutPrefix, properties.getProperty(key), source));
            }
        }
    }


    private void register(ConfigProperty property) {
        if (propertyMap.containsKey(property.getKey())) {
            throw new IllegalStateException(property + ": key is already registerd");
        }
        propertyMap.put(property.getKey(), property);
        // todo mbean
    }

    /**
     * <p>read.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String read(final String key) {
        ConfigProperty property = propertyMap.get(key);
        if (property == null) {
            throw new RequirementException("No config property for key: " + key);
        }
        return property.readValue();

    }


    private ConfigProperty getPropertyOrThrow(String key) {
        ConfigProperty property = propertyMap.get(key);
        if (property == null) {
            throw new RequirementException("No config property for key: " + key);
        }
        return property;
    }

    /**
     * <p>read.</p>
     *
     * @param key          a {@link java.lang.String} object.
     * @param defaultValue a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public synchronized String read(final String key, final String defaultValue) {
        ConfigProperty property = propertyMap.get(key);
        if (property == null) {
            property = new ConfigProperty(key, defaultValue);
            register(property);
        }
        return property.readValue();
    }

    /**
     * <p>getReadCounter.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a long.
     */
    public long getReadCounter(final String key) {
        return getPropertyOrThrow(key).getReadCounter();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (String key : propertyMap.keySet()) {
            sb.append(propertyMap.get(key)).append(System.lineSeparator());
        }
        return sb.toString();
    }
}
