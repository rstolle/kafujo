package net.kafujo.config;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Collects some {@link PropertiesSource} and allows to create a (Typed)Properties out of the actually available
 * sources. The collection is done with the constructor, makes this class immutable. A early version had builder
 * like approach which caused more bad than good.
 *
 * @author rstolle
 * @since 0.3.0
 */
public class PropertiesSourceCollector {
    private final List<PropertiesSource> available = new LinkedList<>();
    private final List<PropertiesSource> notAvailable = new LinkedList<>();

    /**
     * Creates a collection of at least one available {@link PropertiesSource}. So it is assured, that there is at
     * least one readable property file or resource, but not, if they actually contain a property.
     *
     * @param first   to ensure, there is at least one source ...
     * @param sources ... but it can be as many as needed.
     * @throws NullPointerException     if first is null
     * @throws IllegalArgumentException of none of the sources is actually available.
     */
    public PropertiesSourceCollector(PropertiesSource first, PropertiesSource... sources) {

        Objects.requireNonNull(first, "REQUIRE (at least one) PropertiesSource ");
        checkAndAdd(first);

        for (var source : sources) {
            checkAndAdd(source);
        }

        if (available.isEmpty()) {
            throw new IllegalArgumentException("None of the " + notAvailable.size() + " given Properties Sources are actually available: " + notAvailable);
        }
    }

    private void checkAndAdd(final PropertiesSource source) {
        if (available.contains(source)) {
            throw new IllegalArgumentException("Property file of " + source + " is already collected: " + available);
        }

        if (source.isAvailable()) {
            available.add(source);
        } else {
            notAvailable.add(source);
        }
    }

    /**
     * Reads the properties from the first available option. All others are entirely ignored.
     *
     * @return properties from the first available option
     */
    public TypedProperties useFirstAvailable() {
        return available.get(0).load();
    }

    /**
     * Merges all the available properties. If there are the same key in different sources, the latest wins.
     *
     * @return properties from all available Sources.
     */
    public TypedProperties merge() {

        final TypedProperties mergedProps = useFirstAvailable();
        if (available.size() == 1) {
            return mergedProps;
        }

        for (var source : available.subList(1, available.size())) {
            mergedProps.takeOver(source);
        }
        return mergedProps;
    }

    public List<PropertiesSource> getAvailableSources() {
        return Collections.unmodifiableList(available);
    }
}
