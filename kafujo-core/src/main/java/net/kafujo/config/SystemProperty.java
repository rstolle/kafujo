/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;

import net.kafujo.base.RequirementException;
import net.kafujo.text.KafuText;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Covers most {@link System#getProperties() java system properties} with enum values and offers methods for type safe
 * access.
 * {@code
 * SystemProperty.JAVA_VERSION.asString();
 * SystemProperty.JMXREMOTE_PORT.asInteger();
 * SystemProperty.USER_HOME.asPath();
 * // with fall back
 * SystemProperty.JAVA_VERSION.asStringFbk("N/A");
 * SystemProperty.JMXREMOTE_PORT.asIntegerFbk(-1);
 * }
 * <p>
 * The type safe accessors are available as well for properties not covered in the enum.
 * {@code
 * SystemProperty.asBoolean ("my.system.property");
 * SystemProperty.asBooleanFbk ("my.optional.system.property", false);
 * <p>
 * }
 * <p>
 * There are some more methods, to find out which properties are (not) covered by the enum and which ones have actually
 * values.
 * <p>
 * <b>Note:</b> The Values of the properties are always queried via {@link System#getProperty(String)}, so this values
 * can change during runtime, which I personally find not so nice. todo: store the initial state and offer method to compare?
 *
 * @author rstolle
 * @since 0.0.3
 */
public enum SystemProperty {

    JAVA_VERSION("java.version"),                             // 11.0.2
    JAVA_VERSION_DATE("java.version.date"),                   // 2019-01-15
    JAVA_VENDOR("java.vendor"),                               // Oracle Corporation
    JAVA_VENDOR_URL("java.vendor.url"),                       // http://java.oracle.com/
    JAVA_VENDOR_URL_BUG("java.vendor.url.bug"),               // http://bugreport.java.com/bugreport/
    JAVA_VENDOR_VERSION("java.vendor.version"),               // 18.9
    JAVA_SPECIFICATION_VENDOR("java.specification.vendor"),   // Oracle Corporation
    JAVA_RUNTIME_VERSION("java.runtime.version"),             // 11.0.2+9
    JAVA_RUNTIME_NAME("java.runtime.name"),                   // OpenJDK Runtime Environment
    JAVA_SPECIFICATION_VERSION("java.specification.version"), // 11
    JAVA_SPECIFICATION_NAME("java.specification.name"),       // Java Platform API Specification
    JAVA_CLASS_VERSION("java.class.version"),                 // 55.0

    // java.vm -> OMMIT the JAVA in the name
    VM_NAME("java.vm.name"),         // OpenJDK 64-Bit Server VM
    VM_INFO("java.vm.info"),         // mixed mode
    VM_VERSION("java.vm.version"),   // 11.0.2+9
    VM_VENDOR("java.vm.vendor"),     // Oracle Corporation
    VM_SPECIFICATION_VERSION("java.vm.specification.version"),
    VM_SPECIFICATION_NAME("java.vm.specification.name"),
    VM_SPECIFICATION_VENDOR("java.vm.specification.vendor"),
    VM_COMPRESSEDOOPSMODE("java.vm.compressedOopsMode"),

    // misc
    JDK_DEBUG("jdk.debug"),
    AWT_TOOLKIT("awt.toolkit"),
    SUN_AWT_GRAPHICSENV("java.awt.graphicsenv"),
    SUN_AWT_PRINTERJOB("java.awt.printerjob"),

    // sun.*
    SUN_CPU_ISALIST("sun.cpu.isalist"),
    SUN_JNU_ENCODING("sun.jnu.encoding"),
    SUN_ARCH_DATA_MODEL("sun.arch.data.model"),
    SUN_OS_PATCH_LEVEL("sun.os.patch.level"),
    SUN_CPU_ENDIAN("sun.cpu.endian"),
    SUN_JAVA_COMMAND("sun.java.command"),
    SUN_JAVA_LAUNCHER("sun.java.launcher"),
    SUN_BOOT_LIBRARY_PATH("sun.boot.library.path"),
    SUN_MANAGEMENT_COMPILER("sun.management.compiler"),
    SUN_IO_UNICODE_ENCODING("sun.io.unicode.encoding"),
    SUN_DESKTOP("sun.desktop"),

    // os information
    OS_NAME("os.name"),
    OS_VERSION("os.version"),
    OS_ARCH("os.arch"),

    // File and Path setting
    FILE_ENCODING("file.encoding"),
    FILE_SEPARATOR("file.separator"),
    LINE_SEPARATOR("line.separator"),
    PATH_SEPARATOR("path.separator"),
    JAVA_IO_TMPDIR("java.io.tmpdir"),
    JAVA_HOME("java.home"),
    JAVA_CLASS_PATH("java.class.path"),
    JAVA_LIBRARY_PATH("java.library.path"),

    // User
    USER_NAME("user.name"),
    USER_COUNTRY("user.country"),
    USER_TIMEZONE("user.timezone"),
    USER_LANGUAGE("user.language"),
    USER_HOME("user.home"),
    USER_DIR("user.dir"),
    USER_SCRIPT("user.script"),
    USER_VARIANT("user.variant"),

    // jmx setting -> OMIT the com.sun.management
    // these are needed to have a basic setup which works most of the time
    JMXREMOTE("com.sun.management.jmxremote"),                           // must be only there
    JMXREMOTE_PORT("com.sun.management.jmxremote.port"),                 // a port -> jconsole localhost:port
    JMXREMOTE_RMI_PORT("com.sun.management.jmxremote.rmi.port"),         // a port
    JMXREMOTE_LOCAL_ONLY("com.sun.management.jmxremote.local.only"),     // false
    JMXREMOTE_SSL("com.sun.management.jmxremote.ssl"),                   // false
    JMXREMOTE_AUTHENTICATE("com.sun.management.jmxremote.authenticate"), // false
    // these are needed to secure the connection
    JMXREMOTE_LOGIN_CONFIG("com.sun.management.jmxremote.login.config"),
    JMXREMOTE_PASSWORD_FILE("com.sun.management.jmxremote.password.file"),
    JMXREMOTE_REGISTRY_SSL("com.sun.management.jmxremote.registry.ssl"),
    JMXREMOTE_SSL_NEED_CLIENT_AUTH("com.sun.management.jmxremote.ssl.need.client.auth"),

    // now some crypto  stuff -> OMIT javax.net
    SSL_KEYSTORE("javax.net.ssl.keyStore"),
    SSL_KEYSTORE_TYPE("javax.net.ssl.keyStoreType"),
    SSL_KEYSTORE_PASSWORD("javax.net.ssl.keyStorePassword"),
    SSL_TRUSTSTORE("javax.net.ssl.trustStore"),
    SSL_TRUSTSTORE_TYPE("javax.net.ssl.trustStoreType"),
    SSL_TRUSTSTORE_PASSWORD("javax.net.ssl.trustStorePassword"),
    CRYPTO_POLICY("crypto.policy"), // see unit test

    // slf4j-simple configuration http://www.slf4j.org/api/org/slf4j/impl/SimpleLogger.html
    SLF4J_DEFAULT_LEVEL ("org.slf4j.simpleLogger.defaultLogLevel"),    // default info
    SLF4J_LOGFILE ("org.slf4j.simpleLogger.logFile"),
    SLF4J_SHOW_TIME ("org.slf4j.simpleLogger.showDateTime"),           // default false
    SLF4J_TIME_FORMAT ("org.slf4j.simpleLogger.dateTimeFormat"),       // a SimpleDateFormat String, default millis since start
    SLF4J_SHOW_THREAD ("org.slf4j.simpleLogger.showThreadName"),       // default true
    SLF4J_SHOW_LOGNAME ("org.slf4j.simpleLogger.showLogName"),         // default true
    SLF4J_SHORTEN_LOGNAME ("org.slf4j.simpleLogger.showShortLogName"), // default false

    // log4j2
    LOG4J2_CONFIG_FILE ("log4j.configurationFile"),      // default: log4j2.xml within the classpath
    LOG4J2_DEBUG ("log4j2.debug"),    // any value will trigger internal debug messages on stdout

    // the awesome kafujo cl interface
    KAFUJO_CL_EXCEPTION ("kafujo.cl.exception"), // print full exceptions, default is (to) hide
    KAFUJO_CL_HEADER ("kafujo.cl.header"),  // full/short -> java version, date etc
    KAFUJO_CL_SLEEP ("kafujo.cl.sleep"),  // value in millies, no value -> forever
    KAFUJO_CL_STATS ("kafujo.cl.stats");  // full/short -> time, memory etc

    public final String key;

    public final static Comparator<SystemProperty> COMPARE_BY_KEY = Comparator.comparing(sp -> sp.key);

    private final static SortedSet<SystemProperty> LISTED = Collections.unmodifiableSortedSet(
            Arrays.stream(values()).collect(Collectors.toCollection(() -> new TreeSet<>(COMPARE_BY_KEY))));

    SystemProperty(String key) {
        this.key = Objects.requireNonNull(key);
    }

    /**
     * Checks whether a system property given by {@code key} is available.
     *
     * @param key system property name to be checken
     * @return true, if {@link System#getProperty(String)} is not null
     * @throws NullPointerException if {@code key} is null
     */
    public static boolean isAvailable(final String key) {
        Objects.requireNonNull(key, "REQUIRE system property key");
        return System.getProperty(key) != null;
    }

    /**
     * Checks whether this {@link SystemProperty} is available.
     *
     * @return true, if this property has a non null value.
     */
    public boolean isAvailable() {
        return System.getProperty(this.key) != null;
        // return isAvailable(this.key); works as well, but the null check is useless
    }

    /**
     * Checks whether this {@link SystemProperty} is available.
     *
     * @return true, if this property has a non null value.
     */
    public boolean isNotAvailable() {
        return System.getProperty(this.key) == null;
    }

    /**
     * Ensures this {@link SystemProperty} is available.
     *
     * @return the non null value of this system property.
     * @throws RequirementException if the value of this property is null
     */
    public String requireAvailability() {
        final String value = System.getProperty(key);
        if (value == null) {
            throw new RequirementException("NO VALUE FOR REQUIRED SYSTEM PROPERTY '" + key + "'");
        }
        return value;
    }

    /**
     * Ensures the {@link System#getProperty(String) system property} is given by {@code key} is not null
     *
     * @param key property name
     * @return the non null value of this system property.
     * @throws RequirementException if the value of this property is null
     */
    public static String requireAvailability(final String key) {
        Objects.requireNonNull(key, "REQUIRE system property key");

        final String value = System.getProperty(key);
        if (value == null) {
            throw new RequirementException("NO VALUE FOR REQUIRED SYSTEM PROPERTY '" + key + "'");
        }
        return value;
    }

    /**
     * Gets a system property given by {@code key} as a String or throws an Exception if there is no such property.
     *
     * @param key property name
     * @return the non null system property value for key
     * @throws NullPointerException if {@code key} is null
     * @throws RequirementException if {@code key} is (currently) not a system property
     */
    public static String asString(final String key) {
        return requireAvailability(key);
    }

    /**
     * Gets this system property as a String or throws if there is no such property.
     *
     * @return the non null system property value for {@code key}
     * @throws RequirementException if {@code key} is (currently) not a system property
     */
    public String asString() {
        return asString(this.key);
    }

    /**
     * Gets a property value with a fallback
     *
     * @param key property name
     * @param fbk fallback value
     * @return the value of system property {@code key} of {@code fbk} if not available
     */
    public static String asStringFbk(final String key, final String fbk) {
        return isAvailable(key) ? System.getProperty(key) : fbk;
    }

    /**
     * Returns the value of this property as a String or a default if there is no such property.
     *
     * @param fbk default value, if there is value for this property
     * @return {@link #asStringFbk(String, String)}
     */
    public String asStringFbk(final String fbk) {
        return asStringFbk(this.key, fbk);
    }

    public String asStringFbk() {
        return asStringFbk(this.key, this.key + " N/A");
    }

    public static boolean asBoolean(final String name) {
        final String val = requireAvailability(name);
        if (val.equalsIgnoreCase("true")) {
            return true;
        }
        if (val.equalsIgnoreCase("false")) {
            return false;
        }
        throw new IllegalArgumentException("SYSTEM PROPERTY '" + name + "' MUST BE 'true' OR 'false'");
    }

    public boolean asBoolean() {
        return asBoolean(this.key);
    }

    public static Boolean asBooleanFbk(final String key, final Boolean fbk) {
        if (isAvailable(key)) {
            return asBoolean(key);
        }
        return fbk;
    }

    public boolean asBooleanFbk(final Boolean fbk) {
        return asBooleanFbk(this.key, fbk);
    }


    /**
     * Type safe property access.
     *
     * @param key property key
     * @return property value as long
     * @throws NullPointerException  if {@code key} is null
     * @throws RequirementException  if there is no value
     * @throws NumberFormatException if value could not be parsed to long
     */
    public static long asLong(final String key) {
        final String val = requireAvailability(key);
        return Long.parseLong(val);
    }

    public long asLong() {
        return asLong(this.key);
    }

    public static Long asLongFbk(final String key, final Long fbk) {
        if (isAvailable(key)) {
            return asLong(key);
        }
        return fbk;
    }

    public Long asLongFbk(final Long fbk) {
        return asLongFbk(this.key, fbk);
    }

    /**
     * Type safe property access.
     *
     * @param key property key
     * @return property value as int
     * @throws NullPointerException  if {@code key} is null
     * @throws RequirementException  if there is no value
     * @throws NumberFormatException if value could not be parsed to int
     */
    public static int asInteger(final String key) {
        final String val = requireAvailability(key);
        return Integer.parseInt(val);
    }

    /**
     * Calls {@link #asString(String)} for this property.
     *
     * @return property value as int
     * @throws NullPointerException  if {@code key} is null
     * @throws RequirementException  if there is no value
     * @throws NumberFormatException if value could not be parsed to int
     */
    public int asInteger() {
        return asInteger(this.key);
    }

    public static Integer asIntegerFbk(final String key, final Integer fbk) {
        if (isAvailable(key)) {
            return asInteger(key);
        }
        return fbk;
    }

    public Integer asIntegerFbk(final Integer fbk) {
        return asIntegerFbk(this.key, fbk);
    }


    public static Path asPath(final String key) {
        return Path.of(asString(key));
    }

    public Path asPath() {
        return asPath(this.key);
    }

    public static Path asPathFbk(final String key, final Path fbk) {
        return isAvailable(key) ? asPath(key) : fbk;
    }

    public Path asPath(final Path fbk) {
        return asPathFbk(this.key, fbk);
    }


    @Override
    public String toString() {
        return key + " = " + this.asStringFbk(KafuText.UNIVERSAL_NA);
    }

    public String toString(int maxLen) {
        return StringUtils.abbreviate(toString(), maxLen);
    }

    /**
     * Maps the {@link System#getProperties() System Property key objects} to a String list. These are ALL the system
     * properties available, not only the ones covered by this class.
     *
     * @return ALL system property keys
     */
    public static SortedSet<String> getKeysAvailable() {
        return System.getProperties().keySet().stream().map(Object::toString).collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * A collection of all system property keys which are {@link #isAvailable(String) available}, but not listed in
     * this enum.
     *
     * @return all properties not covered {@link SystemProperty here}.
     */
    public static SortedSet<String> getKeysAvailableNotListed() {
        SortedSet<String> total = getKeysAvailable();
        getListed().forEach(e -> total.remove(e.key));
        return total;
    }


    /**
     * @return all properties listed in this enum
     */
    public static Set<SystemProperty> getListed() {
        return LISTED;
    }

    /**
     * Determines all {@link SystemProperty SystemProperties} which actually have a value, which means they are listed
     * in {@link System#getProperties()} as well.
     * <p>
     * <em>There might be even more Properties, which are not listed in this enum.</em>
     *
     * @return all {@link SystemProperty SystemProperties} which have a non null value.
     */
    public static SortedSet<SystemProperty> getListedAvailable() {
        return LISTED.stream()
                .filter(SystemProperty::isAvailable)
                .collect(Collectors.toCollection(() -> new TreeSet<>(COMPARE_BY_KEY)));
    }

    /**
     * Determines all {@link SystemProperty SystemProperties} which have no value, which means they are NOT listed
     * {@link System#getProperties()}.
     *
     * @return a set of all the {@link SystemProperty SystemProperties}, which are not set.
     */
    public static SortedSet<SystemProperty> getListedNotAvailable() {
        return LISTED.stream()
                .filter(SystemProperty::isNotAvailable)
                .collect(Collectors.toCollection(() -> new TreeSet<>(COMPARE_BY_KEY)));
    }


    public Object set (String val) {
        return System.getProperties().setProperty(this.key, val);
    }
    /**
     * Creates a combination of {@link System#getProperties() system properties} values divided by '|'.
     *
     * @param keys properties of interest
     * @return a String combining the values of the given properties
     */
    public static String combine(final String... keys) {
        final var tmp = new StringBuilder(System.getProperty(keys[0]));

        for (int i = 1; i < keys.length; i++) {
            var val = System.getProperty(keys[i]);

            if (StringUtils.isBlank(val)) {
                val = keys[i] + " N/A";
            }
            tmp.append(" | ").append(val);
        }
        return tmp.toString();
    }

    /**
     * Creates a combination of {@link System#getProperties() system properties} values.
     *
     * @param props properties of interest
     * @return a String combining the values of the given properties
     */
    public static String combine(final SystemProperty... props) {
        final var tmp = new String[props.length];

        for (int idx = 0; idx < tmp.length; idx++) {
            tmp[idx] = props[idx].key;
        }
        return combine(tmp);
    }

    /**
     * Combines the values for {@link #OS_NAME}, {@link #OS_VERSION} and {@link #OS_ARCH}.
     * E.g.: "Linux 3.10.0-42 (amd64)"
     *
     * @return the OS information from the system properties
     */
    public static String combineOsInfo() {
        var tmp = OS_NAME.asStringFbk() + ' ' + OS_VERSION.asStringFbk();

        if (!StringUtils.isBlank(OS_ARCH.asStringFbk(""))) {
            return tmp + " (" + OS_ARCH.asString() + ')';
        }
        return tmp;
    }

    /**
     * Combines the values for {@link #VM_NAME}, {@link #VM_VERSION} and {@link #VM_INFO}.
     * E.g.: "OpenJDK 64-Bit Server VM 11.0.2+9 (mixed mode)"
     *
     * @return the java vm information like
     */
    public static String combineVmInfo() {
        return VM_NAME.asStringFbk()
                + ' ' + VM_VERSION.asStringFbk()
                + " (" + VM_INFO.asStringFbk() + ')';
    }

    /**
     * @return jmx settings
     */
    public static String combineJmxFull() {
        String build = "";

        if (JMXREMOTE.isAvailable()) {
            build += "jmx.remote='" + JMXREMOTE.asString() + "'; ";
        }

        if (JMXREMOTE_PORT.isAvailable()) {
            // asString() cause its not worth risking a NumberFormatException
            build += "port=" + JMXREMOTE_PORT.asString() + "; ";
        }

        if (JMXREMOTE_RMI_PORT.isAvailable()) {
            build += "rmi.port=" + JMXREMOTE_RMI_PORT.asString() + "; ";
        }

        if (JMXREMOTE_LOCAL_ONLY.isAvailable()) {
            build += "local.only=" + JMXREMOTE_LOCAL_ONLY.asString() + "; ";
        }

        if (JMXREMOTE_SSL.isAvailable()) {
            build += "ssl=" + JMXREMOTE_SSL.asString() + "; ";
        }

        if (JMXREMOTE_REGISTRY_SSL.isAvailable()) {
            build += "registry.ssl=" + JMXREMOTE_REGISTRY_SSL.asString() + "; ";
        }

        if (JMXREMOTE_SSL_NEED_CLIENT_AUTH.isAvailable()) {
            build += "ssl.need.client.auth=" + JMXREMOTE_SSL_NEED_CLIENT_AUTH.asString() + "; ";
        }

        if (JMXREMOTE_AUTHENTICATE.isAvailable()) {
            build += "auth=" + JMXREMOTE_AUTHENTICATE.asString() + "; ";
        }

        if (JMXREMOTE_PASSWORD_FILE.isAvailable()) {
            build += "pwfile=" + JMXREMOTE_PASSWORD_FILE.asString() + "; ";
        }

        if (JMXREMOTE_LOGIN_CONFIG.isAvailable()) {
            build += "login.config=" + JMXREMOTE_LOGIN_CONFIG.asString() + "; ";
        }

        if (StringUtils.isBlank(build)) {
            build = KafuText.UNIVERSAL_NA;
        }

        return build;
    }

    /**
     * @return jmx settings
     */
    public static String combineJmxPorts() {
        String build = "";

        if (JMXREMOTE_PORT.isAvailable()) {
            // asString() cause its not worth risking a NumberFormatException
            build += JMXREMOTE_PORT.asString();
        }

        if (JMXREMOTE_RMI_PORT.isAvailable()) {
            build += " (rmi " + JMXREMOTE_RMI_PORT.asString() + ')';
        }

        if (StringUtils.isBlank(build)) {
            return "N/A";
        }

        return build.trim();
    }

    /**
     * combines USER properties
     *
     * @return a nicely formatted String
     */
    public static String combineUserInfo() {
        return USER_NAME.asStringFbk() + " (" + createUserLocaleFbk("N/A", "N/A")
                + "); dir=" + USER_DIR.asStringFbk() + "; home=" + USER_HOME.asStringFbk();
    }

    /**
     * Creates a {@link Locale} out of {@link #USER_LANGUAGE} and {@link #USER_COUNTRY} or throws if one of them is missing.
     *
     * @return Locale according to user system properties
     * @throws RequirementException if the needed system properties are not {@link #requireAvailability(String)}
     */
    public static Locale createUserLocale() {
        return new Locale(USER_LANGUAGE.asString(), USER_COUNTRY.asString());
    }

    public static Locale createUserLocaleFbk(final String language, final String country) {
        return new Locale(USER_LANGUAGE.asStringFbk(language), USER_COUNTRY.asStringFbk(country));
    }

    /**
     * Shortcut to safely get the users Locale
     *
     * @return Locale of user or "en" if this doesn't exist
     */
    public static Locale createUserLocaleFbk() {
        return createUserLocaleFbk("en", "");
    }

}
