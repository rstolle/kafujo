package net.kafujo.config;

import net.kafujo.network.EmailAddress;
import net.kafujo.network.Host;
import net.kafujo.network.Hostname;
import net.kafujo.network.Port;
import net.kafujo.text.KafuText;
import net.kafujo.units.KafuDuration;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;

/**
 * Offers a wide range of read methods to be used to read configs and more. Most of this is done with default
 * methods, which should not be overwritten. They build up on the two only methods to be implemented:
 * {@link #readString(CharSequence)} and {@link #isAvailable(CharSequence)}
 *
 * @author rstolle
 * @since 0.3.0
 */
public interface TypedReader {

    /**
     * Checks if there is a value for {@code key}
     *
     * @param key key to be checked
     * @return true, if there is a value for {@code key}, false otherwise
     */
    boolean isAvailable(CharSequence key);

    /**
     * @param key key
     * @return the value of the {@code key} as String
     */
    String readString(CharSequence key);

    default String readStringFbk(final CharSequence key, final CharSequence fbk) {
        if (isAvailable(key)) {
            return readString(key);
        }

        return fbk == null ? null : fbk.toString();
    }

    /**
     * Calls {@link #readStringFbk(CharSequence, CharSequence)} with {@link KafuText#UNIVERSAL_NA} as fallback.
     *
     * @param key key to be read.
     * @return the value or {@link KafuText#UNIVERSAL_NA} if not available.
     */
    default String readStringFbk(final CharSequence key) {
        return readStringFbk(key, KafuText.UNIVERSAL_NA);
    }

    /**
     * Converts "true" to true and "false" to false. Its case insensitive, but still much stricter than
     * {@link Boolean#parseBoolean(String)}.
     *
     * @param key to be read
     * @return true or false
     * @throws IllegalArgumentException if the value is not case insensitive "true" or "false".
     *                                  {@link java.util.IllegalFormatException} would fit much nicer
     *                                  but its not entirely public.
     */
    default boolean readBoolean(final CharSequence key) {
        final String valueString = readString(key);
        if (valueString.equalsIgnoreCase("true")) {
            return true;
        }
        if (valueString.equalsIgnoreCase("false")) {
            return false;
        }
        throw new IllegalArgumentException("key value for '" + key + "' MUST BE 'true' OR 'false'");
    }

    default Boolean readBooleanFbk(final CharSequence key, final Boolean fbk) {
        if (isAvailable(key)) {
            return readBoolean(key);
        }
        return fbk;
    }

    /**
     * Reads a short, supporting min and max.
     *
     * @param key to be read
     * @return the value converted to short. {@link Short#MAX_VALUE} if value is {@link KafuText#UNIVERSAL_MAX}.
     * {@link Short#MIN_VALUE} if value is {@link KafuText#UNIVERSAL_MIN}.
     */
    default short readShort(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return Short.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return Short.MAX_VALUE;
        }

        return Short.parseShort(valueString);
    }

    default Short readShortFbk(final CharSequence key, final Short fbk) {
        if (isAvailable(key)) {
            return readShort(key);
        }
        return fbk;
    }

    default int readInt(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return Integer.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return Integer.MAX_VALUE;
        }

        return Integer.parseInt(valueString);
    }

    default Integer readIntFbk(final CharSequence key, final Integer fbk) {
        if (isAvailable(key)) {
            return readInt(key);
        }
        return fbk;
    }

    default long readLong(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return Long.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return Long.MAX_VALUE;
        }

        return Long.parseLong(valueString);
    }

    default Long readLongFbk(final CharSequence key, final Long fbk) {
        if (isAvailable(key)) {
            return readLong(key);
        }
        return fbk;
    }

    default BigInteger readBigInteger(final CharSequence key) {
        return new BigInteger(readString(key));
    }

    default BigInteger readBigIntegerFbk(final CharSequence key, final BigInteger fbk) {
        final String valueString = readStringFbk(key, null);
        if (valueString == null) {
            return fbk;
        }
        return new BigInteger(valueString);
    }

    default Port readPort(final CharSequence key) {
        return Port.of(readString(key));
    }

    default Port readPortFbk(final CharSequence key, final Port fbk) {
        final String valueString = readStringFbk(key, null);
        if (valueString == null) {
            return fbk;
        }
        return Port.of(valueString);
    }

    default float readFloat(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return Float.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return Float.MAX_VALUE;
        }

        return Float.parseFloat(valueString);
    }

    default Float readFloatFbk(final CharSequence key, final Float fbk) {
        if (isAvailable(key)) {
            return readFloat(key);
        }
        return fbk;
    }

    default double readDouble(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return Double.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return Double.MAX_VALUE;
        }

        return Double.parseDouble(valueString);
    }

    default Double readDoubleFbk(final CharSequence key, final Double fbk) {
        if (isAvailable(key)) {
            return readDouble(key);
        }
        return fbk;
    }

    default BigDecimal readBigDecimal(final CharSequence key) {
        return new BigDecimal(readString(key));
    }

    default BigDecimal readBigDecimalFbk(final CharSequence key, final BigDecimal fbk) {
        final String valueString = readStringFbk(key, null);
        if (valueString == null) {
            return fbk;
        }
        return new BigDecimal(valueString);
    }

    /**
     * Considers the value as a number of millis to be a {@link Duration}. This method uses {@link #readLong(CharSequence)}}
     * so MAX and MIN will work as {@link Long#MIN_VALUE} and {@link Long#MAX_VALUE}
     *
     * @param key element key
     * @return Duration.ofMillies (value)
     */
    default Duration readDurationOfMillis(final String key) {
        return Duration.ofMillis(readLong(key));
    }

    default Duration readDurationOfMillisFbk(final String key, Duration fbk) {
        final Long longValue = readLongFbk(key, null);

        if (longValue == null) {
            return fbk;
        }
        return Duration.ofMillis(longValue);
    }

    default Duration readDurationOfMillisFbk(final String key, final Long fbk) {
        final Long longValue = readLongFbk(key, null);

        if (longValue == null) {
            if (fbk == null) {
                return null;
            } else {
                return Duration.ofMillis(fbk);
            }
        }

        return Duration.ofMillis(longValue);
    }

    default Duration readDurationOfSeconds(final String key) {
        return Duration.ofSeconds(readLong(key));
    }

    default Duration readDurationOfSecondsFbk(final String key, Duration fbk) {
        final Long longValue = readLongFbk(key, null);

        if (longValue == null) {
            return fbk;
        }
        return Duration.ofSeconds(longValue);
    }

    default Duration readDurationOfSecondsFbk(final String key, final Long fbk) {
        final Long longValue = readLongFbk(key, null);

        if (longValue == null) {
            if (fbk == null) {
                return null;
            } else {
                return Duration.ofSeconds(fbk);
            }
        }

        return Duration.ofSeconds(longValue);
    }


    default Duration readDuration(final CharSequence key) {
        final String valueString = readString(key);

        if (KafuText.equalsUniversalMin(valueString)) {
            return KafuDuration.MIN_VALUE;
        }

        if (KafuText.equalsUniversalMax(valueString)) {
            return KafuDuration.MAX_VALUE;
        }

        return Duration.parse(valueString);
    }

    default Duration readDurationFbk(final CharSequence key, final Duration fbk) {
        if (isAvailable(key)) {
            return readDuration(key);
        }
        return fbk;
    }

    default URL readUrl(final CharSequence key) {
        try {
            return new URL(readString(key));
        } catch (MalformedURLException fail) {
            throw new IllegalArgumentException(fail);
        }
    }

    default URL readUrlFbk(final CharSequence key, final URL fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            return fbk;
        }
        return readUrl(key);
    }

    default EmailAddress readEmailAddress(final CharSequence key) {
        return new EmailAddress(readString(key));
    }

    default EmailAddress readEmailAddressFbk(final CharSequence key, final EmailAddress fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            return fbk;
        }
        return readEmailAddress(key);
    }

    default EmailAddress readEmailAddressFbk(final CharSequence key, final String fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk == null) {
                return null;
            }
            return new EmailAddress(fbk);
        }
        return readEmailAddress(key);
    }

    default Hostname readHostname(final CharSequence key) {
        return new Hostname(readString(key));
    }

    default Hostname readHostnameFbk(final CharSequence key, final Hostname fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            return fbk;
        }
        return readHostname(key);
    }

    default Hostname readHostnameFbk(final CharSequence key, final String fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk == null) {
                return null;
            }
            return new Hostname(fbk);
        }
        return readHostname(key);
    }

    default Host readHost(final CharSequence key) {
        return new Host(readString(key));
    }

    default Host readHostFbk(final CharSequence key, final Host fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            return fbk;
        }
        return readHost(key);
    }

    default Host readHostFbk(final CharSequence key, final String fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk == null) {
                return null;
            }
            return new Host(fbk);
        }
        return readHost(key);
    }

    default Host readHostWithPort(final CharSequence key) {
        Host host = new Host(readString(key));
        if (host.getPort().isEmpty()) {
            throw new IllegalArgumentException("Need a host with port but got only " + host);
        }
        return host;
    }

    default Host readHostWithPortFbk(final CharSequence key, final Host fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk != null && fbk.getPort().isEmpty()) {
                throw new IllegalArgumentException("Need a fallback host with port but got only " + fbk);
            }
            return fbk;
        }
        return readHost(key);
    }

    default Host readHostWithPortFbk(final CharSequence key, final String fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk == null) {
                return null;
            }
            Host host = new Host(fbk);
            if (host.getPort().isEmpty()) {
                throw new IllegalArgumentException("Need a fallback host with port but got only " + fbk);
            }
            return host;
        }
        return readHost(key);
    }

    default Path readPath(final CharSequence key) {
        return Path.of(readString(key));
    }

    default Path readPathFbk(final CharSequence key, final Path fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            return fbk;
        }
        return Path.of(value);
    }

    default Path readPathFbk(final CharSequence key, final String fbk) {
        final String value = readStringFbk(key, null);
        if (value == null) {
            if (fbk == null) {
                return null;
            }
            return Path.of(fbk);
        }
        return Path.of(value);
    }

    /**
     * Considers the value as a {@link #readPath(CharSequence) Path to a File} and reads its content
     *
     * @param key to be read
     * @return content of file as byte[]
     */
    default byte[] readFileToBytes(final CharSequence key) {
        try {
            return Files.readAllBytes(readPath(key));
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
    }

    /**
     * Reads the content of a file as UTF-8
     *
     * @param key to be read
     * @return content of file as String
     */
    default String readFileToString(final CharSequence key) {
        try {
            return Files.readString(readPath(key));
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
    }

    default List<String> readFileToLines(final CharSequence key) {
        try {
            return Files.readAllLines(readPath(key));
        } catch (IOException io) {
            throw new UncheckedIOException(io);
        }
    }
}
