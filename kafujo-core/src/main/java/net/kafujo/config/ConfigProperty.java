/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;


import java.time.LocalDateTime;
import java.util.EnumMap;
import java.util.Objects;

/**
 * stores a key-value pair, its read count and setValue history
 */
class ConfigProperty {

    private final EnumMap<ConfigPropertySource, String> sources = new EnumMap<>(ConfigPropertySource.class);

    private final LocalDateTime registered = LocalDateTime.now();
    private final String key;
    private String value;

    private ConfigPropertySource source;
    private long readCounter = 0;

    /**
     * Creates a Property with the given source. The key is final, but the value might get updates
     * from other sources.
     *
     * @param key
     * @param value
     * @param source
     */
    ConfigProperty(final String key, final String value, final ConfigPropertySource source) {
        this.key = Objects.requireNonNull(key, "ConfigProperty key cannot be null");
        this.value = Objects.requireNonNull(value, "ConfigProperty value cannot be null");
        this.source = Objects.requireNonNull(source, "ConfigProperty source cannot be null");
        sources.put(source, value);
    }

    ConfigProperty(final String key, final String value) {
        this(key, value, ConfigPropertySource.SRC);
    }

    /**
     * Updates the property from a new source. This source must not be used so far.
     *
     * @param value
     * @param source
     */
    void setValue(String value, ConfigPropertySource source) {
        if (sources.containsKey(source)) {
            throw new IllegalArgumentException("Source " + source + " + has already a value: " + sources.get(source));
        }
        this.value = Objects.requireNonNull(value, "ConfigProperty value cannot be null");
        this.source = Objects.requireNonNull(source, "ConfigProperty source cannot be null");
        sources.put(source, value);
    }

    /**
     * <p>Getter for the field <code>key</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKey() {
        return key;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValue() {
        return value;
    }

    /**
     * <p>readValue.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String readValue() {
        readCounter++;
        return value;
    }

    /**
     * <p>Getter for the field <code>source</code>.</p>
     *
     * @return a {@link net.kafujo.config.ConfigPropertySource} object.
     */
    public ConfigPropertySource getSource() {
        return source;
    }

    /**
     * <p>Getter for the field <code>readCounter</code>.</p>
     *
     * @return a long.
     */
    public long getReadCounter() {
        return readCounter;
    }

    /**
     * <p>Getter for the field <code>registered</code>.</p>
     *
     * @return a {@link java.time.LocalDateTime} object.
     */
    public LocalDateTime getRegistered() {
        return registered;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigProperty property = (ConfigProperty) o;
        return Objects.equals(key, property.key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("%-11s %3d reads: %s = %s", sources.keySet(), readCounter, key, value);
    }

}
