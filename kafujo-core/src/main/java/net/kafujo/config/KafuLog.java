package net.kafujo.config;

import org.slf4j.Logger;

/**
 * Utility class for logging issues. Kafujo strictly works with slf4j.
 */
public final class KafuLog {

    // Its an pure utility class - not need for instances
    private KafuLog() {
    }

    /**
     * Logs a line in each log level to see if the logger config works.
     *
     * @param logger the logger to be visualized
     */
    public static void logAllLevels(final Logger logger) {
        logger.error("!! ERROR  -  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  - EEROR !!");
        logger.warn("!! WARN   -  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  -  WARN !!");
        logger.info("!! INFO   -  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  -  INFO !!");
        logger.debug("!! DEBUG  -  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  - DEBUG !!");
        logger.trace("!! TRACE  -  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  - TRACE !!");
        logger.error("!! And a last fake error log ............ mind the colors if you use console !!");
    }

    /**
     * Logs depending on loglevel. If debug, the exception is fully included. If not, only its toString().
     *
     * @param lgr logger to be used
     * @param msg message
     * @param exc throwable
     */
    public static void logThrowableOnDebug(Logger lgr, String msg, Throwable exc) {
        if (lgr.isDebugEnabled()) {
            lgr.debug(msg, exc);
        } else {
            lgr.info("{}: {} ", msg, exc.toString());
        }
    }


}
