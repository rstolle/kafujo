package net.kafujo.config;

import net.kafujo.io.Resources;
import net.kafujo.text.KafuText;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Source to initialize a new TypeProperties from.
 * <p>
 * Two sources are considered equal, if they have the same path.
 */
public class PropertiesSource {

    private static int COUNT = 0;
    private final int count = ++COUNT;

    private final String resource;
    private final boolean xml;
    private final Path path;  // can be null which makes this source not available


    public static PropertiesSource ofFile(Path path) {
        return new PropertiesSource(path, null, false);
    }

    public static PropertiesSource ofFileXml(Path path) {
        return new PropertiesSource(path, null, true);
    }

    public static PropertiesSource ofFile(String path) {
        return new PropertiesSource(path, null, false);
    }

    public static PropertiesSource ofFileXml(String path) {
        return new PropertiesSource(path, null, true);
    }

    public static PropertiesSource ofResource(String path) {
        return new PropertiesSource((Path) null, path, false);
    }

    public static PropertiesSource ofResourceXml(String path) {
        return new PropertiesSource((Path) null, path, true);
    }

    private PropertiesSource(final String path, String resource, boolean xml) {
        this(path == null ? null : Path.of(path), resource, xml);
    }

    private PropertiesSource(Path path, String resource, boolean xml) {
        this.resource = resource;
        this.xml = xml;
        this.path = path;
    }

    public boolean isAvailable() {
        if (path != null) {
            return Files.exists(path);
        }

        if (resource != null) {
            return Resources.ofCurrentThread().isAvailable(resource);
        }

        return false; // neither file nor resource given
    }

    public TypedProperties load() {

        if (!isAvailable()) {
            throw new IllegalStateException("Source cannot be loaded cause its not available: " + this);
        }


        if (path != null) {
            return xml ? TypedProperties.loadFileXml(path) : TypedProperties.loadFile(path);
        }

        return xml ? TypedProperties.loadResourceXml(resource) : TypedProperties.loadResource(resource);
    }


    @Override
    public String toString() {
        String xmlStr = xml ? "' (xml)" : "' (properties)";
        if (!isAvailable()) {
            xmlStr += " *** " + KafuText.UNIVERSAL_NA + " ***";
        }
        if (resource != null) {
            return "[" + count + "] RESOURCE '" + resource + xmlStr;
        } else {
            return "[" + count + "] FILE '" + path + xmlStr;
        }
    }

    /**
     * Same path, same source.
     *
     * @param o object to be compared with
     * @return true if same path.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropertiesSource that = (PropertiesSource) o;
        return resource == that.resource &&
                xml == that.xml &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resource, xml, path);
    }
}
