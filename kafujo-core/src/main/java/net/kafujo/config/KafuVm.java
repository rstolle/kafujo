/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;

import net.kafujo.base.RequirementException;
import net.kafujo.units.DataSize;
import net.kafujo.units.KafuDateTime;
import net.kafujo.units.KafuDuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.util.Locale;
import java.util.Objects;

/**
 * Utility class concerning the jvm.
 *
 * @author rstolle
 * @since 0.0.7
 */
public final class KafuVm {

    // Its an pure utility class - not need for instances
    private KafuVm() {
    }

    private static Logger lgr = LoggerFactory.getLogger(KafuVm.class);

    private static final Instant VM_STARTED = Instant.ofEpochMilli(ManagementFactory.getRuntimeMXBean().getStartTime());

    /**
     * Gets the {@link Locale#getDefault() default Locale} of the vm or throws if there is none. The rare negative
     * case will be logged to info level.
     *
     * @return the default locale
     * @throws RequirementException if the default locale or its language are null.
     */
    public static Locale getDefaultLocale() {
        final Locale locale = Locale.getDefault();
        if (locale == null) {
            // this might actually happen:
            // https://stackoverflow.com/questions/44188340/java-8-locale-getdefault-returning-null
            lgr.info("UNEXPECTED: Locale.getDefault() == null");
            throw new RequirementException("Locale.getDefault() == null");
        }
        return locale;
    }

    /**
     * Gets the default language of the vm or throws if there is none. The unlikely negative
     * case will be logged to info level.
     *
     * @return the default locale language
     * @throws RequirementException if the default locale or its language are null.
     */
    public static String getDefaultLanguage() {
        final String lang = getDefaultLocale().getLanguage();

        if (lang == null) {
            lgr.info("UNEXPECTED: Locale.getDefault().getDefaultLanguage() == null");
            throw new RequirementException("Locale.getDefault().getDefaultLanguage() == null");
        }
        return lang;
    }

    public static Locale getDefaultLocaleFbk(final Locale fbk) {
        final Locale locale = Locale.getDefault();
        if (locale == null) {
            return fbk;
        }
        return locale;
    }


    /**
     * A NPE safe way to get the language of the given {@link Locale}
     *
     * @param locale Locale to be queried
     * @param fbk    will be returned, if there is no default language (null)
     * @return the default language string or def
     */
    public static String getLanguageFbk(final Locale locale, final String fbk) {
        if (locale != null && locale.getLanguage() != null) {
            return locale.getLanguage();
        }
        return fbk;
    }

    /**
     * A safe way to get the {@link Locale#getDefault() default language} of this vm.
     *
     * @param fbk will be returned, if there is no default language (null)
     * @return the default language string or def
     */
    public static String getDefaultLanguageFbk(final String fbk) {
        Objects.requireNonNull(fbk, "REQUIRE fallback");
        final Locale locale = Locale.getDefault();
        if (locale != null && locale.getLanguage() != null) {
            return locale.getLanguage();
        }
        return fbk;
    }

    /**
     * Creates a String containing {@link Locale#getDefault() default Locale}
     * and {@link SystemProperty#createUserLocaleFbk(String, String)}. Whats not available will be "N/A".
     *
     * @return detailed locale info
     */
    public static String getInfoLocale() {
        return "default: " + getDefaultLocaleFbk(new Locale("N/A")) +
                " (user system properties: " + SystemProperty.createUserLocaleFbk("N/A", "N/A") + ')';
    }

    /**
     * Wrapper for {@link Runtime#maxMemory()}
     *
     * @return "Maximum amount of memory that the Java virtual machine will attempt to use."
     */
    public static DataSize getMaxMemory() {
        return DataSize.of(Runtime.getRuntime().maxMemory());
    }

    /**
     * Wrapper for {@link Runtime#freeMemory()}
     *
     * @return "The total amount of memory currently available for current and future objects."
     */
    public static DataSize getFreeMemory() {
        return DataSize.of(Runtime.getRuntime().freeMemory());
    }

    /**
     * Wrapper for {@link Runtime#totalMemory()}
     *
     * @return "Amount of free memory in the Java Virtual Machine."
     */
    public static DataSize getTotalMemory() {
        return DataSize.of(Runtime.getRuntime().totalMemory());
    }

    /**
     * All you need to know about the running VM: {@link SystemProperty#combineVmInfo()} and the OS process pid.
     *
     * @return info string about the running java vm
     */
    public static String getInfo() {
        return SystemProperty.combineVmInfo() + " [PID=" + getProcessPid() + ']';
    }

    /**
     * {@link SystemProperty#combineVmInfo()}, the OS process pid and the uptime of this vm
     *
     * @return info string about the running java vm
     */
    public static String getInfoAndUptime() {
        return SystemProperty.combineVmInfo() + " [PID=" + getProcessPid() + "] up since " + KafuDuration.adaptUnits(getUptime());
    }

    /**
     * {@link SystemProperty#combineVmInfo()}, the OS process pid and the uptime of this vm
     *
     * @return info string about the running java vm
     */
    public static String getInfoAndStarted() {
        return SystemProperty.combineVmInfo() + " [PID=" + getProcessPid() + "] up since " + KafuDateTime.full(VM_STARTED);
    }

    /**
     * The {@link RuntimeMXBean#getStartTime() approximate time} the vm was started.
     * <p>
     * https://stackoverflow.com/questions/817801/time-since-jvm-started
     *
     * @return the approximate Instant the vm was started
     */
    public static Instant getStarted() {
        return VM_STARTED;
    }

    /**
     * VM uptime, the difference between {@link #VM_STARTED} and {@link Instant#now()}
     *
     * @return the uptime of this vm.
     */
    public static Duration getUptime() {
        return Duration.between(VM_STARTED, Instant.now());
    }

    /**
     * The OS process pid of the running vm.
     *
     * @return pid
     */
    public static long getProcessPid() {
        // since java 9 its pretty much needless
        return ProcessHandle.current().pid();
    }

    /**
     * The OS user who started this running vm and its Cpu Duration
     *
     * @return info string
     */
    public static String getProcessInfo() {
        ProcessHandle.Info info = ProcessHandle.current().info();
        String val = "VM Process started ";

        var started = info.startInstant();
        if (started.isPresent()) {
            val += KafuDateTime.full(started.get());
        }

        var user = info.user();
        if (user.isPresent()) {
            val += " by " + user.get();
        } else {
            val += " by N/A";
        }

        var cpu = info.totalCpuDuration();

        if (cpu.isPresent()) {
            val += " (" + KafuDuration.adaptUnits(cpu.get()) + " total cpu duration by uptime of " + KafuDuration.adaptUnits(getUptime()) + ')';
        }

        return val;
    }


    /**
     * {@link #getFreeMemory() Free}, {@link #getTotalMemory() total} and {@link #getMaxMemory() max} memory in one String.
     *
     * @return full memory info
     */
    public static String getMemoryInfo() {
        final var total = getTotalMemory();
        final var max = getMaxMemory();

        final BigDecimal percentage = total.toBigDecimal()
                .multiply(BigDecimal.valueOf(100))
                .divide(max.toBigDecimal(), RoundingMode.FLOOR);

        return String.format("%s free; %s total; %s max; %s%% of available memory allocated",
                getFreeMemory(), total, max, percentage.setScale(2, RoundingMode.FLOOR));
    }

    public static void main(String[] args) {
        System.out.println(getMemoryInfo());
    }
}
