/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.config;

import net.kafujo.io.KafuInput;
import net.kafujo.units.KafuDateTime;
import net.kafujo.units.KafuDuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class KafuOs {

    // Its an pure utility class - not need for instances
    private KafuOs() {
    }

    /**
     * Executes the given command and returns its stdout/stderr as a nicely packed String.
     *
     * @param command to be executed
     * @param options parameter
     * @return stdout/err of the given command
     */
    public static String execute(final Path command, final String... options) {
        Objects.requireNonNull(command, "REQUIRE command to execute");
        var list = new ArrayList<String>(options.length + 1);
        list.add(command.toString());
        list.addAll(Arrays.asList(options));
        return execute(list);
    }

    /**
     * Executes the command with the given parameters.
     *
     * @param commandAndOptions command and parameters to be executed
     * @return a String with the stdout. If there is stderr as well, its coming in a separate part overlined by STDERR
     * @throws NullPointerException if {@code commandAndOptions} is null
     * The same for any excecption to happen.
     */
    public static String execute(final List<String> commandAndOptions) {

        Objects.requireNonNull(commandAndOptions, "REQUIRE command (and options");
        // todo: what to do with empty list?
        StringBuilder sb = new StringBuilder();
        try {
            final ProcessBuilder pb = new ProcessBuilder(commandAndOptions);
            final Process p = pb.start();
            final String stdout = KafuInput.asString(p.getInputStream());
            final String stderr = KafuInput.asString(p.getErrorStream());
            if (!StringUtils.isBlank(stdout)) {
                sb.append('\n').append(stdout);
            }

            if (!StringUtils.isBlank(stderr)) {
                sb.append("\n\n")
                        .append(StringUtils.leftPad(" STDERR:", 100, "> "))
                        .append('\n').append(stderr);
            }
        } catch (final RuntimeException | IOException ioAndRt) {
            sb.append("\n\n").append(StringUtils.leftPad(" " + ioAndRt.getClass().getName() + ":", 100, "> ")).append('\n');
            if (ioAndRt.getMessage() != null) {
                sb.append(ioAndRt.getMessage());
            } else {
                sb.append(ioAndRt);
            }
        }
        return sb.toString();
    }

    public static String executePretty(final List<String> commandAndOptions) {
        Objects.requireNonNull(commandAndOptions, "REQUIRE commandAndOptions");

        if (commandAndOptions.isEmpty()) {
            throw new IllegalStateException("command cannot be empty");
        }

        final var now = LocalDateTime.now();

        StringBuilder sb = new StringBuilder(StringUtils.leftPad(" " + KafuDateTime.full(now) + "  EXECUTE OS COMMAND", 100, "> "))
                .append("\n> ").append(commandAndOptions.get(0));
        for (int idx = 1; idx < commandAndOptions.size(); idx++) {
            sb.append(' ').append(commandAndOptions.get(idx));
        }
        sb.append('\n').append(execute(commandAndOptions));

        var took = KafuDuration.adaptUnits(Duration.between(now, LocalDateTime.now()));

        String done = " EXECUTED '" + commandAndOptions.get(0) + "'  " + took;
        sb.append("\n\n").append(StringUtils.leftPad(done, 100, "< ")).append("\n\n");
        return sb.toString();
    }

    public static String executePretty(final String command) {
        Objects.requireNonNull(command, "REQUIRE command to execute");
        return executePretty(Arrays.asList(command.split(" ")));
    }

    /**
     * A simple test, if the current OS is an Windows OS. If more precise informations are required, use the
     * {@link SystemUtils} methods of the Apache Commons Lang project.
     *
     * @return if the underlying OS is Windows, false if not or not sure.
     */
    public static boolean isWindows() {
        var osName = SystemProperty.OS_NAME.asString();
        return osName.toLowerCase().contains("windows");
    }
}
