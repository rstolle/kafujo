package net.kafujo.config;

import net.kafujo.base.RequirementException;
import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


/**
 * A pragmatic example of a {@link TypedReader} and a massive improvement of {@link Properties}. Static loaders
 * and type safe read methods make it very convenient to use. Some quirks of {@link Properties} are eliminated as
 * well. Its not possible anymore to create a  "compromised" Properties object anymore.
 *
 * @author rstolle
 * @since 0.3.0
 */
public class TypedProperties extends Properties implements TypedReader {

    private List<PropertiesSource> sources = new LinkedList<>();

    /**
     * See {@link PropertiesSourceCollector}.
     *
     * @param first   make sure, there is at least one source
     * @param sources add as many as needed
     * @return collector to collect property sources
     */
    public static PropertiesSourceCollector collectSources(final PropertiesSource first, final PropertiesSource... sources) {
        return new PropertiesSourceCollector(first, sources);
    }

    public static TypedProperties load(PropertiesSource source) {
        Objects.requireNonNull(source, "REQUIRE source");
        if (!source.isAvailable()) {
            throw new IllegalArgumentException("Properties source is not available: " + source);
        }
        return source.load();
    }

    public static TypedProperties loadFile(final String path) {
        Objects.requireNonNull(path, "REQUIRE path to property file as String");
        return loadFile(Path.of(path));
    }

    public static TypedProperties loadFile(final Path path) {
        KafuFile.requireExists(path);
        final TypedProperties tProps = new TypedProperties();
        try (var in = Files.newInputStream(path)) {
            tProps.load(in);
            tProps.sources.add(PropertiesSource.ofFile(path.toAbsolutePath()));
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return tProps;
    }

    public static TypedProperties loadFileXml(final String path) {
        Objects.requireNonNull(path, "REQUIRE path to xml property file as String");
        return loadFileXml(Path.of(path));
    }

    public static TypedProperties loadFileXml(final Path path) {
        KafuFile.requireExists(path);
        final TypedProperties tProps = new TypedProperties();
        try (var in = Files.newInputStream(path)) {
            tProps.loadFromXML(in);
            tProps.sources.add(PropertiesSource.ofFileXml(path.toAbsolutePath()));
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return tProps;
    }

    public static TypedProperties loadResource(final String name) {
        Objects.requireNonNull(name, "REQUIRE name of properties resource");
        final TypedProperties tProps = new TypedProperties();
        try (InputStream in = Resources.ofCurrentThread().asStream(name)) {
            tProps.load(in);
            tProps.sources.add(PropertiesSource.ofResource(name));
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return tProps;

    }

    public static TypedProperties loadResourceXml(final String name) {
        Objects.requireNonNull(name, "REQUIRE name of xml properties resource");
        final TypedProperties tProps = new TypedProperties();
        try (InputStream in = Resources.ofCurrentThread().asStream(name)) {
            tProps.loadFromXML(in);
            tProps.sources.add(PropertiesSource.ofResourceXml(name));
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return tProps;
    }

    public TypedProperties() {
    }

    /**
     * Takes over ALL Elements of the underlying Map of {@code takeOver}
     *
     * @param takeOverProperties properties to be transfered
     */
    public TypedProperties(final Properties takeOverProperties) {
        this.takeOver(takeOverProperties);
    }

    public void takeOver(Properties takeOver) {
        Objects.requireNonNull(takeOver, "REQUIRE takeOver Properties");
        takeOver.keySet().forEach(key -> this.put(key, takeOver.get(key)));
    }

    public void takeOver(PropertiesSource source) {
        takeOver(source.load());
        sources.add(source);
    }


    /**
     * @return the name and type of the property file, when crated by one of the load methods,
     */
    public String getLoadedInfo() {
        final var fileInfo = new StringBuilder("Sources: ").append(sources.size());

        for (PropertiesSource src : sources) {
            fileInfo.append("\n ").append(src);
        }
        return fileInfo.toString();
    }

    @Override
    public boolean isAvailable(CharSequence key) {
        Objects.requireNonNull(key, "REQUIRE key");
        return get(key.toString()) != null;
    }

    /**
     * @param key to be read
     * @return the value of {@code key} as string. Behaves different from {@link Properties#getProperty(String)}
     * todo explain property trap!
     */
    @Override
    public String readString(CharSequence key) {
        Objects.requireNonNull(key, "REQUIRE key");
        final Object value = get(key.toString());

        if (value == null) {
            throw new RequirementException("There is no value for key: " + key);
        }
        return value.toString();
    }

    @Override
    public String readStringFbk(CharSequence key, CharSequence fbk) {
        Objects.requireNonNull(key, "REQUIRE key");
        final Object value = get(key.toString());

        if (value != null) {
            return value.toString();
        }
        return fbk == null ? null : fbk.toString();
    }

    /**
     * Addresses the unfortunate behavior of {@link Properties}, that you can call
     * {@link Properties#put(Object, Object) put()}  with any objects, but only Strings
     * are processed correctly. This implementation uses the String representation of its arguments, so
     * setProperty ("bool", "true") would be the same as put ("bool", true) which looks much nicer and is type safe.
     *
     * @param key   a {@link CharSequence}
     * @param value any object. Its toString will be used to store in in the Map.
     * @return the previous value associated with {@code key}
     * @throws NullPointerException     if {@code key} or {@code value} are null
     * @throws IllegalArgumentException if {@code key} is not instanceOf {@link CharSequence}
     */
    @Override
    public synchronized Object put(Object key, Object value) {
        Objects.requireNonNull(key, "REQUIRE key");

        if (!(key instanceof CharSequence)) {
            throw new IllegalArgumentException("key must be a CharSequence");
        }

        Objects.requireNonNull(value, "REQUIRE value");
        // we cannot use setProperty() cause it call (this.)put() -> stack overflow
        return super.put(key.toString(), value.toString());
    }

    /**
     * Stores the properties in a temporary file using {@link Properties#store (OutputStream, String)}.
     *
     * @param comments passed on
     * @return Path to the created property file
     */
    public Path store(String comments) {
        return store(comments, KafuFile.createTempFile());
    }

    /**
     * Wrapper for {@link Properties#store(OutputStream, String)}.
     *
     * @param comments passed on
     * @param path     the file to be created. Will be overwritten if already there!
     * @return Path to the created property file
     */
    public Path store(final String comments, final Path path) {
        try (var out = Files.newOutputStream(path)) {
            super.store(out, comments);
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return path;
    }

    /**
     * Stores the properties in a temporary file using {@link Properties#storeToXML(OutputStream, String)}.
     *
     * @param comments passed on
     * @return Path to the created property file
     */
    public Path storeToXml(String comments) {
        return storeToXml(comments, KafuFile.createTempFile());
    }

    /**
     * Wrapper for {@link Properties#storeToXML(OutputStream, String)}.
     *
     * @param comments passed on
     * @param path     the file to be created. Will be overwritten if already there!
     * @return Path to the created property file
     */
    public Path storeToXml(final String comments, final Path path) {
        try (var out = Files.newOutputStream(path)) {
            super.storeToXML(out, comments);
        } catch (IOException trouble) {
            throw new UncheckedIOException(trouble);
        }
        return path;
    }

    @Override
    public synchronized String toString() {
        final StringBuilder builder = new StringBuilder(size() + " Properties");
        if (!sources.isEmpty()) {
            builder.append("; initially loaded from ").append(getLoadedInfo());
        }
        List<String> sort = new LinkedList<>(stringPropertyNames());
        Collections.sort(sort);

        for (String key : sort) {
            builder.append(String.format("\n  %37s = %s", key, readString(key)));
        }

        return builder.toString();
    }


    public static void main(String[] args) {

        //TypedProperties tp = loadFileXml("d:\\temp\\prop.xml");
        TypedProperties tp2 = TypedProperties.collectSources(
                PropertiesSource.ofFileXml("/b/res/types.xml"),
                PropertiesSource.ofFile("/b/res/types.properties"))
                .useFirstAvailable();
        System.out.println(tp2.toString());
    }

}
