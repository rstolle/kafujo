package net.kafujo.network;


import net.kafujo.base.UncheckedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * Textual representation of a hostname with an optional {@link Port}.
 */
public class Host implements HostIdentifier {

    private static final long serialVersionUID = -6620466287330533952L;

    public final static Logger lgr = LoggerFactory.getLogger(Host.class);

    private final String identifier;
    private final Port port;

    /**
     * Creates a new Host with host and port from url.
     *
     * @param url to be converted. Path will be ignored
     * @return host of url
     */
    public static Host ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract host from");
        return new Host(url.getHost(), Port.ofUrl(url));
    }

    public Host(final String host) {
        this(host, false);
    }

    protected Host(final String host, final boolean ignorePort) {

        KafuNet.requireValidHost(host);

        try {
            URL url = new URL("http://" + host);
            this.identifier = url.getHost().strip();
            if (ignorePort) {
                this.port = Port.NOT_PRESENT;
            } else {
                this.port = Port.ofUrl(url);
            }
        } catch (MalformedURLException cannotHappen) {
            throw new UncheckedException(cannotHappen);   // isValidHost() made sure this cannot happen
        }
    }

    public Host(final String identifier, final Port port) {
        Objects.requireNonNull(identifier, "REQUIRE name of host to construct Host object");
        Objects.requireNonNull(port, "REQUIRE port to construct Host object");

        this.identifier = KafuNet.requireValidHostname(identifier.strip());
        this.port = port;
    }


    @Override
    public Port getPort() {
        return port;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return port.attachTo(identifier);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Host)) return false;
        Host host = (Host) o;
        return identifier.equals(host.identifier) &&
                port.equals(host.port);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier, port);
    }
}
