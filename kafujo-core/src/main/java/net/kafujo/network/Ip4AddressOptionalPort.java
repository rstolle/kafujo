package net.kafujo.network;

import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

/**
 * Ip4 Address with optional port.
 */
public class Ip4AddressOptionalPort extends Host implements Serializable {

    private static final long serialVersionUID = 3769197629954609351L;

    public static Ip4AddressOptionalPort ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract ip4 address and port from");
        return new Ip4AddressOptionalPort(url.getHost(), url.getPort());
    }

    public Ip4AddressOptionalPort(final String host) {
        super(host); // this ctor will split the optional port
        KafuNet.requireValidIp4Address(getIdentifier());
    }

    public Ip4AddressOptionalPort(final String host, final int portNo) {
        this(host, new Port(portNo));
    }

    public Ip4AddressOptionalPort(final String host, Port port) {
        super(host, port);
        KafuNet.requireValidIp4Address(getIdentifier());
    }
}
