package net.kafujo.network;

import java.net.URL;
import java.util.Objects;

/**
 * Represents a plain IP4 address, like 192.168.0.126. Its basically a HostIp4WithOptionalPort which ensures
 * not to have a port.
 */
public class Ip4Address implements HostIdentifierNoPort {

    private static final long serialVersionUID = -1066731858227851076L;

    public static Ip4Address ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract ip4 address from");
        return new Ip4Address(url.getHost());
    }

    private final String ip;

    public Ip4Address(final String ip4address) {
        Objects.requireNonNull(ip4address, "ip4 address String to create Ip4Address object");
        this.ip = KafuNet.requireValidIp4Address(ip4address.trim());
    }

    @Override
    public String getIdentifier() {
        return ip;
    }
}
