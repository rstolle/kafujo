/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.network;

import net.kafujo.base.RequirementException;
import net.kafujo.base.UncheckedException;
import org.apache.commons.lang3.StringUtils;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

/**
 * Utility class concerning network
 *
 * @author rstolle
 * @since 0.1.0
 */
public final class KafuNet {

    // Its an pure utility class - not need for instances
    private KafuNet() {
    }

    /**
     * https://stackoverflow.com/a/106223/7441000
     * ValidIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
     */
    private static final Pattern PATTERN_HOSTNAME = Pattern.compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$");

    // https://stackoverflow.com/a/5667417
    static final Pattern PATTERN_IP4 = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");


    /**
     * Checks if {@code hostname} a valid hostname.
     *
     * @param hostname Host name to be checked. Leading or trailing whitespaces will return false!
     * @return true if {@code hostname} is a valid name or ip address without.
     */
    public static boolean isValidHostname(final String hostname) {
        if (hostname == null || hostname.isBlank() || !hostname.strip().equals(hostname)) {
            return false;
        }

        return PATTERN_HOSTNAME.matcher(hostname).matches();
    }

    public static String requireValidHostname(final String hostname) {
        if (!isValidHostname(hostname)) {
            throw new RequirementException("'" + hostname + "' is not a valid hostname");
        }
        return hostname;
    }

    /**
     * Checks if {@code host} is a valid {@link Hostname} plus optional port.
     *
     * @param host the host as a String, e.g. www.kernel.org:8080
     * @return true, if {@code host} could be converted to a Host object, false if not.
     */
    public static boolean isValidHost(final String host) {

        if (host == null || host.isBlank() || !host.strip().equals(host) || host.contains("@")) {
            // URL cannot deal with '@' in the hostname -> getHost will return empty String!?
            return false;
        }

        if (host.endsWith(":-1")) {
            return false; // would be accepted by URL and thus be valid
        }

        try {
            final URL url = new URL("http://" + host);
            if (StringUtils.isNotEmpty(url.getPath())) {
                return false;
            }

            final int port = url.getPort();
            if (!Port.isValid(port) && port != -1) {
                return false;
            }

            return KafuNet.isValidHostname(url.getHost());

        } catch (MalformedURLException malformed) {
            // lgr.debug("MALFORMED host: " + host, malformed);
            return false;
        }
    }

    public static String requireValidHost(final String host) {
        if (!isValidHost(host)) {
            throw new RequirementException("'" + host + "' is not a valid host");
        }
        return host;
    }

    public static boolean isValidIp4Address(final String ip) {
        if (ip==null || ip.isBlank()) {
            return false;
        }
        return PATTERN_IP4.matcher(ip).matches();
    }

    public static String requireValidIp4Address(final String ip) {
        if (!isValidIp4Address(ip)) {
            throw new IllegalArgumentException("Not a valid IP4 address: " + ip);
        }
        return ip;
    }



    public static String getHostInfo() {
        try {
            return InetAddress.getLocalHost().toString();
        } catch (UnknownHostException e) {
            throw new UncheckedException(e);
        }
    }

    public static String getHostInfoFbk(final String fbk) {
        try {
            return InetAddress.getLocalHost().toString();
        } catch (UnknownHostException ignore) {
            return fbk;
        }
    }

    public static String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            throw new UncheckedException(e);
        }
    }

    public static String getHostNameFbk(final String fbk) {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ignore) {
            return fbk;
        }
    }

    public static String getCanonicalHostName() {
        try {
            return InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            throw new UncheckedException(e);
        }
    }

    public static String getCanonicalHostNameFbk(final String fbk) {
        try {
            return InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException ignore) {
            return fbk;
        }
    }

    public static boolean isValidIp4(String ip) {

        return false;
    }

    public static boolean isValidIp4WithOptionalPort(final String ipWithOptionalPort) {

        return false;
    }

}
