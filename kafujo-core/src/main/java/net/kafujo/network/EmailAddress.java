package net.kafujo.network;


import net.kafujo.base.UncheckedException;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Represents an email address like john@example.com. The goal of this class IS NOT to entirely validate all kind of
 * valid emails, cause this is a pretty hard. (todo: sources)
 * Instead I wanna have a base and a {@link net.kafujo.config.TypedReader readable} class.
 */
public class EmailAddress implements Serializable {

    private static final long serialVersionUID = -1917522446192564518L;

    public static EmailAddress of(final URL url) {
        if ("mailto".equalsIgnoreCase(url.getProtocol())) {
            return new EmailAddress(url.getPath());
        }
        throw new IllegalArgumentException("URL '" + url + "' is not an email address");
    }

    private final String email;

    // https://stackoverflow.com/a/24320945/7441000
    private static final Pattern pattern = Pattern.compile("^.+@.+\\..+$");

    /**
     * Creates an EmailAddress out of a String.
     *
     * @param emailStr string to be a correct email. This will be stripped.
     */
    public EmailAddress(final String emailStr) {
        Objects.requireNonNull(emailStr, "REQUIRE email string to create EmailAddress object from");
        final String stripped = emailStr.strip();

        if (!isValid(stripped)) {
            throw new IllegalArgumentException(emailStr + " is not a valid email");
        }
        this.email = stripped;
    }

    public static boolean isValid(final String email) {

        if (email == null
                || email.strip().length() < 3
                || StringUtils.countMatches(email, '@') != 1
                || email.startsWith("mailto:")
                || email.contains(" ")
                || email.contains("..")
                || email.contains("@-")
                || email.contains("-@")
                || email.contains("@.")
                || email.contains(".@")
                || email.startsWith("@")
                || email.endsWith("@")
                || email.startsWith(".")
                || email.endsWith(".")
                || email.endsWith(";")) {
            return false;
        }

//         var split = email.split("@");

//        fails for email@[123.123.123.123] -> check
//        if (!Hostname.isValid(split[1])) {
//            return false;
//        }

        return pattern.matcher(email).matches();
    }

    /**
     * @return new URL(mailto:email)
     */
    public URL toUrl() {
        try {
            return new URL("mailto:" + email);
        } catch (MalformedURLException trouble) {
            throw new UncheckedException("Could not create url out of " + email, trouble);
        }
    }

    /**
     * Same as {link {@link #toString()}} but makes it totally clear what it does.
     *
     * @return the plain email
     */
    public String getEmailAsString() {
        return email;
    }

    /**
     * @return the plain email
     */
    @Override
    public String toString() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailAddress that = (EmailAddress) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}
