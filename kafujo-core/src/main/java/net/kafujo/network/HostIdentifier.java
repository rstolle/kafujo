package net.kafujo.network;

public interface HostIdentifier extends HostIdentifierNoPort {
    /**
     * @return the ooptional port, meaning port could be {@link Port#NOT_PRESENT}
     */
    Port getPort();
}
