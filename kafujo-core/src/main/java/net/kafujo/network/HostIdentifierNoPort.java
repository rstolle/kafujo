package net.kafujo.network;

import net.kafujo.base.UncheckedException;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Objects;

public interface HostIdentifierNoPort extends Serializable {

    String getIdentifier();

    @SuppressWarnings("all")
    default boolean isResolvable() {
        try {
            InetAddress.getByName(getIdentifier());
            return true;
        } catch (UnknownHostException trouble) {
            return false;
        }
    }

    default URL toUrl(final String protocol) {
        return toUrl(protocol, "");
    }

    default URL toUrl(final String protocol, final String path) {
        Objects.requireNonNull(protocol, "REQUIRE protocol to create url for " + getIdentifier());
        try {
            return new URL(protocol, getIdentifier(), -1, path);
        } catch (MalformedURLException trouble) {
            throw new UncheckedException("could not create url for '" + getIdentifier()
                    + "', protocol " + protocol
                    + " and path " + path, trouble);
        }
    }

    default InetAddress toInetAddress() {
        // final String full = getPort().attachTo(getIdentifier());
        try {
            return InetAddress.getByName(getIdentifier());
        } catch (UnknownHostException trouble) {
            throw new UncheckedException("could not resolve " + getIdentifier(), trouble);
        }
    }


}
