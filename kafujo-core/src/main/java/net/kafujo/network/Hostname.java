package net.kafujo.network;

import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

/**
 * Immutable (textual) representation of a hostname without port. This can be anything, just a computers name,
 * an FQDN or an IP address, as long as it does not have a port.
 */
public class Hostname extends Host implements Serializable, HostIdentifierNoPort {

    private static final long serialVersionUID = 2297332467745164457L;

    /**
     * Creates a hostname out of {@code url} ignoring port and path.
     *
     * @param url of interest
     * @return hostname with host from url.
     */
    public static Hostname ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract hostname from");
        return new Hostname(url.getHost());
    }

    /**
     * Creates a
     *
     * @param host
     */
    public Hostname(final String host) {
        super(host, true);
    }
}
