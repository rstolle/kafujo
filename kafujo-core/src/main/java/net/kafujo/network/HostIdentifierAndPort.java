package net.kafujo.network;

/**
 * Same as {@link HostIdentifier} but {@link HostIdentifier#getPort()} MUST NOT return {@link Port#NOT_PRESENT}
 */
public interface HostIdentifierAndPort extends HostIdentifier {

}
