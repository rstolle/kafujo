package net.kafujo.network;


import java.net.URL;
import java.util.Objects;

/**
 * Textual representation of a hostname with an optional {@link Port}.
 */
public class HostAndPort extends Host implements HostIdentifierAndPort {

    private static final long serialVersionUID = 6195017788689432616L;

    /**
     * Creates a new Host with host and port from url.
     *
     * @param url to be converted. Path will be ignored
     * @return host of url
     */
    public static HostAndPort ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract host from");
        return new HostAndPort(url.getHost(), Port.ofUrl(url));
    }

    public HostAndPort(final String host) {
        super(host);
        Port.requirePresent(getPort());
    }


    public HostAndPort(final String identifier, final Port port) {
        super(identifier, port);
        Port.requirePresent(getPort());
    }
}
