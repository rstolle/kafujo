package net.kafujo.network;

import net.kafujo.base.RequirementException;
import net.kafujo.base.UncheckedException;
import net.kafujo.text.KafuText;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * A {@link Number} representing a port, meaning it must be between {@link #MIN_PORT_NO} and {@link #MAX_PORT_NO}
 * (both inclusive). This class also covers the case, if there is no explicit port given ({@link #NOT_PRESENT}).
 * This is internally done by using -1 as in {@link URL}.
 * <p>
 * Look https://en.wikipedia.org/wiki/Port_(computer_networking) for more background.
 * <p>
 * There are definitions for some standard ports as well.
 *
 * @author rstolle
 * @see Port
 * @since 0.3.0
 */
public class Port extends Number implements Serializable {

    private static final long serialVersionUID = -8434081743063482592L;

    /**
     * The smallest possible port.
     */
    public static final int MIN_PORT_NO = 0;

    /**
     * System ports range from 0 to 1023 (0x0 to 0x3FF).
     */
    public static final int MAX_SYSTEM_PORT_NO = 1023;

    /**
     * The ports between 1024 to 49151 (0x400 to 0xBFFF) are called user ports.
     */
    public static final int MAX_USER_PORT_NO = 49151;

    /**
     * The max port number. At the same time the end of range for dynamic ports (49152 to 65535; 0xC000 to 0xFFFF)
     */
    public static final int MAX_PORT_NO = 65535;

    /**
     * Matching to {@link URL} if the port is not set. Its {@link #toString() String representation} is
     * {@link net.kafujo.text.KafuText#UNIVERSAL_NA}, which can also be used for {link {@link #of(String)}}
     * and therefore in configuration files to be read by {@link net.kafujo.config.TypedReader}
     */
    public static final Port NOT_PRESENT = new Port(-1);

    /**
     * File Transfer Protocol (FTP) Data Transfer
     */
    public static final Port FTP = new Port(20);

    /**
     * File Transfer Protocol (FTP) Command Control
     */
    public static final Port FTPCTL = new Port(21);

    /**
     * Secure Shell (SSH) Secure Login
     */
    public static final Port SSH = new Port(22);

    /**
     * Telnet remote login service, unencrypted text messages
     */
    public static final Port TELNET = new Port(23);

    /**
     * Domain Name System (DNS) service.
     */
    public static final Port DNS = new Port(53);

    /**
     * Network News Transfer Protocol (NNTP).
     */
    public static final Port NNTP = new Port(119);

    /**
     * Network Time Protocol (NTP).
     */
    public static final Port NTP = new Port(123);

    /**
     * Internet Relay Chat (IRC).
     */
    public static final Port IRC = new Port(194);

    /**
     * Hypertext Transfer Protocol (HTTP) used in the World Wide Web.
     */
    public static final Port HTTP = new Port(80);

    /**
     * HTTP Secure (HTTPS) HTTP over TLS/SSL
     */
    public static final Port HTTPS = new Port(443);

    /**
     * Simple Mail Transfer Protocol (SMTP) E-mail routing.
     */
    public static final Port SMTP = new Port(25);

    /**
     * Post Office Protocol.
     */
    public static final Port POP2 = new Port(109);

    /**
     * Post Office Protocol.
     */
    public static final Port POP3 = new Port(110);

    /**
     * Internet Message Access Protocol (IMAP) Management of digital mail.
     */
    public static final Port IMAP = new Port(143);

    /**
     * Internet Message Access Protocol over TLS/SSL (IMAPS).
     */
    public static final Port IMAPS = new Port(993);

    /**
     * Default port for mysql/mariaDB.
     */
    public static final Port MYSQL = new Port(3306);
    /**
     * Default port for PostgeSQL.
     */
    public static final Port POSTGRESQL = new Port(5432);

    /**
     * Default port for MS SQL Server
     */
    public static final Port SQLSERVER = new Port(1433);

    private final int portNo; // short would be -32000 to + 32000

    /**
     * The only constructor creates Port objects out of int values.
     *
     * @param portNo from {@link #MIN_PORT_NO} to {@link #MIN_PORT_NO}, or -1 to create
     *               an {@link #NOT_PRESENT} port.
     * @throws IllegalArgumentException if {@code portNo} is not in the described range.
     */
    public Port(final int portNo) {
        if (!isValid(portNo) && portNo != -1) {
            throw new IllegalArgumentException("INVALID PORT: " + portNo + "; must be -1, 65535 or in between");
        }
        this.portNo = portNo;
    }

    /**
     * Checks an int to be a proper port value.
     *
     * @param portNo port number to be checked
     * @return true if {@code portNo} is between {@link #MIN_PORT_NO} (including) and {@link #MIN_PORT_NO} (including),
     * otherwise false.
     */
    public static boolean isValid(final int portNo) {
        return portNo >= MIN_PORT_NO && portNo <= MAX_PORT_NO;
    }

    /**
     * Creates a new Port out of a String.
     *
     * @param portStr A {@link #isValid(int) valid port number} or "-1" or {@link KafuText#UNIVERSAL_NA} to get
     *                {@link #NOT_PRESENT}
     * @return {@link Port} with the given port number.
     * @throws IllegalArgumentException if {@code portStr} doesn't contain a valid port.
     */
    public static Port of(final String portStr) {
        Objects.requireNonNull(portStr, "REQUIRE port String representation to create Port object");
        if (KafuText.equalsUniversalNotAvailable(portStr) || portStr.equals("-1")) {
            return NOT_PRESENT;
        }
        return new Port(Integer.parseInt(portStr));
    }

    public static Port of(Host host) {
        Objects.requireNonNull(host, "REQUIRE host to fetch port from");
        return host.getPort();
    }

    /**
     * Extracts the port out of an {@link URL}.
     *
     * @param url url to extract port from
     * @return the port of {@code url}
     * @throws IllegalArgumentException if url contains an invalid port, which is totally possible.
     */
    public static Port ofUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to extract port from");
        return new Port(url.getPort());
    }

    public static Port ofUrl(final String url) {
        Objects.requireNonNull(url, "REQUIRE url to extract port from");
        try {
            return ofUrl(new URL(url));
        } catch (MalformedURLException fail) {
            throw new IllegalArgumentException(fail);
        }
    }

    public boolean isEmpty() {
        return portNo == -1;
    }

    public boolean isPresent() {
        return portNo != -1;
    }

    public boolean isSystemPort() {
        return portNo >= MIN_PORT_NO && portNo <= MAX_SYSTEM_PORT_NO;
    }

    public boolean isUserPort() {
        return portNo > MAX_SYSTEM_PORT_NO && portNo <= MAX_USER_PORT_NO;
    }

    public boolean isDynamicPort() {
        return portNo > MAX_USER_PORT_NO && portNo <= MAX_PORT_NO;
    }

    @Override
    public int intValue() {
        return portNo;
    }

    @Override
    public long longValue() {
        return portNo;
    }

    @Override
    public float floatValue() {
        return portNo;
    }

    @Override
    public double doubleValue() {
        return portNo;
    }

    /**
     * @return the port as a String or {@link KafuText#UNIVERSAL_NA "N/A"} if port is {@link #NOT_PRESENT}
     */
    @Override
    public String toString() {
        return this.equals(NOT_PRESENT) ? KafuText.UNIVERSAL_NA : Integer.toString(portNo);
    }

    /**
     * @return an appendable port like ':8080' or "" if the port is {@link #NOT_PRESENT}
     */
    public String toHostAttachment() {
        if (this.isEmpty()) {
            return "";
        }
        return ":" + portNo;
    }

    /**
     * Adds this port to the given {@link URL} if its port is not set.
     * <p>
     * Note: this method will not change existing ports!
     *
     * @param url URL to be modified.
     * @return {@code url:port} if the port of {@code url} is {@link #NOT_PRESENT} or  {@code url} as it is, if its port is
     * equal to this.
     * @throws IllegalArgumentException if {@code url} has already a port set different form this.
     * @throws IllegalArgumentException if  {@code url} is not a valid url
     */
    public String attachToUrl(final String url) {
        Objects.requireNonNull(url, "REQUIRE url to attach port " + this);
        final Port current = Port.ofUrl(url);

        if (current.equals(this)) {
            return url;
        }

        if (current.isEmpty()) {
            return url + ":" + portNo;
        }

        // now current is not empty and both are not equal
        throw new IllegalArgumentException("Don't want to change existing port of " + url + " to " + this);
    }

    public URL attachToUrl(final URL url) {
        Objects.requireNonNull(url, "REQUIRE url to attach port " + this);
        final Port current = new Port(url.getPort());

        if (current.equals(this)) {
            return url;
        }

        if (current.isEmpty()) {
            try {
                return new URL(url.getProtocol(), url.getHost(), this.portNo, url.getFile());
            } catch (MalformedURLException malformed) {
                throw new UncheckedException("Cannot happen", malformed);
            }
        }
        // now current is not empty and both are not equal
        throw new IllegalArgumentException("Don't want to change existing port of " + url + " to " + this);
    }

    /**
     * Attaches this port to the given hostname.
     *
     * @param hostname hostname to be extended by port.
     * @return {@link Host}
     */
    public Host attachTo(final Hostname hostname) {
        Objects.requireNonNull(hostname, "REQUIRE hostname to attach port '" + this + "' to");
        return new Host(hostname.toString(), this);
    }

    /**
     * Attaches this port to the given host, if there is no other port attached already
     *
     * @param host host to be extended by port.
     * @return {@link Host}
     */
    public Host attachTo(final Host host) {
        Objects.requireNonNull(host, "REQUIRE host to attach port '" + this + "' to");
        if (this.equals(host.getPort())) {
            return host;
        }

        if (host.getPort().isEmpty()) {
            return new Host(host.getIdentifier(), this);
        }

        // now there is only host.port != EMPTY but different from this
        throw new IllegalArgumentException("Don't want to change existing port of " + host + " to " + this);
    }

    /**
     * Attaches this port to {@code arbitrary}. There will be no checks for {@code arbitrary}, it even can be null.
     * To make sure to work with proper hostnames, use {@link #attachTo(Hostname)}.
     *
     * @param arbitrary String to be extended by ':port'
     * @return {@code arbitrary:port}
     */
    public String attachTo(final String arbitrary) {
        return arbitrary + this.toHostAttachment();
    }

    /**
     * intellij generated
     *
     * @param o Object to compare with
     * @return true if equals, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Port port1 = (Port) o;
        return portNo == port1.portNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(portNo);
    }

    public static Port requirePresent (Port port){
        if (port == null || port.isEmpty()) {
            throw new RequirementException("REQUIRE port!");
        }
        return port;
    }
}
