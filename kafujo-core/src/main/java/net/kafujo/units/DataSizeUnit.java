/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * Defines all possible datasize units.
 * <p>
 * These are missinge, cause they would be too big for long a the base datatype:
 * <ul>
 * <li>ZB("Zettabyte", "Zettabytes", EB.size.times(1000))</li>
 * <li>YB("Yottabyte", "Yottabytes", ZB.size.times(1000))</li>
 * <li>ZiB("Zebibyte", "Zebibytes", EiB.size.times(1024))</li>
 * <li>YiB("Yobibyte", "Yobibytes", ZiB.size.times(1024))</li>
 * </ul>
 */
public enum DataSizeUnit implements Comparable<DataSizeUnit> {
    Byte("Byte", "DataSize", DataSize.of(1), true),
    KB("Kilobyte", "Kilobytes", Byte.size.times(1000), false),
    KiB("Kibibyte", "Kibibytes", Byte.size.times(1024), true),

    MB("Megabyte", "Megabytes", KB.size.times(1000), false),
    MiB("Mebibyte", "Mebibytes", KiB.size.times(1024), true),

    GB("Gigabyte", "Gigabytes", MB.size.times(1000), false),
    GiB("Gibibyte", "Gibibytes", MiB.size.times(1024), true),

    TB("Terabyte", "Terabytes", GB.size.times(1000), false),
    TiB("Tebibyte", "Tebibytes", GiB.size.times(1024), true),

    PB("Petabyte", "Petabytes", TB.size.times(1000), false),
    PiB("Pebibyte", "Pebibytes", TiB.size.times(1024), true),

    EB("Exabyte", "Exabytes", PB.size.times(1000), false),
    EiB("Exbibyte", "Exbibytes", PiB.size.times(1024), true);

    public final DataSize size;
    public final String name;
    public final String namePlural;
    private final boolean iec;

    DataSizeUnit(String name, String namePlural, DataSize dataSize, boolean iec) {
        this.name = name;
        this.namePlural = namePlural;
        this.size = dataSize;
        this.iec = iec;
    }

    /**
     * Converts a data size given by a value to DataSize.
     *
     * @param value - the size of the data in THIS unit
     * @return the same value as DataSize
     * @throws IllegalArgumentException if the resulting DataSize is bigger than {@link DataSize#MAX_VALUE}
     * @throws NullPointerException     if value is null
     */
    DataSize convert(final BigDecimal value) {
        Objects.requireNonNull(value, "BigDecimal value REQUIRED");
        BigDecimal bytes = value.multiply(size.toBigDecimal());

        if (bytes.compareTo(KafuNumber.MAX_LONG_BDEC) > 0) {
            throw new IllegalArgumentException("value too big to be converted to DataSize: " + bytes + " Bytes");
        }

        if (bytes.compareTo(KafuNumber.MIN_LONG_BDEC.add(BigDecimal.ONE)) < 0) {
            throw new IllegalArgumentException("byte value too small to be converted to DataSize: " + bytes + " Bytes");
        }

        return DataSize.of(bytes.longValue());
    }

    /**
     * @param bytes to be formated
     * @return formatted String representing this
     */
    public String format(final long bytes) {
        return format(bytes, DataSize.DEFAULT_SCALE);
    }


    /**
     * @param bytes to be formated
     * @param scale precision
     * @return formatted String representing this
     */
    public String format(final long bytes, int scale) {
        return format(DataSize.of(bytes), scale);
    }

    /**
     * @param val to be formated
     * @return formatted String representing this
     */
    public String format(DataSize val) {
        return format(val, DataSize.DEFAULT_SCALE);
    }

    /**
     * @param val   to be formated
     * @param scale precision
     * @return formatted String representing this Datasize
     */
    public String format(DataSize val, int scale) {
        if (this == Byte) {
            scale = 0;
        }
        BigDecimal valOut = val.toBigDecimal().divide(this.size.toBigDecimal(), scale, RoundingMode.HALF_DOWN);
        return String.format("%s %s", valOut.toPlainString(), this);
    }

    /**
     * @param dataSize to be formated
     * @param scale    precision
     * @return formatted String in long form representing this Datasize
     */
    public String formatLong(final DataSize dataSize, int scale) {
        if (this == Byte) {
            scale = 0;
        }

        BigDecimal valOut = dataSize.toBigDecimal().divide(this.size.toBigDecimal(), scale, RoundingMode.HALF_DOWN);
        if (valOut.compareTo(BigDecimal.ONE) == 0) { //  valOut.equals(BigDecimal.ONE) works only for "1!, not for "1.0"
            return String.format("%s %s", valOut.toPlainString(), this.name);
        } else {
            return String.format("%s %s", valOut.toPlainString(), this.namePlural);
        }
    }

    public String formatLong(final DataSize ds) {
        return formatLong(ds, DataSize.DEFAULT_SCALE);
    }

    public String formatLong(final long bytes) {
        return formatLong(bytes, DataSize.DEFAULT_SCALE);
    }

    public String formatLong(final long bytes, int scale) {
        return formatLong(DataSize.of(bytes), scale);
    }

    /**
     * Is this unit based on 1024
     *
     * @return true if IEC format
     */
    public boolean isIec() {
        return iec;
    }

    /**
     * Is this unit based on 1000
     *
     * @return true if IS format
     */
    public boolean isIs() {
        if (this == Byte) {
            return true;
        }
        return !iec;
    }

}
