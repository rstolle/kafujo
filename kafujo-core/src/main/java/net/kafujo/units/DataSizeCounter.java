/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import java.io.Serializable;
import java.util.Objects;

/**
 * A combination of a number of Items (in most cases files) and their total {@link DataSize}
 */

public class DataSizeCounter implements Serializable {
    private long byteCount = 0;
    private long itemCount = 0;

    public DataSizeCounter add(final DataSize file) {
        byteCount += file.longValue();
        itemCount++;
        return this;
    }

    public DataSizeCounter add(final DataSizeCounter other) {
        byteCount += other.byteCount;
        itemCount += other.itemCount;
        return this;
    }

    public DataSize getDataSize() {
        return DataSize.of(byteCount);
    }

    public long getItemCount() {
        return itemCount;
    }

    @Override
    public String toString() {
        return "counted " + itemCount + " items of total " + getDataSize();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataSizeCounter that = (DataSizeCounter) o;
        return byteCount == that.byteCount &&
                itemCount == that.itemCount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(byteCount, itemCount);
    }
}
