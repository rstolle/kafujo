/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Objects;

/**
 * Helpers dealing with {@link java.time}
 *
 * @author rstolle
 * @since 0.0.6
 */
public final class KafuTime {

    public static final DateTimeFormatter USUSAL_TIME = DateTimeFormatter.ofPattern("hh:mm:ss");
    public static final DateTimeFormatter USUSAL_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * Usual timestamp format {@link DateTimeFormatter#ofPattern(String) yyyy-MM-dd hh:mm:ss}
     */
    public static final DateTimeFormatter USUSAL_TIMESTAMP = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    private static final Logger lgr = LoggerFactory.getLogger(KafuTime.class);

    /**
     * Formats the current local time to {@link DateTimeFormatter#ofPattern(String) hh:mm:ss}
     *
     * @return {@link #ususal(Temporal)} of {@link LocalTime#now()}
     */
    public static String ususal() {
        return ususal(LocalTime.now());
    }

    /**
     * Formats the given time to {@link DateTimeFormatter#ofPattern(String) hh:mm:ss}.
     *
     * @param time time to be formatted
     * @return {@code time} as nicely formatted String
     * @throws NullPointerException             if {@code time} is null
     * @throws UnsupportedTemporalTypeException if time misses required fields for time
     */
    public static String ususal(final Temporal time) {
        Objects.requireNonNull(time, "REQUIRE time to be formatted");
        return USUSAL_TIME.format(time);
    }

    /**
     * Formats the given time to {@link DateTimeFormatter#ofPattern(String) hh:mm:ss}
     * or quietly falls back to the given default string, if {@code time} is null or could not be formatted.
     * There will be a short INFO log, if formatting fails for some reason. The whole problem as well as the null case
     * will be logged in DEBUG level.
     *
     * @param time time to be formatted
     * @param def  default value
     * @return {@code time} formatted or {@code def}, if {@code time} is null or could not be formatted
     */
    public static String ususal(final Temporal time, final String def) {
        if (time == null) {
            lgr.debug("fall back to default '{}', cause time was null", def);
            return def;
        }

        try {
            return ususal(time);
        } catch (final RuntimeException anyRt) {
            if (lgr.isDebugEnabled()) {
                lgr.debug("COULD NOT FORMAT '" + time + "' TO TIME", anyRt);
            } else {
                lgr.info("COULD NOT FORMAT '{}' TO TIME CAUSE {}", time, anyRt.toString());
            }

            return def;
        }
    }

    /**
     * Formats the current local date to {@link DateTimeFormatter#ofPattern(String) yyyy-MM-dd}
     *
     * @return {@link #ususalDate(Temporal)} of {@link LocalDate#now()}
     */
    public static String ususalDate() {
        return ususalDate(LocalDate.now());
    }

    /**
     * Formats the given date to {@link DateTimeFormatter#ofPattern(String) yyyy-MM-dd}.
     *
     * @param date date to be formatted
     * @return {@code date} as nicely formatted String
     * @throws NullPointerException             if {@code date} is null
     * @throws UnsupportedTemporalTypeException if date misses required fields for date
     */
    public static String ususalDate(Temporal date) {
        return USUSAL_DATE.format(date);
    }

    /**
     * Formats the given date to {@link DateTimeFormatter#ofPattern(String) yyyy-MM-dd}
     * or falls back to the given default string, if {@code date} is null or could not be formatted.
     * There will be a short INFO log, if formatting fails for some reason. The whole problem will be logged in
     * DEBUG level as well as the null case.
     *
     * @param date date to be formatted
     * @param def  default value
     * @return {@code date} formatted or {@code def}, if {@code date} is null or could not be formatted
     */
    public static String ususalDate(final Temporal date, final String def) {
        if (date == null) {
            lgr.debug("fall back to default '{}', cause date was null", def);
            return def;
        }

        try {
            return ususalDate(date);
        } catch (final RuntimeException anyRt) {
            if (lgr.isDebugEnabled()) {
                lgr.debug("COULD NOT FORMAT '" + date + "' TO DATE", anyRt);
            } else {
                lgr.info("COULD NOT FORMAT '{}' TO DATE CAUSE {}", date, anyRt.toString());
            }
            return def;
        }
    }
}
