/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import net.kafujo.base.RequirementException;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Handy methods to deal with big numbers, especially to check their ranges
 *
 * @author rstolle
 * @since 0.0.2
 */
public final class KafuNumber {

    // Its an pure utility class - not need for instances
    private KafuNumber() {
    }

    /**
     * {@link Long#MAX_VALUE} as {@link BigDecimal}
     */
    public static final BigDecimal MAX_LONG_BDEC = new BigDecimal(Long.MAX_VALUE);

    /**
     * {@link Long#MIN_VALUE} as {@link BigDecimal}
     */
    public static final BigDecimal MIN_LONG_BDEC = new BigDecimal(Long.MIN_VALUE);

    /**
     * Checks if value is within a specific range.
     *
     * @param value number to be checked (not included)
     * @param begin begin of range (not included)
     * @param end   end of range
     * @return true if {@code value} is between {@code begin} and {@code end}
     */
    public static boolean isBetweenExclusive(final BigDecimal value, final BigDecimal begin, final BigDecimal end) {
        if (begin.compareTo(end) > 0) {
            throw new IllegalArgumentException(begin + " (begin) must smaller than (end) " + end);
        }
        return value.compareTo(begin) > 0 && value.compareTo(end) < 0;
    }

    public static boolean isBetweenInclusive(final BigDecimal value, final BigDecimal start, final BigDecimal end) {
        if (start.compareTo(end) > 0) {
            throw new IllegalArgumentException(start + " (start) must smaller than (end) " + end);
        }
        return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
    }

    public static boolean isLongRange(final BigDecimal value) {
        Objects.requireNonNull(value, "BigDecimal value REQUIRED");
        return isBetweenInclusive(value, MIN_LONG_BDEC, MAX_LONG_BDEC);
    }

    public static void requireLongRange(final BigDecimal value) {
        if (!isLongRange(value)) {
            throw new RequirementException(value + " is not within long range");
        }
    }

    /**
     * Checks if a {@link Number} is zero.
     *
     * @param number number to be checked
     * @return true if {@code number} is zero, false otherwise
     * @throws NullPointerException if {@code number} is {@code null}
     */
    public static boolean isZero(final Number number) {
        Objects.requireNonNull(number, "REQUIRE number");

        if (number instanceof Integer || number instanceof Long || number instanceof Short
                || number instanceof Byte || number instanceof DataSize) {
            return number.longValue() == 0L;
        }

        if (number instanceof BigDecimal) {
            // number.equals(BigDecimal.ZERO) fails sometimes, e.g.  new BigDecimal ("0.000")
            BigDecimal bd = (BigDecimal) number;
            return bd.compareTo(BigDecimal.ZERO) == 0; // Another solution would be: bd.negate().equals(bd)
        }

        // now the simple approach:
        String asString = number.toString();
        if (asString.startsWith("-")) {
            asString = asString.substring(1);
        }

        return asString.equals("0") || asString.equals("0.0");
    }

    /**
     * Checks if a {@link Number} is negative (smaller than zero).
     *
     * @param number number to be checked
     * @return true, if {@code number} is smaller than zero, false if it is zero or bigger
     * @throws NullPointerException if {@code number} is null
     */
    public static boolean isNegative(final Number number) {
        Objects.requireNonNull(number, "REQUIRE number");

        if (isZero(number)) {
            return false;
        }

        return number.toString().stripLeading().startsWith("-");
    }

    public static void requireGreaterThanNull(int number) {
        if (number <= 0) {
            throw new RequirementException("Number > 0: " + number);
        }
    }
}
