/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.kafujo.units.DataSizeUnit.Byte;
import static net.kafujo.units.DataSizeUnit.*;

/**
 * Manages a number of Bytes and offers many ways for displaying and parsing:
 * {@code
 * <p>
 * final DataSize bytes42 = DataSize.of (42); // creation using a number of bytes
 * out.println(bytes42);  // "42 Byte"
 * <p>
 * final DataSize OneKiB = DataSize.of(1, DataSizeUnit.KiB); // 1 KiB
 * out.println(OneKiB);                               // 1.00 KiB
 * out.println(DataSizeUnit.Byte.format(OneKiB));     // 1024 Byte
 * out.println(DataSizeUnit.GiB.format(OneKiB, 20));  // 0.00000095367431640625 GiB
 * <p>
 * // this is especially useful for config files:
 * final DataSize OneKiB_ = DataSize.of ("1 KiB");
 * final DataSize OneKiB__ = DataSize.of ("0.00000095367431640625 GiB"); // exact parsing!
 * <p>
 * out.println(OneKiB.equals(OneKiB_));  // true
 * out.println(OneKiB.equals(OneKiB__)); // true
 * }
 * <p>
 * There are as well arithmetic operations like {@link #times(long)} or {@link #plus(DataSize)}
 * <p>
 * Note: The actual amount of bytes is stored in a long, which can easily be stored in a database. Therefore there are
 * {@link #MAX_VALUE} and {@link #MIN_VALUE}. This should suffice most situations. If bigger values are needed, consider
 * (to implement) BigDataSize with a BigInteger as a base. But then, database storage is not that easy anymore.
 *
 * @author rstolle
 * @since 2018-10-21, 0.0.1
 */
public class DataSize extends Number implements Serializable, Comparable<DataSize> {

    // needs to be initialized before any DataSize.of(String)
    private static final Pattern parser = Pattern.compile("\\p{Alpha}");

    /**
     * The maximal value to be handled. Its {@link Long#MAX_VALUE} {@link DataSizeUnit#Byte Bytes} or
     * <em>7.999999999999999999132638262011596452794037759304046630859375 {@link DataSizeUnit#EiB Exbibyte} </em>
     */
    public static final DataSize MAX_VALUE = of(Long.MAX_VALUE);

    /**
     * The minimal value to be handled: -7.999999999999999999132638262011596452794037759304046630859375
     * {@link DataSizeUnit#EiB Exbibyte}
     * <p>
     * In {@link DataSizeUnit#Byte bytes}: {@link Long#MIN_VALUE Long.MIN_VALUE+1} or
     * {@link Long#MAX_VALUE Long.MAX_VALUE*-1}.
     * </p>
     */
    public static final DataSize MIN_VALUE = of(Long.MIN_VALUE + 1);


    // no unit means byte
    public static final DataSize ZERO = of(0);
    public static final DataSize ONE_BYTE = DataSize.of(1);
    public static final DataSize ONE_KB = DataSize.of(1000);
    public static final DataSize ONE_KiB = DataSize.of(1024);

    // name it if you need something different from byte ...
    public static final DataSize ONE_MB = DataSize.of(1, DataSizeUnit.MB);
    public static final DataSize ONE_MiB = DataSize.of(1, DataSizeUnit.MiB);

    // ... or just use the String parser, same result as above but shorter
    public static final DataSize ONE_GiB = DataSize.of("1GiB");
    public static final DataSize ONE_TiB = DataSize.of("1TiB");
    public static final DataSize ONE_EiB = DataSize.of("1EiB");

    static final int DEFAULT_SCALE = 2;

    private final long bytes;

    /**
     * private ctor to enforce the usa of of()
     *
     * @param bytes the data size in bytes
     */
    private DataSize(long bytes) {
        this.bytes = bytes;
    }

    /**
     * The default way to create a DataSize object based on bytes
     *
     * @param bytes the data size in bytes
     * @return resulting Datasize
     */
    public static DataSize of(long bytes) {
        if (bytes == Long.MIN_VALUE) {
            throw new IllegalArgumentException("Long.MIN_VALUE is too small to be converted to DataSize, " +
                    "minimal value allowed is Long.MIN_VALUE + 1");
        }

        return new DataSize(bytes);
    }

    /**
     * Creates a DataSize based on a given {@link DataSizeUnit}. The result will be precice.
     *
     * @param value the data size in the given unit
     * @param unit  the unit value should be interpreted
     * @return resulting Datasize
     */
    public static DataSize of(long value, DataSizeUnit unit) {
        return unit.size.times(value);
    }

    /**
     * Creates a DataSize based on a given {@link DataSizeUnit}. The result might not be precice, cause of double.
     *
     * @param value the data size in the given unit
     * @param unit  the unit value should be interpreted
     * @return resulting Datasize
     * @throws IllegalArgumentException if the resulting DataSize is bigger than {@link #MAX_VALUE}
     */

    public static DataSize of(double value, DataSizeUnit unit) {
        return unit.convert(BigDecimal.valueOf(value));
    }

    /**
     * Creates a DataSize based on a given {@link DataSizeUnit}. The result will be precice.
     *
     * @param value the data size in the given unit
     * @param unit  the unit value should be interpreted
     * @return resulting Datasize
     * @throws IllegalArgumentException if the resulting DataSize is bigger than {@link #MAX_VALUE}
     */
    public static DataSize of(BigDecimal value, DataSizeUnit unit) {
        return unit.convert(value);
    }

    /**
     * Creates a DataSize based on a given {@link DataSizeUnit}. The result will be precice.
     *
     * @param value the data size in the given unit. The String will be converted to BigDecimal
     * @param unit  the unit value should be interpreted
     * @return resulting Datasize
     * @throws IllegalArgumentException if the resulting DataSize is bigger than {@link #MAX_VALUE}
     */
    public static DataSize of(String value, DataSizeUnit unit) {
        return unit.convert(new BigDecimal(value));
    }

    public static String toString(final long bytes) {
        return new DataSize(bytes).toString();
    }

    public static String toString(final long bytes, final int scale) {
        return new DataSize(bytes).toString(scale);
    }

    public static String toStringLong(final long bytes) {
        return new DataSize(bytes).toStringLong();
    }

    public static String toStringLong(final long bytes, final int scale) {
        return new DataSize(bytes).toStringLong(scale);
    }

    /**
     * Parses a String to a DataSize. The String can be something like <ul>
     * <li> 10 Byte </li>
     * <li> 1.5 KiB </li>
     * <li> 10 MiB </li>
     * <li> 10 MB </li>
     * <li> 10MB </li>
     * <li> 10MiB </li>
     * </ul>
     * <p>
     * Spaces before, middle or aftert will be stripped, so they dont matter.
     * The unit must be the exact {@link DataSizeUnit} enum name.
     *
     * @param in string describing a data size with number and unit
     * @return a the data size matching the string
     * @throws IllegalArgumentException if in is not in the form of "number unit" or an invalid unit was given
     * @throws IllegalArgumentException if the resulting DataSize is bigger than {@link #MAX_VALUE}
     */
    public static DataSize of(final String in) {
        Matcher m = parser.matcher(in);
        if (!m.find() || m.start() == 0) {
            throw new IllegalArgumentException("Format needed: \"!{number} {DataSizeUnit}\"");
        }

        BigDecimal number = new BigDecimal(in.substring(0, m.start()).strip());
        DataSizeUnit unit = valueOf(in.substring(m.start()).strip());

        return unit.convert(number);
    }

    /**
     * @return the amount of bytes of this datasize as long
     */
    @Override
    public long longValue() {
        return bytes;
    }

    @Override
    public int intValue() {
        if (bytes > Integer.MAX_VALUE || bytes < Integer.MIN_VALUE) {
            throw new IllegalStateException("DataSize does not fit in int: " + this);
        }
        return (int) bytes;
    }

    @Override
    public float floatValue() {
        return bytes;
    }

    @Override
    public double doubleValue() {
        return 0;
    }

    /**
     * @return the amount of bytes of this datasize as BigDecimal
     */
    public BigDecimal toBigDecimal() {
        return new BigDecimal(bytes);
    }

    public boolean isZero() {
        return bytes == 0;
    }

    public boolean isSmallerThan(final DataSize other) {
        return bytes < other.bytes;
    }

    public boolean isBiggerThan(final DataSize other) {
        return bytes > other.bytes;
    }

    public boolean isNegative() {
        return bytes < 0;
    }

    public DataSize abs() {
        if (isNegative()) {
            return new DataSize(bytes * -1);
        }
        return this;
    }

    /**
     * Adds <code>amountToAdd</code> to this
     *
     * @param amountToAdd times factor
     * @return a new DataSize
     * @throws NullPointerException if amountToSubtract is null
     */
    public DataSize plus(final DataSize amountToAdd) {
        Objects.requireNonNull(amountToAdd, "cannot add null ByteSize");
        return plus(amountToAdd.bytes);
    }

    /**
     * Subtracts <code>amountToSubtract</code> from this
     *
     * @param bytesToAdd ...
     * @return a new DataSize
     */
    public DataSize plus(final long bytesToAdd) {
        if (bytesToAdd == 0) {
            return this;
        }
        return DataSize.of(Math.addExact(this.bytes, bytesToAdd));
    }

    /**
     * Subtracts <code>amountToSubtract</code> from this
     *
     * @param amountToSubtract ...
     * @return a new DataSize
     * @throws NullPointerException if amountToSubtract is null
     */
    public DataSize minus(final DataSize amountToSubtract) {
        Objects.requireNonNull(amountToSubtract, "cannot subtract null ByteSize");
        return minus(amountToSubtract.bytes);
    }

    /**
     * Subtracts <code>bytesToSubtract</code> from this
     *
     * @param bytesToSubtract ...
     * @return a new DataSize
     */
    public DataSize minus(final long bytesToSubtract) {
        if (bytesToSubtract == 0) {
            return this;
        }
        return DataSize.of(Math.subtractExact(this.bytes, bytesToSubtract));
    }

    /**
     * @param factor multiply factor
     * @return a new DataSize with a size times factor of this
     * @throws ArithmeticException if the result overflows a long
     */
    public DataSize times(final long factor) {
        if (factor == 0) {
            return ZERO;
        }

        if (factor == 1) {
            return this;
        }

        return DataSize.of(Math.multiplyExact(this.bytes, factor));
    }

    /**
     * Formats a long (representing a number of size) to a human readable String just like the -h options in
     * unix commands like df or du, hence the name. Its similar to org.apache.commons.io.FileUtils.byteCountToDisplaySize()
     * but nicer, more precise and goes up to YB (Yottabyte)
     *
     * @return nicely formatted size in byte, KB, MB ...
     */
    @Override
    public String toString() {
        return toString(DEFAULT_SCALE);
    }

    /**
     * Formats using the most appropriate unit,  eg. 10 KB).
     * If you need formatting in a specific unit, use the  {@link DataSizeUnit} .format methods
     *
     * @param scale number to digits after decimal point
     * @return a nice string
     */
    public String toString(final int scale) {

        if (this.isNegative()) {
            return '-' + this.abs().toString(scale);
        }

        if (this.isSmallerThan(KiB.size)) {
            return Byte.format(this, scale);
        }

        if (this.isSmallerThan(MiB.size)) {
            return KiB.format(this, scale);
        }

        if (this.isSmallerThan(GiB.size)) {
            return MiB.format(this, scale);
        }

        if (this.isSmallerThan(TiB.size)) {
            return GiB.format(this, scale);
        }

        if (this.isSmallerThan(PiB.size)) {
            return TiB.format(this, scale);
        }

        if (this.isSmallerThan(EiB.size)) {
            return PiB.format(this, scale);
        }

        return EiB.format(this, scale);

    }

    public String toStringLong() {
        return toStringLong(DEFAULT_SCALE);
    }

    /**
     * Formats using the most appropriate unit using long names (eg. Kilobytes instead of KB).
     * If you need formatting in a specific unit, use the  {@link DataSizeUnit} .formatLong methods
     *
     * @param scale number to digits after decimal point
     * @return a nice string
     */
    public String toStringLong(final int scale) {

        if (this.isNegative()) {
            return '-' + this.abs().toStringLong(scale);
        }

        if (this.isSmallerThan(KiB.size)) {
            return Byte.formatLong(this, scale);
        }

        if (this.isSmallerThan(MiB.size)) {
            return KiB.formatLong(this, scale);
        }

        if (this.isSmallerThan(GiB.size)) {
            return MiB.formatLong(this, scale);
        }

        if (this.isSmallerThan(TiB.size)) {
            return GiB.formatLong(this, scale);
        }

        if (this.isSmallerThan(PiB.size)) {
            return TiB.formatLong(this, scale);
        }

        if (this.isSmallerThan(EiB.size)) {
            return PiB.formatLong(this, scale);
        }

        return EiB.formatLong(this, scale);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DataSize dataSize = (DataSize) o;
        return bytes == dataSize.bytes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bytes);
    }

    @Override
    public int compareTo(DataSize o) {
        return Long.compare(bytes, o.bytes);
    }

    /**
     * Creates a new byte array the this size.
     *
     * @return new, empty array
     * @throws IllegalArgumentException if size in bytes exceeds max array size ({@link Integer#MAX_VALUE})
     */
    public byte[] newByteArray() {
        if (this.bytes > Integer.MAX_VALUE - 8) {
            // as stated in https://stackoverflow.com/a/8381338/7441000 this is the max array size
            // (depending on vm and memory)
            System.out.println("* " + this.bytes);
            throw new IllegalArgumentException("cannot create array bigger than " + (Integer.MAX_VALUE - 8));
        }
        return new byte[this.intValue()];
    }
}