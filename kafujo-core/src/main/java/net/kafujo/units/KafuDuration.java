/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import net.kafujo.base.RequirementException;
import net.kafujo.base.UncheckedException;
import net.kafujo.config.KafuVm;
import net.kafujo.text.KafuText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.temporal.Temporal;
import java.util.Locale;
import java.util.Objects;

import static java.time.Duration.*;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDuration;

/**
 * Everything about {@link java.time.Duration}
 *
 * @author rstolle
 * @since 0.0.6
 */
public final class KafuDuration {

    private static final Logger lgr = LoggerFactory.getLogger(KafuDuration.class);

    /**
     * The maximum duration (as of jdk 11) is 292471208677 years, 195 days 15h ...
     */
    public static final Duration MAX_VALUE = Duration.ofSeconds(Long.MAX_VALUE, 999_999_999);

    /**
     * The minimum duration (as of jdk 11) is -292471208677 years, 195 days 15h ...
     */
    public static final Duration MIN_VALUE = Duration.ofSeconds(Long.MIN_VALUE, 0);

    public static final Duration ONE_NANO = ofNanos(1);
    public static final Duration ONE_MILLI = ofMillis(1);
    public static final Duration ONE_SECOND = ofSeconds(1);
    public static final Duration ONE_MINUTE = ofMinutes(1);
    public static final Duration ONE_HOUR = ofHours(1);
    public static final Duration ONE_DAY = ofDays(1);
    public static final Duration ONE_YEAR = ofDays(365);
    public static final Duration ONE_DECADE = ONE_YEAR.multipliedBy(10);
    public static final Duration ONE_CENTURY = ONE_YEAR.multipliedBy(100);

    // Its an pure utility class - not need for instances
    private KafuDuration() {
    }

    /**
     * Creates an readable duration string adapting to the time to display.
     *
     * @param duration duration to be formatted
     * @return formatted duration
     */
    public static String adaptUnits(final Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to be displayed");

        if (duration.isZero()) {
            return "0ns";
        }

        if (duration.equals(MAX_VALUE)) {
            return KafuText.UNIVERSAL_MAX + ", more than 292 Billion years";
        }

        if (duration.equals(MIN_VALUE) || duration.abs().equals(MAX_VALUE)) {
            return KafuText.UNIVERSAL_MIN + ", less than 292 Billion years";
        }

        if (duration.isNegative()) {
            return "-" + adaptUnits(duration.abs());
        }

        try {
            // up to 1 ms it will be nano
            if (duration.toNanos() < 1000000L) {
                return duration.toNanos() + "ns";
            }

            // up to 10 sec, it will be millis
            if (duration.toMillis() < 10000L) {
                return duration.toMillis() + "ms";
            }

        } catch (ArithmeticException ignore) {
            // to big of a duration to express it in a nanos or millis - no problem for formatting cause
            // in this case, nanos and millis will be ignored anyway.
        }

        if (duration.toSeconds() < 60L) { // cant cause ArithmeticException anymore
            return String.format("%d.%02ds", duration.toSecondsPart(), duration.toMillisPart() / 10);
        }

        if (duration.toMinutes() < 60L) {
            return String.format("%d:%02dmin", duration.toMinutesPart(), duration.toSecondsPart());
        }

        if (duration.toHours() < 24L) {
            return String.format("%d:%02d:%02dh", duration.toHoursPart(), duration.toMinutesPart(), duration.toSecondsPart());
        }

        final long daysPart = duration.toDaysPart();

        assert daysPart > 0;

        if (daysPart < 365) {
            if (daysPart == 1) {
                return String.format("%d day %d:%02dh", daysPart, duration.toHoursPart(), duration.toMinutesPart());
            } else {
                return String.format("%d days %d:%02dh", daysPart, duration.toHoursPart(), duration.toMinutesPart());
            }
        }

        final long yearsPart = daysPart / 365;
        final long daysPartLeft = daysPart % 365;

        if (yearsPart == 1) {
            return String.format("%d year, %d days %dh", yearsPart, daysPartLeft, duration.toHoursPart());
            // todo: day/days
        } else {
            return String.format("%d years, %d days %dh", yearsPart, daysPartLeft, duration.toHoursPart());
        }
    }

    public static String adaptUnitsFbk(final Duration duration, final String fbk) {
        if (duration == null) {
            return fbk;
        }
        return adaptUnits(duration);
    }

    public static String adaptUnits(Temporal startInclusive, Temporal endExclusive) {
        return adaptUnits(Duration.between(startInclusive, endExclusive));
    }

    public static String adaptUnitsOfMillis(final long millis) {
        if (millis == 0) {
            return "0ms";
        }

        if (millis < 0) {
            if (millis == Long.MIN_VALUE) {
                return "-" + adaptUnitsOfMillis(Long.MAX_VALUE);
            }
            return "-" + adaptUnitsOfMillis(-1 * millis);
        }

        if (millis < 10000L) {
            return millis + "ms";
        }

        return KafuDuration.adaptUnits(Duration.ofMillis(millis));
    }

    private static String usualDE(final Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to display");

        if (duration.isZero()) {
            return "0:00:00";
        }

        if (duration.isNegative()) {
            return "-" + usualDE(duration.abs());
        }

        final long days = duration.toDays();

        if (days == 0) {
            return String.format("%d:%02d:%02d",
                    duration.toHoursPart(), duration.toMinutesPart(),
                    duration.toSecondsPart());
        }

        if (days == 1) {
            return String.format("1 Tag %d:%02d:%02d",
                    duration.toHoursPart(), duration.toMinutesPart(),
                    duration.toSecondsPart());
        }

        return String.format("%d Tage %d:%02d:%02d", days,
                duration.toHoursPart(), duration.toMinutesPart(),
                duration.toSecondsPart());
    }

    /**
     * The
     * <p>
     * https://stackoverflow.com/questions/20827047/formatting-a-duration-in-java-8-jsr310/44902688 ...
     *
     * @param duration duration to be formatted
     * @return formatted duration
     */
    private static String usualEN(final Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to display");

        if (duration.isZero()) {
            return "0:00:00";
        }

        if (duration.isNegative()) {
            return "-" + usualEN(duration.abs());
        }

        if (duration.toDays() < 1) {
            return formatDuration(duration.toMillis(), "H:mm:ss");
        }

        if (duration.abs().toDays() == 1) {
            return formatDuration(duration.toMillis(), "d 'day' H:mm:ss");
        }

        return formatDuration(duration.toMillis(), "d 'days' H:mm:ss");
    }


    /**
     * The usual formatting in the default locale.
     *
     * @param duration duration to be formatted
     * @return formatted duration
     */
    public static String usual(final Duration duration) {
        return usual(Locale.getDefault(), duration);
    }

    public static String usual(final Locale locale, final Duration duration) {
        Objects.requireNonNull(locale, "REQUIRE locale");

        switch (KafuVm.getLanguageFbk(locale, "en").toLowerCase()) { // todo check if lower case is needed
            case "de":
                return usualDE(duration);
            default:
                return usualEN(duration);
        }
    }

    public static String usual(final Locale locale, final Duration duration, final String def) {
        if (duration == null) {
            return def;
        }
        return usual(locale, duration);
    }

    public static String usual(final Duration duration, final String def) {
        if (duration == null) {
            return def;
        }
        return usual(duration);
    }

    /**
     * A safe {@link Duration#toMillis()}, meaning that overflows will be caught (and logged). One use case would be
     * a Duration as a parameter for {@link Thread#sleep(long)} - Durations greater than 292471208 years would always
     * become 292471208 years, 247 days 7h, which should be fine in most cases.
     *
     * @param duration duration to be converted to millis
     * @return {@link Duration#toNanos()} if this fits in long, otherwise {@link Long#MAX_VALUE} for positive durations
     * or {@link Long#MIN_VALUE} for negative.
     */
    public static long toMillisSafely(Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to convert safely to milli seconds");
        long millis;
        try {
            millis = duration.toMillis();
        } catch (ArithmeticException threw) {
            lgr.info("Duration bigger than 292471208 years, 247 days 7h. Cannot fit millis into Long:  " + adaptUnits(duration));

            if (duration.isNegative()) {
                millis = Long.MIN_VALUE;
            } else {
                millis = Long.MAX_VALUE;
            }
        }
        return millis;
    }

    /**
     * A safe {@link Duration#toNanos()}, meaning that overflows will be caught (and logged).
     *
     * @param duration duration to be converted to nanos
     * @return {@link Duration#toNanos()} if this fits in long, otherwise {@link Long#MAX_VALUE} for positive durations
     * or {@link Long#MIN_VALUE} for negative.
     */
    public static long toNanosSafely(final Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to convert safely to nano seconds");
        long nanos;
        try {
            nanos = duration.toNanos();
        } catch (ArithmeticException tooBig) {
            lgr.info("Duration bigger than 292 years, 171 days 23h. Cannot fit nanos into Long:  " + adaptUnits(duration));
            if (duration.isNegative()) {
                nanos = Long.MIN_VALUE;
            } else {
                nanos = Long.MAX_VALUE;
            }
        }
        return nanos;
    }


    /**
     * Sleeps for the millis given in duration. nanos are ignored. If durations millies exceed Long.MIN/MAX ... todo
     *
     * @param duration time to sleep
     * @return the nanos stayed in this method
     */
    public static long sleep(Duration duration) {
        final long nanosStart = System.nanoTime();
        try {
            long millis = toMillisSafely(duration);

            if (millis == 0) {
                Thread.sleep(0, duration.toNanosPart());
            }
            Thread.sleep(millis);
        } catch (InterruptedException interrupted) {
            throw new UncheckedException(interrupted);
        }
        return System.nanoTime() - nanosStart;
    }


    public static String adaptUnitsOfNanos(final long nanos) {

        if (nanos == 0) {
            return "0ns";
        }

        if (nanos < 0) {
            if (nanos == Long.MIN_VALUE) {
                return "-" + adaptUnitsOfNanos(Long.MAX_VALUE);
            }
            return "-" + adaptUnitsOfNanos(-1 * nanos);
        }

        if (nanos < 1000000L) {
            return nanos + "ns";
        }

        if (nanos < 10000000000L) {
            return nanos / 1000000 + "ms";
        }

        return KafuDuration.adaptUnits(Duration.ofNanos(nanos));
    }

    public static Duration requirePositiveNotZero(final Duration duration) {
        Objects.requireNonNull(duration, "REQUIRE duration to check to be positive");
        if (duration.compareTo(ZERO) <= 0) {
            throw new RequirementException("Duration required to be greater than ZERO: " + duration);
        }
        return duration;
    }
}
