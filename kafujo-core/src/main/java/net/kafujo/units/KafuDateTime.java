/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.units;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Objects;

public final class KafuDateTime {

    public static final DateTimeFormatter COMPACT = DateTimeFormatter.ofPattern("dd.MM HH:mm");
    public static final DateTimeFormatter FULL_HUMAN = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss");
    public static final DateTimeFormatter FULL = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter FULL_MILLIS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    public static final DateTimeFormatter FILENAME_SECONDS = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss");
    public static final DateTimeFormatter FILENAME_MILLIS = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss-SSS");

    private static final Logger lgr = LoggerFactory.getLogger(KafuDateTime.class);

    public static String compact(final LocalDateTime ldt, final String def) {
        return ldt == null ? def : ldt.format(COMPACT);
    }

    public static String compact(final LocalDateTime ldt) {
        Objects.requireNonNull(ldt, "REQUIRE LocalDateTime to format to String");
        return ldt.format(COMPACT);
    }

    public static String fullHuman(final LocalDateTime ldt, final String def) {
        return ldt == null ? def : ldt.format(FULL_HUMAN);
    }

    /**
     * Formats the {@link java.time.LocalDateTime#now current local tike} to {@link #FULL}.
     *
     * @return nice formatted timestamp of now.
     */
    public static String full() {
        return full(LocalDateTime.now());
    }

    /**
     * Formats the given temporal to {@link #FULL}.
     *
     * @param timestamp to be formatted
     * @return {@code date} as nicely formatted String
     * @throws NullPointerException             if {@code date} is null
     * @throws UnsupportedTemporalTypeException if date misses required fields for date
     */
    public static String full(final Temporal timestamp) {
        Objects.requireNonNull(timestamp, "REQUIRE timestamp to format");
        return FULL.format(timestamp);
    }

    /**
     * Formats the given timestamp to {@link #FULL} or falls back to the given default string, if
     * {@code timestamp} is null or could not be formatted. There will be a short INFO log, if formatting fails for
     * some reason. The whole problem will be logged in DEBUG level as well as the null case.
     *
     * @param timestamp timestamp to be formatted
     * @param def       default value
     * @return {@code timestamp} formatted or {@code def}, if null or could not be formatted
     */
    public static String full(final Temporal timestamp, final String def) {
        if (timestamp == null) {
            lgr.debug("fall back to default '{}', cause timestamp was null", def);
            return def;
        }

        try {
            return full(timestamp);
        } catch (final RuntimeException anyRt) {
            if (lgr.isDebugEnabled()) {
                lgr.debug("COULD NOT FORMAT '" + timestamp + "' TO TIMESTAMP", anyRt);
            } else {
                lgr.info("COULD NOT FORMAT '{}' TO TIMESTAMP CAUSE {}", timestamp, anyRt.toString());
            }

            return def;
        }
    }

    public static String full(final Instant instant) {
        Objects.requireNonNull(instant, "REQUIRE instant to be formatted");
        return full(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
    }


    /**
     * Calls {@link #getUtcOffset(ZoneId)} with {@link ZoneId#systemDefault()}.
     *
     * @return the UTC offset of the system default zone
     */
    public static ZoneOffset getUtcOffset() {
        return getUtcOffset(ZoneId.systemDefault());
    }

    /**
     * @param zone zone to be checked
     * @return the UTC offset of {@code zone}
     */
    public static ZoneOffset getUtcOffset(final ZoneId zone) {
        return zone.getRules().getOffset(Instant.now());
    }

    /**
     * @param time time to be checked
     * @return the UTC offset of {@code time}
     */
    public static ZoneOffset getUtcOffset(final ZonedDateTime time) {
        return time.getZone().getRules().getOffset(Instant.now());
    }

    /**
     * There is no short way to find out, if two different zones have actually the same offset.
     * (E.g. ZoneId.of("Europe/Berlin") or ZoneId.of("Europe/Paris"). So here is one solution
     * as proposed for example here: https://stackoverflow.com/a/37501492
     *
     * @param zone1 zone to compare
     * @param zone2 other zone to compare
     * @return true, if both zones have the same UTC offset
     */
    public static boolean sameUtcOffset(final ZoneId zone1, final ZoneId zone2) {
        return getUtcOffset(zone1).equals(getUtcOffset(zone2));
    }

    /**
     * Same as {@link #sameUtcOffset(ZoneId, ZoneId)}, just with {@link ZonedDateTime}
     *
     * @param time time to compare
     * @param time2 other time to compare
     * @return true, if both times have the same UTC offset
     */
    public static boolean sameUtcOffset(final ZonedDateTime time, final ZonedDateTime time2) {
        return getUtcOffset(time).equals(getUtcOffset(time2));
    }
}
