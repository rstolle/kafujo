/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.text;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

public final class KafuFormat {

    /**
     * https://stackoverflow.com/questions/139076/how-to-pretty-print-xml-from-java
     *
     * @param input  the xml to be formatted
     * @param indent in spaces
     * @return formatted xml
     */
    public static String xml(final String input, final int indent) {
        try {
            Source xmlInput = new StreamSource(new StringReader(input));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", indent);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(xmlInput, xmlOutput);
            return xmlOutput.getWriter().toString();
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public static String xml(final String input) {
        return xml(input, 2);
    }


    /**
     * Tries to format an xml but falls back on the original input, if there were problems.
     *
     * @param input xml to be formatted.
     * @return the formatted xml or the input as it was, if there were problems
     */
    public static String xmlSafely(final String input) {
        try {
            return xml(input, 2);
        } catch (Exception ignore) {
            return input;
        }
    }

    /**
     * strips heading and trailng whitespaces and reduces multiple whitespaces in between with one space.
     * <ul>
     * <li> "Hallo" - "Hallo"</li>
     * <li> " Hallo " - "Hallo"</li>
     * <li> " H a l l o " - "H a l l o"</li>
     * <li> " H    a    l    l     o    " - "H a l l o"</li>
     * <li> " Ha\tl\nl o " - "H a l l o"</li>
     * <li> " Ha\tl\nl o " - "H a l l o"</li>
     * </ul>
     *
     * @param in String to be stripped
     * @return the stripped String
     */
    public static String stripAll(final String in) {
        return in.strip().replaceAll("\\s+", " ");
    }

}
