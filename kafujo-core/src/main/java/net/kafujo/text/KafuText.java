package net.kafujo.text;

import java.util.Objects;

public final class KafuText {

    private KafuText() {
        // pure utility class
    }

    /**
     * A universal String representation of NOT AVAILABLE.
     * https://de.wikipedia.org/wiki/N/A
     */
    public static final String UNIVERSAL_NA = "N/A";

    public static boolean equalsUniversalNotAvailable(final String test) {
        return UNIVERSAL_NA.equalsIgnoreCase(test);
    }

    public static final String UNIVERSAL_MAX = "MAX";
    public static final String UNIVERSAL_MIN = "MIN";

    public static final String NULL = null;

    public static boolean equalsUniversalMin(final String valueString) {
        //Objects.requireNonNull(valueString, "REQUIRE value as String");
        return UNIVERSAL_MIN.equalsIgnoreCase(valueString);
    }

    public static boolean equalsUniversalMax(final String valueString) {
        //Objects.requireNonNull(valueString, "REQUIRE value as String");
        return UNIVERSAL_MAX.equalsIgnoreCase(valueString);
    }

    /**
     * https://stackoverflow.com/questions/454908/split-java-string-by-new-line
     *
     * @param manyLines text
     * @return first line of the text
     */
    public static String getFirstLine(final CharSequence manyLines) {
        Objects.requireNonNull(manyLines, "REQUIRE manaylines");
        return manyLines.toString().split("\\r?\\n", 2)[0];
    }

    /**
     * Nicely replaces german umlauts (öäü, ÖÄÜ and ß). Started from https://stackoverflow.com/a/32696479
     * where the Ü - Ue part did not work.
     *
     * @param input string to be cleaned
     * @return an umlaut-free string
     */
    public static String replaceUmlauts(final String input) {

        Objects.requireNonNull(input, "REQUIRE input to be cleaned from umlauts");
        if (input.length() == 0) {
            return input;
        }

        //replace all lower Umlauts
        String output = input.replace("ü", "ue")
                .replace("ö", "oe")
                .replace("ä", "ae")
                .replace("ß", "ss");

        if (output.length() > 1 && (output.startsWith("Ä") || output.startsWith("Ö") || output.startsWith("Ü"))) {
            String tail = output.substring(1);

            if (tail.equals(tail.toLowerCase())) {
                output = output.replace("Ü", "Ue")
                        .replace("Ö", "Oe")
                        .replace("Ä", "Ae");
            }
        }

        //now replace all the other capital umlaute
        output = output.replace("Ü", "UE")
                .replace("Ö", "OE")
                .replace("Ä", "AE");

        return output;
    }
}
