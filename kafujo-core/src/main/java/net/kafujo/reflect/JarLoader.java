package net.kafujo.reflect;

import net.kafujo.base.UncheckedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

/**
 * Maintains a classloader to fetch classes and resources. For this, a {@link URLClassLoader} is kept open.
 * So its important to {@link #close()} the ClassesInTheLoader objects when done. I recommend to use try with
 * resources.
 *
 * @since 0.8.0 as ClassesInTheLoader
 */
public class JarLoader implements Closeable {

    private static final Logger LGR = LoggerFactory.getLogger(JarLoader.class);
    private final URLClassLoader loader;
    private final List<String> classNames;
    private final Path jar;

    /**
     * Shortcut to {@link #JarLoader(Path, Collection)})} listing all classes in this jar.
     *
     * @param jar jar file to be analyzed
     * @throws UncheckedIOException on any IO Error
     */
    public JarLoader(final Path jar) {
        this.classNames = new JarAnalyzer(jar, JarEntryFilter.ALL_CLASSES).getClassNames(); // unmodifiable already
        this.loader = KafuReflect.loadJar(jar);
        this.jar = jar;
    }

    public JarLoader(final Path jar, ClassLoader parent) {
        this.classNames = new JarAnalyzer(jar, JarEntryFilter.ALL_CLASSES).getClassNames(); // unmodifiable already
        this.loader = KafuReflect.loadJar(jar, parent);
        this.jar = jar;
    }

    public JarLoader(final Path jar, final Collection<String> classNames, ClassLoader parent) {
        this.classNames = List.copyOf(classNames);
        this.loader = KafuReflect.loadJar(jar, parent);
        this.jar = jar;
    }

    /**
     * Analyzes a jar file and stores the names of all classes matching {@code filter}.
     * {@link KafuReflect#loadJar(Path) loads} the jar.
     *
     * @param jar        jar file to be analyzed
     * @param classNames classes to be considered
     * @throws UncheckedIOException on any IO Error
     */
    public JarLoader(final Path jar, final Collection<String> classNames) {
        this.classNames = List.copyOf(classNames);
        this.loader = KafuReflect.loadJar(jar);
        this.jar = jar;
    }



    /**
     * @param superClassDesired
     * @param <T>
     * @return
     */
    @SuppressWarnings({"all"}) // todo: check for better possibilities
    public <T> List<Class<T>> fetchClassesAssignableFrom(final Class<T> superClassDesired) {
        final var result = new LinkedList<Class<T>>();
        for (String name : classNames) {
            final Class candidate = KafuReflect.loadQuietly(loader, name);

            if (candidate != null && superClassDesired.isAssignableFrom(candidate)) {
                LGR.debug(superClassDesired + " isAssignableFrom -> " + candidate);
                result.add(candidate);
            }
        }
        return result;
    }

    /**
     *
     * @param anno annotation to be looked for
     * @return a list of classes annotated with {@code anno}
     */
    public List<Class<?>> fetchClassesAnnotatedWith (final Class<? extends Annotation> anno) {
        final var result = new LinkedList<Class<?>>();
        for (String name : classNames) {
            final Class<?> candidate = KafuReflect.loadQuietly(loader, name);

            if (candidate != null && candidate.isAnnotationPresent(anno)) {
                LGR.debug(candidate + " is annotated with " + anno);
                result.add(candidate);
            }
        }
        return result;
    }

    public <T> List<Class<T>> fetchClassesImplementingInterface(final Class<T> interfaceDesired) {

        Objects.requireNonNull(interfaceDesired, "REQUIRE interface to check for");
        if (!interfaceDesired.isInterface()) {
            throw new IllegalArgumentException(interfaceDesired + " must be an interface");
        }

        final var result = new LinkedList<Class<T>>();
        for (String name : classNames) {
            Class candidate = KafuReflect.loadQuietly(loader, name);

            if (candidate == null || Modifier.isAbstract(candidate.getModifiers())) {
                continue;
            }

            for (Class i : candidate.getInterfaces()) {
                if (i.equals(interfaceDesired)) {
                    LGR.debug(candidate + " implements " + interfaceDesired);
                    result.add(candidate);
                    break;
                }
            }

        }
        return result;
    }

    @SuppressWarnings({"all"}) // todo: check for better possibilities
    public <T> List<Class<T>> fetchClassesImplementingInterface(final String interfaceDesired) {

        if (interfaceDesired == null || interfaceDesired.isBlank()) {
            throw new IllegalArgumentException("REQUIRE interface nameto check for");
        }

        final var result = new LinkedList<Class<T>>();
        for (String name : classNames) {
            final Class candidate = KafuReflect.loadQuietly(loader, name);

            if (candidate == null || Modifier.isAbstract(candidate.getModifiers())) {
                continue;
            }

            for (Class i : candidate.getInterfaces()) {
                if (i.getName().equals(interfaceDesired)) {
                    LGR.debug(candidate + " implements " + interfaceDesired);
                    result.add(candidate);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Tries to create an object of the given class, no matter if this class matches the {@link JarEntryFilter filter}
     * or not.
     *
     * @param className class to be loaded.
     * @return an object of the given class, as long as this class exists in this jar.
     * @throws UncheckedException wrapping all kind of exception happening when there is no such class or the object
     *                            cannot be instantiated.
     */
    public Object fetchObjectIgnoringFilter(final String className) {
        Objects.requireNonNull(className, "REQUIRE classname to create object from");
        try {
            LGR.debug("try to load class '" + className + "' and create an object from it");
            final Class<?> cls = loader.loadClass(className);
            return cls.getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            LGR.info("object creation failed", e);
            throw new UncheckedException("Problems creating object of class " + className, e);
        }
    }


    public InputStream fetchResourceAsStream(final String name) {
        return loader.getResourceAsStream(name);
    }


    public <T> T fetchObjectImplementingInterface(final Class<T> interfaceDesired) {
        var classes = fetchClassesImplementingInterface(interfaceDesired);

        classes.removeIf(c -> Modifier.isAbstract(c.getModifiers()));
        classes.removeIf(c -> Modifier.isInterface(c.getModifiers()));

        if (classes.isEmpty()) {
            throw new IllegalArgumentException("No non-abstract class implementing '" + interfaceDesired + "' available");
        }

        if (classes.size() > 1) {
            throw new IllegalArgumentException(classes.size() + " objects of interface '" + interfaceDesired + "' available: " + classes);
        }

        try {
            LGR.debug("create " + classes.get(0) + " object of as " + interfaceDesired);
            return classes.get(0).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            LGR.info("object creation failed", e);
            throw new UncheckedException("Problems creating object implementing " + interfaceDesired, e);
        }
    }

    /**
     * Creates objects of all <b>non abstract</b> classes implementing {@code interfaceDesired}.
     *
     * @param superClassDesired
     * @param <T>
     * @return a list of objects which are assignable to
     */
    public <T> List<T> fetchObjectsAssignableFrom(final Class<T> superClassDesired) {
        final var classes = fetchClassesAssignableFrom(superClassDesired);

        classes.removeIf(c -> Modifier.isAbstract(c.getModifiers()));
        classes.removeIf(c -> Modifier.isInterface(c.getModifiers())); // todo: check if is this possible

        if (classes.isEmpty()) {
            return Collections.emptyList();
        }


        try {
            List<T> objects = new ArrayList<>(classes.size());
            for (var cls : classes) {
                LGR.debug("create " + cls + " object");
                objects.add(cls.getDeclaredConstructor().newInstance());
            }
            return objects;
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            LGR.info("object creation failed", e);
            throw new UncheckedException("Problems creating object implementing " + superClassDesired, e);
        }
    }

    /**
     * Creates objects of all <b>non abstract</b> classes annotated with
     *
     * @param anno Annotation to search for.
     * @return a list of objects which are assignable to
     */
    public List<Object> fetchObjectsAnnotatedWith(final Class<? extends Annotation> anno) {
        final var classes = fetchClassesAnnotatedWith(anno);

        classes.removeIf(c -> Modifier.isAbstract(c.getModifiers()));
        classes.removeIf(c -> Modifier.isInterface(c.getModifiers()));

        if (classes.isEmpty()) {
            return Collections.emptyList();
        }

        try {
            List<Object> objects = new ArrayList<>(classes.size());
            for (var cls : classes) {
                LGR.debug("create " + cls + " object");
                objects.add(cls.getDeclaredConstructor().newInstance());
            }
            return objects;
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            LGR.info("object creation failed", e);
            throw new UncheckedException("Problems creating object from annotated class " + anno, e);
        }
    }

    public Object fetchObjectAnnotatedWith(final Class<? extends Annotation> anno) {
        var classes = fetchClassesAnnotatedWith(anno);

        classes.removeIf(c -> Modifier.isAbstract(c.getModifiers()));
        classes.removeIf(c -> Modifier.isInterface(c.getModifiers()));

        if (classes.isEmpty()) {
            throw new IllegalArgumentException("No non-abstract class annotated with '" + anno + "' available");
        }

        if (classes.size() > 1) {
            throw new IllegalArgumentException(classes.size() + " class annotated with '" + anno + "' available: " + classes);
        }

        try {
            LGR.debug("create " + classes.get(0) + " object of class annotated with as " + anno);
            return classes.get(0).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            LGR.info("object creation failed", e);
            throw new UncheckedException("Problems creating object of class annotated with " + anno, e);
        }
    }


    public URLClassLoader getLoader() {
        return loader;
    }

    public List<String> getClassNames() {
        return classNames;
    }

    public Path getJar() {
        return jar;
    }

    /**
     *
     * @return a map containing className which could not {@link ClassLoader#loadClass(String) loaded} plus the reason.
     */
    public Map<String, Throwable> fetchNotLoadableClasses() {
        final var result = new HashMap<String, Throwable>();
        for (String className : classNames) {
            try {
                loader.loadClass(className);
            } catch (Throwable fail) {
                result.put(className, fail);
            }
        }
        return result;
    }

    public int countNotLoadableClasses() {
        return fetchNotLoadableClasses().size();
    }
    /**
     * Closes the loader and log this to INFO.
     *
     * @throws IOException wont happen, stupid interface
     */
    @Override
    public void close() throws IOException {
        LGR.info("closing loader " + loader + " on " + getJar());
        loader.close();
    }
}
