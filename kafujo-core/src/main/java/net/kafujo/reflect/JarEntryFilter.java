package net.kafujo.reflect;

import java.util.*;
import java.util.function.Predicate;
import java.util.jar.JarEntry;

// todo: count the entries filtered
// EntriesInTheJar -> und ClassesInTheJar als Spezial-Fall -> Alle Tests sollten laufen!

/**
 * A {@link Predicate} to filter entries within a jar by package name. Further you can include/exclude
 * top level and/or nested classes.
 * <p>
 * Examples:
 * <pre>{@code
 *   // all entries
 *   new JarEntryClassFilter (true, true)
 *
 *   // all top level classes in package org.junit.platform
 *   new JarEntryClassFilter (true, false, "org.junit.platform")
 *
 *   // Same result: you may add one * as a wildcard.
 *   // It just serves readability and will be simply ignored
 *   new JarEntryClassFilter (true, false, "org.junit.platform.*")
 *
 *   // all nested classes in packages org.junit.platform and org.junit.jupiter
 *   new JarEntryClassFilter (false, true, "org.junit.platform", "org.junit.jupiter")
 * }</pre>
 * Note: There is no class loading or reflection involved, only textual comparison.
 *
 * @since 0.9.0
 */
public class JarEntryFilter implements Predicate<JarEntry> {

    public enum Type {
        EVERYTHING(true, true, true, "All entries"),
        RESOURCES_ONLY(true, false, false, "Resources"),
        RESOURCES_AND_NESTED(true, false, false, "Resources and neseted classes"),
        RESOURCES_AND_TOPLEVEL(true, false, false, "Resources and top level classes"),
        CLASSES_ONLY(false, true, true, "Nested and top level classes"),
        NESTED_ONLY(false, false, true, "Nested classes"),
        TOPLEVEL_ONLY(false, true, false, "Top level classes");

        private final boolean res;
        private final boolean top;
        private final boolean nested;
        private final String desc;

        Type(boolean res, boolean top, boolean nested, String desc) {
            this.res = res;
            this.top = top;
            this.nested = nested;
            this.desc = desc;
        }

        boolean test(String entry) {
            if (entry.endsWith(".class")) {
                if (!top && !nested) {
                    return false;
                }

                if (top && nested) {
                    return true;
                }

                if (top && entry.contains("$")) {
                    return false;
                }

                if (nested && !entry.contains("$")) {
                    return false;
                }

                return true;
            }

            return res;

        }

    }

    private final Set<String> packageNames;
    private final Type type;

    /**
     * Ready to use filter to match all classes. Every {@link JarEntry} representing a class will be
     * {@link #test(JarEntry) tested} to {@code true}.
     */
    public final static JarEntryFilter ALL_ENTRIES = new JarEntryFilter(Type.EVERYTHING);
    public final static JarEntryFilter ALL_RESOURCES = new JarEntryFilter(Type.RESOURCES_ONLY);
    public final static JarEntryFilter ALL_CLASSES = new JarEntryFilter(Type.CLASSES_ONLY);
    public final static JarEntryFilter ALL_CLASSES_TOPLEVEL = new JarEntryFilter(Type.TOPLEVEL_ONLY);

    /**
     * Creates a new filter.
     *
     * @param type         Entry types to be considered
     * @param packageNames optional: filter by package names. These are Strings to start the full class name with. E.g.
     * @throws IllegalArgumentException on illegal/empty package names
     */
    public JarEntryFilter(final Type type, final String... packageNames) {
        this.type = Objects.requireNonNull(type, "REQUIRE type for JarEntryFilter");

        if (packageNames != null && packageNames.length != 0) {
            var packageTemp = new HashSet<String>(packageNames.length);
            for (final String name : packageNames) {
                final String checked = checkPackageName(name);

                if (checked.isEmpty()) {
                    continue;
                }
                // by replacing . with / here, we don't need to replace it in the JarEntry each time we compare
                packageTemp.add(checked.replace(".", "/"));
            }
            this.packageNames = Collections.unmodifiableSet(packageTemp);
        } else {
            this.packageNames = Collections.emptySet();
        }
    }

    /**
     * Tests if the given {@link JarEntry} matches the criteria of this Filter.
     *
     * @param entry entry to be checked.
     * @return {@code true}, if entry is a class matching criteria of this filer. Otherwise {@code false}.
     */
    public boolean test(final JarEntry entry) {
        Objects.requireNonNull(entry, "REQUIRE JarEntry to be tested");
        final String name = entry.getName();

        if (name.startsWith("META-INF")) {
            return false;
        }

        if (!packageNames.isEmpty()) {
            boolean packageHits = false;
            for (String packageName : packageNames) {
                if (name.startsWith(packageName)) {
                    packageHits = true;
                    break;
                }
            }

            if (!packageHits) {
                return false;
            }
        }

        return type.test(name);

    }

    /**
     * @param name the name of the package given by the caller
     * @return the same name or the name without the trailing *
     */
    private String checkPackageName(String name) {
        if (name == null || !name.matches("\\S+") || name.startsWith(".")) {
            // I know, this is weak, but to check for totally correct package names can be wild.
            // After all, nothing bad can happen. The caller just wont find any classes.
            throw new IllegalArgumentException("invalid package name: " + name);
        }

        if (name.equals("*")) {
            return ""; // all packages, means nothing to filter
        }

        if (name.endsWith("*")) {
            name = name.substring(0, name.length() - 1);
        }

        if (name.contains("*")) {
            throw new IllegalArgumentException("package name can only contain one wild card at the end: " + name);
        }
        return name;
    }

    /**
     * @return the set of package name of this filter. The names will be in path form, e.g. org/apache/wicket
     */
    public Set<String> getPackageNames() {
        return packageNames;
    }

    /**
     * @return {@code true} if top level classes are included. Otherwise {@code false}
     */
    public boolean isTopLevel() {
        return type.top;
    }

    /**
     * @return {@code true}, if nested classes are included. Otherwise {@code false}
     */
    public boolean isNested() {
        return type.nested;
    }

    @Override
    public String toString() {
        if (packageNames.isEmpty()) {
            return type.desc + " of all packages";
        }
        return type.desc + " of packages: " + packageNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JarEntryFilter that = (JarEntryFilter) o;
        return packageNames.equals(that.packageNames) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageNames, type);
    }
}
