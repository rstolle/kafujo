package net.kafujo.reflect;

import net.kafujo.io.KafuFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * Immutable class to conveniently and safely analyze a jar file. It can be used to look in a jar without using or
 * blocking any resources. Use {@link JarLoader} to fetch real classes and resources.
 *
 * @since 0.8.0 (as ClassesInTheJar)
 */
public class JarAnalyzer {

    private static final Logger LGR = LoggerFactory.getLogger(JarAnalyzer.class);

    private final JarEntryFilter filter;
    private final List<String> classNames; // unmodifiable, sorted list of class names matching the filter
    private final List<String> resources;  // unmodifiable, sorted list of resource names - NOT FILTERED
    private final List<String> metaInf;    // unmodifiable, sorted list of files in META-INF - NOT FILTERED
    private final Path jar;                // jar to be analyzed

    private int totalDirectoryCount;
    private int totalFileCount;
    private int ignoreCount;

    private final Manifest manifest;
    private final Runtime.Version version;
    private final boolean multiRelease;


    public JarAnalyzer(final Path jar) {
        this(jar, JarEntryFilter.ALL_ENTRIES);
    }

    /**
     * Analyzes a jar file and stores the names of all classes matching {@code filter}.
     *
     * @param jar    jar file to be analyzed
     * @param filter {@link JarEntryFilter to be used}
     * @throws UncheckedIOException on any IO Error
     */
    public JarAnalyzer(final Path jar, final JarEntryFilter filter) {
        this.filter = Objects.requireNonNull(filter, "REQUIRE filter");
        this.jar = KafuFile.requireExists(jar);
        KafuFile.requireNotDirectory(jar);

        try (JarFile jarFile = new JarFile(jar.toFile())) {
            this.multiRelease = jarFile.isMultiRelease();
            this.manifest = jarFile.getManifest();
            this.version = jarFile.getVersion();

            final Enumeration<JarEntry> entries = jarFile.entries();
            final var tempNames = new LinkedList<String>();     // will be wrapped unmodifiable when done
            final var tempResources = new LinkedList<String>(); // same
            final var tempMetaInf = new LinkedList<String>();   // same

            while (entries.hasMoreElements()) {
                final JarEntry entry = entries.nextElement();

                String name = entry.getName();

                if (LGR.isTraceEnabled()) {
                    LGR.trace(String.format("JarEntry %4d -> %s", totalDirectoryCount + totalFileCount + 1, name));
                }

                if (name.endsWith("/")) {
                    totalDirectoryCount++;
                    continue;
                } else {
                    totalFileCount++;
                }

                if (name.startsWith("META-INF/")) {
                    tempMetaInf.add(name);
                    continue; // so: .class within META-INF wont be counted as class files!
                }

                if (!filter.test(entry)) {
                    ignoreCount++;
                    if (LGR.isDebugEnabled()) {
                        LGR.debug("JarEntry doesn't match filter: '" + name + "'; ignoreCount" + ignoreCount);
                    }
                    continue;
                }

                if (!name.endsWith(".class")) {
                    tempResources.add(name);
                    continue;
                }

                name = name.substring(0, name.length() - 6);      // remove .class
                name = name.replace('/', '.');   // path separators (/ -> .)
                // No need to remove the nested class separator - it wont work if done!
                tempNames.add(name);
            }
            Collections.sort(tempNames);
            this.classNames = Collections.unmodifiableList(tempNames);
            Collections.sort(tempResources);
            this.resources = Collections.unmodifiableList(tempResources);
            Collections.sort(tempMetaInf);
            this.metaInf = Collections.unmodifiableList(tempMetaInf);
        } catch (IOException io) {
            LGR.info("IO problem reading " + jar.toAbsolutePath(), io);
            throw new UncheckedIOException(jar + " could not be read. It might be an invalid jar file.", io);
        }
        LGR.info(createGeneralInfo());
        LGR.info(createManifestInfo());
    }

    /**
     * @return the result of {@link JarFile#getManifest()}
     */
    public Manifest getManifest() {
        return manifest;
    }

    /**
     * @return the result of {@link JarFile#getVersion()}
     */
    public Runtime.Version getVersion() {
        return version;
    }

    /**
     * @return the result of {@link JarFile#isMultiRelease()}
     */
    public boolean isMultiRelease() {
        return multiRelease;
    }

    /**
     * @return an unmodifiable, sorted list of the class names.
     */
    public List<String> getClassNames() {
        return classNames;
    }

    /**
     * @return a sorted list of resources in this jar matching {@link JarEntryFilter filter}.
     */
    public List<String> getResources() {
        return resources;
    }

    /**
     * @return a sorted list of all files within META-INF directory.
     */
    public List<String> getMetaInfFiles() {
        return metaInf;
    }

    public int getIgnoreCount() {
        return ignoreCount;
    }

    /**
     * @return the number of files within META-INF directory.
     */
    public int countMetaInfFiles() {
        return metaInf.size();
    }

    /**
     * @return the total number of resources in this jar.
     */
    public int countResouces() {
        return resources.size();
    }

    /**
     * @return the number of classes matching the {@link JarEntryFilter}.
     */
    public int countClasses() {
        return classNames.size();
    }

    /**
     * @return the number of top level classes matching the {@link JarEntryFilter}.
     */
    public int countTopClasses() {
        return (int) classNames.stream().filter(s -> !s.contains("$")).count();
    }

    /**
     * @return the number of nested classes matching the {@link JarEntryFilter}.
     */
    public int countNestedClasses() {
        return (int) classNames.stream().filter(s -> s.contains("$")).count();
    }

    /**
     * @return filter used.
     */
    public JarEntryFilter getFilter() {
        return filter;
    }

    public Path getJar() {
        return jar;
    }

    /**
     * @return The total number of entries (files and directories) in this jar.
     */
    public int getTotalEntryCount() {
        return totalFileCount + totalDirectoryCount;
    }

    /**
     * @return The total number of directories in this jar.
     */
    public int getTotalDirectoryCount() {
        return totalDirectoryCount;
    }

    /**
     * @return The total number of files this jar
     */
    public int getTotalFileCount() {
        return totalFileCount;
    }

    /**
     * @return a String containing mostly file counts (classes, resources etc.)
     */
    public String createGeneralInfo() {
        final var result = new StringBuilder(KafuFile.sizeAndPath(jar)).append(":\n");
        result.append("  files / directories:        ")
                .append(totalFileCount).append(" + ")
                .append(totalDirectoryCount).append(" = ")
                .append(getTotalEntryCount());
        result.append("\n  Resources:   ").append(countResouces()).append("  (files in META-INF: ")
                .append(countMetaInfFiles());

        result.append(")\n  Manifest entries / main attributes: ")
                .append(manifest.getEntries().size()).append(" / ")
                .append(manifest.getMainAttributes().size())
                .append("\n  MultiRelease: ").append(isMultiRelease())
                .append("\n  Version     : ").append(getVersion());

        result.append("\n FILTER: ").append(filter).append('\n');
        result.append("  top level : ").append(countTopClasses()).append('\n');
        result.append("  nested:     ").append(countNestedClasses()).append('\n');
        result.append("  combined:   ").append(countClasses()).append('\n');
        return result.toString();
    }

    /**
     * @return a nicely formatted String showing the {@link Manifest} entries of this jar.
     */
    public String createManifestInfo() {
        final var result = new StringBuilder("Manifest of ");
        result.append(KafuFile.sizeAndName(jar)).append('\n');
        var entries = manifest.getEntries();

        if (entries.isEmpty()) {
            result.append("[NO entries]\n");
        } else {
            result.append(entries.size()).append(" entries:");
            for (String entry : entries.keySet()) {
                result.append("\n  ").append(entry).append(" -> ").append(entries.get(entry));
            }
        }

        final Attributes attributes = manifest.getMainAttributes();
        if (attributes.isEmpty()) {
            result.append("[NO main attributes]\n");
        } else {
            result.append(attributes.size()).append(" main attributes:");
            for (var key : attributes.keySet()) {
                result.append("\n  ").append(key).append(" -> ").append(attributes.get(key));
            }
        }
        return result.toString();
    }

    /**
     * IntelliJ generated default implementation.
     *
     * @param o Object to be compared.
     * @return {@code true} if same class, same Path and same classes. Otherwise {@code false}.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JarAnalyzer that = (JarAnalyzer) o;
        return classNames.equals(that.classNames) &&
                jar.equals(that.jar);
    }

    /**
     * IntelliJ generated default implementation.
     *
     * @return {@link Objects#hash(Object...)}.
     */
    @Override
    public int hashCode() {
        return Objects.hash(classNames, jar);
    }
}
