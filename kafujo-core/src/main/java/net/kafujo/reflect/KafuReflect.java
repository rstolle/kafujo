/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.reflect;

import net.kafujo.base.KafujoException;
import net.kafujo.base.RequirementException;
import net.kafujo.base.UncheckedException;
import net.kafujo.container.Triple;
import net.kafujo.container.TripleString;
import net.kafujo.io.KafuFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarFile;

/**
 * Kafu for Reflection
 *
 * @since 0.0.4
 */
public final class KafuReflect {

    private static final Logger LGR = LoggerFactory.getLogger(KafuReflect.class);


    /**
     * Instantiates an object o a class by its name. The class must have a default constructor.
     * <p>
     * {@code
     * Object = instantiateByName("java.lang.String");
     * }
     * <p>
     * Its inspired from
     * https://stackoverflow.com/questions/4865153/loading-a-class-from-a-string
     *
     * @param className Class to be instantiated
     * @return an object of the given class
     * @since 2018-03-06
     * @since 0.0.4
     */
    public static Object instantiateByName(final String className) {
        Objects.requireNonNull(className);

        try {
            final Class<?> clazz = Class.forName(className);
            return clazz.getConstructor().newInstance();
        } catch (ClassNotFoundException
                | InstantiationException
                | NoSuchMethodException
                | IllegalAccessException
                | InvocationTargetException e) {
            throw new KafujoException("KafuRefelect.instantiateByName() could not create object of class " + className, e);
        }
    }


    /**
     * Calls {@link #instantiateByName(String)} and casts the created object to type
     *
     * @param className Class to be instantiated
     * @param type      type
     * @param <T>       type
     * @return an object of the given class
     */
    public static <T> T instantiateByName(final String className, final Class<T> type) {
        Objects.requireNonNull(type);
        return type.cast(instantiateByName(className));
    }


    /**
     * Tries to load the Class named {@code className} with the given Classloader. All errors be ignored, but logged in
     * debug level. Classes which cause errors I consider as {@link JarLoader#fetchNotLoadableClasses() not loadable}.
     *
     * @param loader Classloader to be used
     * @param className name ot the class to be loaded
     * @return The Class or null, if loading failed. I decided against {@link java.util.Optional} cause this method
     * might be used to walk through huge jars and I wanna avoid creating thousands of objects.
     */
    public static Class loadQuietly(final ClassLoader loader, final String className) {
        try {
            if (LGR.isTraceEnabled()) {
                LGR.trace("loadSafely (" + loader + ", " + className);
            }
            return loader.loadClass(className);
        } catch (NoClassDefFoundError exception) {
            LGR.debug("NoClassDefFoundError loading class '" + className + "'! Reason: " + exception);
        } catch (ReflectiveOperationException exception) {
            LGR.debug("ReflectiveOperationException loading class '" + className + "'! Reason: " + exception);
        } catch (IncompatibleClassChangeError error) {
            LGR.debug("IncompatibleClassChangeError loading class '" + className + "'! Reason: " + error);
        } catch (Throwable throwable) { // I can
            LGR.debug("THROWABLE thrown loading class '" + className + "'! Reason: " + throwable);
        }
        return null;
    }

    /**
     * gets an object and returns a map with all the get and is methods including their results
     * 2000-00-00_stolle: created
     * 2017-07-01_Stolle: switch to a sorted list and make it much nicer
     *
     * @param obj - the object to observe
     * @return property map
     */
    public static List<TripleString> objectStateList(final Object obj, final boolean gettersOnly, final int maxCharacters) {

        final List<TripleString> result = new ArrayList<>();

        if (obj == null) {
            result.add(new TripleString("object", "==", "null"));
            return result;
        }

        for (final Method m : obj.getClass().getMethods()) {

            if (m.getParameterCount() > 0) {
                continue;
            }

            final Class clazz = m.getReturnType();
            if (clazz == void.class) {
                // System.out.println("IGNORE: " + clazz + " because " + clazz);
                continue;
            }

            // has no parameters and returns not void
            final String name = m.getName();

            if (gettersOnly && !(name.startsWith("get") || name.startsWith("is") ||
                    name.equals("hashCode") || name.equals("length") || name.equals("size"))) {
                continue;
            }

            final TripleString tuple = new TripleString(clazz.getSimpleName(), name, "?");
            result.add(tuple);

            try {
                final Object ret = m.invoke(obj);
                if (ret != null) {
                    tuple.setRight(ret.toString());
                } else {
                    tuple.setRight("null");
                }

            } catch (final Exception t) {
                tuple.setRight(t.toString());
            }

            if (maxCharacters > 0 && tuple.getRight().length() > maxCharacters - 1) {
                tuple.setRight(tuple.getRight().substring(0, maxCharacters)); // todo: mark this
            }
        }

        result.sort(Triple.MIDDLE);
        return result;
    }

    /**
     * A
     *
     * @param obj
     * @return
     */
    public static String objectStateString(final Object obj) {
        return (objectStateString(obj, false, 0));
    }

    /**
     * @param obj
     * @param gettersOnly
     * @param maxCharacters
     * @return
     */
    public static String objectStateString(final Object obj, final boolean gettersOnly, final int maxCharacters) {
        final StringBuilder sb = new StringBuilder();
        int n = 1;

        final List<TripleString> opm = objectStateList(obj, gettersOnly, maxCharacters);

        for (final TripleString tuple : opm) {
            sb.append(String.format("%02d %20s %-12s == '%s'\n", n, tuple.getLeft(), tuple.getMiddle(), tuple.getRight()));
            n++;
        }
        return sb.toString();
    }

    /**
     * {@link URLClassLoader} needs an array of URL. This is painful enough. These URLs shall be compatible with
     * {@link java.net.JarURLConnection} which can also point to files within a jar. The clearest statement to create
     * a correct URL that points to a jar file was:
     * <pre>
     *     URL refers to the whole JAR file:   jar:http://www.foo.com/bar/baz.jar!/
     * </pre>
     * <p>
     * For a long time I just used {@link Path#toUri()} and then toURL() which worked as well.
     *
     * @param jar string representing a path to a jar file. There are no tests if this file exists.
     * @return a url as described above
     */
    public static URL createJarUrl(final Path jar) {
        Objects.requireNonNull(jar, "REQUIRE path to a jar");
        try {
            // new URL("jar:file", pathToJar,"!/"); fails with unknown protocol: jar:file
            return new URL("jar:file:" + jar + "!/");
        } catch (MalformedURLException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static URL createJarUrl(final Path jar, final String entry) {
        Objects.requireNonNull(jar, "REQUIRE path to a jar");
        Objects.requireNonNull(jar, "REQUIRE entry within jar");
        try {
            return new URL("jar:file:" + jar + "!/" + entry);
        } catch (MalformedURLException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * @param jar jar file
     * @return an open ClassLoader which must be closed after usage.
     */
    public static URLClassLoader loadJar(final Path jar) {
        if (!isValidJar(jar)) {
            throw new IllegalArgumentException(jar + " seems not to be a valid jar file");
        }
        final URL[] urls = new URL[]{createJarUrl(jar)};
        return new URLClassLoader(urls);
    }

    /**
     * @param jar    jar file
     * @param parent parent classloader. Might be null.
     * @return an open ClassLoader which must be closed after usage.
     */
    public static URLClassLoader loadJar(final Path jar, ClassLoader parent) {
        if (!isValidJar(jar)) {
            throw new IllegalArgumentException(jar + " seems not to be a valid jar file");
        }
        KafuFile.requireNotDirectory(jar);
        KafuFile.requireReadable(jar);
        final URL[] urls = new URL[]{createJarUrl(jar)};
        return new URLClassLoader(urls, parent);
    }

    /**
     * Beside stuff like https://stackoverflow.com/a/20153045 there is not much to be found how to test a file to
     * be a valid jar. {@link URLClassLoader} swallows every file with no complaints. So at least a simple check
     * should be performend.
     *
     * @param jar jar file to be tested
     * @return true if it is a existing {@link JarFile} with at least one entry.
     */
    public static boolean isValidJar(Path jar) {
        Objects.requireNonNull(jar, "REQUIRE jar file");
        KafuFile.requireNotDirectory(jar);
        KafuFile.requireReadable(jar);

        try (var jfile = new JarFile(jar.toFile())) {
            if (jfile.size() < 1) {
                return false;
            }

        } catch (IOException ignore) {
            return false;
        }
        return true;
    }
}
