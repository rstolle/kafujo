package junit.reflect;


import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import net.kafujo.reflect.KafuReflect;
import org.junit.jupiter.api.Test;
import support.Data;

import java.io.IOException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * https://github.com/classgraph/classgraph looks pretty good, when it comes to class path scanning.
 * I do some tests here comparing it with {@link net.kafujo.reflect.JarAnalyzer}.
 */
public class StudyClassGraph {

    @Test
    public void allClasssAllInfo() throws IOException {

        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableAllInfo()
                     .scan()) {
            assertEquals(26, scan.getAllClasses().size());
        }
    }

    @Test
    public void allClasssClassInfo() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableClassInfo()
                     .scan()) {
            // doesn't count anonymous class: net.useless.package1.Class15ContainsPublicInnerClasses$1
            // and private inner class: net.useless.package2.Class22Implements$PrivateWithinClass22
            assertEquals(24, scan.getAllClasses().size());
        }
    }

    @Test
    public void packageUseful() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableClassInfo().acceptPackages("net.useful")
                     .scan()) {
            assertEquals(3, scan.getAllClasses().size());
        }
    }

    @Test
    public void packageNotExist() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .acceptPackages("net.sdfasfa.useful")
                     .enableClassInfo()
                     .scan()) {
            assertEquals(0, scan.getAllClasses().size());
        }
    }

    @Test
    void subtype() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableClassInfo()
                     .scan()) {

            var sub = scan.getSubclasses(Properties.class.getName());
            assertEquals(1, sub.size());
            ClassInfo subInfo = sub.get(0);
            assertEquals("net.useful.PropertiesSafe", subInfo.getName());
        }
    }

    @Test
    void anno() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableAnnotationInfo()
                     .scan()) {
            var depList = scan.getClassesWithAnnotation(Deprecated.class.getName());
            assertEquals(1, depList.size());
            ClassInfo depInfo = depList.get(0);
            assertEquals("net.useless.OldStuff", depInfo.getName());
        }
    }

    @Test
    void filter() throws IOException {
        try (var loader = KafuReflect.loadJar(Data.TEST_JAR);
             var scan = new ClassGraph().overrideClassLoaders(loader)
                     .enableAllInfo()
                     .scan()) {

            var depList = scan.getAllClasses().filter(ci ->
                    (!ci.isAbstract() && !ci.isInterface() && ci.hasAnnotation(Deprecated.class.getName())));

            assertEquals(1, depList.size());
            ClassInfo depInfo = depList.get(0);
            assertEquals("net.useless.OldStuff", depInfo.getName());
        }
    }
}