package junit.reflect;

import net.kafujo.base.KafujoException;
import net.kafujo.base.RequirementException;
import net.kafujo.base.UncheckedException;
import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;
import net.kafujo.reflect.JarLoader;
import net.kafujo.reflect.KafuReflect;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ClassLoaderTemporaryPlayGround {
    /**
     * Loads the given jar with an @link {@link URLClassLoader} and the current threads @link {@link ClassLoader}.
     *
     * @param jar jar file to be loaded
     * @return a classloader for the given jar
     * @throws NullPointerException if jar is null
     * @throws RequirementException if jar is not a readable file
     */
    public static URLClassLoader getClassLoaderForJar(final Path jar) {
        KafuFile.requireReadable(jar);
        KafuFile.requireNotDirectory(jar);

        try {
            final URL[] urls = new URL[]{jar.toUri().toURL()};
            return new URLClassLoader(urls, Thread.currentThread().getContextClassLoader());
        } catch (MalformedURLException e) {
            throw new KafujoException("FAILED TO LOAD " + jar, e);
        }
    }

    static URLClassLoader loadJar(String jar) {
        String urlStr = "jar:file:" + jar + "!/";
        System.out.println("Path to PJTS jar: " + urlStr);
        try {
            URL[] urls = {new URL(urlStr)};
//            return new URLClassLoader(urls, ClassForMotherClassLoader.class.getClassLoader()); ???
            return new URLClassLoader(urls);
        } catch (MalformedURLException problem) {
            throw new UncheckedException(problem);
        }
    }

    void lookMetainf() {
//        Path path = KafuFile.requireNotDirectory(Data.j);
//        Path path = Path.of("/temp/hello.jar");

//        final Path jar = Resources.ofCurrentThread().asPath("jar/java-basics-6da2f68.jar");
        final Path jar = Path.of("d:\\tmp\\jars\\plugin.jar");


        JarLoader dd = new JarLoader(jar);

        var loader = KafuReflect.loadJar(jar);

        var res = Resources.of(loader);

        assertTrue(res.isAvailable("META-INF/info.xml"));

        System.out.println(res.asString("META-INF/info.xml"));


    }


}
