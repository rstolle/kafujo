package junit.reflect;

import net.kafujo.reflect.JarLoader;
import net.kafujo.reflect.JarAnalyzer;
import net.kafujo.reflect.JarEntryFilter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import support.Data;

import java.io.IOException;
import java.io.Serializable;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.Remote;
import java.util.List;
import java.util.Properties;

import static net.kafujo.reflect.JarEntryFilter.Type.TOPLEVEL_ONLY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class JarLoaderTests {

    private static final JarLoader citjAll = new JarLoader(Data.TEST_JAR);

    @AfterAll
    static void afterAll() throws IOException {
        citjAll.close();
    }

    @Test
    void bySubtype() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final var serializables = citjAll.fetchClassesAssignableFrom(Serializable.class);
        assertThat(serializables).hasSize(5);
        for (var cls : serializables) {
            var inst = cls.getDeclaredConstructor().newInstance();
            assertTrue(inst instanceof Serializable);
        }
    }

    @Test
    void implementsByClass() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final var serializables = citjAll.fetchClassesImplementingInterface(Serializable.class);
        assertThat(serializables).hasSize(4); // PropertiesSafe not here, because it just inherited Serializable?
        for (var cls : serializables) {
            var inst = cls.getDeclaredConstructor().newInstance();
            System.out.println(inst);
            assertTrue(inst instanceof Serializable);
        }
    }

    @Test
    void implementsByName() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final var serializables = citjAll.fetchClassesImplementingInterface("java.io.Serializable");
        assertThat(serializables).hasSize(4); // PropertiesSafe not here, because it just inherited Serializable?
        for (var cls : serializables) {
            var inst = cls.getDeclaredConstructor().newInstance();
            assertTrue(inst instanceof Serializable);
        }
    }

    @Test
    void implementsByName2() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final var implementors = citjAll.fetchClassesImplementingInterface("net.useless.Interface");
        assertThat(implementors).hasSize(4);
        for (var cls : implementors) {
            var inst = cls.getDeclaredConstructor().newInstance();
            assertNotNull(inst);
        }
    }

    @Test
    void fetchObjectInAll() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Remote r = citjAll.fetchObjectImplementingInterface(Remote.class);
        assertNotNull(r);
    }

    @Test
    void fetchObjectInPackage() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final var package2filter = new JarEntryFilter(TOPLEVEL_ONLY, "net.useless.package2.*");
        final var package2 = new JarLoader(Data.TEST_JAR, new JarAnalyzer(Data.TEST_JAR, package2filter).getClassNames());
        assertThat(package2.getClassNames()).hasSize(4);

        var theOnes = package2.fetchClassesImplementingInterface(Serializable.class);
        var theOnes2 = package2.fetchClassesImplementingInterface(Serializable.class);
        assertThat(theOnes).hasSize(1);
        assertThat(theOnes2).hasSize(1);

        Class<Serializable> one = theOnes.get(0);
        Class<Serializable> two = theOnes2.get(0);

        assertEquals(one, two);
        assertEquals(one.getClassLoader(), two.getClassLoader());


        // both fail cause java.lang.ClassNotFoundException: net.useless.Util
        theOnes.get(0).getDeclaredConstructor().newInstance();
        Serializable theOne = package2.fetchObjectImplementingInterface(Serializable.class);
    }

    @Test
    void fetchObject() {
        Remote remote = citjAll.fetchObjectImplementingInterface(Remote.class);
        assertNotNull(remote);
    }

    @Test
    void provokeFail() {
        final List<Class<Serializable>> serializables;

        var packageTwoTopLevelFilter = new JarEntryFilter(TOPLEVEL_ONLY, "net.useless.package2.*");
        var packageTwoTopLevel= new JarAnalyzer(Data.TEST_JAR, packageTwoTopLevelFilter);

        try (var package2 = new JarLoader(Data.TEST_JAR, packageTwoTopLevel.getClassNames())) {
            assertThat(package2.getClassNames()).hasSize(4);
            serializables = package2.fetchClassesImplementingInterface(Serializable.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        // the loader is closed, but we have the classes, in this case one:
        assertThat(serializables).hasSize(1);

        Class<Serializable> serializable = serializables.get(0);
        assertEquals("net.useless.package2.Class22Implements", serializable.getName());
        assertEquals("Class22Implements", serializable.getSimpleName());

        // but trying to create an object will fail:
        assertThrows(java.lang.NoClassDefFoundError.class,
                () -> serializable.getDeclaredConstructor().newInstance());

        /*
          JDK_STRANGE: the first time you get java.lang.NoClassDefFoundError: net/useless/Util
          trying a second time: NoClassDefFoundError: Could not initialize class net.useless.package2.Class22Implements

          And if Class22Implements would not need Util - it would even work!

          Anyhow, it doesnt work reliable, after closing the loader.
         */
    }


    @Test
    void invokeMethodAccessingResources() throws Exception {
        Object o = citjAll.fetchObjectIgnoringFilter("net.useful.ResourceReader");
        var m = o.getClass().getMethod("read", String.class);

        assertEquals("3.1415", m.invoke(o, "pi"));
        assertEquals("42", m.invoke(o, "dont.panic"));
        assertEquals("hello", m.invoke(o, "greeting"));
    }

    @Test
    void resourceAsStream() {
        try (var in = citjAll.fetchResourceAsStream("net/useless/const.properties")) {
            Properties p = new Properties();
            p.load(in);
            assertEquals("3.1415", p.getProperty("pi"));
            assertEquals("42", p.getProperty("dont.panic"));
            assertEquals("hello", p.getProperty("greeting"));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Test
    void classAnnotatedWith() {
        final var oldies = citjAll.fetchClassesAnnotatedWith(Deprecated.class);
        assertThat(oldies).hasSize(1);
        assertEquals ("OldStuff", oldies.get(0).getSimpleName());
    }

    @Test
    void objectsAnnotatedWith() {
        final List<Object> oldies = citjAll.fetchObjectsAnnotatedWith(Deprecated.class);
        assertThat(oldies).hasSize(1);
    }

    @Test
    void objectAnnotatedWith() {
        final Object oldie = citjAll.fetchObjectAnnotatedWith(Deprecated.class);
        System.out.println("oldie = " + oldie.getClass());;
        assertNotNull(oldie);
        assertEquals ("net.useless.OldStuff", oldie.getClass().getName());
    }

    @Test
    void notLoadable() {
        final var fails = citjAll.fetchNotLoadableClasses();
        assertThat(fails).hasSize(0);
        assertEquals(0, citjAll.countNotLoadableClasses());
    }
}
