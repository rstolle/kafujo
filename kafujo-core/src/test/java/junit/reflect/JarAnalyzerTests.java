package junit.reflect;

import net.kafujo.reflect.JarAnalyzer;
import net.kafujo.reflect.JarEntryFilter;
import net.kafujo.reflect.JarEntryFilter.Type;
import org.junit.jupiter.api.Test;
import support.Data;

import java.io.IOException;
import java.util.jar.JarFile;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class JarAnalyzerTests {

    private static final JarAnalyzer everything = new JarAnalyzer(Data.TEST_JAR, JarEntryFilter.ALL_ENTRIES);

    @Test
    void total() {
        assertEquals(39, everything.getTotalEntryCount());
        assertEquals(30, everything.getTotalFileCount());
        assertEquals(9, everything.getTotalDirectoryCount());
        assertEquals(everything.getTotalEntryCount(), everything.getTotalFileCount() + everything.getTotalDirectoryCount());
        assertEquals(0, everything.getIgnoreCount());
    }

    @Test
    void sizeInJarFile() throws IOException {
        try (var jf = new JarFile(Data.TEST_JAR.toFile())) {
            assertEquals(jf.size(), everything.getTotalEntryCount());
        }
    }

    @Test
    void classes() {
        everything.getClassNames().forEach(System.out::println);

        assertEquals(26, everything.countClasses());
        assertEquals(8, everything.countNestedClasses());
        assertEquals(18, everything.countTopClasses());
        assertEquals(everything.countClasses(), everything.countTopClasses() + everything.countNestedClasses());
    }

    @Test
    void ignore() {
        assertEquals(3, everything.countMetaInfFiles());
        assertFalse(everything.isMultiRelease());
        assertEquals(0, everything.getIgnoreCount());
    }

    @Test
    void resources() {
        assertEquals(1, everything.countResouces());
        assertThat(everything.getResources()).hasSize(1).contains("net/useless/const.properties");
    }

    @Test
    void metaInf() {
        assertEquals(3, everything.countMetaInfFiles());
        //   assertEquals(citjAll.countMetaInfFiles(), citjAll.getTotalFileCount() - citjAll.getTotalNestedClassCount() -
        //           citjAll.getTotalTopLevelClassCount() - citjAll.countResouces());
        //   assertThat(citjAll.getMetaInfFiles()).hasSize(3).contains("META-INF/MANIFEST.MF");
    }

    @Test
    void compareAll() {
        final var verify = new JarAnalyzer(Data.TEST_JAR);

        assertThat(verify.getClassNames()).hasSize(26)
                .contains("net.useless.package1.Class11")
                .contains("net.useful.PropertiesSafe");

        assertEquals(verify, everything);

        assertEquals(verify.getFilter(), everything.getFilter());

    }

    @Test
    void topLevel() {
        final var citj1 = new JarAnalyzer(Data.TEST_JAR, JarEntryFilter.ALL_CLASSES_TOPLEVEL);
        final var citj2 = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.TOPLEVEL_ONLY));

        assertThat(citj1.getClassNames()).hasSize(18).contains("net.useless.Transportable");
        assertEquals(18, citj1.countClasses());
        assertEquals(18, citj1.countTopClasses());
        assertEquals(0, citj1.countNestedClasses());

        assertEquals(citj1, citj2);
    }

    @Test
    void nestedOnly() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.NESTED_ONLY));
        assertThat(citj.getClassNames()).hasSize(8).contains("net.useless.package1.Class15ContainsPublicInnerClasses$PublicWithinClass15Serializable");
        assertEquals(8, citj.countClasses());
        assertEquals(0, citj.countTopClasses());
        assertEquals(8, citj.countNestedClasses());
    }

    @Test
    void starPackages() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "*"));
        assertThat(citj.getClassNames()).hasSize(26).contains("net.useless.Transportable");
    }

    @Test
    void netStarPackages() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "net.*"));
        assertThat(citj.getClassNames()).hasSize(26).contains("net.useless.Transportable");
    }

    @Test
    void netPackages() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "net."));
        assertThat(citj.getClassNames()).hasSize(26).contains("net.useless.Transportable");
    }

    @Test
    void notExistingPackages() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "not.there.*"));
        assertThat(citj.getClassNames()).isEmpty();
        assertEquals(0, citj.countClasses());
        assertEquals(0, citj.countTopClasses());
        assertEquals(0, citj.countNestedClasses());
    }

    @Test
    void packageUseless() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "net.useless"));
        assertThat(citj.getClassNames()).hasSize(23).contains("net.useless.Transportable");
    }

    @Test
    void packageUseful() {
        final var citj = new JarAnalyzer(Data.TEST_JAR, new JarEntryFilter(Type.CLASSES_ONLY, "net.useful"));
        assertEquals(3, citj.countClasses());
        assertEquals(3, citj.countTopClasses());
        assertEquals(0, citj.countNestedClasses());
        assertThat(citj.getClassNames()).hasSize(3).contains("net.useful.PropertiesSafe");
    }
}
