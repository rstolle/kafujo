package junit.reflect;

import net.kafujo.base.KafujoException;
import net.kafujo.reflect.JarLoader;
import net.kafujo.reflect.KafuReflect;
import net.kafujo.units.DataSize;
import org.junit.jupiter.api.Test;
import support.Data;

import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class KafuReflectTests {

    @Test
    void nocast () {
        // no default ctor
        assertThrows (KafujoException.class, () -> KafuReflect.instantiateByName("net.kafujo.units.DataSize"));

        Object obj = KafuReflect.instantiateByName("java.lang.String");
        assertEquals(String.class, obj.getClass());
    }
    @Test

    void casted () {
        // no default ctor
        assertThrows (KafujoException.class, () -> KafuReflect.instantiateByName("net.kafujo.units.DataSize", DataSize.class));

        String obj = KafuReflect.instantiateByName("java.lang.String", String.class);
        assertEquals(String.class, obj.getClass());
    }

    @Test
    void validJarFile() {
        // URLCkassloader seems to swallow anything
        URLClassLoader loader = new URLClassLoader(new URL[]{KafuReflect.createJarUrl(Data.PATH_TO_ONE_LINE_FILE)});
        assertNotNull (loader);
        assertNull (loader.findResource("asdflka"));

        // not so KafuReflect:
        assertThrows(IllegalArgumentException.class, () -> KafuReflect.loadJar (Data.PATH_TO_ONE_LINE_FILE));
        assertFalse(KafuReflect.isValidJar(Data.PATH_TO_ONE_LINE_FILE));
        assertTrue(KafuReflect.isValidJar(Data.TEST_JAR));
    }
}
