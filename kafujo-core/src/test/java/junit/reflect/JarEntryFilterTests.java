package junit.reflect;

import net.kafujo.reflect.JarEntryFilter;
import org.junit.jupiter.api.Test;
import support.Data;

import java.util.jar.JarEntry;

import static net.kafujo.reflect.JarEntryFilter.Type.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class JarEntryFilterTests {

    final JarEntry TOP1 = new JarEntry("TopLevel.class");
    final JarEntry NESTED = new JarEntry("Nested$Class.class");
    final JarEntry WORLD = new JarEntry("net/world/Star.class"); // we compare names as they are in the zip
    final JarEntry EARTH = new JarEntry("net/earth/Mountain.class");

    @Test
    void illegal() {
        assertThrows(IllegalArgumentException.class, () -> new JarEntryFilter(CLASSES_ONLY, ""));
        assertThrows(IllegalArgumentException.class, () -> new JarEntryFilter(CLASSES_ONLY, "  "));
        assertThrows(IllegalArgumentException.class, () -> new JarEntryFilter(CLASSES_ONLY, "*.kafujo"));
        assertThrows(IllegalArgumentException.class, () -> new JarEntryFilter(CLASSES_ONLY, "kafujo.**"));
        assertThrows(IllegalArgumentException.class, () -> new JarEntryFilter(CLASSES_ONLY, "ka*fujo.*"));
    }

    @Test
    void samePackageNames() {
        final var same = new JarEntryFilter(CLASSES_ONLY, "net.", "net.*", "net.");
        assertThat(same.getPackageNames()).hasSize(1).contains("net/");
        assertTrue(same.isTopLevel());
        assertTrue(same.isNested());
        Data.lgr.info(same.toString());
    }

    @Test
    void emptyPackageNames() {
        final var one = new JarEntryFilter(TOPLEVEL_ONLY);
        assertThat(one.getPackageNames()).isEmpty();
        assertTrue(one.isTopLevel());
        assertFalse(one.isNested());

        final var two = new JarEntryFilter(NESTED_ONLY, "*");
        assertThat(two.getPackageNames()).isEmpty();
        assertFalse(two.isTopLevel());
        assertTrue(two.isNested());

        final var three = new JarEntryFilter(CLASSES_ONLY, "*", "*", "*");
        assertThat(three.getPackageNames()).isEmpty();
        assertTrue(three.isTopLevel());
        assertTrue(three.isNested());
    }

    @Test
    void emptyPackageNamesMakesEqualFilter() {
        final var one = new JarEntryFilter(CLASSES_ONLY);
        assertThat(one.getPackageNames()).isEmpty();
        assertTrue(one.isTopLevel());
        assertTrue(one.isNested());

        final var two = new JarEntryFilter(CLASSES_ONLY, "*");
        final var three = new JarEntryFilter(CLASSES_ONLY, "*", "*", "*");

        assertEquals(one, two);
        assertEquals(one, three);
        assertEquals(two, three);

    }

    @Test
    void allClasses() {
        assertTrue(JarEntryFilter.ALL_CLASSES.test(TOP1));
        assertTrue(JarEntryFilter.ALL_CLASSES.test(EARTH));
        assertTrue(JarEntryFilter.ALL_CLASSES.test(NESTED));
    }

    @Test
    void toplevel() {
        assertTrue(JarEntryFilter.ALL_CLASSES_TOPLEVEL.test(TOP1));
        assertTrue(JarEntryFilter.ALL_CLASSES_TOPLEVEL.test(EARTH));
        assertFalse(JarEntryFilter.ALL_CLASSES_TOPLEVEL.test(NESTED));
    }

    @Test
    void packagePlain() {
        final var jecf = new JarEntryFilter(CLASSES_ONLY, "net");
        assertFalse(jecf.test(TOP1));
        assertTrue(jecf.test(EARTH));
        assertFalse(jecf.test(NESTED));
    }

    @Test
    void twoPackages() {
        final var jecf = new JarEntryFilter(CLASSES_ONLY, "net.earth.*", "net.world.");
        assertThat(jecf.getPackageNames()).hasSize(2).contains("net/earth/", "net/world/");
        System.out.println(jecf);

        assertFalse(jecf.test(TOP1));
        assertFalse(jecf.test(NESTED));

        assertTrue(jecf.test(EARTH));
        assertTrue(jecf.test(WORLD));
    }

    @Test
    void packageWildcardOnly() {
        final var jecf = new JarEntryFilter(CLASSES_ONLY, "*");
        assertTrue(jecf.test(TOP1));
        assertTrue(jecf.test(EARTH));
        assertTrue(jecf.test(NESTED));
    }

    @Test
    void packageWildcard() {
        final var jecf = new JarEntryFilter(CLASSES_ONLY, "net.*");
        assertFalse(jecf.test(TOP1));
        assertTrue(jecf.test(EARTH));
        assertFalse(jecf.test(NESTED));
    }

    @Test
    void packageWildcard2() {
        final var jecf = new JarEntryFilter(CLASSES_ONLY, "net.world.*");
        assertFalse(jecf.test(TOP1));
        assertTrue(jecf.test(WORLD));
        assertFalse(jecf.test(NESTED));
    }
}
