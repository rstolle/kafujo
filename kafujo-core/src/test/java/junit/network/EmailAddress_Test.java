package junit.network;

import net.kafujo.network.EmailAddress;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmailAddress_Test {

    static final EmailAddress OK = new EmailAddress("ok@ok.com");

    // some examples from https://gist.github.com/cjaoude/fd9910626629b53c4d25

    final List<String> invalid = List.of(".", "@", "'",
            "plainaddress",
            "#@%^%#$@#$@#.com",
            "@example.com",
            "Joe Smith <email@example.com>",
            "email.example.com",
            "email@example@example.com",
            ".email@example.com",
            "email.@example.com",
            "email..email@example.com",
//            "あいうえお@example.com", why invalid?
            "email@example.com (Joe Smith)",
            "email@example",
            "email@-example.com",
//            "email@example.web",
//            "email@111.222.333.44444",
            "email@example..com",
            "Abc..123@example.com",
            "rstolle@-posteo.de",
            "@12",
            "adfas@");

    final List<String> valid = List.of("email@example.com",
            "firstname.lastname@example.com",
            "email@subdomain.example.com",
            "firstname+lastname@example.com",
            "email@123.123.123.123",
            "email@[123.123.123.123]",
            "\"email\"@example.com",
            "1234567890@example.com",
            "email@example-one.com",
            "_______@example.com",
            "email@example.name",
            "email@example.museum",
            "email@example.co.jp",
            "firstname-lastname@example.com",
            "rstolle@posteo.de");

    @Test
    void strings() {
        assertEquals("ok@ok.com", OK.toString());
    }

    @Test
    void validateInvalids() {
        invalid.forEach(e -> assertFalse(EmailAddress.isValid(e), e + " should be an INVALID email address"));
    }

    @Test
    void validateValids() {
        valid.forEach(e -> assertTrue(EmailAddress.isValid(e), e + " should be VALID email address"));
    }


    @Test
    void url() throws MalformedURLException {
        final URL urlOK = new URL("mailto:" + OK);
        assertEquals(OK, EmailAddress.of(urlOK));
        assertEquals(urlOK, OK.toUrl());

        final URL urlHttp = new URL("http://ok@ok.com");
        assertThrows(IllegalArgumentException.class, () -> EmailAddress.of(urlHttp));

        final URL urlNotOk = new URL("mailto:not.@.ok.com");
        assertThrows(IllegalArgumentException.class, () -> EmailAddress.of(urlNotOk));
    }
}
