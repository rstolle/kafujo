package junit.network;

import net.kafujo.base.RequirementException;
import net.kafujo.network.Host;
import net.kafujo.network.KafuNet;
import net.kafujo.network.Port;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;
import static junit.network.NetworkData.*;

class Host_Test {

    @Test
    void jdkFailsOnStrangeUrl() throws MalformedURLException {
        final URL uri = new URL(HOSTNAME_JDKFAIL_AT);
        assertEquals(HOSTNAME_JDKFAIL_AT, uri.toString());
        assertEquals("", uri.getHost());   //jdkfail: the host part is entirely gone !
        assertEquals(-1, uri.getPort());

        // hostname handles it better:
        assertFalse(KafuNet.isValidHost(HOSTNAME_JDKFAIL_AT));
        assertFalse(KafuNet.isValidHost(HOSTNAME_JDKFAIL_AT));
        assertThrows(RequirementException.class, () -> new Host(HOSTNAME_JDKFAIL_AT));
    }

    @Test
    void ctor1NoHost() {
        final Host wiki = new Host(WIKI_STRING);
        assertTrue(wiki.getPort().isEmpty());
        assertFalse(wiki.getPort().isPresent());
        assertEquals(Port.NOT_PRESENT, wiki.getPort());
        assertEquals(WIKI_STRING, wiki.toString());
        assertEquals(WIKI_STRING, wiki.getIdentifier());
    }

    @Test
    void ctor1WithHost() {
        final Host wiki = new Host(Port.SMTP.attachTo(WIKI_STRING));
        assertFalse(wiki.getPort().isEmpty());
        assertTrue(wiki.getPort().isPresent());
        assertEquals(Port.SMTP, wiki.getPort());
        assertEquals(Port.SMTP.attachTo(WIKI_STRING), wiki.toString());
        assertEquals(WIKI_STRING, wiki.getIdentifier());
    }

    @Test
    void ctor2NoHost() {
        final Host wiki = new Host(WIKI_STRING, Port.NOT_PRESENT);
        assertTrue(wiki.getPort().isEmpty());
        assertFalse(wiki.getPort().isPresent());
        assertEquals(Port.NOT_PRESENT, wiki.getPort());
        assertEquals(WIKI_STRING, wiki.toString());
        assertEquals(WIKI_STRING, wiki.getIdentifier());
    }

    @Test
    void ctor2WithHost() {
        final Host wiki = new Host(WIKI_STRING, Port.NTP);

        assertFalse(wiki.getPort().isEmpty());
        assertTrue(wiki.getPort().isPresent());
        assertEquals(Port.NTP, wiki.getPort());
        assertEquals(Port.NTP.attachTo(WIKI_STRING), wiki.toString());
        assertEquals(WIKI_STRING, wiki.getIdentifier());
    }

    @Test
    void ofToUrlNoPathNoPort() throws MalformedURLException {
        final URL url = new URL("http", WIKI_STRING, -1, "");
        final Host host = Host.ofUrl(url);
        assertTrue(host.getPort().isEmpty());
        assertEquals(Port.NOT_PRESENT, host.getPort());

        assertEquals(WIKI_STRING, host.toString());
        assertEquals(WIKI_STRING, host.getIdentifier());
        assertEquals(url, host.toUrl("http"));
    }

    @Test
    void ofToUrlWithPath() throws MalformedURLException {
        final URL url = new URL("http", WIKI_STRING, Port.POP2.intValue(), "/hello");
        final Host host = Host.ofUrl(url);
        assertTrue(host.getPort().isPresent());
        assertEquals(Port.POP2, host.getPort());
        assertEquals(Port.POP2.attachTo(WIKI_STRING), host.toString());
        assertEquals(WIKI_STRING, host.getIdentifier());
        assertNotEquals(url, host.toUrl("http")); // cause path is missing
    }


    @Test
    @Tag("online")
    void resolve() {
        assertTrue(new Host(WIKI_STRING).isResolvable());
        assertTrue(new Host(WIKI_STRING, Port.NNTP).isResolvable()); // port doesn't matter

        // assertFalse(new Host("sasdf.sdfa.sdfa.sfd.de").isResolvable()); this one exists!
        assertFalse(new Host("syydf.ssdfa.sdmfaw.sfwd.de2").isResolvable());
    }

    @Test
    void valids() {
        VALID_HOSTS.forEach(e -> assertTrue(KafuNet.isValidHost(e), "Host + '" + e + "' should be VALID host"));
        // All valid hostnames must be valid hosts as well:
        VALID_HOSTNAMES.forEach(e -> assertTrue(KafuNet.isValidHost(e), "Hostname + '" + e + "' should be VALID host"));
    }

    @Test
    void invalids() {
        INVALID_HOSTS.forEach(e -> assertFalse(KafuNet.isValidHost(e), e + " should be an INVALID host"));
    }

    @Test
    void portsOk() {
        assertTrue(KafuNet.isValidHost(WIKI_STRING + ":1"));
        assertFalse(KafuNet.isValidHost(WIKI_STRING + ":-2"));
        assertFalse(KafuNet.isValidHost(WIKI_STRING + ":-1")); // I dont wanna copy the jdk way ...
        // ... so this doesnt work anymore assertEquals("self.wiki.org", new Host("self.wiki.org:-1").toString());
        assertFalse(KafuNet.isValidHost(WIKI_STRING + ":909550"));

        final Host telnet = Port.TELNET.attachTo(WIKI_HOSTNAME);
        assertTrue(KafuNet.isValidHost(telnet.toString()));
        assertFalse(KafuNet.isValidHostname(telnet.toString()));
        assertEquals(Port.TELNET, telnet.getPort());

        final Host http = new Host(WIKI_HOSTNAME + ":80");
        assertTrue(KafuNet.isValidHost(http.toString()));
        assertFalse(KafuNet.isValidHostname(http.toString()));
        assertEquals(Port.HTTP, http.getPort());
    }
}
