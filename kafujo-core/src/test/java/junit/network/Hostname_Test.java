package junit.network;

import net.kafujo.base.RequirementException;
import net.kafujo.network.Hostname;
import net.kafujo.network.KafuNet;
import net.kafujo.network.Port;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;
import static junit.network.NetworkData.*;

class Hostname_Test {

    Hostname UNRESOLVEABLE = new Hostname("sfasfsdfaadhcverea.sfassdfaf.sfasfadf.de");

    @Test
    void ctorAndToString() {
        final Hostname wiki = new Hostname(WIKI_STRING);
        assertTrue(wiki.getPort().isEmpty());
        assertEquals(WIKI_STRING, wiki.getIdentifier());
        assertEquals(WIKI_STRING, wiki.toString());
    }

    @Test
    void ctorMustFail() {
        for (String host : INVALID_HOSTS) {
            assertThrows(RequirementException.class,
                    () -> new Hostname(host),
                    "Created a Hostname with invalid hostname: " + host);
        }
    }

    @Test
    void ignorePort() {
        var noPort = new Hostname(WIKI_STRING);
        var ignoredPort = new Hostname(Port.POP3.attachTo(WIKI_STRING));
        assertEquals(noPort, ignoredPort);
    }

    @Test
    void ignorePortofUrl() throws MalformedURLException {
        final URL urlNoPort = new URL("http", WIKI_STRING, -1, "/");
        final Hostname noPort = Hostname.ofUrl(urlNoPort);

        final URL urlWithPort = new URL("http", WIKI_STRING, 8090, "/");
        final Hostname ignoredPort = Hostname.ofUrl(urlWithPort);

        assertEquals(noPort, ignoredPort);
        assertEquals(WIKI_STRING, ignoredPort.getIdentifier());
        assertEquals(WIKI_STRING, noPort.toString());
    }

    @Test
    @Tag("online")
    void resolvable() {
        assertTrue(WIKI_HOSTNAME.isResolvable());
        assertFalse(UNRESOLVEABLE.isResolvable());
    }

    @Test
    @Tag("online")
    void addr() {
        assertNotNull(WIKI_HOSTNAME.toInetAddress());
    }

    @Test
    void toUrlNoPort() {
//        URL url = KAFUJO.toUrl("http");
//        assertEquals(Port.NOT_PRESENT.intValue(), url.getPort());
        // todo assertEquals(KAFUJO, new Hostname(url));
    }

    @Test
    void toUrlHttpPort() {
//        URL url = KAFUJO.toUrl("http");
//        assertEquals(-1, url.getPort(), "hostnames have not port");
        // todo assertEquals(KAFUJO, new Hostname(url));
    }

    @Test
    void validateInvalids() {
        INVALID_HOSTNAMES.forEach(e -> assertFalse(KafuNet.isValidHostname(e), e + " should be an INVALID hostname"));
    }

    @Test
    void validateValids() {
        VALID_HOSTNAMES.forEach(e -> assertTrue(KafuNet.isValidHostname(e), e + " should be VALID hostname"));
    }

}
