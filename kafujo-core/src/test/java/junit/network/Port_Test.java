package junit.network;

import net.kafujo.network.Host;
import net.kafujo.network.Port;
import net.kafujo.sec.Randomize;
import net.kafujo.text.KafuText;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static net.kafujo.network.Port.MAX_PORT_NO;
import static net.kafujo.network.Port.NOT_PRESENT;
import static org.junit.jupiter.api.Assertions.*;
import static junit.network.NetworkData.*;

class Port_Test {

    @Test
    void valid() {
        assertTrue(Port.isValid(10));
        assertTrue(Port.isValid(MAX_PORT_NO));
        assertFalse(Port.isValid(MAX_PORT_NO + 1));

        assertTrue(Port.isValid(Port.HTTP.intValue()));
        assertTrue(Port.isValid(Port.MYSQL.intValue()));
        assertTrue(Port.isValid(Port.POSTGRESQL.shortValue()));

        assertFalse(Port.isValid(-1)); // NOT a valid port but would be valid to create Port NOT_PRESENT
        assertFalse(Port.isValid(-2));
    }

    @Test
    void ctor() {
        assertEquals(Port.HTTP, new Port(80));

        final Port na = new Port(-1);
        assertEquals(NOT_PRESENT, na);
        assertFalse(na.isPresent());
        assertTrue(na.isEmpty());
    }

    @Test
    void edgeCases() {
        assertEquals(0, new Port(0).intValue());
        assertEquals(65, new Port(65).shortValue());
        assertEquals(65535L, new Port(65535).longValue());
        assertEquals(NOT_PRESENT, new Port(-1));

        assertThrows(IllegalArgumentException.class, () -> new Port(-2));
        assertThrows(IllegalArgumentException.class, () -> new Port(70000));
        assertThrows(IllegalArgumentException.class, () -> new Port(MAX_PORT_NO + 1));

        assertEquals(0, Port.MIN_PORT_NO);

    }

    @Test
    void ofString() {
        assertEquals(Port.HTTP, Port.of("80"));

        final Port na = Port.of(KafuText.UNIVERSAL_NA);
        assertEquals(NOT_PRESENT, na);
        assertEquals(Port.of("-1"), na);
        assertEquals(new Port(-1), na);

        assertFalse(na.isPresent());
        assertTrue(na.isEmpty());
    }

    @Test
    void ofUrlOk() throws MalformedURLException {
        final Port http = Port.ofUrl(new URL("http://www.kernel.org:80"));
        final Port noPort = Port.ofUrl(new URL("http://www.kernel.org"));

        assertEquals(Port.HTTP, http);
        assertTrue(http.isPresent());
        assertFalse(http.isEmpty());

        assertEquals(NOT_PRESENT, noPort);
        assertFalse(noPort.isPresent());
        assertTrue(noPort.isEmpty());
    }

    @Test
    void ofUrlEdgeCases () throws MalformedURLException {
        final Port minusOne = Port.ofUrl(new URL("http://www.heide.de:-1")); // yes, this is ok!
        assertEquals(NOT_PRESENT, minusOne);
        assertFalse(minusOne.isPresent());
        assertTrue(minusOne.isEmpty());

        assertThrows(MalformedURLException.class, () -> new URL("http://www.heide.de:-2")); // not ok anymore

        final URL invalidPort1 = new URL("http", WIKI_STRING, MAX_PORT_NO + 1, "/"); //jdkfail: its ok, as long ...
        final URL invalidPort2 = new URL("http", WIKI_STRING, Integer.MAX_VALUE, "/"); // ... as port is int

        assertThrows(IllegalArgumentException.class, () -> Port.ofUrl(invalidPort1));
        assertThrows(IllegalArgumentException.class, () -> Port.ofUrl(invalidPort2));
    }


    @Test
    void ofUrlString() {
        assertEquals(Port.HTTP, Port.ofUrl("http://www.kernel.org:80"));
        assertEquals(NOT_PRESENT, Port.ofUrl("http://www.kernel.org"));

        var https = Port.ofUrl("https://www.kernel.org.de:443");
        assertEquals(Port.HTTPS, https);
        assertTrue(https.isPresent());
        assertFalse(https.isEmpty());

        var noPort = Port.ofUrl("https://www.kernel.org");
        assertEquals(NOT_PRESENT, noPort);
        assertFalse(noPort.isPresent());
        assertTrue(noPort.isEmpty());
    }

    @Test
    void ofHost() {
        var http = Port.of(WIKI_HOST_HTTP);
        assertEquals(Port.HTTP, http);
        assertTrue(http.isPresent());
        assertFalse(http.isEmpty());

        var noPort = Port.of(WIKI_HOST);
        assertEquals(NOT_PRESENT, noPort);
        assertFalse(noPort.isPresent());
        assertTrue(noPort.isEmpty());
    }


    @Test
    void toStrings() {
        final var without = Port.of(WIKI_HOST);
        final var with = Port.of(WIKI_HOST_HTTP);

        assertEquals(KafuText.UNIVERSAL_NA, without.toString());
        assertEquals("", without.toHostAttachment());

        assertEquals("80", with.toString());
        assertEquals(":80", with.toHostAttachment());

        // test some constants
        assertEquals(KafuText.UNIVERSAL_NA, NOT_PRESENT.toString());
        assertEquals("", NOT_PRESENT.toHostAttachment());
        assertEquals(":80", Port.HTTP.toHostAttachment());
    }


    @Test
    void attachToString() {
        assertEquals("safe.but.pointless:22", Port.SSH.attachTo("safe.but.pointless"));
        assertEquals(":20", Port.FTP.attachTo(""));
        assertEquals("null:20", Port.FTP.attachTo((String)null));
    }

    @Test
    void attachToHost() {
        assertTrue(WIKI_HOSTNAME.getPort().isEmpty());

        final Host ftpWiki = Port.FTP.attachTo(WIKI_HOSTNAME);
        assertTrue(ftpWiki.getPort().isPresent());
        assertEquals(Port.FTP, ftpWiki.getPort());

        assertEquals(ftpWiki, Port.FTP.attachTo(ftpWiki));

        assertThrows(IllegalArgumentException.class, () -> Port.HTTP.attachTo(ftpWiki));
        assertThrows(IllegalArgumentException.class, () -> NOT_PRESENT.attachTo(ftpWiki));
    }


    @Test
    void attachToUrl() throws MalformedURLException {
        final URL without = new URL("http", WIKI_STRING, -1, "/hello");
        final URL with = new URL("http", WIKI_STRING, 80, "/hello");

        assertEquals(with, Port.HTTP.attachToUrl(with));   // stays the same, so its ok ...
        assertThrows(IllegalArgumentException.class, () -> Port.HTTPS.attachToUrl(with)); // ... but changing fails
        assertEquals(with, Port.HTTP.attachToUrl(without));

        assertEquals(without, NOT_PRESENT.attachToUrl(without)); // stays the same ...
        assertThrows(IllegalArgumentException.class, () -> NOT_PRESENT.attachToUrl(with)); // cannot "unset" port like this
    }

    @Test
    void attachToUrlString() {
        // wont accept illegal urls (no protocol in this case):
        assertThrows(IllegalArgumentException.class, () -> Port.HTTP.attachToUrl(WIKI_STRING));

        final String without = "http://" + WIKI_STRING;
        final String with = without + ":80";

        assertEquals(without, NOT_PRESENT.attachToUrl(without)); // stays the same, so its ok
        assertThrows(IllegalArgumentException.class, () -> NOT_PRESENT.attachToUrl(with)); // cannot "unset" port like this
        assertEquals(with, Port.HTTP.attachToUrl(without));

        assertEquals(with, Port.HTTP.attachToUrl(with)); // attach the same port works - url wont change
        assertThrows(IllegalArgumentException.class, () -> Port.HTTPS.attachToUrl(with));
    }

    @Test
    void ranges() {
        assertTrue(Port.FTPCTL.isSystemPort());
        assertFalse(Port.IRC.isUserPort());
        assertFalse(Port.DNS.isDynamicPort());
        assertEquals(new Port(80), Port.HTTP);

        assertTrue(new Port(Port.MAX_SYSTEM_PORT_NO).isSystemPort());
        assertFalse(new Port(Port.MAX_SYSTEM_PORT_NO + 1).isSystemPort());

        assertTrue(new Port(Port.MAX_SYSTEM_PORT_NO + 1).isUserPort());
        assertTrue(new Port(8080).isUserPort());
        assertTrue(new Port(Port.MAX_USER_PORT_NO).isUserPort());

        assertFalse(new Port(Port.MAX_USER_PORT_NO + 1).isUserPort());
        assertTrue(new Port(Port.MAX_USER_PORT_NO + 1).isDynamicPort());
        assertTrue(new Port(Port.MAX_PORT_NO).isDynamicPort());
        assertFalse(Port.isValid(Port.MAX_PORT_NO + 1));
    }

    @Test
    void randomUserPort() {
        for (int i = 0; i < 1000; i++) {
            Port user = new Port (Randomize.JUL.nextInt(Port.MAX_SYSTEM_PORT_NO +1, Port.MAX_USER_PORT_NO));
            assertTrue(user.isUserPort());
        }
    }
}
