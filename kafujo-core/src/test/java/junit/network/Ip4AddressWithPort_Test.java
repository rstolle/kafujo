package junit.network;

import net.kafujo.network.Ip4AddressOptionalPort;
import net.kafujo.network.KafuNet;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static net.kafujo.network.Port.IMAP;
import static net.kafujo.network.Port.IMAPS;
import static org.junit.jupiter.api.Assertions.*;
import static junit.network.NetworkData.IP4_HOME_1_0;
import static junit.network.NetworkData.WIKI_STRING;

class Ip4AddressWithPort_Test {

    @Test
    void ctor1ParamWithPort() {
        final Ip4AddressOptionalPort home = new Ip4AddressOptionalPort(IMAP.attachTo(IP4_HOME_1_0));
        assertEquals(IP4_HOME_1_0, home.getIdentifier());
        assertEquals(IMAP.attachTo(IP4_HOME_1_0), home.toString());
        assertThrows(IllegalArgumentException.class, () -> new Ip4AddressOptionalPort(WIKI_STRING));
    }

    @Test
    void ctor1ParamNoPort() {
        final Ip4AddressOptionalPort home = new Ip4AddressOptionalPort(IP4_HOME_1_0);
        assertEquals(IP4_HOME_1_0, home.getIdentifier());
        assertTrue(home.getPort().isEmpty());
    }

    @Test
    void ctor2ParamsWithPort() {
        final Ip4AddressOptionalPort home = new Ip4AddressOptionalPort(IP4_HOME_1_0, IMAP);
        assertEquals(IP4_HOME_1_0, home.getIdentifier());
        assertEquals(IMAP.attachTo(IP4_HOME_1_0), home.toString());
        assertThrows(IllegalArgumentException.class, () -> new Ip4AddressOptionalPort(WIKI_STRING));
    }

    @Test
    void ofUrl() throws MalformedURLException {
        final URL urlNoPort = new URL("http", IP4_HOME_1_0, -1, "/");
        final Ip4AddressOptionalPort noPort = Ip4AddressOptionalPort.ofUrl(urlNoPort);

        assertEquals(IP4_HOME_1_0, noPort.getIdentifier());
        assertEquals(IP4_HOME_1_0, noPort.toString());
        assertTrue(noPort.getPort().isEmpty());
        assertTrue(KafuNet.isValidIp4Address(noPort.getIdentifier()));

        final URL urlWithPort = new URL("http", IP4_HOME_1_0, 993, "/");
        final Ip4AddressOptionalPort withPort = Ip4AddressOptionalPort.ofUrl(urlWithPort);

        assertFalse(withPort.getPort().isEmpty());
        assertEquals(IMAPS, withPort.getPort());

        assertEquals(withPort.getIdentifier(), noPort.getIdentifier());
        assertNotEquals(withPort.toString(), noPort.toString());
    }

    @Test
    void ofUrlFail() throws MalformedURLException {
        final URL urlNoPort = new URL("http", WIKI_STRING, -1, "/");
        assertThrows(IllegalArgumentException.class, () -> Ip4AddressOptionalPort.ofUrl(urlNoPort));
    }
}
