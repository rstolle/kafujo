package junit.network;

import net.kafujo.network.Host;
import net.kafujo.network.Hostname;
import net.kafujo.network.Port;

import java.util.List;

public class NetworkData {

    public final static String IP4_HOME_1_0 = "192.168.1.0";
    public final static String IP4_HOME_1_0_8080 = "192.168.1.0:8080";

    //java.net.URL can't deal with hostnames including '@'. See tests for details
    public static final String HOSTNAME_JDKFAIL_AT = "http://dsdds@@@@:20";

    public static final String WIKI_STRING = "www.wikipedia.org";
    public static final Host WIKI_HOST = new Host(WIKI_STRING);
    public static final Host WIKI_HOST_HTTP = new Host(WIKI_STRING, Port.HTTP);
    public static final Hostname WIKI_HOSTNAME = new Hostname(WIKI_STRING);

    public final static List<String> VALID_HOSTNAMES = List.of("cnn.com",
            "www.heise.de",
            "de.wikipedia.org",
            "heise.de",
            "kafujo.net",
            "23.123.123.123");

    public static final List<String> INVALID_HOSTNAMES = List.of("", ".", "@", "'",
            "www.we@heaven.com",
            "www.wee,aven.com",
            "@12",
            "kernel.org/",
            "adfas@",
            "tron.@@.net",
            "de.wikipedia.org:8080",
            HOSTNAME_JDKFAIL_AT);


    public final static List<String> VALID_HOSTS = List.of("cnn.com",
            "www.heise.de",
            "de.wikipedia.org",
            "heise.de",
            "kafujo.net",
            "de.wikipedia.org:8080",
            "23.123.123.123",
            WIKI_STRING,
            WIKI_HOST.toString(),
            IP4_HOME_1_0,
            IP4_HOME_1_0_8080);

    public static final List<String> INVALID_HOSTS = List.of(".", "@", "'",
            "@12",
            "kernel.org/",
            "adfas@",
            "tron.@@.net",
            HOSTNAME_JDKFAIL_AT);
}
