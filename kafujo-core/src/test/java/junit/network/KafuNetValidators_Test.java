package junit.network;

import net.kafujo.network.KafuNet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class KafuNetValidators_Test {

    @Test
    void valids() {
        assertTrue(KafuNet.isValidIp4Address("123.211.022.0"));
        assertTrue(KafuNet.isValidIp4Address("192.168.1.255"));
        assertFalse(KafuNet.isValidIp4Address("192.168.1.256"));
        assertTrue(KafuNet.isValidIp4Address("023.011.000.0"));

        assertTrue(KafuNet.isValidIp4Address(NetworkData.IP4_HOME_1_0));
        assertFalse(KafuNet.isValidIp4Address(NetworkData.IP4_HOME_1_0_8080));

        assertFalse(KafuNet.isValidIp4Address("aas#asdfaasdfasd"));
        assertFalse(KafuNet.isValidIp4Address("123.211.322.0"));
        assertFalse(KafuNet.isValidIp4Address("123.211.O22.0"));

        assertFalse(KafuNet.isValidIp4Address(""));
        assertFalse(KafuNet.isValidIp4Address(null));
    }

}
