/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package junit.unit;

import net.kafujo.units.KafuDateTime;
import net.kafujo.units.KafuTime;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.time.temporal.Temporal;
import java.time.temporal.UnsupportedTemporalTypeException;

import static net.kafujo.text.KafuText.UNIVERSAL_NA;
import static org.junit.jupiter.api.Assertions.*;

class KafuTime_Test {

    private final static Temporal ONLY_DATE = LocalDate.now();
    private final static Temporal ONLY_TIME = LocalTime.now();


    @Test
    void time() {
        assertNotNull(KafuTime.ususal());
        assertNull(KafuTime.ususal(null, null));
        assertNull(KafuTime.ususal(ONLY_DATE, null));
        assertEquals(UNIVERSAL_NA, KafuTime.ususal(ONLY_DATE, UNIVERSAL_NA));
        assertThrows (UnsupportedTemporalTypeException.class, () -> KafuTime.ususal(ONLY_DATE));
    }

    @Test
    void date() {
        assertNotNull(KafuTime.ususalDate());
        assertNull(KafuTime.ususalDate(null, null));

        assertNull(KafuTime.ususalDate(ONLY_TIME, null));
        assertEquals(UNIVERSAL_NA, KafuTime.ususalDate(ONLY_TIME, UNIVERSAL_NA));

        assertThrows (UnsupportedTemporalTypeException.class, () -> KafuTime.ususalDate(ONLY_TIME));
    }

    @Test
    void timestamp() {
        assertNotNull(KafuDateTime.full());
        assertNull(KafuDateTime.full(null, null));

        assertNull(KafuDateTime.full(ONLY_TIME, null));
        assertEquals(UNIVERSAL_NA, KafuDateTime.full(ONLY_TIME, UNIVERSAL_NA));

        assertThrows (UnsupportedTemporalTypeException.class, () -> KafuDateTime.full(ONLY_TIME));
    }

    @Test
    void offsets() throws InterruptedException{
        ZoneId z1 = ZoneId.of("Europe/Paris");
        ZoneId z2 = ZoneId.of("Europe/Berlin");

        assertNotEquals(z1, z2);

        ZoneOffset o1 = KafuDateTime.getUtcOffset(z1);
        ZoneOffset o2 = KafuDateTime.getUtcOffset(z2);

        assertEquals(o1, o2);
        assertTrue(KafuDateTime.sameUtcOffset(z1, z2));

        ZoneId z3 = ZoneId.of("Asia/Aden");
        assertNotEquals(z1, z2);
        assertNotEquals(o1, KafuDateTime.getUtcOffset(z3));
        assertFalse(KafuDateTime.sameUtcOffset(z1, z3));

        var time1 = ZonedDateTime.now(z1);
        Thread.sleep(100);
        var time2 = ZonedDateTime.now(z2);

        assertNotEquals(time1, time2);
        assertTrue(KafuDateTime.sameUtcOffset(time1, time2));
    }
}
