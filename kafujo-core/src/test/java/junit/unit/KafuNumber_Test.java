package junit.unit;

import net.kafujo.base.RequirementException;
import net.kafujo.units.DataSize;
import net.kafujo.units.KafuNumber;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static net.kafujo.units.KafuNumber.MAX_LONG_BDEC;
import static net.kafujo.units.KafuNumber.MIN_LONG_BDEC;
import static org.junit.jupiter.api.Assertions.*;

class KafuNumber_Test {

    private final BigDecimal MAX_LONG_PLUS_ONE = MAX_LONG_BDEC.add(BigDecimal.ONE);
    private final BigDecimal MIN_LONG_MINUS_ONE = MIN_LONG_BDEC.subtract(BigDecimal.ONE);

    @Test
    void bigDecimalLongRange() {

        assertEquals(Long.MAX_VALUE, MAX_LONG_BDEC.longValue());
        assertEquals(Long.MIN_VALUE, MIN_LONG_BDEC.longValue());

        assertTrue(KafuNumber.isLongRange(BigDecimal.ONE));
        assertTrue(KafuNumber.isLongRange(MAX_LONG_BDEC));
        assertTrue(KafuNumber.isLongRange(MIN_LONG_BDEC));

        assertFalse(KafuNumber.isLongRange(MAX_LONG_PLUS_ONE));
        assertFalse(KafuNumber.isLongRange(MIN_LONG_MINUS_ONE));

        assertThrows(RequirementException.class, () -> KafuNumber.requireLongRange(MAX_LONG_PLUS_ONE));
        assertThrows(RequirementException.class, () -> KafuNumber.requireLongRange(MIN_LONG_MINUS_ONE));
    }

    @Test
    void inclusiveExclusive() {
        assertTrue(KafuNumber.isBetweenInclusive(BigDecimal.ONE, MIN_LONG_BDEC, MAX_LONG_BDEC));
        assertTrue(KafuNumber.isBetweenExclusive(BigDecimal.ONE, MIN_LONG_BDEC, MAX_LONG_BDEC));

        assertTrue(KafuNumber.isBetweenInclusive(MIN_LONG_BDEC, MIN_LONG_BDEC, MAX_LONG_BDEC));
        assertTrue(KafuNumber.isBetweenInclusive(MAX_LONG_BDEC, MIN_LONG_BDEC, MAX_LONG_BDEC));

        assertFalse(KafuNumber.isBetweenExclusive(MIN_LONG_BDEC, MIN_LONG_BDEC, MAX_LONG_BDEC));
        assertFalse(KafuNumber.isBetweenExclusive(MAX_LONG_BDEC, MIN_LONG_BDEC, MAX_LONG_BDEC));

        assertTrue(KafuNumber.isBetweenExclusive(MIN_LONG_BDEC, MIN_LONG_MINUS_ONE, MAX_LONG_PLUS_ONE));
        assertTrue(KafuNumber.isBetweenExclusive(MAX_LONG_BDEC, MIN_LONG_MINUS_ONE, MAX_LONG_PLUS_ONE));

        assertTrue(KafuNumber.isBetweenExclusive(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN));

        assertThrows(IllegalArgumentException.class,
                () -> assertTrue(KafuNumber.isBetweenExclusive(BigDecimal.ONE, BigDecimal.TEN, BigDecimal.ZERO)));

    }

    @Test
    void isZero() {
        assertTrue(KafuNumber.isZero(0));
        assertTrue(KafuNumber.isZero(0L));
        assertTrue(KafuNumber.isZero(0.0));
        assertTrue(KafuNumber.isZero(-0));
        assertTrue(KafuNumber.isZero(-0L));
        assertTrue(KafuNumber.isZero(-0.0));

        // BigInteger
        assertTrue(KafuNumber.isZero(new BigInteger("0")));
        assertTrue(KafuNumber.isZero(BigInteger.valueOf(0)));
        assertTrue(KafuNumber.isZero(BigInteger.ZERO));
        assertTrue(KafuNumber.isZero(new BigInteger("-0")));

        // BigDecimal
        assertTrue(KafuNumber.isZero(new BigDecimal("0")));
        assertTrue(KafuNumber.isZero(new BigDecimal("0.0")));
        assertTrue(KafuNumber.isZero(new BigDecimal("0.000000000")));
        assertTrue(KafuNumber.isZero(BigDecimal.valueOf(0)));
        assertTrue(KafuNumber.isZero(BigDecimal.ZERO));
        assertTrue(KafuNumber.isZero(new BigDecimal("-0.0")));
        assertTrue(KafuNumber.isZero(new BigDecimal("-0.00000000")));

        // Atomic
        assertTrue(KafuNumber.isZero(new AtomicInteger(0)));
        assertTrue(KafuNumber.isZero(new AtomicLong(0)));

        // DataSize
        assertTrue(KafuNumber.isZero(DataSize.of(-0)));
        assertTrue(KafuNumber.isZero(DataSize.ZERO));
        assertTrue(KafuNumber.isZero(DataSize.of(0)));

    }

    @Test
    void isNotZero() {
        assertFalse(KafuNumber.isZero(1));
        assertFalse(KafuNumber.isZero(-1));
        assertFalse(KafuNumber.isZero(1L));

        assertFalse(KafuNumber.isZero(0.001));
        assertFalse(KafuNumber.isZero(-0.00000001));

        // BigInteger
        assertFalse(KafuNumber.isZero(new BigInteger("10")));
        assertFalse(KafuNumber.isZero(BigInteger.valueOf(-1)));
        assertFalse(KafuNumber.isZero(BigInteger.TEN));

        // BigDecimal
        assertFalse(KafuNumber.isZero(new BigDecimal("0.0000000000000000000000000000000000000000000000001")));
        assertFalse(KafuNumber.isZero(BigDecimal.valueOf(-1)));
        assertFalse(KafuNumber.isZero(BigDecimal.ONE));
        assertFalse(KafuNumber.isZero(BigDecimal.TEN.multiply(BigDecimal.valueOf(-1))));

        // misc
        assertFalse(KafuNumber.isZero(new AtomicInteger(1)));
        assertFalse(KafuNumber.isZero(new AtomicLong(-1L)));

        assertFalse(KafuNumber.isZero(DataSize.ONE_BYTE));
    }

    @Test
    void isNegative() {
        assertTrue(KafuNumber.isNegative(-1));
        assertTrue(KafuNumber.isNegative(-10L));
        assertTrue(KafuNumber.isNegative(-0.4));

        assertFalse(KafuNumber.isNegative(1));
        assertFalse(KafuNumber.isNegative(10L));
        assertFalse(KafuNumber.isNegative(0.043));

        assertFalse(KafuNumber.isNegative(0));
        assertFalse(KafuNumber.isNegative(0L));
        assertFalse(KafuNumber.isNegative(0.0));
        assertFalse(KafuNumber.isNegative(-0));
        assertFalse(KafuNumber.isNegative(-0L));
        assertFalse(KafuNumber.isNegative(-0.0));

        // BigInteger
        assertTrue(KafuNumber.isNegative(new BigInteger("-30")));
        assertFalse(KafuNumber.isNegative(new BigInteger("-0")));
        assertFalse(KafuNumber.isNegative(new BigInteger("1432324421")));
        assertFalse(KafuNumber.isNegative(new BigInteger("0")));

        // BigDecimal
        assertTrue(KafuNumber.isNegative(new BigDecimal("-0.000000000000000000000000000001")));
        assertFalse(KafuNumber.isNegative(new BigDecimal("-0.0")));
        assertFalse(KafuNumber.isNegative(new BigDecimal("0.0")));
        assertFalse(KafuNumber.isNegative(new BigDecimal("3214.353254352")));

        // misc
        assertFalse(KafuNumber.isNegative(new AtomicInteger(1)));
        assertTrue(KafuNumber.isNegative(new AtomicLong(-1L)));

        assertFalse(KafuNumber.isNegative(DataSize.ZERO));
        assertFalse(KafuNumber.isNegative(DataSize.ONE_EiB));
        assertTrue(KafuNumber.isNegative(DataSize.of("-0.0000001GiB")));
    }
}
