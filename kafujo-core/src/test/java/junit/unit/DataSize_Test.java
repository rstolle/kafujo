package junit.unit;

import net.kafujo.config.KafuVm;
import net.kafujo.sec.Randomize;
import net.kafujo.units.DataSize;
import net.kafujo.units.DataSizeUnit;
import org.junit.jupiter.api.Test;
import support.Data;

import java.math.BigDecimal;

import static net.kafujo.units.DataSize.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DataSize_Test {

    private final long ONE_AND_HALF_KiB = 1536L;  // 1.5 KiB
    private final long ONE_MiB_IN_BYTES = 1048576L;
    private final long ONE_TiB_IN_BYTES = 1099511627776L;
    private final long ONE_EiB_IN_BYTES = 1152921504606846976L;

    @Test
    void allUnitSizes() {

        long bytesIec = 1;
        long bytesIs = 1;

        for (DataSizeUnit du : DataSizeUnit.values()) {
            assertEquals(du.size, DataSize.of(1, du));
            if (du.isIec()) {
                assertEquals(bytesIec, du.size.longValue());
                bytesIec *= 1024;
            }

            if (du.isIs()) {
                assertEquals(bytesIs, du.size.longValue());
                bytesIs *= 1000;
            }
        }
    }

    @Test
    void orderAndSize() {

        DataSizeUnit tmp = null;
        for (DataSizeUnit du : DataSizeUnit.values()) {

            if (tmp == null) {
                assertEquals(DataSizeUnit.Byte, du);
                tmp = du;
            } else {
                assertTrue(tmp.size.isSmallerThan(du.size));
                tmp = du;
            }
        }
    }

    @Test
    void formatting() {
        DataSize ds1023 = DataSize.of(1023);
        assertEquals("1023 Byte", ds1023.toString(10));
        DataSize ds1024 = DataSize.of(1024);
        assertEquals("1.00 KiB", ds1024.toString());

        DataSize dsOneLessThanMiB = DataSize.of(ONE_MiB_IN_BYTES - 1);
        assertEquals("1023.999 KiB", dsOneLessThanMiB.toString(3));

        // todo is this really what I want, or should this be 1023.99 ? But how?
        assertEquals("1024.00 KiB", dsOneLessThanMiB.toString(2));
        assertEquals("1.00 MiB", ONE_MiB.toString());
    }


    @Test
    void units() {
        assertEquals(ONE_TiB_IN_BYTES, ONE_TiB.longValue());
        assertEquals(ONE_EiB_IN_BYTES, ONE_EiB.longValue());

        assertEquals(ONE_MiB, ONE_KiB.times(1024));
        assertEquals(ONE_MB, ONE_KB.times(1000));

        DataSize b1536 = DataSize.of(new BigDecimal("1.5"), DataSizeUnit.KiB);
        assertEquals(ONE_AND_HALF_KiB, b1536.longValue());
    }


    @Test
    void overflow() {
        DataSize max = DataSize.of(Long.MAX_VALUE);
        assertEquals(max.toString(30), "7.999999999999999999132638262012 EiB");
        assertThrows(ArithmeticException.class, () -> max.plus(ONE_BYTE), "overflow should happen");
    }


    @Test
    void parse() {
        final DataSize tenMiB1 = DataSize.of(10, DataSizeUnit.MiB);
        final DataSize tenMiB2 = DataSize.of(ONE_MiB_IN_BYTES * 10);

        assertEquals(tenMiB1, tenMiB2);

        DataSize tenMiBparsed = DataSize.of("10 MiB");
        assertEquals(tenMiB1, tenMiBparsed);

        tenMiBparsed = DataSize.of(ONE_MiB_IN_BYTES * 10 + " Byte");
        assertEquals(tenMiB1, tenMiBparsed);

        tenMiBparsed = DataSize.of(DataSizeUnit.GiB.format(tenMiB1, 15)); // "0.009765625000000 GiB"

        assertEquals(tenMiB1, tenMiBparsed);

        tenMiBparsed = DataSize.of(DataSizeUnit.GiB.format(tenMiB1, 4)); // "0.0097 GiB"  NOT ENOUGH

        assertNotEquals(tenMiB1, tenMiBparsed);
    }


    @Test
    void formattingBytes() {
        // formatting using the most appropriate unit
        assertEquals(ONE_BYTE.toString(), DataSize.toString(1L));
        assertEquals(ONE_BYTE.toString(), DataSize.toString(1L));
        assertEquals(ONE_KB.toString(), DataSize.toString(1000L));
        assertEquals(ONE_KiB.toString(), DataSize.toString(1024L));
        assertEquals(ONE_KiB.toString(5), DataSize.toString(1024, 5));

        // formatting using specific units
        assertEquals(DataSizeUnit.Byte.format(ONE_KiB), DataSizeUnit.Byte.format(1024));
    }

    @Test
    void of() {
        final DataSize twoGB = DataSize.of(2, DataSizeUnit.GiB);
        assertEquals(twoGB.minus(ONE_GiB), ONE_GiB);
    }


    @Test
    void biggerSmaller() {
        final DataSize oneKb = DataSize.of(1024);
        assertEquals(oneKb, ONE_KiB);

        final DataSize twoKiB = DataSize.of(2, DataSizeUnit.KiB);

        assertTrue(oneKb.isSmallerThan(twoKiB));
        assertTrue(twoKiB.isBiggerThan(oneKb));

        DataSize one2 = twoKiB.minus(oneKb);
        assertEquals(oneKb, one2);
    }

    @Test
    void plusAndMultiply() {
        final DataSize oneKb = ONE_KiB;

        final DataSize twoKb = oneKb.times(3);
        assertTrue(oneKb.isSmallerThan(twoKb));
        assertTrue(twoKb.isBiggerThan(oneKb));

        DataSize tmp = twoKb.minus(oneKb);
        assertEquals(tmp, ONE_KiB.times(2));

        tmp = tmp.minus(oneKb);
        assertEquals(tmp, ONE_KiB);
    }

    @Test
    void decreaseToZero() {

        DataSize size = ONE_MiB;
        for (int i = 0; i < 1024; i++) {
            size = size.minus(ONE_KiB);
        }
        assertEquals(DataSize.ZERO, size);

        size = DataSize.of(2, DataSizeUnit.KiB);
        for (int i = 0; i < 1024; i++) {
            size = size.minus(ONE_BYTE);
        }
        assertEquals(ONE_KiB, size);

        for (int i = 0; i < 1024; i++) {
            size = size.minus(ONE_BYTE);
        }
        assertEquals(DataSize.ZERO, size);
    }

    @Test
    void ofString() {
        DataSize ds = DataSize.of("1.0", DataSizeUnit.MiB);
        assertEquals(ONE_MiB, ds);
    }

    @Test
    void ofUnitString() {
        DataSize ds = DataSize.of("1.0MiB");
        assertEquals(ONE_MiB, ds);

        ds = DataSize.of("1.0 MiB");
        assertEquals(ONE_MiB, ds);

        ds = DataSize.of("   1.0MiB");
        assertEquals(ONE_MiB, ds);

        ds = DataSize.of("  1.0    MiB   ");
        assertEquals(ONE_MiB, ds);

        ds = DataSize.of("1.0  MiB  ");
        assertEquals(ONE_MiB, ds);
    }

    @Test
    void ofUnitStrings() {
        for (int i = 1; i < 100; i++) {
            long bytes = Randomize.JUL.nextInt(); // not long to avoid overflow (see overflowOn8EiB())

            DataSize ds = DataSize.of(bytes);
            assertEquals(bytes, ds.longValue());
            DataSize ds2 = DataSize.of(ds.toString());

            // We can only compare the formatted values, cause of rounding, e.g.:
            // 1100000Bytes are formatted 1.05 MiB but if we parse this, we get 1101004 bytes
            // In this example, we could use toString(11) -> 1.04904174805 MiB which would
            // parse back to 1100000 bytes. But with the big numbers, we would need more than
            // 11 digits. See ofUnitStringsLooooong
            assertEquals(ds.toString(), ds2.toString());

        }
    }

    /**
     * Converts the DataSize to 100 digit Strings and parses these back to DataSize
     */
    @Test
    void ofUnitStringsLooooong() {
        for (int i = 1; i < 100; i++) {
            long bytes = Randomize.JUL.nextLong();

            DataSize ds = DataSize.of(bytes);
            // System.out.println(ds.toString(100));
            assertEquals(bytes, ds.longValue());
            DataSize ds2 = DataSize.of(ds.toString(100));
            assertEquals(ds, ds2);

        }
    }

    @Test
    void negative() {
        final DataSize negative = DataSize.of(ONE_MiB.longValue() * -1);

        assertTrue(negative.isNegative());
        assertTrue(negative.isSmallerThan(ZERO));
        assertFalse(negative.isBiggerThan(ZERO));
        assertFalse(negative.isZero());

        assertEquals("-1.00 MiB", negative.toString());
        assertEquals("-1.00 Mebibyte", negative.toStringLong());

        final var positive = negative.abs();
        assertFalse(positive.isNegative());
        assertFalse(positive.isSmallerThan(ZERO));
        assertTrue(positive.isBiggerThan(ZERO));
        assertFalse(positive.isZero());

        assertEquals("1.00 MiB", positive.toString());
        assertEquals("1.00000 Mebibyte", positive.toStringLong(5));


        assertEquals(ZERO, negative.plus(positive));


    }

    @Test
    void longFormatting() {
        DataSize ds = DataSize.of(ONE_MiB.longValue());
        assertEquals("1.00 Mebibyte", ds.toStringLong());

        ds = DataSize.of(ONE_MiB.longValue() + 100);

        // Mebibyte!!
        assertEquals("1.00 Mebibyte", ds.toStringLong());

        // Mebibytes !!
        assertEquals("1.0000953674 Mebibytes", ds.toStringLong(10));
    }


    /**
     * This test covers the problem that a DataSize cannot always be recovered from its String representation.
     */
    @Test
    void overflowOn8EiB() {
        final DataSize tooBigWithTwoDigits = DataSize.of(9219689736071579424L);

        final String tooLessDigits = tooBigWithTwoDigits.toString();
        assertEquals("8.00 EiB", tooLessDigits);

        final String enoughDigits = tooBigWithTwoDigits.toString(60);
        assertEquals("7.996806113192890724805650393136602360755205154418945312500000 EiB", enoughDigits);

        // ok, cause there are enough digits to exactly recover 9219689736071579424L
        final DataSize recoveredFromString = DataSize.of(enoughDigits);
        assertEquals(tooBigWithTwoDigits, recoveredFromString);

        // bigger than DataSize.MAX cause of rounding. Before commit e462777f68fe70057f2f735163c9ce60ae50e062
        // this let to a stackoverflow error, which i did not really investigate yet
        assertThrows(IllegalArgumentException.class, () -> DataSize.of(tooLessDigits));
    }

    @Test
    void minAndMax() {
        assertEquals(Long.MAX_VALUE, MAX_VALUE.longValue());
        assertEquals("8.00 EiB", MAX_VALUE.toString());

        assertEquals(Long.MIN_VALUE + 1, MIN_VALUE.longValue());
        assertEquals(Long.MAX_VALUE * -1, MIN_VALUE.longValue());

        assertEquals("-8.00 EiB", MIN_VALUE.toString());

        assertEquals(MAX_VALUE, DataSize.of("7.999999999999999999132638262011596452794037759304046630859375EiB"));
        assertEquals(MIN_VALUE, DataSize.of("-7.999999999999999999132638262011596452794037759304046630859375EiB"));


        assertThrows(IllegalArgumentException.class, () -> DataSize.of(Long.MIN_VALUE));
        assertThrows(IllegalArgumentException.class, () -> DataSize.of("7.999999999999999999132638262011596452794037759304046630859376EiB"));
        assertThrows(IllegalArgumentException.class, () -> DataSize.of("-7.999999999999999999132638262011596452794037759304046630859376EiB"));

    }

    @Test
    void zero() {
        assertTrue(ZERO.isZero());

        final DataSize ds = DataSize.of(10);
        assertFalse(ds.isZero());

        assertTrue(ds.minus(ds).isZero());
    }

    @Test
    void toStringLong() {
        DataSize ds = DataSize.of("1MiB");

        assertEquals("1.00 Mebibyte", ds.toStringLong());
        assertEquals("1.000 Mebibyte", ds.toStringLong(3));

        assertEquals("1.00 Mebibyte", DataSize.toStringLong(ONE_MiB_IN_BYTES));
        assertEquals("1.000 Mebibyte", DataSize.toStringLong(ONE_MiB_IN_BYTES, 3));
    }

    @Test
    void toXy() {
        final DataSize ds = DataSize.of(10);

        assertEquals(10, ds.intValue());
        assertEquals(10L, ds.longValue());
        assertEquals(BigDecimal.valueOf(10), ds.toBigDecimal());

        final DataSize doubled = ds.plus(10L);

        assertEquals(20, doubled.intValue());
        assertEquals(20L, doubled.longValue());
        assertEquals(BigDecimal.valueOf(20), doubled.toBigDecimal());

        assertEquals(ds, doubled.minus(10L));

    }

    @Test
    void byteArray() {
        final byte[] bytes = ONE_KiB.newByteArray();
        assertThat(bytes).hasSize(1024);

        final var oneByteTooBig = DataSize.of(Integer.MAX_VALUE - 7); // https://stackoverflow.com/a/8381338/7441000
        assertThrows(IllegalArgumentException.class, oneByteTooBig::newByteArray);

        // this passes only when enough heap space given to the vm, e.g.: -Xmx3G
        Data.lgr.info("Memory status: " + KafuVm.getMemoryInfo());
        if (Runtime.getRuntime().maxMemory() > oneByteTooBig.longValue() + ONE_MiB_IN_BYTES * 3) {
            DataSize maxSize = oneByteTooBig.minus(1);
            Data.lgr.info("Allocating " + maxSize + " of heap memory.");
            final byte[] max = maxSize.newByteArray();
            assertThat(max).hasSize(maxSize.intValue());
            Data.lgr.info("Memory status: " + KafuVm.getMemoryInfo());
        } else {
            Data.lgr.info("Skip test cause not enough memory");
        }
    }
}

