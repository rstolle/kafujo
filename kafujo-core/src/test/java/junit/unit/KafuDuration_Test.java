package junit.unit;

import net.kafujo.base.RequirementException;
import net.kafujo.sec.Randomize;
import net.kafujo.text.KafuText;
import net.kafujo.units.KafuDuration;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static java.time.Duration.*;
import static java.util.Locale.ENGLISH;
import static java.util.Locale.GERMAN;
import static net.kafujo.text.KafuText.UNIVERSAL_NA;
import static net.kafujo.units.KafuDuration.*;
import static org.junit.jupiter.api.Assertions.*;

class KafuDuration_Test {

    private static final Duration TWO_DAYS = ofDays(2);
    private static final Duration NULL_DUR = null;

    private static final Duration BIG_SEVEN = ofDays(7)
            .plus(ofHours(7).plus(ofMinutes(7).plus(ofSeconds(7))));

    @Test
    void adaptUnitsEdgesAndFbk() {
        assertEquals("no info", adaptUnitsFbk(null, "no info"));
        assertThrows(NullPointerException.class, () -> adaptUnits(NULL_DUR));
        assertThrows(NullPointerException.class, () -> adaptUnits(null));
    }

    @Test
    @SuppressWarnings("all")
    void edges() {
        assertTrue(adaptUnits(MAX_VALUE).startsWith(KafuText.UNIVERSAL_MAX));
        assertTrue(adaptUnits(MIN_VALUE).startsWith(KafuText.UNIVERSAL_MIN));

        assertTrue(adaptUnits(MAX_VALUE.minusNanos(1)).startsWith("292"));
        assertTrue(adaptUnits(MIN_VALUE.plusNanos(2)).startsWith("-292"));

        assertThrows(ArithmeticException.class, () -> MAX_VALUE.plusNanos(1));
        assertThrows(ArithmeticException.class, () -> MIN_VALUE.minusNanos(1));
    }


    @Test
    void adaptUnitsNanos() {
        assertEquals("1ns", adaptUnits(ONE_NANO));
        assertEquals("10ns", adaptUnits(ofNanos(10)));
        assertEquals("1000ns", adaptUnits(ofNanos(1000)));

        // approaching milli seconds ...
        final long one_milli_nanos = 1000000L;
        assertEquals("999999ns", adaptUnits(ofNanos(one_milli_nanos - 1)));

        // reaching milli seconds
        assertEquals("1ms", adaptUnits(ofNanos(one_milli_nanos)));
        assertEquals("1ms", adaptUnits(ofNanos(one_milli_nanos + 1)));

        assertEquals("10ms", adaptUnits(ofNanos(one_milli_nanos * 10)));
        assertEquals("100ms", adaptUnits(ofNanos(one_milli_nanos * 100)));
        assertEquals("1000ms", adaptUnits(ofNanos(one_milli_nanos * 1000)));
        assertEquals("9999ms", adaptUnits(ofNanos(one_milli_nanos * 10000 - 1)));

        assertEquals("10.00s", adaptUnits(ofNanos(one_milli_nanos * 10000)));
        assertEquals("10.00s", adaptUnits(ofNanos(one_milli_nanos * 10000 + 1)));

        assertTrue(adaptUnitsOfNanos(Randomize.JUL.nextLong(1000000)).endsWith("ns"));
    }

    @Test
    void adaptUnitsMillis() {
        assertEquals("1ms", adaptUnits(ONE_MILLI));
        assertEquals("1010ms", adaptUnits(ofMillis(1010)));
        assertEquals("1234ms", adaptUnits(ofMillis(1234)));
        assertEquals("6666ms", adaptUnits(ofMillis(6666)));
        assertEquals("9999ms", adaptUnits(ofMillis(9999)));
        assertEquals("10.00s", adaptUnits(ofMillis(10000)));
        assertEquals("10.20s", adaptUnits(ofMillis(10200)));
        assertEquals("10.21s", adaptUnits(ofMillis(10210)));
        assertEquals("11.11s", adaptUnits(ofMillis(11111)));
        assertEquals("59.99s", adaptUnits(ofMillis(59999)));
    }

    @Test
    void adaptUnitsSeconds() {
        assertEquals("1000ms", adaptUnits(ONE_SECOND));
        assertEquals("5000ms", adaptUnits(ofSeconds(5)));
        assertEquals("10.00s", adaptUnits(ofSeconds(10)));
        assertEquals("59.00s", adaptUnits(ofSeconds(59)));
        assertEquals("1:00min", adaptUnits(ofSeconds(60)));
        assertEquals("1:01min", adaptUnits(ofSeconds(61)));
        assertEquals("1:00:00h", adaptUnits(ofSeconds(3600)));
    }

    @Test
    void adaptUnitsMinutes() {
        assertEquals("1:00min", adaptUnits(ONE_MINUTE));
        assertEquals("2:00min", adaptUnits(ofMinutes(2)));
        assertEquals("59:00min", adaptUnits(ofMinutes(59)));
        assertEquals("1:30:00h", adaptUnits(ofMinutes(90)));
    }

    @Test
    void adaptUnitsHours() {
        assertEquals("1:00:00h", adaptUnits(ONE_HOUR));
        assertEquals("1 day 0:00h", adaptUnits(ofHours(24)));
        assertEquals("1 day 1:00h", adaptUnits(ofHours(25)));
    }

    @Test
    void adaptUnitsDays() {
        assertEquals("1 day 0:00h", adaptUnits(ONE_DAY));
        assertEquals("2 days 0:00h", adaptUnits(TWO_DAYS));
        assertEquals("364 days 0:00h", adaptUnits(ofDays(364)));
        assertEquals("1 year, 0 days 0h", adaptUnits(ONE_YEAR));
        assertEquals("1 year, 1 days 0h", adaptUnits(ofDays(366)));
        assertEquals("2 years, 0 days 0h", adaptUnits(ofDays(365 * 2)));
        assertEquals("10 years, 0 days 0h", adaptUnits(ofDays(365 * 10)));
        assertEquals("10 years, 1 days 0h", adaptUnits(ofDays(365 * 10 + 1)));
    }

    @Test
    void adaptUnitsNegative() {
        assertEquals("-1 day 0:00h", adaptUnits(ofDays(-1)));
        assertEquals("-10.00s", adaptUnits(ofMillis(-10000)));
        assertEquals("-2:00min", adaptUnits(ofMinutes(-2)));
    }

    @Test
    void usualDE() {
        assertEquals("1 Tag 0:00:00", usual(GERMAN, ONE_DAY));
        assertNull(usual(GERMAN, NULL_DUR, null));
        assertEquals(UNIVERSAL_NA, usual(GERMAN, NULL_DUR, UNIVERSAL_NA));
        assertEquals("7 Tage 7:07:07", usual(GERMAN, BIG_SEVEN));
        assertEquals("-0:00:01", usual(GERMAN, ONE_SECOND.negated()));
        assertEquals("-1:00:10", usual(GERMAN, ONE_HOUR.plusSeconds(10).negated()));
    }

    @Test
    void usualEN() {
        assertEquals("1 day 0:00:00", usual(ENGLISH, ONE_DAY));
        assertEquals("2 days 0:00:00", usual(ENGLISH, TWO_DAYS));
        assertEquals("2 days 1:00:00", usual(ENGLISH, TWO_DAYS.plus(ONE_HOUR)));

        assertEquals("-0:00:01", usual(ENGLISH, ONE_SECOND.negated()));
        assertEquals("2 days 1:10:03", usual(ENGLISH, TWO_DAYS.plus(ONE_HOUR).plus(ofMinutes(10).plus(ofSeconds(3)))));
        assertEquals("7 days 7:07:07", usual(ENGLISH, BIG_SEVEN));
        assertEquals("14 days 14:14:14", usual(ENGLISH, BIG_SEVEN.multipliedBy(2)));

        assertNull(usual(ENGLISH, NULL_DUR, null));
        assertEquals(UNIVERSAL_NA, usual(ENGLISH, NULL_DUR, UNIVERSAL_NA));
    }


    @Test
    void adaptUnitsOfMillisEdges() {
        assertEquals("292471208 years, 247 days 7h", KafuDuration.adaptUnitsOfMillis(Long.MAX_VALUE));
        assertEquals("-292471208 years, 247 days 7h", KafuDuration.adaptUnitsOfMillis(Long.MIN_VALUE));
    }

    @Test
    void adaptUnitsOfMillisLoop() {
        for (int i = 0; i < 100; i++) {
            final long random = Randomize.JUL.nextLong();
            assertEquals(KafuDuration.adaptUnits(Duration.ofMillis(random)), KafuDuration.adaptUnitsOfMillis(random));
        }
    }

    @Test
    void adaptUnitsOfNanosEdges() {
        assertEquals("292 years, 171 days 23h", KafuDuration.adaptUnitsOfNanos(Long.MAX_VALUE));
        assertEquals("-292 years, 171 days 23h", KafuDuration.adaptUnitsOfNanos(Long.MIN_VALUE));
    }

    @Test
    void adaptUnitsOfNanosLoop() {
        for (int i = 0; i < 100; i++) {
            long random = Randomize.JUL.nextLong();
            assertEquals(KafuDuration.adaptUnits(Duration.ofNanos(random)), KafuDuration.adaptUnitsOfNanos(random));
        }
    }

    @Test
    void toMillisSafely() {
        assertEquals(Long.MAX_VALUE, KafuDuration.toMillisSafely(MAX_VALUE));
        assertEquals(Long.MIN_VALUE, KafuDuration.toMillisSafely(MIN_VALUE));
        assertEquals(Long.MAX_VALUE, KafuDuration.toMillisSafely(Duration.ofMillis(Long.MAX_VALUE)));
        assertEquals(Long.MIN_VALUE, KafuDuration.toMillisSafely(Duration.ofMillis(Long.MIN_VALUE)));

        for (int i = 0; i < 100; i++) {
            long random = Randomize.JUL.nextLong();
            assertEquals(random, KafuDuration.toMillisSafely(Duration.ofMillis(random)));
        }
    }

    @Test
    void toNanosSafely_() {
        assertEquals(Long.MAX_VALUE, KafuDuration.toNanosSafely(MAX_VALUE));
        assertEquals(Long.MIN_VALUE, KafuDuration.toNanosSafely(MIN_VALUE));

        assertEquals(Long.MAX_VALUE, KafuDuration.toNanosSafely(Duration.ofMillis(Long.MAX_VALUE)));
        assertEquals(Long.MAX_VALUE, KafuDuration.toNanosSafely(Duration.ofNanos(Long.MAX_VALUE)));

        System.out.println(KafuDuration.adaptUnits(Duration.ofNanos(Long.MAX_VALUE)));
        System.out.println(KafuDuration.adaptUnits(Duration.ofMillis(Long.MAX_VALUE)));

        for (int i = 0; i < 100; i++) {
            long random = Randomize.JUL.nextLong();
            assertEquals(random, KafuDuration.toNanosSafely(Duration.ofNanos(random)));
        }
    }

    @Test
    void sleep() {
        long nanos = KafuDuration.sleep(ONE_SECOND);
        assertTrue(nanos > 900_000_000);
        assertTrue(nanos < 1900_000_000);
    }

    @Test
    void requirePositiveNotZero_() {
        assertThrows(RequirementException.class, () -> KafuDuration.requirePositiveNotZero(Duration.ZERO));
        assertThrows(RequirementException.class, () -> KafuDuration.requirePositiveNotZero(Duration.ZERO.negated()));

        assertThrows(RequirementException.class, () -> KafuDuration.requirePositiveNotZero(ONE_NANO.negated()));
        assertThrows(RequirementException.class, () -> KafuDuration.requirePositiveNotZero(ONE_DECADE.negated()));

        assertSame(ONE_MILLI, KafuDuration.requirePositiveNotZero(ONE_MILLI));
        assertSame(ONE_DAY, KafuDuration.requirePositiveNotZero(ONE_DAY));
    }
}
