package junit.unit;

import net.kafujo.units.DataSizeCounter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static net.kafujo.units.DataSize.ONE_KiB;
import static net.kafujo.units.DataSize.ONE_MiB;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DataSizeCounter_Test {

    @Test
    void addItems() {

        // count 1024 times one kib
        DataSizeCounter itemCounter = new DataSizeCounter();
        for (int i = 0; i < 1024; i++) {
            itemCounter.add(ONE_KiB);
        }

        assertEquals(ONE_MiB, itemCounter.getDataSize());
        assertEquals(1024, itemCounter.getItemCount());

    }

    @Test
    @DisplayName("Test the counters ")
    void addOtherCounter() {

        DataSizeCounter counter1 = new DataSizeCounter();
        DataSizeCounter counter2 = new DataSizeCounter();
        for (int i = 0; i < 100; i++) {
            counter1.add(ONE_KiB);
            counter2.add(ONE_MiB);
        }

        counter1.add(counter2);

        assertEquals(ONE_KiB.times(100).plus(ONE_MiB.times(100)), counter1.getDataSize());
        assertEquals(200, counter1.getItemCount());

    }

}

