package junit.sec;

import net.kafujo.sec.Rsa;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;

import static org.junit.jupiter.api.Assertions.*;

class RsaTests {

    private final byte[] plain = "Hello in the world of pain".getBytes();
    private final KeyPair pair1 = Rsa.createKeyPair(512);
    private final KeyPair pair2 = Rsa.createKeyPair(512);

    @Test
    void corresponding() {
        assertTrue(Rsa.isCorresponding(pair1, true));
        assertTrue(Rsa.isCorresponding(pair2, false));

        KeyPair pairMixed = new KeyPair(pair1.getPublic(), pair2.getPrivate());
        assertFalse(Rsa.isCorresponding(pairMixed, false));
        assertFalse(Rsa.isCorresponding(pairMixed));
    }

    @Test
    void correspondingIncomplete() {
        KeyPair prvkOnly = new KeyPair(null, pair1.getPrivate());
        KeyPair pubkOnly = new KeyPair(pair1.getPublic(), null);

        assertThrows(NullPointerException.class, () -> Rsa.isCorresponding(null));
        assertThrows(NullPointerException.class, () -> Rsa.isCorresponding(prvkOnly));
        assertThrows(NullPointerException.class, () -> Rsa.isCorresponding(pubkOnly));
    }

    @Test
    void oneKeyOnly() {
        var pubkOnly = new Rsa(pair1.getPublic());
        var prvkOnly = new Rsa(pair1.getPrivate());

        var cipher = pubkOnly.encrypt(plain);
        assertArrayEquals(plain, prvkOnly.decrypt(cipher));

        assertThrows(RuntimeException.class, () -> prvkOnly.encrypt(plain));
        assertThrows(RuntimeException.class, () -> pubkOnly.decrypt(cipher));

        assertThrows(RuntimeException.class, () -> new Rsa(new KeyPair(null, null)));
    }

    @Test
    void createByKeySize() {
        Rsa rsa = new Rsa(2048);
        var cipher = rsa.encrypt(plain);
        assertArrayEquals(plain, rsa.decrypt(cipher));
    }


}
