package junit.sec;

import net.kafujo.container.KafuCollection;
import net.kafujo.sec.Randomize;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Randomize_Test {

    @Test
    void randomBooleanDistribution() {
        int trues = 0;
        int falses = 0;
        for (int ignored : KafuCollection.range(1000)) {
            if (Randomize.JUL.nextBoolean()) {
                trues++;
            } else {
                falses++;
            }
        }
        assertTrue(trues > 250);
        assertTrue(falses > 250);
    }

    @Test
    void randomIntEdges() {
        assertThrows(IllegalArgumentException.class, () -> Randomize.JUL.nextInt(-10));
        assertEquals(0, Randomize.JUL.nextInt(1));
        assertEquals(0, Randomize.SECURE.nextInt(1));
    }

    @Test
    void randomIntBetweenEdges() {
        assertThrows(IllegalArgumentException.class, () -> Randomize.JUL.nextInt(2, 1));
        assertThrows(IllegalArgumentException.class, () -> Randomize.JUL.nextInt(1, 1));
        assertEquals(1, Randomize.JUL.nextInt(1, 2));

        for (int i = 0; i < 10000; i++) {
            assertTrue(Randomize.JUL.nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE) != Integer.MAX_VALUE);
        }
    }

    @Test
    void randomIntBetweenLoopPositive() {
        for (int i = 6; i < Integer.MAX_VALUE - 1000000; i += 100000) {
            int rand = Randomize.JUL.nextInt(5, i);
            assertTrue(rand >= 5);
            assertTrue(rand < i);
        }
    }

    @Test
    void randomIntBetweenLoopPositiveNegative() {
        for (int i = 1; i < Integer.MAX_VALUE - 1000000; i += 100000) {
            int next = Randomize.JUL.nextInt(-i, i);
            assertTrue(next >= -i, "rand must be >= " + -i + " but is " + next);
            assertTrue(next < i, "rand must be < " + i + " but is " + next);
        }
    }

    @Test
    void randomIntBetweenLoopNegative() {
        for (int i = -5; i > -100; i -= 10) {
            int rand = Randomize.JUL.nextInt(i, 0);
            assertTrue(rand < 0, "rand must be smaller than 0 but is " + rand);
            assertTrue(rand >= i, "rand must be >= " + i + " but is " + rand);
        }
    }

    @Test
    void randomLongEdges() {
        assertThrows(IllegalArgumentException.class, () -> Randomize.JUL.nextLong(-10));
        assertEquals(0, Randomize.JUL.nextLong(1));

        int nulls = 0, ones = 0;
        for (int i = 0; i < 1000; i++) {
            long next = Randomize.JUL.nextLong(2);
            if (next == 0) {
                nulls++;
                continue;
            }

            if (next == 1) {
                ones++;
                continue;
            }
            fail("randomLong(2) should never return a " + next);
        }

        assertTrue(nulls > 250);
        assertTrue(ones > 250);
    }

    @Test
    void randomLongShortRange() {
        for (int i = 0; i < 1000; i++) {
            long next = Randomize.JUL.nextLong(Short.MAX_VALUE);
            assertTrue(next >= 0, "next must be > 0 but is " + next);
            assertTrue(next < Short.MAX_VALUE, "next must be < " + Short.MAX_VALUE + " but is " + next);
        }
    }

    @Test
    void randomLongGrowingRange() {
        for (long loop = 1; loop < Long.MAX_VALUE / 2; loop *= 2) {
            final long next = Randomize.JUL.nextLong(loop);
            assertTrue(next >= 0, "next must be > 0 but is " + next);
            assertTrue(next < loop, "next must be < " + loop + " but is " + next);
        }
    }


    @Test
    void randomLongBound() {
        for (long loop = 1; loop < Long.MAX_VALUE / 2; loop *= 2) {
            final long next = Randomize.JUL.nextLong(loop);
            assertTrue(next >= 0, "next must be > 0 but is " + next);
            assertTrue(next < loop, "next must be < " + loop + " but is " + next);
        }
    }
}
