package junit.sec;

import net.kafujo.sec.PasswordHash;
import net.kafujo.sec.Randomize;
import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PasswordHash_Test {

    private final byte[] HASH = Randomize.SECURE.bytes(20);
    private final byte[] SALT = Randomize.SECURE.bytes(8);

    private final PasswordHash pwh = new PasswordHash(HASH, SALT);

    @Test
    void bytes() {
        assertThat(HASH).isEqualTo(pwh.getHash());
        assertThat(HASH).isNotSameAs(pwh.getHash()); // make sure, there is no mutability through array
        assertThat(pwh.getHash()).isEqualTo(pwh.getHash());
        assertThat(pwh.getHash()).isNotSameAs(pwh.getHash());

        assertThat(SALT).isEqualTo(pwh.getSalt());
        assertThat(SALT).isNotSameAs(pwh.getSalt());
        assertThat(pwh.getSalt()).isEqualTo(pwh.getSalt());
        assertThat(pwh.getSalt()).isNotSameAs(pwh.getSalt());
    }

    @Test
    void base64() {
        final String hashB64 = Base64.getEncoder().encodeToString(HASH);
        final String saltB64 = Base64.getEncoder().encodeToString(SALT);

        assertThat(hashB64).isEqualTo(pwh.getHashB64());
        assertThat(saltB64).isEqualTo(pwh.getSaltB64());
    }

    @Test
    void edges() {
        assertThrows(NullPointerException.class, () -> new PasswordHash(HASH, null));
        assertThrows(NullPointerException.class, () -> new PasswordHash(null, SALT));
    }
}
