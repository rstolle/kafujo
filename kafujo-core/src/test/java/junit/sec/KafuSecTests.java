package junit.sec;

import net.kafujo.sec.KafuSec;
import org.junit.jupiter.api.Test;

import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.HashSet;

import static junit.sec.SupportSec.BC_PROV;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class KafuSecTests {

    @Test
    void providers() {
        var list = KafuSec.fetchProviderNameList();

        for (String name : list) {
            assertNotNull(Security.getProvider(name));
        }
        assertNull(Security.getProvider("nixus.minimax"));
    }

    @Test
    void countBc() {

        final var serviceTypes = KafuSec.fetchServiceTypesSet(BC_PROV);
        final var serviceTypesVerify = new HashSet<String>();
        final var allServicesArray = BC_PROV.getServices();

        final var allServices = new ArrayList<>(allServicesArray);

        // remove all services with typs presented in serviceTypes
        for (Provider.Service service : allServicesArray) {
            String type = service.getType();
            serviceTypesVerify.add(type);
            if (serviceTypes.contains(type)) {
                allServices.remove(service);
            }
        }

        assertTrue(allServices.isEmpty());
        assertThat(serviceTypes).hasSameElementsAs(serviceTypesVerify);
        assertThat(serviceTypes).hasSameElementsAs(KafuSec.fetchServiceTypeList(BC_PROV));
    }

    @Test
    void countAll() {
        var allTypes = KafuSec.fetchServiceTypesSet();

        for (String type : allTypes) {
            assertFalse(Security.getAlgorithms(type).isEmpty());
        }

        assertThat(allTypes).hasSameElementsAs(KafuSec.fetchServiceTypeList());
    }

    @Test
    void multipleImpl() {
        var spm = KafuSec.createServiceNameProviderMap();
        assertThat(spm.keySet()).hasSameElementsAs(KafuSec.fetchServiceNameSet());
    }


    @Test
    void aliasMap() {
        var map = KafuSec.createServiceNameAliasMap(BC_PROV);
        var names = KafuSec.fetchServiceNameSet(BC_PROV);
        var namesMap = map.keySet();

        if (names.size() == namesMap.size()) {
            assertThat(namesMap).containsExactlyElementsOf(names);
        } else {
            assertThat(namesMap).hasSize(names.size() + 1); // there is one more element -> the null value ...
            assertThat(namesMap).containsAll(names); // ... for the alias names without Service
            assertTrue(namesMap.contains(null));
        }
    }
}
