package junit.sec;

import net.kafujo.sec.ServiceName;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceNameTests {

    @Test
    void officialName() {
        ServiceName official = new ServiceName("Signature.HandWritten");
        assertFalse(official.isAlias());
        assertEquals("Signature", official.getType());
        assertEquals("HandWritten", official.getAlgorithm());
        assertEquals("Signature.HandWritten", official.getName());
        assertEquals("Signature.HandWritten", official.toString());
    }

    @Test
    void aliasName() {
        ServiceName alias = new ServiceName("Alg.Alias.Signature.HandWritten");
        assertTrue(alias.isAlias());
        assertEquals("Signature", alias.getType());
        assertEquals("HandWritten", alias.getAlgorithm());
        assertEquals("Signature.HandWritten", alias.getName());
        assertEquals("Alg.Alias.Signature.HandWritten", alias.toString());
    }

    @Test
    void toStings() {
        for (var key : new BouncyCastleProvider().keySet()) {
            ServiceName sn = new ServiceName(key);
            assertEquals(sn.toString(), key.toString());
        }

    }
}
