package junit.sec;

import net.kafujo.sec.KafuSec;
import net.kafujo.sec.Randomize;
import net.kafujo.sec.ServiceType;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Provider;
import java.security.Security;
import java.util.TreeSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ServiceTypeTests {

    private static final Logger LGR = LoggerFactory.getLogger(ServiceTypeTests.class);

    @Test
    void integrity() {
        assertThrows(UnsupportedOperationException.class, () -> ServiceType.LIST.add(ServiceType.Signature));
        assertThrows(UnsupportedOperationException.class, () -> ServiceType.LIST_STR.add("ServiceType.Signature"));
    }

    @Test
    void coverage() {

        if (Randomize.JUL.nextBoolean()) {
            LGR.info("add bouncy castle");
            Security.addProvider(new BouncyCastleProvider());
        }

        var notAvailable = ServiceType.fetchCurrentlyNotAvailable();
        var notListed = ServiceType.fetchNotListed();

        var available = KafuSec.fetchServiceTypeList();

        if (notAvailable.isEmpty()) {
            LGR.info("All listed ServiceTypes are available");
            assertThat(available).containsAll(ServiceType.LIST_STR);
        } else {
            LGR.info("These listed ServiceTypes are currently not available: " + notAvailable);
        }

        if (notListed.isEmpty()) {
            LGR.info("There aren't any ServiceTypes which are not listed");
            assertThat(ServiceType.LIST_STR).containsAll(available);
        } else {
            LGR.info("These ServiceTypes are not listed: " + notListed);
        }

        if (notAvailable.isEmpty() && notListed.isEmpty()) {
            assertThat(available).isEqualTo(available);
        }
    }

    @Test
    void list() {
        assertThat(ServiceType.Cipher.fetchAlgorithmSet())
                .hasSizeGreaterThan(0)
                .containsAll(ServiceType.Cipher.fetchAlgorithmList());
    }

    @Test
    void uniqueAlgInProvider() {
        var list = ServiceType.MessageDigest.fetchAlgorithmList(SupportSec.BC_PROV);
        var set = new TreeSet<>(list);
        assertThat(list).containsAll(set);
    }

    @Test
    void allProviders() {

        var allOfSecurity = ServiceType.Signature.fetchAlgorithmSet(); // contains MD2WITHRSA, MD5ANDSHA1WITHRSA ...
        var allPerProvider = new TreeSet<String>();

        for (Provider provider : Security.getProviders()) {
            var thisProvider = ServiceType.Signature.fetchAlgorithmList(provider);
            // contains MD2withRSA, MD5andSHA1withRSA, MD5withRSA, NONEwithDSA etc ...
            thisProvider.forEach(e -> allPerProvider.add(e.toUpperCase()));
        }
        assertEquals(allOfSecurity, allPerProvider);
        assertThat(allOfSecurity).containsAll(allPerProvider);
    }
}
