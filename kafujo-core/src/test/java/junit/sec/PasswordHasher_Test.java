package junit.sec;

import net.kafujo.sec.PasswordHash;
import net.kafujo.sec.PasswordHasher;
import net.kafujo.units.DataSize;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PasswordHasher_Test {

    private final String pwSimple = "1234";
    private final String pwGood = ".a%daüß1!-@\"A#*";

    private final PasswordHasher defaultHasher = PasswordHasher.DEFAULT;
    private final PasswordHash hashSimple = defaultHasher.hash(pwSimple);
    private final PasswordHash hashGood = defaultHasher.hash(pwGood);

    @Test
    void verifySimple() {
        assertTrue(defaultHasher.verify(pwSimple, hashSimple));
        assertFalse(defaultHasher.verify(pwSimple + '#', hashSimple));
    }

    @Test
    void verifyGood() {
        assertTrue(defaultHasher.verify(pwGood, hashGood));
        assertFalse(defaultHasher.verify(pwSimple , hashGood));
        assertFalse(defaultHasher.verify(pwGood , hashSimple));
    }

    @Test
    void sizes() {
        assertThat(hashSimple.getHash()).hasSize(20);
        assertThat(hashGood.getHash()).hasSize(20);

        assertThat(hashSimple.getSalt()).hasSize(8);
        assertThat(hashGood.getSalt()).hasSize(8);
    }


    @Test
    void differentSalts() {
        final var hash2 = defaultHasher.hash(pwSimple);
        assertNotEquals(hashSimple.getSaltB64(), hash2.getSaltB64());
        assertNotEquals(hashSimple.getHashB64(), hash2.getHashB64());

        assertThat(hashSimple.getSalt()).isNotEqualTo(hash2.getSalt());
        assertThat(hashSimple.getHash()).isNotEqualTo(hash2.getHash());

        assertTrue(defaultHasher.verify(pwSimple, hash2));
        assertFalse(defaultHasher.verify(pwSimple, hash2.getHash(), hashSimple.getSalt()));
    }

    @Test
    void alternativeSetting() {
        var hasher = new PasswordHasher(PasswordHasher.PBKDF2_WITH_HMAC_SHA1, 80, 500, DataSize.of(4));
        var hash = hasher.hash("hello");

        if (!hasher.verify("hello", hash)) {
            // WRONG PASSWORD!
        }

        assertThat(hash.getHash()).hasSize(10);
        assertThat(hash.getSalt()).hasSize(4);

        assertTrue(hasher.verify("hello", hash));
        assertFalse(hasher.verify("hello2", hash));
    }
}
