package junit.sec;

import net.kafujo.sec.AES;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.IvParameterSpec;

import static org.junit.jupiter.api.Assertions.*;


public class AESTests {

    byte[] plain = "Nello".getBytes();

    @Test
    void iv() {
        final var ivps = new IvParameterSpec(plain);
        assertArrayEquals(plain, ivps.getIV());
        assertNotSame(plain, ivps.getIV());
    }

    @Test
    void one() {
        AES aes = new AES(128);
        var cipher = aes.encrypt(plain);
        assertEquals(16, cipher.length);
        assertArrayEquals (plain, aes.decrypt(cipher));
    }

    @Test
    void two() {
        AES aes = new AES(192);
        var cipher = aes.encrypt(plain);
        assertEquals(16, cipher.length);
        assertArrayEquals (plain, aes.decrypt(cipher));
    }

    @Test
    void three() {
        AES aes = new AES(256);
        var cipher = aes.encrypt(plain);
        assertEquals(16, cipher.length);
        assertArrayEquals (plain, aes.decrypt(cipher));
    }
}
