package junit.sec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Provider;

public class SupportSec {
    static final Provider BC_PROV = new BouncyCastleProvider();
}
