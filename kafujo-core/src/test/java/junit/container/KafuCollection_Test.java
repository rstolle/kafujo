package junit.container;

import net.kafujo.container.KafuCollection;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class KafuCollection_Test {

    @Test
    void inOut() {

        Properties p = new Properties();

        p.setProperty("p1", "v1");
        p.setProperty("p2", "v2");
        p.setProperty("p1", "v1.1");

        Map<String, String> map = KafuCollection.properties2StringMap(p);

        assertEquals(2, map.size());

    }

    @Test
    void rangeEdges() {
        assertThrows(IllegalArgumentException.class, () -> KafuCollection.range(0,0));
        assertThrows(IllegalArgumentException.class, () -> KafuCollection.range(0,-1));
    }

    @Test
    void rangePositive() {
        var range10 = KafuCollection.range(0, 10);
        assertEquals(10, range10.size());
        for (int i = 0; i < 10; i++) {
            assertTrue(range10.remove((Integer) i));
        }
        assertEquals(0, range10.size());
    }

    @Test
    void rangeNegative() {
        var range = KafuCollection.range(-20, 10);
        assertEquals(30, range.size());
        for (int i = -20; i < 10; i++) {
            assertTrue(range.remove((Integer) i));
        }
        assertEquals(0, range.size());
    }
}
