package junit.base;

import net.kafujo.base.KafujoException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KafuException_Test {

    @Test
    void getMessage () {
        final var withMessage = new KafujoException("Test");

        assertEquals("Test", withMessage.getMessage());
        assertEquals("Test", withMessage.extractMessage());
        assertEquals("KafujoException: Test", withMessage.extractClassAndMessage());

        final var withoutMessage = new KafujoException();
        assertNull(withoutMessage.getMessage());
        System.out.println(withoutMessage.toString());
        assertEquals("net.kafujo.base.KafujoException", withoutMessage.extractMessage());
        assertEquals("net.kafujo.base.KafujoException", withoutMessage.extractClassAndMessage());

        assertThrows(KafujoException.class, () -> { throw withMessage; } );
    }

}
