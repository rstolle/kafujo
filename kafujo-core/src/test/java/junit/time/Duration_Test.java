package junit.time;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static net.kafujo.units.KafuDuration.MAX_VALUE;
import static net.kafujo.units.KafuDuration.MIN_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class Duration_Test {

    @Test
    void edges() {

        Duration max = Duration.ofSeconds(Long.MAX_VALUE, 999999999);
        Duration min = Duration.ofSeconds(Long.MIN_VALUE, 0);

        Duration min_plus_1 = Duration.ofSeconds(Long.MIN_VALUE, 1);
        Duration min_plus_2 = Duration.ofSeconds(Long.MIN_VALUE, 2);

        assertEquals(min_plus_1.abs(), max);

        assertEquals(max, MAX_VALUE);
        assertEquals(min, MIN_VALUE);

    }

    @Test
    void createNegative() {

        var almostMinusTwoSeconds = Duration.ofSeconds(-1, -999999999);
        var almostZero = Duration.ofSeconds(-1, 999999999);

        assertEquals(-1999, almostMinusTwoSeconds.toMillis());
        assertEquals(0, almostZero.toMillis());
        assertEquals(-1, almostZero.toNanos());

        // we cannot go smaller than Long.MIN_VALUE seconds, not even a nano second:
        assertThrows(ArithmeticException.class, () -> Duration.ofSeconds(Long.MIN_VALUE, -1));

    }
}
