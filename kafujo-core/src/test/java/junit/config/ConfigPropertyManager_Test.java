package junit.config;

import net.kafujo.config.ConfigPropertyManager;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static support.Data.*;


public class ConfigPropertyManager_Test {

    @Test
    void fromProperties() {

        Properties p = new Properties();
        p.setProperty("movie.name", "Pulp Fiction");
        p.setProperty("movie.year", "1996");

        p.setProperty("name", "1996");


        ConfigPropertyManager cpm = ConfigPropertyManager.fromProperties(p, "java.vm.");

        assertEquals(0, cpm.getReadCounter("movie.name"));
        assertEquals("Pulp Fiction", cpm.read("movie.name"));
        assertEquals(1, cpm.getReadCounter("movie.name"));
        assertEquals("1996", cpm.read("movie.year"));

        assertEquals("Quentin Tarantino", cpm.read("producer", "Quentin Tarantino"));
        assertEquals(1, cpm.getReadCounter("producer"));
        assertEquals("Quentin Tarantino", cpm.read("producer", "Quentin Tarantino"));
        assertEquals(2, cpm.getReadCounter("producer"));
        assertEquals("Quentin Tarantino", cpm.read("producer"));
        assertEquals(3, cpm.getReadCounter("producer"));

        System.out.println(cpm);

    }

    @Test
    @Disabled("cannot set env vars with '.' on some linux: https://stackoverflow.com/a/58043914/7441000")
    void fromEnv () {
        assertEquals(MOVIE_DATABASE_NAME_VALUE, MOVIE_DATABASE_NAME_VALUE_ENV);

        Properties p = new Properties();
        p.setProperty(MOVIE_DATABASE_NAME_PROPERTY, "Will be overwritten by env value");

        ConfigPropertyManager cpm = ConfigPropertyManager.fromProperties(p, CP_PREFIX);

        final String name = cpm.read(MOVIE_DATABASE_NAME_PROPERTY);
        assertEquals(name, MOVIE_DATABASE_NAME_VALUE_ENV);

        System.out.println(cpm);


    }

}
