package junit.config;

import net.kafujo.config.PropertiesSource;
import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;
import net.kafujo.text.KafuText;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static support.Data.*;

class PropertiesSource_Test {

    @Test
    void nullable () {
        final PropertiesSource source = PropertiesSource.ofFile((String)null);
        assertFalse(source.isAvailable());
        assertTrue(source.toString().contains(KafuText.UNIVERSAL_NA));
    }

    @Test
    void file_String () {
        final String temp = Resources.ofCurrentThread().asTempFile(ONE_NUMBER).toString();
        final PropertiesSource source = PropertiesSource.ofFile(temp);
        assertTrue(source.isAvailable());
        Properties props = source.load();
        assertEquals(1, props.size());
        assertEquals("1", props.get("one"));

        assertTrue(KafuFile.deleteQuietly(temp)); // assert, cause if delete fails it destroys the next assert
        assertFalse(source.isAvailable());
    }

    @Test
    void file_Path () {
        final Path temp = Resources.ofCurrentThread().asTempFile(ONE_NUMBER);
        final PropertiesSource source = PropertiesSource.ofFile(temp);
        assertTrue(source.isAvailable());
        Properties props = source.load();
        assertEquals(1, props.size());
        assertEquals("1", props.get("one"));

        assertTrue(KafuFile.deleteQuietly(temp)); // assert, cause if delete fails it destroys the next assert
        assertFalse(source.isAvailable());
    }


    @Test
    void fileXml_Path() {
        final Path temp = Resources.ofCurrentThread().asTempFile(TWO_PROPERTIES_XML);
        final PropertiesSource source = PropertiesSource.ofFileXml(temp);

        assertTrue(source.isAvailable());
        Properties props = source.load();
        assertEquals(2, props.size());
        assertEquals("1", props.get("one"));
        assertEquals("2", props.get("two"));

        assertTrue(KafuFile.deleteQuietly(temp)); // assert, cause if delete fails it destroys the next assert
        assertFalse(source.isAvailable());
    }

    @Test
    void fileXml_String() {
        final String temp = Resources.ofCurrentThread().asTempFile(TWO_PROPERTIES_XML).toString();
        final PropertiesSource source = PropertiesSource.ofFileXml(temp);

        assertTrue(source.isAvailable());
        Properties props = source.load();
        assertEquals(2, props.size());
        assertEquals("1", props.get("one"));
        assertEquals("2", props.get("two"));

        assertTrue(KafuFile.deleteQuietly(temp)); // assert, cause if delete fails it destroys the next assert
        assertFalse(source.isAvailable());
    }

    @Test
    void resource_String() {
        final PropertiesSource source = PropertiesSource.ofResource(ONE_NUMBER);
        assertTrue(source.isAvailable());
        Properties props = source.load();
        assertEquals(1, props.size());
        assertEquals("1", props.get("one"));
    }

    @Test
    void resourceXml_String() {
        final PropertiesSource ps = PropertiesSource.ofResourceXml(ONE_NUMBER_XML);
        assertTrue(ps.isAvailable());
        Properties props = ps.load();
        assertEquals(1, props.size());
        assertEquals("1", props.get("one"));
    }

}
