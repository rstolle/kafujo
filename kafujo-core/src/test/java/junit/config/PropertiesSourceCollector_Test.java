package junit.config;

import net.kafujo.config.PropertiesSource;
import net.kafujo.config.PropertiesSourceCollector;
import org.junit.jupiter.api.Test;
import support.Data;

import static org.junit.jupiter.api.Assertions.*;
import static support.Data.*;

class PropertiesSourceCollector_Test {

    private static final PropertiesSource NOT_THERE = PropertiesSource.ofFile(NONSENSE);

    private static final PropertiesSource ONE = PropertiesSource.ofResourceXml(ONE_NUMBER_XML);
    private static final PropertiesSource TWO = PropertiesSource.ofResourceXml(TWO_PROPERTIES_XML);

    private static final PropertiesSource THREE_NUM = PropertiesSource.ofResource(THREE_NUMBERS);
    private static final PropertiesSource THREE_ESPERANTO = PropertiesSource.ofResource(Data.THREE_ESPERANTO);

    @Test
    void failsCauseNoneAvailable() {
        assertThrows(IllegalArgumentException.class, () -> new PropertiesSourceCollector(NOT_THERE));
        assertThrows(IllegalArgumentException.class, () -> new PropertiesSourceCollector(NOT_THERE, NOT_THERE));
    }

    @Test
    void failsCauseDoubleUsage() {
        var problem = assertThrows(IllegalArgumentException.class, () -> new PropertiesSourceCollector(ONE, ONE));
        assertTrue (problem.getMessage().contains("already collected"));
    }

    @Test
    void countingAvailablesOrder1() {
        final var collector = new PropertiesSourceCollector(NOT_THERE, ONE, THREE_ESPERANTO);
        final var available = collector.getAvailableSources();

        assertEquals(2, available.size());

        assertFalse(available.contains(NOT_THERE));
        assertTrue(available.contains(ONE));
        assertTrue(available.contains(THREE_ESPERANTO));

        assertEquals(1, collector.useFirstAvailable().size());
        assertEquals(3, collector.merge().size());
    }

    @Test
    void countingAvailablesOrder2 () {
        final var collector = new PropertiesSourceCollector(ONE, NOT_THERE, THREE_ESPERANTO);
        final var available = collector.getAvailableSources();
        assertEquals(2, available.size());
        assertFalse(available.contains(NOT_THERE));
        assertTrue(available.contains(ONE));
        assertTrue(available.contains(THREE_ESPERANTO));
        assertEquals(1, collector.useFirstAvailable().size());
        assertEquals(3, collector.merge().size());
    }

    @Test
    void countingAvailablesOrder3 () {
        final var collector = new PropertiesSourceCollector(ONE, THREE_ESPERANTO, NOT_THERE);
        final var available = collector.getAvailableSources();
        assertEquals(2, available.size());
        assertFalse(available.contains(NOT_THERE));
        assertTrue(available.contains(ONE));
        assertTrue(available.contains(THREE_ESPERANTO));
        assertEquals(1, collector.useFirstAvailable().size());
        assertEquals(3, collector.merge().size());
    }

    @Test
    void one_two() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(ONE, TWO);
        assertEquals(1, psc.useFirstAvailable().size());
        assertEquals(2, psc.merge().size());
    }

    @Test
    void two_one() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(TWO, ONE);
        assertEquals(2, psc.useFirstAvailable().size());
        assertEquals(2, psc.merge().size());
    }

    @Test
    void three_one_merge_does_nothing() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(THREE_NUM, ONE);

        var first = psc.useFirstAvailable();
        var merge = psc.merge();

        assertEquals(first.size(), merge.size());

        assertEquals(first.readInt("one"), merge.readInt("one"));
        assertEquals(first.readInt("three"), merge.readInt("three"));
        assertEquals(first.readInt("two"), merge.readInt("two"));
    }

    @Test
    void three_one_merge_changes_one() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(THREE_ESPERANTO, ONE);

        var first = psc.useFirstAvailable();
        var merge = psc.merge();

        assertEquals(first.size(), merge.size()); // still the same size

        assertThrows(NumberFormatException.class, () -> first.readInt("one")); // "unu"
        assertEquals(1, merge.readInt("one")); // replaced by merge

        assertEquals(first.readString("two"), merge.readString("two"));
        assertEquals("du", merge.readString("two"));

        assertEquals(first.readString("three"), merge.readString("three"));
        assertEquals("tri", merge.readString("three"));
    }


    @Test
    void one_three() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(ONE, THREE_ESPERANTO);
        final var first = psc.useFirstAvailable();
        final var merge = psc.merge();

        assertEquals(1, first.size());
        assertEquals(3, merge.size());

        assertEquals(1, first.readInt("one"));

        assertEquals("unu", merge.readString("one"));
        assertEquals("du", merge.readString("two"));
        assertEquals("tri", merge.readString("three"));
    }

    @Test
    void nothing_three() {
        PropertiesSourceCollector psc = new PropertiesSourceCollector(NOT_THERE, THREE_ESPERANTO);

        final var first = psc.useFirstAvailable();
        final var merge = psc.merge();

        assertEquals(3, first.size());
        assertEquals(3, merge.size());

        assertEquals("unu", merge.readString("one"));
        assertEquals("du", merge.readString("two"));
        assertEquals("tri", merge.readString("three"));
    }
}
