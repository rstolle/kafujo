/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package junit.config;

import net.kafujo.config.KafuOs;
import net.kafujo.config.KafuVm;
import net.kafujo.config.SystemProperty;
import net.kafujo.io.KafuFile;
import net.kafujo.units.DataSize;
import net.kafujo.units.KafuDuration;
import org.junit.jupiter.api.Test;
import support.Data;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Duration;
import java.time.Instant;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class KafuVm_Test {

    private final static RuntimeMXBean RUNTIME_MX_BEAN = ManagementFactory.getRuntimeMXBean();

    @Test
    void started() {
        final var now = Instant.now();
        assertTrue(now.isAfter(KafuVm.getStarted()));

        assertEquals(KafuVm.getStarted().toEpochMilli(), RUNTIME_MX_BEAN.getStartTime());
    }

    @Test
    void uptimeBean() {
        final Duration uptimeKafu = KafuVm.getUptime();
        final Duration uptimeBean = Duration.ofMillis(RUNTIME_MX_BEAN.getUptime());
        // it takes some time to query the bean.
        Data.lgr.info("time to query bean: " + KafuDuration.adaptUnits(KafuVm.getUptime().minus(uptimeKafu)));

        assertFalse(uptimeKafu.isNegative());
        assertFalse(uptimeKafu.isZero());

        assertFalse(uptimeBean.isNegative());
        assertFalse(uptimeBean.isZero());

        // there is a substantial difference. I dont know, where it comes from. Its not only the time needed to
        // query the bean, as you can see in the log ...
        final Duration difference = uptimeBean.minus(uptimeKafu);
        Data.lgr.info("difference KafuVm.getUptime() and bean: " + KafuDuration.adaptUnits(difference));
        // on MacOS its usually around 100ms. If it ever gets more, I might switch to the bean value. For now,
        // I stick with my approach, cause this seems much less heavy and there is no need to be exact
        // 2019-09-11: the test failed, cause difference was 202! Mhm.
        assertTrue(difference.abs().toMillis() < 300);
    }

    @Test
    void uptimeSleep() throws InterruptedException {
        final Duration uptime1 = KafuVm.getUptime();
        assertTrue(uptime1.toNanos() > 0);

        Thread.sleep(100);
        final Duration uptime2 = KafuVm.getUptime();

        final long difference = uptime2.minus(uptime1).toMillis();
        // 2019-09-12: it happened a few times in a row that is was below 100ms! On Windows 7
        assertTrue(difference >= 99, "difference in millis" + difference);

    }

    @Test
    void memory() {
        final DataSize free = KafuVm.getFreeMemory();
        final DataSize total = KafuVm.getTotalMemory();
        final DataSize max = KafuVm.getMaxMemory();

        assertFalse(free.isZero(), "free memory ZERO: " + free);
        assertFalse(free.isNegative(), "free memory negative: " + free);

        assertFalse(total.isZero(), "total memory ZERO: " + total);
        assertFalse(total.isNegative(), "total memory negative: " + total);

        assertFalse(max.isZero(), "max memory ZERO: " + max);
        assertFalse(max.isNegative(), "max memory negative: " + max);

        assertTrue(max.isBiggerThan(free), max + " max > free " + free);
        assertTrue(max.isBiggerThan(total), max + " max > free " + total);
    }

    @Test
    void language() {
        final Locale locale = Locale.getDefault();

        if (locale != null && locale.getLanguage() != null) {
            assertEquals(locale.getLanguage(), KafuVm.getDefaultLanguage());
        } else {
            Data.lgr.info("There really was no language!");
            assertEquals("N/A", KafuVm.getDefaultLanguageFbk("N/A"));
        }
    }

    @Test
    void info() {
        final String info = KafuVm.getInfo();
        assertNotNull(info);
        assertTrue(info.contains(SystemProperty.VM_VERSION.asStringFbk()));
        assertTrue(info.contains(Long.toString(KafuVm.getProcessPid())));
    }

    @Test
    void infoAndUptime() {
        final String info = KafuVm.getInfoAndUptime();
        assertNotNull(info);
        assertTrue(info.contains(SystemProperty.VM_VERSION.asStringFbk()));
        assertTrue(info.contains("since"));
        assertTrue(info.contains(Long.toString(KafuVm.getProcessPid())));
    }

    @Test
    void infoAndStarted() {
        final String info = KafuVm.getInfoAndStarted();
        assertNotNull(info);
        System.out.println(info);
        assertTrue(info.contains(SystemProperty.VM_VERSION.asStringFbk()));
        assertTrue(info.contains("since"));
        assertTrue(info.contains(Long.toString(KafuVm.getProcessPid())));
    }

    @Test
    void pid() {
        // just tests, if another way leads to the same result
        assertEquals(KafuVm.getProcessPid(), RUNTIME_MX_BEAN.getPid());
    }


    public static void main(String[] args) throws InterruptedException{
        System.out.println("isWindows()    : " + KafuOs.isWindows());
        System.out.println("info           : " + KafuVm.getInfo());
        System.out.println("infoLocale     : " + KafuVm.getInfoLocale());
        System.out.println("infoAndStarted : " + KafuVm.getInfoAndStarted());
        System.out.println("infoAndUptime  : " + KafuVm.getInfoAndUptime());
        System.out.println("processInfo    : " + KafuVm.getProcessInfo());

        Thread.sleep(5000);

        System.out.println();
        System.out.println("  -----   5 seconds later -------");
        System.out.println();

        System.out.println("infoAndStarted : " + KafuVm.getInfoAndStarted());
        System.out.println("infoAndUptime  : " + KafuVm.getInfoAndUptime());
        System.out.println("processInfo    : " + KafuVm.getProcessInfo());

        Thread.sleep(10000);
        System.out.println();
        System.out.println("  -----   another 10 seconds later -------");
        System.out.println();

        System.out.println("infoAndStarted : " + KafuVm.getInfoAndStarted());
        System.out.println("infoAndUptime  : " + KafuVm.getInfoAndUptime());
        System.out.println("processInfo    : " + KafuVm.getProcessInfo());

        System.out.println();
        System.out.println("  -----   and now really doing something .... -------");
        System.out.println();
        System.out.println("sizeOfDirectory(\"/\") =  " + KafuFile.sizeQuietly("/"));

        System.out.println("infoAndStarted : " + KafuVm.getInfoAndStarted());
        System.out.println("infoAndUptime  : " + KafuVm.getInfoAndUptime());
        System.out.println("processInfo    : " + KafuVm.getProcessInfo());
    }

}
