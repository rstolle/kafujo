package junit.config;

import net.kafujo.config.TypedProperties;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Point out problems with {@link Properties} not fixed by {@link TypedProperties}
 */
class PropertiesProblems_Test {

    @Test
    void keyUsedTwice () {
        final var props = TypedProperties.loadResource("properties/Fail_KeyUsedTwice.properties");
        assertEquals(2, props.size());
        assertEquals(2, props.stringPropertyNames().size());

        assertEquals(1, props.readInt("one"));
        assertTrue(props.isAvailable("two")); // its there, but what value??
    }

    @Test
    void keyUsedTwiceXml () {
        final var props = TypedProperties.loadResourceXml("properties/Fail_KeyUsedTwice.xml");
        assertEquals(2, props.size());
        assertEquals(2, props.stringPropertyNames().size());

        assertEquals(1, props.readInt("one"));
        assertTrue(props.isAvailable("two")); // its there, but what value??
    }
}
