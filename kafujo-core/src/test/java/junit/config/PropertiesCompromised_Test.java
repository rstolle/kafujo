package junit.config;

import net.kafujo.io.KafuFile;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

/**
 * These tests point out unfortunate behavior of {@link Properties}. When you use
 * {@link Properties#put(Object, Object)} with non-String-values,
 * {@link Properties#getProperty(String, String)} will give you null values. So its seems smart to
 * always use {@link Properties#setProperty(String, String)}, but its annoying, when working with
 * other types. In the {@link Properties javadoc} they strongly discourage the use of
 * {@link Properties#put(Object, Object) put}. They call a Properties object with non Strings "compromised". Well, that
 * helps.
 * <p>
 * JavaMail, which heavily relies on Properties, seems not to have a problem with "compromised" Properties objects,
 * which makes me feel even more uncomfortable.
 */
class PropertiesCompromised_Test {

    private static final Properties compromised = new Properties();

    @BeforeAll
    static void init () {
        compromised.setProperty("one_as_String", "1"); // the recommended way
        compromised.put("two_as_String", "2");         // not recommended but still works, cause its String
        compromised.put("three_as_Int", 3);            // now we have a "compromised" Properties object - what a bullshit
    }


    @Test
    void countTrap() {
        assertEquals(3, compromised.size());  // looks right, but there are "official" only ...
        assertEquals(2, compromised.stringPropertyNames().size()); // ... two properties
    }

    @Test
    void getPropertyFails() {
        // the string properties are fine
        assertEquals(String.class, compromised.get("one_as_String").getClass());
        assertEquals("1", compromised.getProperty("one_as_String"));
        assertEquals("1", compromised.get("one_as_String"));

        assertEquals(compromised.getProperty("one_as_String"), compromised.get("one_as_String")); // "1" as String
        assertEquals(compromised.getProperty("two_as_String"), compromised.get("two_as_String")); // "2" as String

        // BUT NOT THIS ONE
        assertEquals(Integer.class, compromised.get("three_as_Int").getClass()); // cause an in was put in
        assertEquals(3, compromised.get("three_as_Int"));    // you can fetch it this way ....

        // ... BUT NOT THIS WAY:
        assertNull(compromised.getProperty("three_as_Int"));  // getProperties gives null on ALL none-String-objects!
    }

    @Test
    void storeFails () {
        final var storage = KafuFile.createTempFile(); // How many properties will be saved ... ?
        assertThrows(ClassCastException.class, () -> compromised.store(Files.newOutputStream(storage), "compromised"));
        assertTrue(KafuFile.isEmptyFile(storage)); // nothing, cause it tried to cast int to String.
        KafuFile.deleteFileQuietly(storage);
    }

    @Test
    void storeToXmlFailesDifferently () throws IOException{
        final Path storage = KafuFile.createTempFile(); // How many properties will be saved ... ?
        compromised.storeToXML(Files.newOutputStream(storage), "compromised");
        assertFalse(KafuFile.isEmptyFile(storage)); // ... at least some, but all? Lets validate

        final Properties validate = new Properties();
        validate.loadFromXML(Files.newInputStream(storage));

        assertEquals(2, validate.stringPropertyNames().size()); // as before
        assertEquals(2, validate.size());      // not as before, because ...
        assertNull (validate.get("three_as_Int"));     // ... the none String property didn't make it to the file

        // the others are fine
        assertEquals ("1", validate.getProperty("one_as_String"));
        assertEquals ("2", validate.getProperty("two_as_String"));

        KafuFile.deleteFileQuietly(storage);
    }
}
