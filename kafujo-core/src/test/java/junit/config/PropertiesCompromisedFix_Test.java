package junit.config;

import net.kafujo.config.TypedProperties;
import net.kafujo.io.KafuFile;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Shows how {@link TypedProperties} fixes the issues described in {@link PropertiesCompromised_Test} and some more
 * advantages of {@link TypedProperties}.
 */
class PropertiesCompromisedFix_Test {

    private static final Properties fixed = new TypedProperties();         // to illustrate it can be used as Properties
    private static final TypedProperties typed = (TypedProperties) fixed;  // to show some TypedProperty features

    @BeforeAll
    static void init() {
        // with TypedProperties it doesn't matter anymore, which method you use to put properties in
        fixed.setProperty("one_as_String", "1"); // the recommended way
        fixed.put("two_as_String", "2");         // another recommended way
        fixed.put("three_as_Int", 3);            // another definitely recommended way, cause put
    }

    @Test
    void countTrapFixed() {
        // sizes are the same now is the same
        assertEquals(3, fixed.size());
        assertEquals(3, fixed.stringPropertyNames().size());
    }

    @Test
    void getPropertyFixed() {
        // this works with normal Properties already
        assertEquals(fixed.getProperty("one_as_String"), fixed.get("one_as_String")); // "1" as String
        assertEquals(fixed.getProperty("two_as_String"), fixed.get("two_as_String")); // "2" as String

        // And now, this works as well
        assertEquals("3", fixed.getProperty("three_as_Int"));
        assertEquals("3", fixed.get("three_as_Int"));             // but its not an int anymore!
        assertEquals(String.class, fixed.get("three_as_Int").getClass()); // the int is converted to String by put()

        // BUT you can still read int unsing the TypedReader methods:
        assertEquals(3, typed.readInt("three_as_Int"));
        assertEquals(Integer.class, ((Object) typed.readInt("three_as_Int")).getClass());
    }

    @Test
    void storeFixed() throws IOException {
        final var storage = KafuFile.createTempFile(); // How many properties will be saved ...
        fixed.store(Files.newOutputStream(storage), "not compromised anymore!");
        assertFalse(KafuFile.isEmptyFile(storage));

        final Properties validate = new Properties();
        validate.load(Files.newInputStream(storage));

        assertEquals(fixed.size(), validate.size());
        assertEquals(fixed.stringPropertyNames(), validate.stringPropertyNames());
        assertEquals(fixed.get("one_as_String"), validate.get("one_as_String"));
        assertEquals(fixed.get("two_as_String"), validate.get("two_as_String"));
        assertEquals(fixed.get("three_as_Int"), validate.get("three_as_Int"));

        KafuFile.deleteFileQuietly(storage);
    }


    /**
     * Same as {@link #storeFixed()} but uses the convenient methods of TypedProperties
     */
    @Test
    void storeToXmlFixed() { // IOExceptions wrapped in UncheckedIOExceptions
        final Path storage = typed.storeToXml("written to temp file");  // thats all!
        assertFalse(KafuFile.isEmptyFile(storage));

        final Properties validate = TypedProperties.loadFileXml(storage);  // again, this simple

        assertEquals(fixed.size(), validate.size());
        assertEquals(fixed.stringPropertyNames(), validate.stringPropertyNames());
        assertEquals(fixed.get("one_as_String"), validate.get("one_as_String"));
        assertEquals(fixed.get("two_as_String"), validate.get("two_as_String"));
        assertEquals(fixed.get("three_as_Int"), validate.get("three_as_Int"));

        KafuFile.deleteFileQuietly(storage);
    }

}
