package junit.config;

import net.kafujo.base.RequirementException;
import net.kafujo.config.PropertiesSource;
import net.kafujo.config.TypedProperties;
import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;
import org.junit.jupiter.api.Test;
import support.Data;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static support.Data.*;

class TypedProperties_Test {

    private static final PropertiesSource NOT_THERE = PropertiesSource.ofFile(NONSENSE);
    private static final PropertiesSource THREE_ESPERANTO = PropertiesSource.ofResource(Data.THREE_ESPERANTO);


    @Test
    void edges() {
        assertThrows(IllegalArgumentException.class, () -> TypedProperties.load(NOT_THERE));
        assertThrows(NullPointerException.class, () -> TypedProperties.loadFile((String) null));
        assertThrows(NullPointerException.class, () -> TypedProperties.loadFile((Path) null));
        assertThrows(RequirementException.class, () -> TypedProperties.loadFile(NOT_EXISTING_PATH));
    }

    @Test
    void loadSource() {
        var props = TypedProperties.load(THREE_ESPERANTO);
        assertEquals(3, props.size());
        assertEquals("tri", props.getProperty("three"));
    }

    @Test
    void loadFile_() {
        Path temp = Resources.ofCurrentThread().asTempFile(ONE_NUMBER);

        var props = TypedProperties.loadFile(temp);
        assertEquals(1, props.size());
        assertEquals("1", props.getProperty("one"));

        KafuFile.deleteQuietly(temp);
    }


}
