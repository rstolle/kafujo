/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package junit.config;

import net.kafujo.base.RequirementException;
import net.kafujo.config.SystemProperty;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

import static net.kafujo.config.SystemProperty.JAVA_VERSION;
import static net.kafujo.text.KafuText.UNIVERSAL_NA;
import static org.junit.jupiter.api.Assertions.*;
import static support.Data.NONSENSE;

class SystemProperty_Test {

    private final static Set<String> keysAvailable = Collections.unmodifiableSet(SystemProperty.getKeysAvailable());

    @BeforeAll
    static void beforeAll() {
        if (keysAvailable.contains(NONSENSE)) {
            throw new IllegalStateException("NONSENSE IS ACTUALLY A SYSTEM PROPERTY: "
                    + NONSENSE + " = " + System.getProperty(NONSENSE));
        }
    }

    @Test
    void listed() {
        final SortedSet<SystemProperty> listedNotAvailable = SystemProperty.getListedNotAvailable();
        final SortedSet<SystemProperty> listedAvailable = SystemProperty.getListedAvailable();

        final Set<SystemProperty> merge = new HashSet<>(); // merge listedNotAvailable and listedAvailable
        int count = 0;

        for (SystemProperty p : listedAvailable) {
            assertTrue(p.isAvailable(), p.key + " MUST be available");
            assertTrue(merge.add(p), p.key + " was already in merge");
            count++;
        }

        for (SystemProperty p : listedNotAvailable) {
            assertFalse(p.isAvailable(), p.key + " CANNOT be available");
            assertTrue(merge.add(p), p.key + " was already in merge");
            count++;
        }

        assertEquals(count, listedAvailable.size() + listedNotAvailable.size());
        assertEquals(count, SystemProperty.values().length);

        final Set<SystemProperty> listed = SystemProperty.getListed();
        assertEquals(count, listed.size());
        assertEquals(listed, merge);
    }

    @Test
    void keys() {
        final SortedSet<String> availableNotListed = SystemProperty.getKeysAvailableNotListed();
        final SortedSet<SystemProperty> listedAvailable = SystemProperty.getListedAvailable();

        assertTrue(keysAvailable.size() >= availableNotListed.size(), "There cannot be more properties than not listed properties");
        assertEquals(keysAvailable.size(), availableNotListed.size() + listedAvailable.size());

        Set<String> merge = new HashSet<>(availableNotListed);

        for (SystemProperty sp : listedAvailable) {  // now add the available properties
            assertTrue(sp.isAvailable());
            assertTrue(SystemProperty.isAvailable(sp.key));
            assertTrue(merge.add(sp.key), sp.key + " was already in combine");
        }

        assertEquals(keysAvailable, merge);
    }


    @Test
    void stringFbk() {
        assertEquals(UNIVERSAL_NA, SystemProperty.asStringFbk(NONSENSE, UNIVERSAL_NA));

        if (JAVA_VERSION.isAvailable()) {
            assertFalse(JAVA_VERSION.isNotAvailable());
            assertEquals(JAVA_VERSION.asString(), SystemProperty.asString(JAVA_VERSION.key));
        } else {
            assertTrue(JAVA_VERSION.isNotAvailable());
        }
    }

    @Test
    void asLong () {
        assertFalse(SystemProperty.isAvailable(NONSENSE));
        assertThrows(RequirementException.class, () -> SystemProperty.asLong(NONSENSE));

        // but the fbk must work:
        assertEquals(42L, SystemProperty.asLongFbk(NONSENSE, 42L));
        assertEquals(-7, SystemProperty.asLongFbk(NONSENSE, -7L));
        assertNull(SystemProperty.asLongFbk(NONSENSE, null));

        System.setProperty(NONSENSE, "3");
        assertTrue(SystemProperty.isAvailable(NONSENSE));

        assertEquals("3", SystemProperty.asString(NONSENSE));
        assertEquals(3L, SystemProperty.asLong(NONSENSE));

        System.setProperty(NONSENSE, "3G");
        assertEquals("3G", SystemProperty.asString(NONSENSE));
        assertThrows(NumberFormatException.class, () -> SystemProperty.asLong(NONSENSE));

        System.getProperties().remove(NONSENSE);
        assertFalse(SystemProperty.isAvailable(NONSENSE));
    }

    @Test
    void asInteger () {
        assertFalse(SystemProperty.isAvailable(NONSENSE));
        assertThrows(RequirementException.class, () -> SystemProperty.asInteger(NONSENSE));

        // but the fbk must work:
        assertEquals(42, SystemProperty.asIntegerFbk(NONSENSE, 42));
        assertEquals(-7, SystemProperty.asIntegerFbk(NONSENSE, -7));
        assertNull(SystemProperty.asIntegerFbk(NONSENSE, null));

        System.setProperty(NONSENSE, "3");
        assertTrue(SystemProperty.isAvailable(NONSENSE));

        assertEquals("3", SystemProperty.asString(NONSENSE));
        assertEquals(3, SystemProperty.asInteger(NONSENSE));

        System.setProperty(NONSENSE, "4G");
        assertEquals("4G", SystemProperty.asString(NONSENSE));
        assertThrows(NumberFormatException.class, () -> SystemProperty.asInteger(NONSENSE));

        System.getProperties().remove(NONSENSE);
        assertFalse(SystemProperty.isAvailable(NONSENSE));
    }

    @Test
    void asBoolean () {
        assertFalse(SystemProperty.isAvailable(NONSENSE));
        assertFalse(SystemProperty.asBooleanFbk(NONSENSE, false));
        assertTrue(SystemProperty.asBooleanFbk(NONSENSE, true));
        assertNull(SystemProperty.asBooleanFbk(NONSENSE, null));

        assertThrows(RequirementException.class, () -> SystemProperty.asBoolean(NONSENSE));

        System.setProperty(NONSENSE, "false");
        assertTrue(SystemProperty.isAvailable(NONSENSE));


        assertEquals("false", SystemProperty.asString(NONSENSE));
        assertFalse(SystemProperty.asBoolean(NONSENSE));

        System.setProperty(NONSENSE, "TRUE");
        assertEquals("TRUE", SystemProperty.asString(NONSENSE));
        assertTrue(SystemProperty.asBoolean(NONSENSE));

        System.setProperty(NONSENSE, "trsdfaue");
        assertEquals("trsdfaue", SystemProperty.asString(NONSENSE));
        assertThrows(IllegalArgumentException.class, () -> SystemProperty.asBoolean(NONSENSE));

        System.getProperties().remove(NONSENSE);
        assertFalse(SystemProperty.isAvailable(NONSENSE));
    }

    @Test
    void toStrings() {
        final String all = SystemProperty.VM_VERSION.toString();
        assertEquals(all, SystemProperty.VM_VERSION.toString(Integer.MAX_VALUE));

        final String s10 = SystemProperty.VM_VERSION.toString(10);
        final String s20 = SystemProperty.VM_VERSION.toString(20);
        final String s99 = SystemProperty.VM_VERSION.toString(99);

        assertTrue(all.startsWith(s10.substring(0,8)));
        assertFalse(all.startsWith(s10.substring(0,9)));
        assertTrue(all.startsWith(s20.substring(0,16)));
        assertTrue(all.startsWith(s99));
        
    }

    @Test
    void crypoPolicyHasNoEffectAnymore() throws NoSuchAlgorithmException {
        // needed to be "unlimited" until java 1.8.0_161 to uses full length keys
        // https://blog.doubleslash.de/jce-unlimited-strenght-cryptography-ab-jetzt-standardmaessig-aktiv/
        // apparently it has no effect anymore
        assertEquals(Integer.MAX_VALUE, Cipher.getMaxAllowedKeyLength("AES"));
        SystemProperty.CRYPTO_POLICY.set("limited");
        assertEquals(Integer.MAX_VALUE, Cipher.getMaxAllowedKeyLength("AES"));
    }
}
