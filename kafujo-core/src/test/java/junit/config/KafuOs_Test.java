/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package junit.config;

import net.kafujo.config.KafuOs;
import net.kafujo.io.KafuFile;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static support.Data.lgr;

class KafuOs_Test {

    @Test
    void execute() {
        assertThrows(NullPointerException.class, () -> KafuOs.execute(null));
        assertThrows(NullPointerException.class, () -> KafuOs.executePretty((String) null));

        final Path tmpDir = KafuFile.createTempDirectory();
        KafuFile.createFile(tmpDir.resolve("must_be_there"));

        final String ls;
        final String lsPretty;

        if (KafuOs.isWindows()) {
            ls = KafuOs.execute(List.of("cmd", "/c", "dir", tmpDir.toString()));
            lsPretty = KafuOs.executePretty(List.of("cmd", "/c", "dir", tmpDir.toString()));
        } else {
            ls = KafuOs.execute(List.of("ls", "-l", tmpDir.toString()));
            lsPretty = KafuOs.executePretty(List.of("ls", "-l", tmpDir.toString()));
        }

        assertNotNull(ls);
        lgr.info(ls);
        assertTrue(ls.contains("must_be_there"));

        assertNotNull(lsPretty);
        lgr.info(lsPretty);
        assertTrue(lsPretty.contains("must_be_there"));

        if (KafuOs.isWindows()) {
            assertTrue(lsPretty.contains("dir"));
        } else {
            assertTrue(lsPretty.contains("ls -l"));
        }
        assertEquals(2, KafuFile.deleteDirectory(tmpDir));
    }


}
