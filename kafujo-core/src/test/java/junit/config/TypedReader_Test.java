package junit.config;

import net.kafujo.base.RequirementException;
import net.kafujo.config.TypedProperties;
import net.kafujo.io.KafuFile;
import net.kafujo.network.EmailAddress;
import net.kafujo.network.Hostname;
import net.kafujo.network.Port;
import net.kafujo.text.KafuText;
import net.kafujo.units.KafuDuration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static junit.network.NetworkData.WIKI_HOSTNAME;
import static junit.network.NetworkData.WIKI_HOST_HTTP;

/**
 * This test the {@link net.kafujo.config.TypedReader} methods using  {@link TypedProperties}.
 */
class TypedReader_Test {

    private static final TypedProperties typedProps = new TypedProperties();
    private static final String FAKE_KEY = "klfs";
    private static final String KEY_NO_NUM = "no number";
    private static final String KEY_NA_VALUE = "not available"; // value is N/A

    private static final float FLOAT_PI = (float) 3.14;
    private static final BigDecimal BIGDEC_PI = new BigDecimal(Math.PI);

    @BeforeAll
    static void initTypedProperties() {
        typedProps.put("no number", "this cannot be parsed to anything");
        typedProps.put("not available", KafuText.UNIVERSAL_NA);

        typedProps.put("max", "max");
        typedProps.put("MAX", "MAX");

        typedProps.put("min", "min");
        typedProps.put("MIN", "MIN");

        // boolean
        typedProps.put("true", true);
        typedProps.put("TRUE", "TRUE");
        typedProps.put("TrUE", "TrUE");

        typedProps.put("false", false);
        typedProps.put("False", "False");

        // numbers
        typedProps.put("minus ten", -10);
        typedProps.put("minus one", -1);
        typedProps.put("one", 1);
        typedProps.put("two", 2);
        typedProps.put("42", 42);
        typedProps.put("PI float", FLOAT_PI);
        typedProps.put("PI double", Math.PI);    // 3.141592653589793
        typedProps.put("PI bigdec", BIGDEC_PI);   // 3.141592653589793115997963468544185161590576171875

        // datasize and durations
        typedProps.put("duration", KafuDuration.ONE_HOUR); // PT1H

        // urls, emails and ports
        typedProps.put("hostname", WIKI_HOSTNAME);
        typedProps.put("hostname:80", WIKI_HOST_HTTP);
        typedProps.put("hostname.string", "www.wikipedia.org");
        typedProps.put("https", "https://www.kernel.org/");
        typedProps.put("mailto", "mailto:rstolle@posteo.de");
        typedProps.put("email.ok", new EmailAddress("ok@ok.net"));
        typedProps.put("email.ok.string", "ok@ok.net");
        typedProps.put("email.fail", "fail.@-fail.com");
        typedProps.put(":8080", 8080);
        typedProps.put(":80", Port.HTTP);
    }

    @Test
    void sizeAndAvailability() {
        assertEquals(30, typedProps.size());

        assertFalse(typedProps.isAvailable(FAKE_KEY));
        assertTrue(typedProps.isAvailable(KEY_NA_VALUE)); // there is a value "N/A"
        assertTrue(typedProps.isAvailable("42"));

        assertTrue(typedProps.isAvailable("true"));
        assertTrue(typedProps.isAvailable("False"));
    }

    @Test
    void readStrings() {
        assertEquals("42", typedProps.readString("42"));

        assertEquals("42", typedProps.readStringFbk("42"));
        assertEquals("42", typedProps.readStringFbk("42", "23"));

        assertNull(typedProps.readStringFbk(FAKE_KEY, null));
        assertEquals("23", typedProps.readStringFbk(FAKE_KEY, "23"));
        assertEquals(KafuText.UNIVERSAL_NA, typedProps.readStringFbk(FAKE_KEY));
    }

    @Test
    void readBooleans() {
        assertEquals("true", typedProps.readString("true"));
        assertEquals("false", typedProps.readString("false"));

        assertTrue(typedProps.readBoolean("true"));
        assertTrue(typedProps.readBoolean("TRUE"));
        assertTrue(typedProps.readBoolean("TrUE"));

        assertFalse(typedProps.readBoolean("false"));
        assertFalse(typedProps.readBoolean("False"));

        assertThrows(IllegalArgumentException.class, () -> typedProps.readBoolean("42"));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readBooleanFbk("42", true));
        assertThrows(RequirementException.class, () -> typedProps.readBoolean(FAKE_KEY));

        assertTrue(typedProps.readBooleanFbk(FAKE_KEY, true));
        assertFalse(typedProps.readBooleanFbk(FAKE_KEY, false));
        assertNull(typedProps.readBooleanFbk(FAKE_KEY, null));
    }

    @Test
    void readShorts() {
        short fbk = 7;

        assertEquals(42, typedProps.readShort("42"));
        assertEquals(-1, typedProps.readShort("minus one"));

        assertEquals(Short.MIN_VALUE, typedProps.readShort("min"));
        assertEquals(Short.MIN_VALUE, typedProps.readShort("MIN"));
        assertEquals(Short.MAX_VALUE, typedProps.readShort("max"));
        assertEquals(Short.MAX_VALUE, typedProps.readShort("MAX"));

        assertThrows(RequirementException.class, () -> typedProps.readShort(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readShort("no number"));
        assertEquals((short) 42, typedProps.readShortFbk("42", fbk));
        assertEquals(fbk, typedProps.readShortFbk(FAKE_KEY, fbk));
    }

    @Test
    void readInts() {
        assertEquals(42, typedProps.readInt("42"));
        assertEquals(-10, typedProps.readInt("minus ten"));

        assertEquals(Integer.MIN_VALUE, typedProps.readInt("MIN"));
        assertEquals(Integer.MAX_VALUE, typedProps.readInt("max"));

        assertThrows(RequirementException.class, () -> typedProps.readInt(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readInt("no number"));
        assertEquals(42, typedProps.readIntFbk("42", 0));
        assertEquals(7, typedProps.readIntFbk(FAKE_KEY, 7));
    }

    @Test
    void readLongs() {
        assertEquals(42, typedProps.readLong("42"));
        assertEquals(-1, typedProps.readLong("minus one"));

        assertEquals(Long.MIN_VALUE, typedProps.readLong("min"));
        assertEquals(Long.MAX_VALUE, typedProps.readLong("max"));

        assertThrows(RequirementException.class, () -> typedProps.readLong(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readLong("no number"));
        assertThrows(NumberFormatException.class, () -> typedProps.readLong("PI double"));
        assertEquals(42, typedProps.readLongFbk("42", 0L));
        assertEquals(7, typedProps.readLongFbk(FAKE_KEY, 7L));
    }

    @Test
    void readBigIntegers() {
        assertEquals(BigInteger.ONE, typedProps.readBigInteger("one"));
        assertThrows(RequirementException.class, () -> typedProps.readBigInteger(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readBigInteger("no number"));
        assertThrows(NumberFormatException.class, () -> typedProps.readBigInteger("PI double"));
        assertEquals(BigInteger.TWO, typedProps.readBigIntegerFbk("two", BigInteger.TEN));
        assertEquals(BigInteger.TEN, typedProps.readBigIntegerFbk(FAKE_KEY, BigInteger.TEN));
    }


    @Test
    void readFloats() {
        final float fbk = (float) 7.2;

        assertEquals(42, typedProps.readFloat("42"));
        assertEquals(FLOAT_PI, typedProps.readFloat("PI float"));
        assertEquals(-1, typedProps.readFloat("minus one"));

        assertEquals(Float.MIN_VALUE, typedProps.readFloat("min"));
        assertEquals(Float.MAX_VALUE, typedProps.readFloat("max"));

        assertThrows(RequirementException.class, () -> typedProps.readFloat(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readFloat("no number"));
        assertEquals(42, typedProps.readFloatFbk("42", fbk));
        assertEquals(fbk, typedProps.readFloatFbk(FAKE_KEY, fbk));
    }

    @Test
    void readDoubles() {
        final double fbk = 6.9;

        assertEquals(42, typedProps.readDouble("42"));
        assertEquals(1, typedProps.readDouble("one"));
        assertEquals(-10, typedProps.readDouble("minus ten"));

        assertEquals(Double.MIN_VALUE, typedProps.readDouble("min"));
        assertEquals(Double.MAX_VALUE, typedProps.readDouble("max"));

        assertEquals(Math.PI, typedProps.readDouble("PI double"));
        assertThrows(RequirementException.class, () -> typedProps.readDouble(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readDouble("no number"));
        assertEquals(42, typedProps.readDoubleFbk("42", fbk));
        assertEquals(fbk, typedProps.readDoubleFbk(FAKE_KEY, fbk));
    }

    @Test
    void readBigDecimals() {
        final BigDecimal fbk = new BigDecimal("1.33333333");

        assertEquals(BigDecimal.ONE, typedProps.readBigDecimal("one"));
        assertEquals(BIGDEC_PI, typedProps.readBigDecimal("PI bigdec"));
        assertThrows(RequirementException.class, () -> typedProps.readBigDecimal(FAKE_KEY));
        assertThrows(NumberFormatException.class, () -> typedProps.readBigDecimal("no number"));
        assertEquals(BigDecimal.valueOf(2), typedProps.readBigDecimalFbk("two", fbk));
        assertEquals(fbk, typedProps.readBigDecimalFbk(FAKE_KEY, fbk));
    }

    @Test
    void readDuration() {
        assertEquals(KafuDuration.ONE_HOUR, typedProps.readDuration("duration"));

        assertEquals(KafuDuration.MIN_VALUE, typedProps.readDuration("min"));
        assertEquals(KafuDuration.MAX_VALUE, typedProps.readDuration("MAX"));

        assertThrows(RequirementException.class, () -> typedProps.readDuration(FAKE_KEY));
        assertThrows(DateTimeParseException.class, () -> typedProps.readDuration("42"));

        assertEquals(KafuDuration.ONE_HOUR, typedProps.readDurationFbk("duration", KafuDuration.ONE_DAY));
        assertEquals(KafuDuration.ONE_DAY, typedProps.readDurationFbk(FAKE_KEY, KafuDuration.ONE_DAY));
    }

    @Test
    void readDurationsOfMillis() {
        assertEquals(42, typedProps.readDurationOfMillis("42").toMillis());
        assertEquals(-10, typedProps.readDurationOfMillis("minus ten").toMillis());
        assertEquals(KafuDuration.ONE_MILLI, typedProps.readDurationOfMillis("one"));

        assertEquals(Long.MIN_VALUE, typedProps.readDurationOfMillis("MIN").toMillis());
        assertEquals(Long.MAX_VALUE, typedProps.readDurationOfMillis("MAX").toMillis());

        assertThrows(RequirementException.class, () -> typedProps.readDurationOfMillis(FAKE_KEY));
        assertEquals(KafuDuration.ONE_MILLI, typedProps.readDurationOfMillisFbk("one", 1000L));
        assertEquals(KafuDuration.ONE_SECOND, typedProps.readDurationOfMillisFbk(FAKE_KEY, 1000L));
        assertEquals(KafuDuration.ONE_DAY, typedProps.readDurationOfMillisFbk(FAKE_KEY, KafuDuration.ONE_DAY));
        assertNull(typedProps.readDurationOfMillisFbk(FAKE_KEY, (Long) null));
        assertNull(typedProps.readDurationOfMillisFbk(FAKE_KEY, (Duration) null));
    }

    @Test
    void readDurationsOfSeconds() {
        assertEquals(42, typedProps.readDurationOfSeconds("42").toSeconds());
        assertEquals(-10, typedProps.readDurationOfSeconds("minus ten").toSeconds());
        assertEquals(KafuDuration.ONE_SECOND, typedProps.readDurationOfSeconds("one"));

        assertEquals(Long.MIN_VALUE, typedProps.readDurationOfSeconds("MIN").toSeconds());
        assertEquals(Long.MAX_VALUE, typedProps.readDurationOfSeconds("MAX").toSeconds());

        assertThrows(RequirementException.class, () -> typedProps.readDurationOfSeconds(FAKE_KEY));
        assertEquals(KafuDuration.ONE_SECOND, typedProps.readDurationOfSecondsFbk("one", 60L));
        assertEquals(KafuDuration.ONE_MINUTE, typedProps.readDurationOfSecondsFbk(FAKE_KEY, 60L));
        assertEquals(KafuDuration.ONE_DAY, typedProps.readDurationOfSecondsFbk(FAKE_KEY, KafuDuration.ONE_DAY));
        assertNull(typedProps.readDurationOfSecondsFbk(FAKE_KEY, (Long) null));
        assertNull(typedProps.readDurationOfSecondsFbk(FAKE_KEY, (Duration) null));
    }

    @Test
    void readUrls() throws MalformedURLException {
        URL https = new URL("https", "www.kernel.org", "/");
        URL mailto = new URL("mailto:rstolle@posteo.de");

        assertEquals(https, typedProps.readUrl("https"));
        assertEquals(mailto, typedProps.readUrl("mailto"));

        assertThrows(RequirementException.class, () -> typedProps.readUrl(FAKE_KEY));
        var illegalArg = assertThrows(IllegalArgumentException.class, () -> typedProps.readUrl("one"));
        assertTrue(illegalArg.getCause().toString().startsWith("java.net.MalformedURLException"));

        assertEquals(https, typedProps.readUrlFbk("https", mailto));
        assertEquals(mailto, typedProps.readUrlFbk(FAKE_KEY, mailto));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readUrlFbk("one", mailto));
    }

    @Test
    void readEmails() {
        final var ok = new EmailAddress("ok@ok.net");
        final var fbk = new EmailAddress("fbk@ok.net");
        assertEquals(ok, typedProps.readEmailAddress("email.ok"));
        assertEquals(ok, typedProps.readEmailAddress("email.ok.string"));

        assertThrows(IllegalArgumentException.class, () -> typedProps.readEmailAddress("email.fail"));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readEmailAddress("mailto"));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readEmailAddressFbk("one", fbk));

        assertEquals(ok, typedProps.readEmailAddressFbk("email.ok", fbk));
        assertEquals(fbk, typedProps.readEmailAddressFbk(FAKE_KEY, fbk));
        assertEquals(fbk, typedProps.readEmailAddressFbk(FAKE_KEY, fbk.toString()));
        assertNull(typedProps.readEmailAddressFbk(FAKE_KEY, KafuText.NULL));
        assertNull(typedProps.readEmailAddressFbk(FAKE_KEY, (EmailAddress) null));
    }

    @Test
    void readHostname() {
        final var fbk = new Hostname("fallback.solutions.org");
        assertEquals(WIKI_HOSTNAME, typedProps.readHostname("hostname"));
        assertEquals(WIKI_HOSTNAME, typedProps.readHostname("hostname.string"));

        assertThrows(RequirementException.class, () -> typedProps.readHostname("email.ok"));

        assertEquals(WIKI_HOSTNAME, typedProps.readHostnameFbk("hostname", fbk));
        assertEquals(fbk, typedProps.readHostnameFbk(FAKE_KEY, fbk));
        assertEquals(fbk, typedProps.readHostnameFbk(FAKE_KEY, fbk.toString()));
        assertNull(typedProps.readHostnameFbk(FAKE_KEY, KafuText.NULL));
        assertNull(typedProps.readHostnameFbk(FAKE_KEY, (Hostname) null));
    }

    @Test
    void readHost() {
        final var fbk = new Hostname("fallback.solutions.org");
        assertEquals(WIKI_HOST_HTTP, typedProps.readHost("hostname:80"));
        assertEquals(WIKI_HOST_HTTP, typedProps.readHostWithPort("hostname:80"));

        assertThrows(IllegalArgumentException.class, () -> typedProps.readHostWithPort("hostname"));

        // todo: MORE!

    }


    @Test
    void readPorts() {
        Port fbk = Port.FTP;

        assertEquals(Port.HTTP, typedProps.readPort(":80"));
        assertEquals(new Port(8080), typedProps.readPort(":8080"));

        assertThrows(RequirementException.class, () -> typedProps.readPort(FAKE_KEY));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readPort(KEY_NO_NUM));
        assertThrows(IllegalArgumentException.class, () -> typedProps.readPort("minus ten"));
        assertEquals(Port.NOT_PRESENT, typedProps.readPort("minus one"));

        assertEquals(Port.HTTP, typedProps.readPortFbk(":80", fbk));
        assertEquals(fbk, typedProps.readPortFbk(FAKE_KEY, fbk));
    }


    @Test
    void readFiles() {
        final String string = "1\n2\n3";
        final byte[] bytes = string.getBytes();
        final Path path = KafuFile.createTempFile(bytes);
        final List<String> lines = string.lines().collect(Collectors.toList());

        // now write properties
        final TypedProperties tp = new TypedProperties();
        assertEquals(0, tp.size());
        tp.put("path", path);
        assertEquals(1, tp.size());

        assertEquals(path, tp.readPath("path"));
        assertThrows(RuntimeException.class, () -> tp.readPath(FAKE_KEY));
        assertEquals(path, tp.readPathFbk(FAKE_KEY, path));
        assertEquals(path, tp.readPathFbk(FAKE_KEY, path.toString()));

        assertEquals(string, tp.readFileToString("path"));
        assertEquals(lines, tp.readFileToLines("path"));
        assertArrayEquals(bytes, tp.readFileToBytes("path"));

        KafuFile.deleteFileQuietly(path);
    }

}
