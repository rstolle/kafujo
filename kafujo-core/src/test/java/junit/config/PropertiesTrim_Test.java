package junit.config;

import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


/**
 * Configuration using {@link Properties}.
 * <ul>
 * <li>(+)included in jdk</li>
 * <li>(-)not typesafe</li>
 * <li>(-)not imutable</li>
 * <li>(-)no chaining</li>
 * <li>(-){@link #trap() general bad design}</li>
 * </ul>
 */
class PropertiesTrim_Test {

    /**
     * Tests all the keys as they are except the unstripped TEXT_NOT_STRIPPED, cause this comes differs
     * when read from properties file (not stripped) or xml file (stripped)
     *
     * @param props props to be checked
     */
    private void assertValues(final Properties props) {
        assertEquals(8, props.size());
        assertNull(props.getProperty("non there"));
        assertEquals("runtime", props.getProperty("ADD_ON"));

        assertEquals("Hello World", props.getProperty("TEXT"));
        assertEquals("Hello World  ", props.getProperty("TEXT_NOT_STRIPPED"));

        assertEquals("42", props.getProperty("INTEGER"));
        assertEquals("3.1415", props.getProperty("DOUBLE"));

        assertEquals("PT1S", props.getProperty("DURATION"));
        assertEquals("2018-01-01", props.getProperty("DATE"));
        assertEquals("19:42:12", props.getProperty("TIME"));
    }



    @Test
    @Disabled
    void readFile() throws IOException {
        final Properties props = new Properties();
        assertEquals(0, props.size());

        props.load(Resources.ofCurrentThread().asStream("types.properties"));
        assertEquals(7, props.size());

        props.setProperty("ADD_ON", "runtime");
        assertValues(props);
    }

    @Test
    @Disabled
    void readXml() throws IOException {
        final Properties props = new Properties();
        assertEquals(0, props.size());

        props.loadFromXML(Resources.ofCurrentThread().asStream("types.xml"));
        assertEquals(7, props.size());

        props.setProperty("ADD_ON", "runtime");
        assertValues(props);
    }

    /**
     * loads the xml and writes to properties
     *
     * @throws IOException on io propblems
     */
    @Test
    @Disabled
    void writeFile() throws IOException {
        final Properties props = new Properties();

        props.loadFromXML(Resources.ofCurrentThread().asStream("types.xml"));
        assertEquals(7, props.size());
        props.setProperty("ADD_ON", "runtime");

        final Path file = KafuFile.createTempFile();
        try (var stream = Files.newOutputStream(file)) {
            // keeps the spaces behind TEXT_NOT_STRIPPED.
            props.store(stream, "My thoughts on this");
            // stream.write("Its really not closed ...".getBytes());
        }
        System.out.println(file);

        final Properties reload = new Properties();
        reload.load(Files.newInputStream(file));
        assertValues(reload);
    }

    @Test
    @Disabled
    void writeXml() throws IOException {
        final Properties props = new Properties();

        props.load(Resources.ofCurrentThread().asStream("types.properties"));
        assertEquals(7, props.size());
        props.setProperty("ADD_ON", "runtime");

        final Path xml = KafuFile.createTempFile();
        try (var stream = Files.newOutputStream(xml)) {
            // keeps the spaces on TEXT_NOT_STRIPPED but reformatting might delete them!
            props.storeToXML(stream, "My thoughts on this");
            // stream.write("Its really not closed ...".getBytes());
        }
        System.out.println(xml);

        final Properties reload = new Properties();
        reload.loadFromXML(Files.newInputStream(xml));
        assertValues(reload);
    }
}
