package junit.text;

import net.kafujo.io.Resources;
import net.kafujo.text.KafuFormat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static support.Data.TRES_XML;
import static support.Data.TRES_XML_ONE_LINE;

public class KafuFormat_Test {

    @Test
    void formatted2oneline() {
        var lines = Resources.ofCurrentThread().asLines(TRES_XML);

        StringBuilder sb = new StringBuilder();
        for (var line : lines) {
            System.out.println("add: '" + line.strip() + "'");
            sb.append(line.strip());
        }

        var one_line = Resources.ofCurrentThread().asString(TRES_XML_ONE_LINE);

        assertEquals(one_line, sb.toString());
    }

    @Test
    void oneline2formatted() {

        var one_line = Resources.ofCurrentThread().asString(TRES_XML_ONE_LINE);

        assertThrows(Exception.class, () -> KafuFormat.xml("slkgal"));
        assertEquals("slkgal", KafuFormat.xmlSafely("slkgal"));

        assertTrue(KafuFormat.xml(one_line).lines().count() > 1);
    }


    @Test
    void stripAll() {
        assertEquals("Hallo", KafuFormat.stripAll("Hallo "));
        assertEquals("Hallo", KafuFormat.stripAll(" Hallo"));
        assertEquals("Hallo", KafuFormat.stripAll(" Hallo "));
        assertEquals("Ha llo", KafuFormat.stripAll(" Ha\tllo "));
        assertEquals("Ha llo", KafuFormat.stripAll(" Ha\nllo "));
        assertEquals("Ha llo", KafuFormat.stripAll(" Ha                llo "));
        assertEquals("H a l l o", KafuFormat.stripAll(" H a l l o "));
        assertEquals("H a l l o", KafuFormat.stripAll("\t  H \ta\t l \t l \to \t"));
        assertEquals("H a l l o", KafuFormat.stripAll("  H   a   l  l o \n"));
        assertEquals("H a l l o", KafuFormat.stripAll("  H   a   l  l o\n"));
        assertEquals("H a l l o", KafuFormat.stripAll("\nH\na\nl\nl\no\n"));
    }
}
