package junit.text;

import net.kafujo.text.KafuText;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class KafuText_Test {

    @Test
    void formatted2oneline() {
        assertEquals("line1", KafuText.getFirstLine("line1"));
        assertEquals("line1", KafuText.getFirstLine("line1\nline2"));
        assertEquals("line1", KafuText.getFirstLine("line1\n"));
        assertEquals("", KafuText.getFirstLine("\nline1\n"));
        assertEquals("  ", KafuText.getFirstLine("  \nline1\n"));
        assertEquals("", KafuText.getFirstLine(""));
    }


    @Test
    void name() {
        assertThrows(NullPointerException.class, () -> KafuText.replaceUmlauts(null));
        assertEquals("Huette", KafuText.replaceUmlauts("Hütte"));
        assertEquals("Hueueueh!", KafuText.replaceUmlauts("Hüüüh!"));
        assertEquals("HUEUEH!", KafuText.replaceUmlauts("HÜÜH!"));

        assertEquals("HUETTE", KafuText.replaceUmlauts("HÜTTE"));
        assertEquals("UEBER", KafuText.replaceUmlauts("ÜBER"));
        assertEquals("Uebung", KafuText.replaceUmlauts("Übung"));
    }
}
