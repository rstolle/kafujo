package junit.io;

import net.kafujo.io.KafuFile;
import net.kafujo.io.ResourceFileEscapee;
import net.kafujo.io.Resources;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import support.Data;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class ResourceFileEscapee_Test {

    private static final String prefix = "RES:";
    private static final String handleRes = prefix + Data.THREE_NUMBERS;
    private static final Path handleFile = Resources.ofCurrentThread().asTempFile(Data.THREE_NUMBERS);

    private ResourceFileEscapee escapee = new ResourceFileEscapee(prefix);

    @AfterAll
    static void cleaning() {
        KafuFile.deleteQuietly(handleFile);
    }

    @Test
    void path() {

        final Path res1 = escapee.toPath(handleRes);
        KafuFile.requireExists(res1);
        assertEquals(1, escapee.getMapper().size());
        assertEquals(1, escapee.getAccessCount());
        assertEquals(1, escapee.getAccessCount(handleRes));
        assertEquals(1, escapee.getResourceCount());
        assertEquals(0, escapee.getFileCount());

        final Path res2 = escapee.toPath(prefix + Data.THREE_NUMBERS);
        assertEquals(1, escapee.getMapper().size());
        assertEquals(res1, res2);
        assertEquals(2, escapee.getAccessCount());
        assertEquals(2, escapee.getAccessCount(handleRes));
        assertEquals(1, escapee.getResourceCount());
        assertEquals(0, escapee.getFileCount());

        // now a file
        assertEquals(0, escapee.getAccessCount(handleFile));
        final Path file1 = escapee.toPath(handleFile);
        assertEquals(file1, handleFile); // could be even the same, but goes via String
        assertEquals(1, escapee.getMapper().size());
        assertEquals(3, escapee.getAccessCount());
        assertEquals(1, escapee.getAccessCount(handleFile));
        assertEquals(1, escapee.getResourceCount());
        assertEquals(1, escapee.getFileCount());

        assertEquals(KafuFile.sizeOfFile(res1), KafuFile.sizeOfFile(file1));

        escapee.close();
        assertFalse(Files.exists(res1));

        assertEquals(0, escapee.getMapper().size());
        assertEquals(2, escapee.getAccessCount(handleRes));
    }
}
