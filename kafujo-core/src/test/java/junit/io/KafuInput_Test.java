package junit.io;

import net.kafujo.io.KafuFile;
import net.kafujo.io.KafuInput;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ClosedChannelException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class KafuInput_Test {

    private static final String hello = "Hello World with Strange letters öäpß´´";

    @Test
    void asString () throws IOException {

        final Path p = KafuFile.createTempFile(hello);
        assertEquals(hello, Files.readString(p, StandardCharsets.UTF_8));

        InputStream inputStream = Files.newInputStream(p);
        assertEquals (hello, KafuInput.asString(inputStream));

        assertThrows(ClosedChannelException.class, inputStream::available); // make sure, its closed ...
        // ... which would not be the case with org.apache.commons.io.IOUtils.toString(inputStream, StandardCharsets.UTF_8));
        assertTrue(KafuFile.deleteFileQuietly(p));
    }
}
