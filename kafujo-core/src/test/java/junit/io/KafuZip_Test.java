package junit.io;

import net.kafujo.io.KafuFile;
import net.kafujo.io.KafuZip;
import org.junit.jupiter.api.Test;
import support.Data;

import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class KafuZip_Test {

    @Test
    void zipNull() {
        String nullStr = null;

        NullPointerException npe = assertThrows(NullPointerException.class, () -> KafuZip.zip(nullStr, nullStr));
        assertEquals("REQUIRE ", npe.getMessage().substring(0, 8));

        assertThrows(NullPointerException.class, () -> KafuZip.zip("...", nullStr));
        assertThrows(NullPointerException.class, () -> KafuZip.zip(nullStr, "..."));
    }

    @Test
    void zipToTemp () {
        Path zip = KafuZip.zipToTempFile(Data.PATH_TO_ONE_LINE_FILE);

        System.out.println(zip);
        KafuFile.requireExists(zip);


    }


}
