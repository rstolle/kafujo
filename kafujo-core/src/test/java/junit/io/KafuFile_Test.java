package junit.io;

import net.kafujo.base.RequirementException;
import net.kafujo.config.KafuOs;
import net.kafujo.io.KafuFile;
import net.kafujo.io.Resources;
import net.kafujo.sec.Randomize;
import net.kafujo.units.DataSize;
import net.kafujo.units.KafuDateTime;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.List;

import static net.kafujo.io.KafuFile.isEmptyFile;
import static org.junit.jupiter.api.Assertions.*;
import static support.Data.*;

class KafuFile_Test {

    @Test
    void requireOKpath() {
        final Path file = KafuFile.createTempFile();   // an empty file
        final Path dir = KafuFile.createTempDirectory();

        // same for files and directories
        KafuFile.requireExists(file); // would be the typical, but we wanna check the return value as well:

        assertSame(file, KafuFile.requireExists(file));
        assertSame(dir, KafuFile.requireExists(dir));

        assertSame(file, KafuFile.requireReadable(file));
        assertSame(dir, KafuFile.requireReadable(dir));

        assertSame(file, KafuFile.requireWritable(file));
        assertSame(dir, KafuFile.requireWritable(dir));

        if (!KafuOs.isWindows()) {
            // on windows, every file seems to be executable!?
            assertFalse(Files.isExecutable(file), "tempfile is executable");
        }
        assertSame(dir, KafuFile.requireExecutable(dir));

        // only files
        assertSame(file, KafuFile.requireNotDirectory(file));
        assertSame(file, KafuFile.requireWritableEmptyFile(file));
        assertTrue(KafuFile.deleteFileQuietly(file));

        // only directories
        assertSame(dir, KafuFile.requireDirectory(dir));
        assertTrue(KafuFile.deleteDirectoryQuietly(dir));

        // both again
        assertSame(NOT_EXISTING_PATH, KafuFile.requireNotExists(NOT_EXISTING_PATH));
        KafuFile.requireNotExists(file);
        KafuFile.requireNotExists(dir);
    }


    @Test
    void requireFails() {
        final Path fileEmpty = KafuFile.createTempFile();
        final Path fileContent = KafuFile.createTempFile("some content");
        final Path dir = KafuFile.createTempDirectory();

        assertThrows(RequirementException.class, () -> KafuFile.requireNotExists(fileEmpty));
        assertThrows(RequirementException.class, () -> KafuFile.requireNotExists(fileContent));
        assertThrows(RequirementException.class, () -> KafuFile.requireNotExists(dir));

        assertThrows(RequirementException.class, () -> KafuFile.requireDirectory(fileEmpty));
        assertThrows(RequirementException.class, () -> KafuFile.requireDirectory(fileContent));
        assertSame(dir, KafuFile.requireDirectory(dir));

        assertSame(fileEmpty, KafuFile.requireWritableEmptyFile(fileEmpty));
        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyFile(fileContent));
        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyFile(dir));

        assertTrue(KafuFile.deleteQuietly(fileEmpty, fileContent, dir));
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(fileEmpty));
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(fileContent));
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(dir));
    }


    @Test
    void emptyStringCreate() {
        final Path file = KafuFile.createTempFile("some content");
        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyFile(file));

        final String str = file.toString();
        assertEquals(file, KafuFile.requireExists(str));

        assertTrue(KafuFile.deleteQuietly(str));
        KafuFile.requireNotExists(file);

        assertSame(file, KafuFile.createFile(file));

        assertSame(file, KafuFile.requireExists(file));
        assertEquals(file, KafuFile.requireExists(str));
        // its hard to provoke an InvalidPathException on MacOS, so I cant test it

        KafuFile.deleteFileQuietly(str);
        assertSame(file, KafuFile.requireNotExists(file));
        assertEquals(file, KafuFile.requireNotExists(str));
    }

    @Test
    void stringDir() {
        final Path dir = KafuFile.createTempDirectory();
        final String dirStr = dir.toString();
        assertEquals(dir, KafuFile.requireExists(dirStr));
        assertTrue(KafuFile.deleteDirectoryQuietly(dirStr));
        assertEquals(dir, KafuFile.requireNotExists(dirStr));
    }


    @Test
    void requireNull() {
        final Path nullPath = null;

        assertThrows(NullPointerException.class, () -> KafuFile.requireExists(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireNotExists(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireDirectory(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireNotDirectory(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireExecutable(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireReadable(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireWritable(nullPath));
        assertThrows(NullPointerException.class, () -> KafuFile.requireWritableEmptyFile(nullPath));
    }

    @Test
    void requireNotExists() {
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(NOT_EXISTING_PATH));
        assertSame(NOT_EXISTING_PATH, KafuFile.requireNotExists(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireDirectory(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireNotDirectory(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireExecutable(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireReadable(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireWritable(NOT_EXISTING_PATH));
        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyFile(NOT_EXISTING_PATH));
    }

    @Test
    void requireWritableEmptyDirectory() {

        Path path = KafuFile.createTempDirectory();
        KafuFile.requireWritableEmptyDirectory(path);

        assertEquals(0, KafuFile.countRootEntries(path));
        assertTrue(KafuFile.isEmptyDirectory(path));

        KafuFile.createFile(path.resolve("test"), "some data".getBytes());

        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyDirectory(path));

        assertEquals(1, KafuFile.countRootEntries(path));
        assertFalse(KafuFile.isEmptyDirectory(path));

        assertEquals(2, KafuFile.deleteDirectory(path));
    }


    @Test
    void temp() {
        final Path tmp = KafuFile.createTempFile();
        KafuFile.requireExists(tmp);
        assertFalse(Files.isDirectory(tmp));
        assertThrows(RequirementException.class, () -> KafuFile.requireDirectory(tmp));

        KafuFile.requireNotDirectory(tmp);

        assertEquals(DataSize.ZERO, KafuFile.sizeOfFile(tmp));
        KafuFile.deleteFileQuietly(tmp);
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(tmp));
    }


    @Test
    void tempInputStream() throws IOException {
        final Path tmp = KafuFile.createTempFile(Resources.ofCurrentThread().asStream(TRES_ONE_LINE_FILE));
        assertEquals("123", Files.readString(tmp));
        KafuFile.requireExists(tmp);
        Files.delete(tmp);
        assertFalse(Files.exists(tmp));
        assertThrows(NullPointerException.class, () -> KafuFile.createTempFile((InputStream)null));
    }

    @Test
    void tempCharSequence() throws IOException {
        final Path tmp = KafuFile.createTempFile("Hello World");
        assertEquals("Hello World", Files.readString(tmp));
        KafuFile.requireExists(tmp);
        Files.delete(tmp);
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(tmp));
    }

    @Test
    void tempBin() throws IOException {
        final Path tmp = KafuFile.createTempFile("Hello World".getBytes());
        assertEquals("Hello World", Files.readString(tmp));
        KafuFile.requireExists(tmp);
        Files.delete(tmp);
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(tmp));
    }

    @Test
    void tempLines() throws IOException {
        final Path tmp = KafuFile.createTempFile(Resources.ofCurrentThread().asLines(ABC_3LINES_RESNAME));

        List<String> readBack = Files.readAllLines(tmp);

        assertEquals(3, readBack.size());
        assertEquals("A", readBack.get(0));

        KafuFile.requireExists(tmp);
        Files.delete(tmp);
        assertThrows(RequirementException.class, () -> KafuFile.requireExists(tmp));
    }

    @Test
    void tempDir() {
        Path tmp = KafuFile.getTempDir();

        assertTrue(Files.exists(tmp));
        assertTrue(Files.isWritable(tmp));
        assertTrue(Files.isExecutable(tmp));

        KafuFile.requireDirectory(tmp);
        KafuFile.requireWritable(tmp);
        KafuFile.requireExecutable(tmp);
    }

    @Test
    void userDir() {
        Path user = KafuFile.getUserHome();

        assertTrue(Files.exists(user));
        assertTrue(Files.isWritable(user));
        assertTrue(Files.isExecutable(user));

        KafuFile.requireDirectory(user);
        KafuFile.requireWritable(user);
        KafuFile.requireExecutable(user);
    }

    @Test
    void workingDir() {
        final Path working = KafuFile.getWorkingDir();
        assertTrue(Files.exists(working));
        KafuFile.requireDirectory(working);

        final Path user = KafuFile.getUserDir();
        assertEquals(user, working);

        // now check if we can change this:
        final Path trouble = KafuFile.createTempDirectory();
        System.setProperty("user.dir", trouble.toString());

        assertEquals(trouble, KafuFile.getUserDir()); // its the same,
        assertNotEquals(user, KafuFile.getUserDir()); // its differ now, of course

        assertNotEquals(trouble, KafuFile.getWorkingDir()); // Path.of(".") apparently does not rely on ...
        assertEquals(working, KafuFile.getWorkingDir());    // ... System.getProperty("user.dir")

        assertEquals(1, KafuFile.deleteDirectory(trouble));
    }


    @Test
    void deleteXyQuietly_Paths() {
        final Path temp1 = KafuFile.createTempFile();
        assertThrows(RequirementException.class, () -> KafuFile.deleteDirectoryQuietly(temp1));
        assertTrue(KafuFile.deleteFileQuietly(temp1));
        assertFalse(KafuFile.deleteFileQuietly(temp1));
        assertFalse(Files.exists(temp1));

        assertFalse(KafuFile.deleteFileQuietly(NOT_EXISTING_PATH));
        assertFalse(KafuFile.deleteDirectoryQuietly(NOT_EXISTING_PATH));

        final Path tempDir = KafuFile.createTempDirectory();
        assertThrows(RequirementException.class, () -> KafuFile.deleteFileQuietly(tempDir));
        assertTrue(KafuFile.deleteDirectoryQuietly(tempDir));
        assertFalse(Files.exists(tempDir));

        final Path temp2 = null;
        assertThrows(NullPointerException.class, () -> KafuFile.deleteFileQuietly(temp2));
    }

    @Test
    void deleteXyQuietly_Strings() {
        final Path temp = KafuFile.createTempFile();

        final String tempAsString = temp.toAbsolutePath().toString();
        assertThrows(RequirementException.class, () -> KafuFile.deleteDirectoryQuietly(tempAsString));
        assertTrue(KafuFile.deleteFileQuietly(tempAsString));
        assertFalse(KafuFile.deleteFileQuietly(tempAsString));
        KafuFile.requireNotExists(temp);
    }

    @Test
    void deleteQuietly_Paths() {
        final Path tempFile = KafuFile.createTempFile();
        final Path tempDir = KafuFile.createTempDirectory();

        assertFalse(KafuFile.deleteQuietly(NOT_EXISTING_PATH));
        assertTrue(KafuFile.deleteQuietly(tempFile));
        assertTrue(KafuFile.deleteQuietly(tempDir));

        KafuFile.requireNotExists(tempFile);
        KafuFile.requireNotExists(tempDir);
    }


    @Test
    void getUsableSpace() {
        // Path in AdoptOpenJDK 11.0.4 for Linux throws InvalidPathException for umlaut, but not for '?'
        assertEquals("Not there", KafuFile.getUsableSpace(Path.of("asfaslgjasfdkasdsfgka"), "Not there"));
    }


    @Test
    void deleteDirectoryQuietly() throws IOException {

        final Path tmp = KafuFile.createTempDirectory();

        final Path t1 = tmp.resolve("1.txt");
        Files.writeString(t1, "Hello");

        assertThrows(RequirementException.class, () -> KafuFile.requireNotExists(t1));

        final Path t2 = tmp.resolve("2.txt");
        Files.writeString(t2, "World");

        assertTrue(Files.exists(tmp));
        assertTrue(Files.exists(t1));
        assertTrue(Files.exists(t2));

        assertTrue(Files.isDirectory(tmp));
        assertTrue(Files.isRegularFile(t1));
        assertTrue(Files.isRegularFile(t2));

        KafuFile.requireExists(tmp);

        assertEquals(3, KafuFile.deleteDirectory(tmp));
        assertThrows(RequirementException.class, () -> KafuFile.deleteDirectory(tmp));

        assertFalse(KafuFile.deleteQuietly(tmp));

        assertFalse(Files.exists(tmp));
        KafuFile.requireNotExists(tmp);

    }

    @Test
    void randomBytes() {
        final int sizeInt = Randomize.JUL.nextInt(DataSize.ONE_MiB.intValue()) + 1024;
        final DataSize ds = DataSize.of(sizeInt);

        assertTrue(ds.isBiggerThan(DataSize.ONE_KiB));
        assertTrue(ds.isSmallerThan(DataSize.ONE_MiB.plus(1025)));

        final var bytes = Randomize.SECURE.bytes(sizeInt);

        final Path tmp = KafuFile.createTempFile(bytes);

        KafuFile.requireExists(tmp);

        assertEquals(sizeInt, bytes.length);

        assertEquals(ds, KafuFile.sizeOfFile(tmp));
        assertEquals(ds, KafuFile.size(tmp));
        assertEquals(ds, KafuFile.sizeQuietly(tmp));

        assertThrows(RequirementException.class, () -> KafuFile.sizeOfDirectoryBytes(tmp));
        assertThrows(RequirementException.class, () -> KafuFile.sizeOfDirectory(tmp));

        KafuFile.deleteQuietly(tmp);
        KafuFile.requireNotExists(tmp);

        assertThrows(RequirementException.class, () -> assertEquals(ds, KafuFile.sizeQuietly(tmp)));

    }

    @Test
    void use() throws IOException {
        final Path path = KafuFile.createTempFile(); // an empty file

        KafuFile.requireExists(path);
        assertTrue(KafuFile.sizeOfFile(path).isZero());

        assertSame(path, KafuFile.createFileOrUseEmpty(path));  // the empty file will be used
        assertTrue(KafuFile.deleteFileQuietly(path));
        KafuFile.requireNotExists(path); // its definitely gone

        KafuFile.createFileOrUseEmpty(path);  // a new file will be created
        KafuFile.requireExists(path); // its definitely there

        Files.writeString(path, "12345"); // write some stuff
        assertEquals(DataSize.of(5), KafuFile.sizeOfFile(path));
        assertFalse(KafuFile.sizeOfFile(path).isZero());  // same test, more generic

        // now its not an empty file anymore
        assertThrows(RequirementException.class, () -> KafuFile.requireWritableEmptyFile(path));
        assertThrows(RequirementException.class, () -> KafuFile.createFileOrUseEmpty(path));

        assertTrue(KafuFile.deleteFileQuietly(path));
        KafuFile.requireNotExists(path);
    }


    @Test
    void createDirs() {
        Path tmp = KafuFile.createTempDirectory();
        assertSame(tmp, KafuFile.createDirectories(tmp));

        assertTrue(KafuFile.deleteDirectoryQuietly(tmp));

        assertSame(tmp, KafuFile.createDirectories(tmp));

        assertTrue(KafuFile.deleteDirectoryQuietly(tmp));

        KafuFile.createFile(tmp);
        KafuFile.requireNotDirectory(tmp);

        assertThrows(RequirementException.class, () -> KafuFile.createDirectories(tmp));

        assertTrue(KafuFile.deleteFileQuietly(tmp));
    }

    @Test
    void sanitizeFilename() {
        assertEquals(Path.of("hello.txt"), KafuFile.sanitizeFilename("hello.txt"));
        assertEquals(Path.of("Hello_World.txt"), KafuFile.sanitizeFilename("Hello World.txt"));
        assertEquals(Path.of("hello_world.txt"), KafuFile.sanitizeFilename("\n\nhello\tworld.txt\n"));

        assertEquals(Path.of("heisses_hoesschen.txt"), KafuFile.sanitizeFilename("heißes_hößchen.txt"));
        assertEquals(Path.of("hello-.txt"), KafuFile.sanitizeFilename("hello-.txt"));

        assertEquals(Path.of("_hello.txt"), KafuFile.sanitizeFilename("-hello.txt"));
        assertEquals(Path.of("_-hello.txt"), KafuFile.sanitizeFilename("--hello.txt"));


        assertEquals(Path.of("_hello_.txt"), KafuFile.sanitizeFilename("/hello\\.txt"));
        assertEquals(Path.of("_hello_$.txt"), KafuFile.sanitizeFilename("/hello_$.txt"));
        assertEquals(Path.of("_hello_$.txt"), KafuFile.sanitizeFilename("/hello&$.txt"));

        assertEquals(Path.of("World_New.txt"), KafuFile.sanitizeFilename("World New.txt"));
    }

    @Test
    void sanitizeFilenameFragments() {
        assertEquals(Path.of("hello.txt"), KafuFile.sanitizeFilename("", "hello.txt"));
        assertEquals(Path.of("Hello-World.txt"), KafuFile.sanitizeFilename("-", "Hello", "World", ".txt"));
        assertEquals(Path.of("Hello-World-txt"), KafuFile.sanitizeFilename("-", "Hello", "World", "txt"));
        assertEquals(Path.of("HelloWorld.txt"), KafuFile.sanitizeFilename("", "Hello", "World", ".txt"));
        assertEquals(Path.of("HelloWorld.txt"), KafuFile.sanitizeFilename("", "Hello", "", "  ", "World", ".txt"));
        assertEquals(Path.of("HelloWorld.txt"), KafuFile.sanitizeFilename("", "", "Hello", "\t", "\n", "World", ".txt"));
        assertEquals(Path.of("Hello-World.txt"), KafuFile.sanitizeFilename("-", "", "Hello", "\t", "\n", "World", ".txt"));
        assertEquals(Path.of("Hello$$World.txt"), KafuFile.sanitizeFilename("$$", "", " Hello", "\t", "\n", "World ", ".txt"));
        assertEquals(Path.of("Hello$$World_new.txt"), KafuFile.sanitizeFilename("$$", "", " Hello", "\t", "\n", "World new ", ".txt"));

        assertEquals(Path.of(".config_test.txt"), KafuFile.sanitizeFilename("_", ".config", "test", ".txt"));

        assertThrows(IllegalArgumentException.class, () -> KafuFile.sanitizeFilename("-", "", "  ", " \t"));
        assertThrows(IllegalArgumentException.class, () -> KafuFile.sanitizeFilename("%", "d", "  d", " \t"));
    }


    @Test
    void timestampedFilenames() {
        Path p1 = KafuFile.timestampFilename("hello.txt");
        Path p2 = KafuFile.timestampFilename("hello.txt", ZoneId.systemDefault());

        assertTrue(p1.toString().endsWith("-hello.txt"));
        assertEquals(p1, p2); // can fail, when a new second started todo: SOLVE!

        ZoneId zone = ZoneId.of("UTC");
        Path p3 = KafuFile.timestampFilename("hello.txt", zone);

        if (KafuDateTime.sameUtcOffset(zone, ZoneId.systemDefault())) {
            assertEquals(p1, p3); // can fail, when a new second started todo: SOLVE!
        } else {
            System.out.println("different zones");
            assertNotEquals(p1, p3);
        }
    }

    @Test
    void isEmpty() {
        Path dir = KafuFile.createTempDirectory();

        assertTrue(KafuFile.isEmpty(dir));
        assertTrue(KafuFile.isEmptyDirectory(dir));
        assertThrows(RequirementException.class, () -> isEmptyFile(dir));

        Path file = dir.resolve("42.txt");

        assertThrows(RequirementException.class, () -> isEmptyFile(file));
        assertThrows(RequirementException.class, () -> KafuFile.isEmptyDirectory(file));

        KafuFile.createFile(file);

        assertFalse(KafuFile.isEmptyDirectory(dir));
        assertTrue(KafuFile.isEmptyFile(file));
        assertThrows(RequirementException.class, () -> KafuFile.isEmptyDirectory(file));

        KafuFile.deleteFileQuietly(file);
        assertTrue(KafuFile.isEmptyDirectory(dir));
        KafuFile.deleteDirectoryQuietly(dir);
    }

    @Test
    void randomFile() {
        final Path rnd1024 = KafuFile.createRandomBinaryTempFile(DataSize.ONE_MB);
        assertEquals(DataSize.ONE_MB, KafuFile.sizeOfFile(rnd1024));
        KafuFile.deleteQuietly(rnd1024);

        assertThrows(IllegalArgumentException.class, () -> KafuFile.createRandomBinaryTempFile(DataSize.ZERO));
    }

    @Test
    void randomFileRandomSize() {
        final Path rnd = KafuFile.createRandomBinaryTempFile(DataSize.of(Randomize.JUL.nextInt(1025, 1048576)));
        assertTrue(KafuFile.sizeOfFile(rnd).isBiggerThan(DataSize.ONE_KiB));   // min 1025 (cause inclusive)
        assertTrue(KafuFile.sizeOfFile(rnd).isSmallerThan(DataSize.ONE_MiB));  // max 1048575 (cause exclusive)
        KafuFile.deleteFileQuietly(rnd);
    }

    @Test
    void equalContent_resourceFiles() {
        final Path p1 = Resources.ofCurrentThread().asTempFile(TRES_ONE_LINE_FILE);
        final Path p2 = Resources.ofCurrentThread().asTempFile(TRES_ONE_LINE_FILE);
        final Path p3 = Resources.ofCurrentThread().asTempFile(TRES_XML_ONE_LINE);

        assertTrue((KafuFile.equalContent(p1, p2)));
        assertTrue((KafuFile.equalContent(p2, p1)));

        assertFalse((KafuFile.equalContent(p1, p3)));
        assertFalse((KafuFile.equalContent(p2, p3)));
        assertFalse((KafuFile.equalContent(p3, p1)));
        assertFalse((KafuFile.equalContent(p3, p2)));

        assertEquals(3, KafuFile.deleteQuietly(List.of(p1, p2, p3)));
    }


    @Test
    void equalContent_sameSizeSmall() {
        final Path rnd_1 = KafuFile.createRandomBinaryTempFile(DataSize.ONE_KiB);
        final Path rnd_2 = KafuFile.createRandomBinaryTempFile(DataSize.ONE_KiB);

        assertEquals(DataSize.ONE_KiB, KafuFile.sizeOfFile(rnd_1));
        assertEquals(DataSize.ONE_KiB, KafuFile.sizeOfFile(rnd_2));

        assertFalse(KafuFile.equalContent(rnd_1, rnd_2));
        assertTrue(KafuFile.equalContent(rnd_1, rnd_1));
        assertEquals(2, KafuFile.deleteQuietly(List.of(rnd_1, rnd_2)));
    }

    @Test
    void equalContent_sameSizeBig() {
        final Path rnd1 = KafuFile.createRandomBinaryTempFile(DataSize.ONE_MiB);
        final Path rnd2 = KafuFile.createRandomBinaryTempFile(DataSize.ONE_MiB);
        assertEquals(DataSize.ONE_MiB, KafuFile.sizeOfFile(rnd1));
        assertEquals(DataSize.ONE_MiB, KafuFile.sizeOfFile(rnd2));
        assertFalse(KafuFile.equalContent(rnd1, rnd2));
        assertTrue(KafuFile.equalContent(rnd1, rnd1));

        assertEquals(2, KafuFile.deleteQuietly(List.of(rnd1, rnd2)));
    }

    @Test
    void sizeAndNameOrPath() {
        Path p = Resources.ofCurrentThread().asTempFile(THREE_ESPERANTO);

        final String size = KafuFile.sizeOfFile(p).toString();

        final String name = KafuFile.sizeAndName(p);
        final String path = KafuFile.sizeAndPath(p);

        assertTrue(name.contains(size));
        assertTrue(path.contains(size));

        final String fileName = Path.of(THREE_ESPERANTO).getFileName().toString();

        assertTrue(name.contains(fileName));
        assertTrue(path.contains(fileName));
        assertTrue(path.length() >= name.length());
        assertTrue(KafuFile.deleteFileQuietly(p));
    }

    @Test
    void absDos () {
        assertTrue(KafuFile.isAbsoluteDosPath("c:"));
        assertTrue(KafuFile.isAbsoluteDosPath("c:\\"));
        assertTrue(KafuFile.isAbsoluteDosPath("C:\\"));
        assertTrue(KafuFile.isAbsoluteDosPath("C:\\windows"));

        assertFalse(KafuFile.isAbsoluteDosPath(""));
        assertFalse(KafuFile.isAbsoluteDosPath("\\"));
        assertFalse(KafuFile.isAbsoluteDosPath("\\windows"));
        assertFalse(KafuFile.isAbsoluteDosPath("/windows"));

    }

    @Test
    void dos2unix () {
        assertEquals("/", KafuFile.separatorsToUnix("/"));
        assertEquals("/", KafuFile.separatorsToUnix("\\"));
        assertEquals("/windows", KafuFile.separatorsToUnix("\\windows"));
        assertEquals("windows/help/me", KafuFile.separatorsToUnix("windows\\help\\me"));
        assertEquals("/windows/really/sucks", KafuFile.separatorsToUnix("\\windows\\really/sucks"));
        assertEquals("really/really/", KafuFile.separatorsToUnix("really\\really\\"));

        assertThrows(IllegalArgumentException.class, () -> KafuFile.separatorsToUnix("c:\\windows"));
    }

}
