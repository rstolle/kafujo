package junit.io;

import net.kafujo.base.RequirementException;
import net.kafujo.io.FileDump;
import net.kafujo.io.KafuFile;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import support.Data;

import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class FileDumpTest {

    public static final Logger LGR = LoggerFactory.getLogger(FileDumpTest.class);

    @Test
    void random() {
        Path file = FileDump.DEFAULT.create(List.of("A", "B", "C"));
        assertThat (file).hasSameTextualContentAs(Data.ABC_4LINES_FILE);
        assertThat (file).hasSameBinaryContentAs(Data.ABC_4LINES_FILE);
        assertTrue (KafuFile.equalContent(file,Data.ABC_4LINES_FILE));

        assertThat (file).hasSameTextualContentAs(Data.ABC_3LINES_FILE);
        assertFalse (KafuFile.equalContent(file,Data.ABC_3LINES_FILE));
    }
    @Test
    void timestampSecondsFail() {
        FileDump fd = new FileDump(FileDump.Naming.TIMESTAMP_SECONDS, "too_many_with_same_name", ".txt");
        int count = 0;
        try {
            fd.create("1");
            count++;
            fd.create("2");
            count++;
            fd.create("3");
            count++;
            fd.create("4");
            count++;
            fd.create("5");
            count++;
            fd.create("6");
            count++;
            fd.create("7");
            count++;
        } catch (RequirementException e) {
            LGR.info("Created {} files and then {}", count, e.getMessage());
            return;
        }
        fail ("Creating seven files should not take 7 seconds");
        // doing the same with TIMESTAMP_MILLIS mostly works, but computers getting faster. So its not so easy to test
    }
}
