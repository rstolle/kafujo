package junit.io;

import net.kafujo.base.RequirementException;
import net.kafujo.io.KafuFile;
import net.kafujo.io.KafuInput;
import net.kafujo.io.Resources;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;
import static support.Data.*;

class Resources_Test {

    // create one reader using the loader of the current thread
    private static final Resources ofCurrentThread = Resources.ofCurrentThread();


    @Test
    void allUseOneLoader() {
        final var context1 = Thread.currentThread().getContextClassLoader();
        final var context2 = Thread.currentThread().getContextClassLoader();

        final var class1 = Resources_Test.class.getClassLoader();
        final var class2 = Resources_Test.class.getClassLoader();

        assertSame(context1, context2);
        assertSame(class1, class2);
        assertSame(class1, context1);
        assertSame(class1, ofCurrentThread.getLoader());
    }

    @Test
    void isAvailable() {
        assertTrue(ofCurrentThread.isAvailable(TRES_ONE_LINE_FILE));
        assertTrue(Resources.ofCurrentThread().isAvailable(TRES_ONE_LINE_FILE));
        assertFalse(ofCurrentThread.isAvailable(NONSENSE));
    }

    @Test
    void requireExists() {
        // check for exiting -> nothing happens
        // final Path path = KafuResource.asPath(TRES_ONE_LINE_FILE);
        assertSame(TRES_ONE_LINE_FILE, ofCurrentThread.requireAvailability(TRES_ONE_LINE_FILE));
        ofCurrentThread.requireAvailability(TRES_ONE_LINE_FILE);


        // but if they dont exist, exception will be thrown
        assertThrows(RequirementException.class, () -> ofCurrentThread.requireAvailability(NONSENSE));
        assertThrows(RequirementException.class, () -> ofCurrentThread.asStream(NONSENSE));
        assertThrows(RequirementException.class, () -> ofCurrentThread.asString(NONSENSE));
        assertThrows(RequirementException.class, () -> ofCurrentThread.asLines(NONSENSE));
        assertThrows(RequirementException.class, () -> ofCurrentThread.asTempFile(NONSENSE));
    }

    @Test
    void asString() {
        assertEquals("123", ofCurrentThread.asString(TRES_ONE_LINE_FILE));
        assertEquals("123", KafuInput.asString(ofCurrentThread.asStream(TRES_ONE_LINE_FILE)));
    }

    @Test
    void asLines() {
        final var line = ofCurrentThread.asLines(TRES_ONE_LINE_FILE);

        assertEquals(1, line.size());
        assertEquals("123", line.get(0));

        final var lines = ofCurrentThread.asLines(ABC_3LINES_RESNAME);

        assertEquals(3, lines.size());
        assertEquals("A", lines.get(0));
        assertEquals("B", lines.get(1));
        assertEquals("C", lines.get(2));
    }

    @Test
    void asPath() {
        try {
            Path path = ofCurrentThread.asPath(TRES_ONE_LINE_FILE);
            assertEquals("123", Files.readString(path));
        } catch (IOException | IllegalArgumentException e) {
            fail("Could not read content of resource " + TRES_ONE_LINE_FILE + ": " + e);
        }
    }

    @Test
    void asTempFile() throws IOException {
        final Path path = ofCurrentThread.asTempFile(TRES_ONE_LINE_FILE);

        // test if the name was preserved in the whole path
        final String filename = Path.of(TRES_ONE_LINE_FILE).getFileName().toString();
        assertEquals(filename, StringUtils.right(path.toString(), filename.length()));

        // now play with just the filename
        final String filenameTemp = path.getFileName().toString();
        assertEquals(filename, StringUtils.right(filenameTemp, filename.length()));
        assertTrue(filename.length() < filenameTemp.length()); // something must have been added to make the temp name unique

        // now test the actual functionality
        assertEquals("123", Files.readString(path));
        assertTrue(KafuFile.deleteFileQuietly(path));
        KafuFile.requireNotExists(path);
    }
}
