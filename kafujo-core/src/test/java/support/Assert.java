package support;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Assert {

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static void assertEmpty (Optional<?> optional) {
        assertTrue (optional.isEmpty());
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static void assertPresent (Optional<?> optional) {
        assertTrue (optional.isPresent());
    }
}
