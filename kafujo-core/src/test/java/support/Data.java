package support;

import net.kafujo.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

public final class Data {

    public static final Logger lgr = LoggerFactory.getLogger("net.kafujo.core.test");

    // most definitely an invalid path, resource or property or whatever key
    public static final String NONSENSE = "sahdflas.la7sk#fjalsl65asd.asdfla4sjflasjf3a";

    // text file containing "123" in one line
    public static final String TRES_ONE_LINE_FILE = "textfiles/123_one_line.txt";
    public static final Path PATH_TO_ONE_LINE_FILE = Resources.ofCurrentThread().asPath(TRES_ONE_LINE_FILE);

    // text file containing "A", "B", "C" in three lines
    public static final String ABC_3LINES_RESNAME = "textfiles/ABC_3LINES.txt";
    public static final Path ABC_3LINES_FILE = Resources.ofCurrentThread().asPath(ABC_3LINES_RESNAME);

    // text file containing "A", "B", "C" and a line feed at the end
    public static final String ABC_4LINES_RESNAME = "textfiles/ABC_4LINES.txt";
    public static final Path ABC_4LINES_FILE = Resources.ofCurrentThread().asPath(ABC_4LINES_RESNAME);

    // neither as path to resource nor as a real path
    public static final Path NOT_EXISTING_PATH = Path.of(NONSENSE);


    public static final String TRES_XML = "xml/formatted.xml";
    public static final String TRES_XML_ONE_LINE = "xml/one_line.xml";


    // ConfigProperties
    public static final String CP_PREFIX = "cpt.";
    public static final String MOVIE_DATABASE_NAME_PROPERTY = "movie.database.name";
    public static final String MOVIE_DATABASE_NAME_VALUE = "IMDB";
    public static final String MOVIE_DATABASE_NAME_VALUE_ENV = System.getenv(CP_PREFIX + MOVIE_DATABASE_NAME_PROPERTY);

    // Properties files
    public static final String ONE_NUMBER = TestResource.ONE_NUMBER.value;
    public static final Path ONE_NUMBER_ASPATH = Path.of(ONE_NUMBER);

    public static final String ONE_NUMBER_XML = TestResource.ONE_NUMBER_XML.value;
    public static final Path ONE_NUMBER_XML_ASPATH = Path.of(ONE_NUMBER_XML);

    public static final String THREE_NUMBERS = TestResource.THREE_NUMBERS.value;
    public static final String THREE_ESPERANTO = TestResource.THREE_ESPERANTO.value;
    public static final String TWO_PROPERTIES_XML = TestResource.TWO_PROPERTIES_XML.value;

    public static final Path TEST_JAR = Resources.ofCurrentThread().asPath("jar/java-basics-ef4ee8.jar");
}
