package support;

/**
 * A try to access the test date in different ways
 */
public enum TestResource {

    ONE_NUMBER(Type.PROPERTIES_FILE, "properties/OneNumber.properties", "Property file containing one=1"),
    ONE_NUMBER_XML(Type.PROPERTIES_FILE_XML, "properties/OneNumber.xml", "Property file containing one=1"),
    THREE_NUMBERS (Type.PROPERTIES_FILE, "properties/ThreeNumbers.properties", "Property file containing one=1, two=2, three=3"),
    THREE_ESPERANTO(Type.PROPERTIES_FILE, "properties/ThreeEsperanto.properties", "Property file containing one=unu, two=du, three=tri"),
    TWO_PROPERTIES_XML (Type.PROPERTIES_FILE_XML, "properties/TwoProperties.xml", "Property file containing one=1 amd two=2");

    public enum Type {
        PROPERTIES_FILE, PROPERTIES_FILE_XML;
    }

    public final Type type;
    public final String value;
    private final String description;

    TestResource(final Type type, final String value, final String description) {
        this.type = type;
        this.value = value;
        this.description = description;
    }
}
