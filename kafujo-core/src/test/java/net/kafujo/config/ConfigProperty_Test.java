package net.kafujo.config;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ConfigProperty_Test {


    /**
     * Sets gradually all the levels of which can be set, queries it and makes sure it cannot be set twice
     */
    @Test
    void update() {

        final String nameSrc  = "Source";
        final String nameFile = "File";
        final String nameEnv  = "Environment";
        final String nameSys  = "System";

        ConfigProperty cp = new ConfigProperty("this.is.the.property.name", nameSrc);
        assertEquals(0, cp.getReadCounter());

        assertEquals(nameSrc, cp.getValue());
        assertEquals(0, cp.getReadCounter());
        assertEquals(nameSrc, cp.readValue());
        assertEquals(1, cp.getReadCounter());
        assertThrows(IllegalArgumentException.class, () ->  cp.setValue("Vincent", ConfigPropertySource.SRC));

        cp.setValue(nameFile, ConfigPropertySource.PROP);
        assertEquals(nameFile, cp.readValue());
        assertEquals(2, cp.getReadCounter());
        assertThrows(IllegalArgumentException.class, () ->  cp.setValue("Vincent", ConfigPropertySource.PROP));

        cp.setValue(nameEnv, ConfigPropertySource.ENV);
        assertEquals(nameEnv, cp.readValue());
        assertEquals(3, cp.getReadCounter());
        assertThrows(IllegalArgumentException.class, () ->  cp.setValue("Vincent", ConfigPropertySource.ENV));

        cp.setValue(nameSys, ConfigPropertySource.SYS);
        assertEquals(nameSys, cp.readValue());
        assertEquals(4, cp.getReadCounter());
        assertThrows(IllegalArgumentException.class, () ->  cp.setValue("Vincent", ConfigPropertySource.SYS));
    }
}
