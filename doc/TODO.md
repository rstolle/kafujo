
# general

- Make the (private) ctor of the utility classes throw as joshia Blooch suggests?
  https://stackoverflow.com/questions/25223553/how-can-i-create-an-utility-class
- JAVADOC
- get rid of commons-io dependency

- consider package system for KafuOs and Vm

# DataSize

- always uses BigInteger and subtype restricts to long or int ?
- javadoc

# new methods

    /**
     * as nicely pointed out here https://superuser.com/questions/358855/what-characters-are-safe-in-cross-platform-file-names-for-linux-windows-and-os
     * you should stick to these letters: [0-9a-zA-Z-._]
     *
     * @param path
     * @param replacement
     * @return
     */
    public static String replaceSeparator(final Path path, final char replacement) {
        Objects.requireNonNull(path, "REQUIRED: path");
        // https://stackoverflow.com/questions/8075373/file-separator-vs-filesystem-getseparator-vs-system-getpropertyfile-separato
        return path.toString().replace(File.separatorChar, replacement);
    }
