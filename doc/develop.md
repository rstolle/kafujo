
# commit tags

BUILD: all related to build, especially maven 

# Conventions

 - return values are never ever null. ONLY EXCEPTION: fallback methods with fallback null
 -- methods with fallback behavior end with Fbk  
 - Utility classes start with Kafu
 - Logging: capital letters signal problems/exceptions; small letters signal success 

# commenting

After going through some pain, I decided to use https://stackoverflow.com/a/542142 when it 
comes to multipe line code examples. 