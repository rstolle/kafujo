/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.samples;

import net.kafujo.io.Resources;
import net.kafujo.units.DataSize;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class DigitalStorageDevices {

    public static final String DEVICES_CSV = "data_storage_devices.csv";
    public static final List<DigitalStorageDevices> LIST;

    private final DataSize capacity;
    private final String name;

    static {
        var list = new LinkedList<DigitalStorageDevices>();

        for (String line : Resources.ofCurrentThread().asLines(DEVICES_CSV)) {
            String[] split = line.split(",");
            list.add(new DigitalStorageDevices(split[0], DataSize.of(split[1])));
        }
        LIST = Collections.unmodifiableList(list);

    }

    public DigitalStorageDevices(final String name, final DataSize capacity) {
        this.capacity = capacity;
        this.name = Objects.requireNonNull(name);
    }

    public DataSize getCapacity() {
        return capacity;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " -> " + capacity;
    }
}
