/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.kafujo.samples;

import java.awt.*;
import java.io.Serializable;
import java.util.List;

public class Vegetable implements Serializable {

    public static final Vegetable TOMATOE = new Vegetable("tomatoe", "tomatoes", Color.RED);
    public static final Vegetable CUCUMBER = new Vegetable("cucumber", "cucumbers", Color.GREEN);
    public static final Vegetable POTATOE = new Vegetable("potato", "pataoes", Color.YELLOW);
    public static final Vegetable PUMPKIN = new Vegetable("pumpkin", "pumpkins", Color.ORANGE);
    public static final Vegetable CAULIFLOWER = new Vegetable("cauliflower", "cauliflowers", Color.WHITE);
    public static final Vegetable CARROT = new Vegetable("carrot", "carrots", Color.RED);
    public static final Vegetable RADSIH = new Vegetable("radish", "radishes", Color.RED);

    public final static List<Vegetable> LIST = List.of(TOMATOE, CUCUMBER, POTATOE, PUMPKIN, CAULIFLOWER, CARROT, RADSIH);

    private final String name;
    private final String plural;
    private final Color color;

    public Vegetable(String name, String plural, Color color) {
        this.name = name;
        this.plural = plural;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getPlural() {
        return plural;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Vegetable '" + name + "'";
    }
}
