package _client;

import net.kafujo.io.Resources;
import net.kafujo.samples.DigitalStorageDevices;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DigitalStorageDevices_Test {


    @Test
    void list() {
        assertNotNull(DigitalStorageDevices.LIST);

        List<String> lines = Resources.ofCurrentThread().asLines(DigitalStorageDevices.DEVICES_CSV);

        assertEquals(lines.size(), DigitalStorageDevices.LIST.size());

    }

}